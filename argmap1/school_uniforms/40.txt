|
|
|
||
|
PROS AND CONS
OF CONTROVERSIAL ISSUES
|
|
School Uniforms – 51st Website from ProCon.org Explores Pros and Cons in the Debate
|
|
Traditionally favored by private and parochial institutions, school uniforms are being adopted by US public schools in increasing numbers. Almost one in five US public schools required students to wear uniforms during the 2011-2012 school year, up from one in eight in 2003-2004.
|
|
|US students wearing school uniforms
|
Source: Challen Stephens, "Huntsville City Schools Consider School Uniforms, Survey Parents through Robocalls," blog.al.com, Aug. 27, 2010
ProCon.org, a nonpartisan research organization devoted to critical thinking on controversial topics, debuts its 51st issue website, http://school-uniforms.procon.org, and delves into the pros and cons of mandatory school uniforms using the latest studies, perspectives, and research.
Proponents say that school uniforms make schools safer for students; create a "level playing field" that reduces socioeconomic disparities; and encourage children to focus on their studies rather than their clothes.
Opponents say school uniforms infringe upon students' right to express their individuality; have no positive effect on behavior and academic achievement; and emphasize the socioeconomic disparities they are intended to disguise.
In response to the website’s core question, "Should students have to wear school uniforms?," ProCon.org presents sourced pros and cons, a historical background section, videos, images, and over 130 footnotes and sources.
Some of the sources quoted include: Bill Clinton (pro), 42nd President of the United States; Dr. Nancy Bailey (con), author of Misguided Education Reform: Debating the Impact on Students; French Toast Official School Wear (pro), a school uniform supplier; and Dr. David L. Brunsma (con), Professor of Sociology at Virginia Tech.
The school uniforms website also features "Did You Know?" information bits including (sources available online):
- The first school district in the United States to require all K-8 students to wear uniforms was Long Beach, CA, in Jan. 1994.
- Americans spend around $1 billion per year on school uniforms.
ProCon.org President & Managing Editor Kamy Akhavan said: "We tackled this debate because many middle school teachers asked us for a controversial topic that tweens could relate to, because it remains a contentious policy issue for public and private schools, and because there was a gap in the existing research in this topic that our unique brand of nonpartisan research could help fill. We have no stake in the issue other than to stimulate critical thinking by presenting the best arguments on both sides."
- US schools with a minority student population of 50% or more are four times as likely to require uniforms than schools with a minority population of 20-49%, and 24 times more likely than schools with minority populations of 5%-19%.
For pros, cons, and related research on school uniforms, visit http://school-uniforms.procon.org
|
|
|[Disclaimer on comment system: The unmoderated comment system below is powered by Facebook. ProCon.org has no control over the content of these comments, and we therefore take no responsibility for them. Comments you post here may also appear on your Facebook page. We have provided this comment system as a courtesy to our readers. We may remove it in the future. If some comments are clearly spam or otherwise abuse the comment system, please report them to Facebook.]
|
|
|