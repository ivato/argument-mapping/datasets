Kids USA Survey Home
"School Uniforms" Survey Results
b. Based on the number of students who voted in this region, what may be one reason for this difference in opinion?
b. Now do the math. First, add up how many students voted in the survey. Of this total, what percentage of students voted "no" to school uniforms? Is this what you predicted?
Check out the answers.
|School Uniforms: Yes or No (by region)|
|Do School Uniforms Make Kids More or Less Competitive About Clothing? (by grade)|
|Favorite School Uniform Colors (by gender)|