Duval Students Speak Out Against School Uniforms
Many commenters spoke out against uniforms at Tuesday’s Duval County School Board meeting.
Superintendent Nikolai Vitti is pushing the switch to uniforms for all elementary and middle schools in the 2016-17 school year.
Vitti says requiring uniforms sets a higher expectation for kids and puts a focus on academics.
But a handful of students at this week’s meeting felt otherwise. Third-grader Fiona Citrone gave Vitti letters from her Fishweir Elementary classmates.
“I want to say that maybe you guys should try going to different Duval County schools and getting votes from the kids because we’re the ones that are going to have to be wearing the school uniforms,” she told Vitti and board members.
Board Chair Ashley Smith Juarez told attendees the board is still researching what would be best for the students.
“I think there are some conflicting ideas about what the Board may already have been asked to act upon,” she said. “We haven’t had any formal discussion of any formal recommendation.”
So far the Board has asked the public to answer an online survey about uniforms, and district staff have engaged parent-student associations.
Juarez says the board will workshop Vitti’s proposal before he can formally recommend uniforms. She says the earliest they could be adopted is April unless a special meeting is called.