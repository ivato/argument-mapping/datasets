Donald Trump - Position Changes
We are not suggesting that position changes are good or bad. We understand they are important to voters, which is why we have included them. We list the current position as Now X (Pro, Con, or Not Clearly Pro or Con) and the former position as Formerly Y (Pro, Con, or Not Clearly Pro or Con).
1
Should Abortion Be Legal?
Now Con
"[Donald Trump:] I am pro-life.
[Chris Matthews:] How do you ban abortion? How do you actually do it?
[Trump:] Well, You go back to a position like they had where people will perhaps go to illegal places but you have to ban it...
[Matthews:] Do you believe in punishment for abortion, yes or no, as a principle?
[Trump:] The answer is that there has to be some form of punishment.
[Matthews:] For the woman?
[Trump:] Yeah. There has to be some form."
Source: Maggie Haberman, "Pressed on Abortion Ban, Donald Trump Sees a Penalty for Women," nytimes.com, Mar. 30, 2016
[Editor's Note: Shortly after the above Mar. 30, 2016 quote was released to the media, Trump's campaign released a statement: "If Congress were to pass legislation making abortion illegal and the federal courts upheld this legislation, or any state were permitted to ban abortion under state and federal law, the doctor or any other person performing this illegal act upon a woman would be held legally responsible, not the woman. The woman is a victim in this case as is the life in her womb. My position has not changed — like Ronald Reagan, I am pro-life with exceptions."
In Sep. 2016, Donald Trump's campaign released a letter to the Susan B. Anthony List, stating that he was committed to: "nominating pro-life justices to the U.S. Supreme Court," "signing into law the Pain-Capable Unborn Child Protection Act, which would end painful late-term abortions nationwide," and making the Hyde Amendment permanent.]
Formerly Not Clearly Pro or Con
"And I am pro-life. And if you look at the question, I was in business. They asked me a question as to pro-life or choice. And I said if you let it run, that I hate the concept of abortion. I hate the concept of abortion. And then since then, I've very much evolved.
And what happened is friends of mine years ago were going to have a child, and it was going to be aborted. And it wasn't aborted. And that child today is a total superstar, a great, great child. And I saw that. And I saw other instances.
And I am very, very proud to say that I am pro-life."
Source: Washington Post, "Annotated Transcript: The Aug. 6 GOP Debate," www.washingtonpost.com, Aug. 6, 2015
[ Editor's Note: Our question specifically asks whether abortion should be "legal." Thus, if a candidate states that they are pro-life but does not advocate changing the law to make abortion illegal (for example, by overturning Roe v. Wade or supporting a constitutional amendment to do so), then we list them as "Not Clearly Pro or Con."]
Formerly Pro
"Trump: I'm very pro-choice. I hate the concept of abortion. I hate it. I hate everything it stands for. I cringe when I hear people debating the subject, but, you still, I just believe in choice.
Tim Russert: But you would not ban it?
Trump: No."
Source: tpmtv, "Trump on Abortion in 1999," www.youtube.com, Apr. 19, 2011
Back to present position
2
Should All Americans Have the Right (Be Entitled to) Health Care?
Now Not Clearly Pro or Con
"As far as single payer [universal health care], it works in Canada. It works incredibly well in Scotland. It could have worked in a different age, which is the age you're talking about here.
What I'd like to see is a private system without the artificial lines around every state. I have a big company with thousands and thousands of employees. And if I'm negotiating in New York or in New Jersey or in California, I have like one bidder. Nobody can bid…
I'm not [in reference to Rand Paul saying Trump is still arguing for a single-payer system]. I don't think you heard me."
Source: TIME, "Transcript: Read the Full Text of the Primetime Republican Debate," time.com, Aug. 11, 2015
Formerly Pro
"I'm a conservative on most issues but a liberal on this one. We should not hear so many stories of families ruined by healthcare expenses. We must not allow citizens with medical problems to go untreated because of financial problems or red tape. It is an unacceptable but accurate fact that the number of uninsured Americans has risen to forty-two million.
Working out detailed plans will take time. But the goal should be clear: Our people are our greatest asset. We must take care of our own. We must have universal healthcare."
Source: Donald Trump, The America We Deserve, 2000
Back to present position
3
Should the Federal Minimum Wage Be Increased?
Now Pro
"Goofy Elizabeth Warren lied when she says I want to abolish the Federal Minimum Wage. See media—asking for increase!”
Source: Donald Trump, Twitter post, twitter.com, May 11, 2016
Formerly Con
"CHUCK TODD: Minimum wage. Minimum wage. At a debate, you know. You remember what you said. You thought you didn't want to touch it. Now you're open to it. What changed?
DONALD TRUMP: ...I have seen what's going on. And I don't know how people make it on $7.25 an hour. Now, with that being said, I would like to see an increase of some magnitude. But I'd rather leave it to the states. Let the states decide. Because don't forget, the states have to compete with each other. So you may have a governor --
CHUCK TODD: Right. You want the fed-- but should the federal government set a floor, and then you let the states--
DONALD TRUMP: No, I'd rather have the states go out and do what they have to do. And the states compete with each other, not only other countries, but they compete with each other, Chuck. So I like the idea of let the states decide. But I think people should get more. I think they're out there. They're working. It is a very low number. You know, with what's happened to the economy, with what's happened to the cost. I mean, it's just-- I don't know how you live on $7.25 an hour. But I would say let the states decide."
Source: NBC News, "Meet the Press - May 8, 2016," nbcnews.com, May 8, 2016
Formerly Not Clearly Pro or Con
"[Wolf Blitzer] American workers, you say they deserve to earn more money right?
[Donald Trump] I want them to earn more money.
[Wolf Blitzer] So Bernie Sanders says he wants $15 an hour minimum wage. And he has really gone after you lately for saying you're happy with $7.25, the current federal minimum wage. You can't live on $7.25 an hour.
[Donald Trump] No. And I'm actually looking at that because I'm very different from most republicans. I mean, you have to have something that you can live on. But, what I'm really looking to do is get people great jobs so they make much more money than that, so they make more money than the $15. Now, if you start playing around too much with the lower level, the lower level number, you're not going to be competitive...
[Wolf Blitzer] You're open to raising the minimum wage?
[Donald Trump] I'm open to doing something with it because I don't like that. But what I really do like is bring our jobs back."
Source: CNN, "Donald Trump's Official CNN Interview as Presumptive Nominee (Part 2)," youtube.com, May 4, 2016
Formerly Con
"You know, we're in a global economy now. It used to be people would leave New York state and companies would leave New York state or leave another state and go to Florida, go to Texas, go to wherever they go because of the wages, you know all sorts of different things. Well, now it's not leaving New York or New Jersey or wherever they may be leaving. Now they are leaving the United States and they're going to other countries because they're competing for low taxes and they're competing for low wages and they're competing for all sorts of things... [W]e are no longer competing against one state against the other, we're competing, it's the United States against other places where, Joe [Scarborough], where the taxes are lower, where the wages are lower, where lots of things are lower. Now, I want to create jobs so you don’t have to worry about the minimum wage, you're doing a great job making much more than the minimum wage. But I think having a low minimum wage is not a bad thing for this country."
Source: Morning Joe, "Trump: Minimum Wage Isn't a Bad Thing," msnbc.com, Aug. 20, 2015
Back to present position
4
Should Recreational Marijuana Be Legal?
Now Not Clearly Pro or Con
"Marijuana is such a big thing. I think medical should happen -- right? Don't we agree? I think so. And then I really believe we should leave it up to the states. And of course you have Colorado. And I love Colorado and the people are great, but there's a question as to how it's all working out there, you know? That's not going exactly trouble-free. So I really think that we should study Colorado, see what's happening."
Source: Jenna Johnson, "Trump Softens Position on Marijuana Legalization,” www.washingtonpost.com, Oct. 29, 2015
Formerly Con
"[Interviewer] Colorado, marijuana, good or bad experiment?
[Donald Trump] I say it's bad. Medical marijuana is another thing, but I think it's bad and I feel strongly about that… If they [Colorado] vote for it, they vote for it but, you know, they've got a lot of problems going on right now in Colorado, some big problems."
Source: C-SPAN, "Donald Trump on Marijuana,” www.cspan.org, June 23, 2015
Formerly Pro
"We're losing badly the war on drugs. You have to legalize drugs to win that war. You have to take the profit away from these drug czars... What I'd like to do maybe by bringing it up is cause enough controversy that you get into a dialogue on the issue of drugs so people will start to realize that this is the only answer; there is no other answer."
Source: Spartanburg Herald-Journal, "Trump Supports Drug Legalization,” news.google.com, Apr. 13, 1990
Back to present position
5
Should Planned Parenthood Receive Federal Funding?
Now Con
"I’m against it. I’m for defunding Planned Parenthood, very strongly. And that’s really become, that's been such a big subject lately, especially when you’re looking on television and you’re seeing, you know, some of those clips that are terrible—so I’m totally for defunding. We shouldn’t be giving to Planned Parenthood.”
Source: Last Night Today, "Donald Trump Speech Holds Rally in Newton, IA 11/19/15 (Full),” www.youtube.com, Nov. 19, 2015
Formerly Not Clearly Pro or Con
"Well, the biggest problem I have with Planned Parenthood is the abortion situation. I mean, it's like an abortion factory, frankly. You can't have it and you just shouldn't be funding it. And that should not be funded by the government. And I feel strongly about that and that's my biggest problem with Planned Parenthood, because, really, if you look at it and the work they do, it really has become heavily centered on abortion and you can't have that.
CUOMO: They say it's [abortion] only three percent of what they do and the money that does go toward abortions is not the money that comes from the federal government, that they separate the two.
TRUMP: Here's what I would do if the time came: I would look at the individual things that they do and maybe some of the things are good and I know a lot of things are bad. The abortion aspect of it should not be funded by government. Absolutely.
CUOMO: So, you would take a look at it before you'd defund it. That's what's being asked for right now. Many in your party are doing the opposite. They're saying defund it and then look at it. You'd say, look at it first.
TRUMP: I would look at the good aspects of it and I would also look as I'm sure they do some things properly and good and good for women and I would look at that. I would look at other aspects, also. But we have to take care of women. We have to absolutely take care of women. The abortion aspect of Planned Parenthood should not absolutely – should not be funded."
Source: CNN, "New Day: One-on-One Interview with Donald Trump. Aired 7:30-8a ET," www.cnn.com, Aug. 11, 2015
Back to present position
6
Should the United States Allow Syrian Refugees into the Country?
Now Con
"We have no idea who these people [Syrian refugees] are, we are the worst when it comes to paperwork… This could be one of the great Trojan horses… We cannot let them into this country, period. Our country has tremendous problems. We can’t have another problem.”
Source: Tal Kopan, "Donald Trump: Syrian Refugees a ‘Trojan Horse,’” www.cnn.com, Nov. 16, 2015
Formerly Pro
"I hate the concept of it [accepting Syrian refugees], but on a humanitarian basis, you have to… But you know, it’s living hell in Syria. There’s no question about it. They’re living in hell and something has to be done.”
Source: Nick Gass, "Trump Calls for Taking in Syrian Refugees,” www.politico.com, Sep. 9, 2015
Back to present position
7
Should Any Federal Taxes Be Increased?
Now Con
"Let me just set it straight. I put in the biggest tax decrease of anyone running for office so far. And that really is a proposal because we have to go to Congress... I said I may have to increase it on the wealthy. I'm not going to allow it to be increased on the middle class. Now, if I increase on the wealthy, that means they're still going to being paying less than they pay now. I'm not talking about increasing [the tax rate] at this point. I'm talking about increasing [the numbers] from my tax proposal."
Source: Eric Levitz, "Donald Trump Does Not Want to Raise Taxes on the Wealthy," nymag.com, May 9, 2016
Formerly Pro
"[Savannah Guthrie:] Do you believe in raising taxes on the wealthy?
[Donald Trump:] I do. I do, including myself. I do."
Source: Today Show, "Trump on Today Town Hall: Abortions Exceptions, Immigration, Raising Taxes," today.com, Apr. 21, 2016
[ Editor’s Note: On an Nov. 10, 1999 appearance on Good Morning America, Trump stated: "Well, basically, this would be a one-time tax, 14.25 percent against people with a net worth of over 10 million… It would pay off in its entirety the national debt of $5.7 trillion, and you’d save $200 billion a year. So taxes for the middle class would go way down, the estate and inheritance tax totally wiped out, and the Social Security system would be saved.”]
Back to present position
8
Should Transgender People Be Allowed to Use the Bathroom of Their Choice?
Now Not Clearly Pro or Con
"[Sean Hannity:] Let me ask, I know there's a huge issue in North Carolina and it's a so-called 'bathroom law,' and a lot of people have made, I don't know how many people this actually impacts, my guess it's a very small number but it's become controversial and Ted Cruz tried to attack you on this today and so I wanted to give you a chance to explain your position on that as it relates to transgenders picking which bathroom they want to go to. I can't even believe I'm asking the question but I'm asking.
[Donald Trump:] Well, it is a small number but we have to take care of everybody, frankly. And North Carolina, which is a great place, which I won by the way. I love North Carolina and they have a law and it's a law that, you know, unfortunately is causing them some problems and I fully understand if they want to go through but they are losing business and they are having a lot of people come out against. With me, I look at it differently, a community, whether it's North Carolina or local communities, is really, they should be involved. We have so many big issues to be thinking about, Sean. We have ISIS to worry about. We have bringing trade back. We have rebuilding our military. But I think this, I think that local communities and states should make the decision and I feel very strongly about that. The federal government should not be involved."
Source: Fox News, "Trump: I Will Reduce Taxes and Take on Wall Street," foxnews.com, Apr. 21, 2016
Formerly Pro
"[Jessica Hershey via Twitter post] Mr. Trump, please be specific and tell us views of LGBT and how you plan to be inclusive. Speak abt [sic] NC Bathroom law…
[Donald Trump:] Oh, I had a feeling that question was going to come up but I will tell you. Look, North Carolina did something that was very strong and they're paying a big price and there's a lot of problems. And I heard one of the best answers I heard was from a commentator yesterday saying 'leave it the way it is' right now. There have been no, very few, problems. Leave it the way it is. North Carolina, what they're going through, with all of the business that's leaving and all of the strife and that's on both sides. You leave the way it is. There have been very few complaints the way it is. People go, they use the bathroom that they feel is appropriate. There has been so little trouble. And the problem with what happened in North Carolina is the strife and the economic, I mean, the economic punishment that they’re taking. So I would say that's probably the worst part—
[Matt Lauer:] Do you have any transgender people working in your organization?
[Donald Trump:] I don't know. I really don't know. I probably do. I really don't know.
[Matt Lauer:] So, so, if Caitlyn Jenner were to walk into Trump Tower and want to use the bathroom, you would be fine with her using any bathroom she chooses?
[Donald Trump:] That is correct."
Source: Eun Kyung Kim, "Donald Trump Joins TODAY Show for Live Town Hall, Answers Voters' Questions," today.com, Apr. 21, 2016
Back to present position
9
Should Gun-Free Zones Exist?
Now Not Clearly Pro or Con
"She [Hillary Clinton] talked about guns in classrooms. I don’t want to have guns in classrooms although, in some cases, teachers should have guns in classrooms, frankly. Because, teachers are, you know, things that are going on in our schools are unbelievable. You look at some of our schools, unbelievable what’s going on. But I’m not advocating guns in classrooms. But remember in some cases, and a lot of people have made this case, teachers should be able to have guns, trained teachers should be able to have guns in classrooms.”
Source: David Edwards, "Donald Trump: ‘I’m Not Advocating Guns in Classrooms’ but ‘Teachers Should Have Guns in Classrooms,’” rawstory.com, May 22, 2016
Formerly Con
"I will get rid of gun-free zones on school, and—you have to—and on military bases. My first day, it gets signed, okay? My first day. There’s no more gun-free zones."
Source: Jenna Johnson, "Donald Trump: 'I Will Get Rid of Gun-Free Zones on Schools,'" washingtonpost.com, Jan. 8, 2016
Back to present position
10
Should More Gun Control Laws Be Enacted?
Now Con
"[Moderator Maria Bartiromo] Mr. Trump, are there any circumstances that you think we should be limiting gun sales of any kind in America?
TRUMP: No. I am a 2nd amendment person. If we had guns in California on the other side where the bullets went in the different direction, you wouldn't have 14 or 15 people dead right now.
If even in Paris, if they had guns on the other side, going in the opposite direction, you wouldn't have 130 people plus dead. So the answer is no."
[ Editor’s Note: On Feb. 10, 2011, Donald Trump told the audience at the Conservative Political Action Congress (CPAC): "I'm against gun control." The speech is available at Gary Franchi, "Donald Trump @ CPAC 2011," youtube.com, Feb. 14, 2011]
Source: Washington Post, "6th Republican Debate Transcript, Annotated: Who Said What and What It Meant," washingtonpost.com, Jan. 14, 2016
Formerly Pro
"This is another issue where you see the extremes of the two existing major parties. Democrats want to confiscate all guns, which is a dumb idea because only the law-abiding citizens would turn in their guns and the bad guys would be the only ones left armed. The Republicans walk the NRA line and refuse even limited restrictions.
I generally oppose gun control, but I support the ban on assault weapons and I support a slightly longer waiting period to purchase a gun.
With today's Internet technology we should be able to tell within seventy-two hours if a potential gun owner has a [criminal] record."
Source: Donald Trump, The America We Deserve, 2010
Back to present position
11
Should the United States Continue the War on Drugs?
Now Not Clearly Pro or Con
"[George] Stephanopoulos: You used to think that legalization [of drugs], taking the profit out, would solve that problem. What changed your mind?
Trump: Well, I did and I—I—not think about it, I said it’s something that should be studied and maybe should continue to be studied. But it’s not something I’d be willing to do right now. I think it’s something that I’ve always said maybe it has to be looked at because we do such a poor job of policing. We don’t want to build walls. We don’t want to do anything. And if you’re not going to want to do the policing, you’re going to have to start thinking about other alternatives. But it’s not something that I would want to do. But it’s something that certainly has been looked at and I looked at it. If we police properly, we shouldn’t do that.”
Source: ABC News, "’This Week’ Transcript: Ben Carson and Donald Trump,” abcnews.go.com, Nov. 8, 2015
Formerly Con
"We’re losing badly the war on drugs. You have to legalize drugs to win that war. You have to take the profit away from these drug czars.”
Source: Sarasota Herald-Tribune, "Donald Trump: Legalize Drugs,” news.google.com, Apr. 14, 1990
Back to present position
12
Should Undocumented Immigrants in the United States Be Allowed to Become Legal Residents?
Now Con
"Our message to the world will be this. You cannot obtain legal status or become a citizen of the United States by illegally entering our country. Can't do it.
This declaration alone will help stop the crisis of illegal crossings and illegal overstays, very importantly. People will know that you can't just smuggle in, hunker down and wait to be legalized. It's not going to work that way. Those days are over…
For those here illegally today, who are seeking legal status, they will have one route and one route only. To return home and apply for reentry like everybody else, under the rules of the new legal immigration system that I have outlined above."
Source: Los Angeles Times staff, "Transcript: Donald Trump's Full Immigration Speech, Annotated,” latimes.com, Sep. 1, 2016
Formerly Not Clearly Pro or Con
"[Donald Trump] So now we have the person [an immigrant who is in the country illegally], 20 years been an upstanding person, the family is great, everyone is great, do we throw them out? Or do we work with them and try and do something?...
[Sean Hannity] You have been sort of indicating that there will be some flexibility... originally you had said they're all out... but here is the big question - no citizenship?
[Donald Trump] No citizenship. Let me go a step further, they’ll pay back taxes, they have to pay taxes, there’s no amnesty as such. There’s no amnesty. But we work with them. Now, okay, but when I look at the rooms, and I have this all over, now everybody agrees we get the bad ones out, but when I go through and I meet thousands and thousands of people on this subject, and I’ve had very strong people come up to me, really great, great people come up to me, and they’ve said, Mr. Trump, I love you, but to take a person that’s been here for 15 or 20 years and throw them and the family out, it’s so tough, Mr. Trump. I mean, I’ve — I have it all the time. It’s a very, very hard thing...
We are going to come out with a decision very soon [on immigration policy]."
Source: Hannity, "Trump Asks His Supporters to Weigh in on Deportation Policy," foxnews.com, Aug. 24, 2016
Formerly Con
"I don't think you'd even be talking about illegal immigration if it weren't for me. So, we have a country of laws, they're going to go out, and they'll come back if they deserve to come back. If they've had a bad record, if they've been arrested, if they've been in jail, they're never coming back. We're going to have a country again. Right now, we don't have a country, we don't have a border, and we're going to do something about it, and it can be done with proper management, and it can be done with heart."
Source: CNN, "GOP Presidential Debate. Aired 8:10-11:15p ET.," transcripts.cnn.com, Sep. 16, 2015
Back to present position
13
Should the US Have Attacked Iraq in 2003?
Now Con
"I'm the only one on this stage that said, 'Do not go into Iraq. Do not attack Iraq.' Nobody else on this stage said that. And I said it loud and strong. And I was in the private sector. I wasn't a politician, fortunately.
But I said it, and I said it loud and clear, 'You'll destabilize the Middle East.' That's exactly what happened...
Obviously, the war in Iraq was a big, fat mistake...
The war in Iraq, we spent $2 trillion, thousands of lives...
Obviously, it was a mistake...
George Bush made a mistake. We can make mistakes. But that one was a beauty. We should have never been in Iraq. We have destabilized the Middle East."
Source: Washington Post, "The CBS News Republican Debate Transcript, Annotated," washingtonpost.com, Feb. 13, 2016
Formerly Pro
"[Howard Stern:] Are you for invading Iraq?
[Donald Trump:] Yeah, I guess so... I wish the first time it was done correctly."
Source: KFILE, "Trump on the Howard Stern Show on Sep, 11, 2002," soundcloud.com (accessed Sep. 8, 2016)
Back to present position
14
Should the US Military Budget Be Increased?
Now Pro
"Under Barack Obama and Hillary Clinton, defense spending is on track to fall to its lowest level as a share of the economy since the end of World War II. We currently have the smallest Army since 1940. The Navy is among the smallest it has been since 1915. And the Air Force is the smallest it has been since 1947...
In the current year, we are spending $548 billion – a cut of 10% in real inflation-adjusted dollars. This reduction was done through what is known as the sequester, or automatic defense budget cuts. Under the budget agreement, defense took half of the cuts – even though it makes up only one-sixth of the budget.
As soon as I take office, I will ask Congress to fully eliminate the defense sequester and will submit a new budget to rebuild our military.
This will increase certainty in the defense community as to funding, and will allow military leaders to plan for our future defense needs.
As part of removing the defense sequester, I will ask Congress to fully offset the costs of increased military spending."
Source: The Hill Staff, "Transcript of Donald Trump’s Speech on National Security in Philadelphia," thehill.com, Sep. 7, 2016
Formerly Con
"We have-- even in the military-- I'm gonna build a military that's gonna be much stronger than it is right now. It's gonna be so strong, nobody's gonna mess with us. But you know what? We can do it for a lot less. They sent a washer from South Carolina to Texas. It cost $997,000, okay?... It was fraud... It was fraud… I think we can make our defense much stronger, spend somewhat less money and increase... Chuck [Todd], when they send an 18 cent washer from South Carolina to Texas and it costs almost a million dollars to bring it there, because of the fraud and abuse and everything else-- I mean, it was a fraud-- but there have been many cases like that-- we can save so much. When a hammer that you buy at Home Depot for $8.00 costs… nine hundred dollars."
Source: NBC News, "Meet the Press Transcript – Oct 4, 2015," nbcnews.com, Oct. 4, 2015
Back to present position
15
Should the Federal Government Guarantee Paid Family and Medical Leave?
Now Pro
"The United States is the only developed country that does not provide cash benefits for new mothers. According to the U.S. Department of Labor: ‘Only 12 percent of U.S. private sector workers have access to paid family leave through their employer.’ Each year, 1.4 million women who work give birth without any paid leave from their employer.
The Trump plan will enhance Unemployment Insurance (UI) to include 6 weeks of paid leave for new mothers so that they can take time off of work after having a baby. This would triple the average 2 weeks of paid leave received by new mothers, which will benefit both the mother and the child.”
Source: Donald J. Trump for President, Inc., "Child Care Reforms That Will Make America Great Again,” donaldjtrump.com (accessed Sep. 14, 2016)
Formerly Not Clearly Pro or Con
"[Stuart Varney:] Fast response, if you could: paid family leave.
[Donald Trump:] Well it's something being discussed. I think we have to keep our country very competitive, so you have to be careful of it. But certainly there are a lot of people discussing it."
Source: Rob Wile, "Trump: Yes, I Would Shut Down Mosques. Also, Salaries of American Workers Are Too High," fusion.net, Oct. 21, 2015
Back to present position