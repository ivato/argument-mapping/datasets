By Linda Lowen
Many points come up in the abortion debate. Here's a look at abortion from both sides - 10 arguments for abortion and 10 arguments against abortion - for a total of 20 statements that represent a range of topics as seen from both sides.
Pro-Life - 10 Arguments Against Abortion
- Since life begins at conception, abortion is akin to murder as it is the act of taking human life. Abortion is in direct defiance of the commonly accepted idea of the sanctity of human life
No civilized society permits one human to intentionally harm or take the life of another human without punishment, and abortion is no different.
Adoption is a viable alternative to abortion and accomplishes the same result. And with 1.5 million American families wanting to adopt a child, there is no such thing as an unwanted child.
An abortion can result in medical complications later in life; the risk of ectopic pregnancies doubles, and the chance of a miscarriage and pelvic inflammatory disease also increases.
In the instance of rape and incest, proper medical care can ensure that a woman will not get pregnant. Abortion punishes the unborn child who committed no crime; instead, it is the perpetrator who should be punished.
Abortion should not be used as another form of contraception.
For women who demand complete control of their body, control should include preventing the risk of unwanted pregnancy through the responsible use of contraception or, if that is not possible, through abstinence.
Many Americans who pay taxes are opposed to abortion, therefore it's morally wrong to use tax dollars to fund abortion.
Those who choose abortions are often minors or young women with insufficient life experience to understand fully what they are doing. Many have lifelong regrets afterwards.
Abortion frequently causes intense psychological pain and stress.
Pro-Choice - 10 Arguments For Abortion
Nearly all abortions take place in the first trimester, when a fetus cannot exist independent of the mother. As it is attached by the placenta and umbilical cord, its health is dependent on her health, and cannot be regarded as a separate entity as it cannot exist outside her womb.
The concept of personhood is different from the concept of human life. Human life occurs at conception, but fertilized eggs used for in vitro fertilization are also human lives and those not implanted are routinely thrown away. Is this murder, and if not, then how is abortion murder?
Adoption is not an alternative to abortion, because it remains the woman's choice whether or not to give her child up for adoption. Statistics show that very few women who give birth choose to give up their babies - less than 3% of white unmarried women and less than 2% of black unmarried women.
Abortion is a safe medical procedure. The vast majority of women - 88% - who have an abortion do so in their first trimester. Medical abortions have less than 0.5% risk of serious complications and do not affect a woman's health or future ability to become pregnant or give birth.
In the case of rape or incest, forcing a woman made pregnant by this violent act would cause further psychological harm to the victim. Often a woman is too afraid to speak up or is unaware she is pregnant, thus the morning after pill is ineffective in these situations.
Abortion is not used as a form of contraception. Pregnancy can occur even with responsible contraceptive use. Only 8% of women who have abortions do not use any form of birth control, and that is due more to individual carelessness than to the availability of abortion.
The ability of a woman to have control of her body is critical to civil rights. Take away her reproductive choice and you step onto a slippery slope. If the government can force a woman to continue a pregnancy, what about forcing a woman to use contraception or undergo sterilization?
Taxpayer dollars are used to enable poor women to access the same medical services as rich women, and abortion is one of these services. Funding abortion is no different from funding a war in the Mideast. For those who are opposed, the place to express outrage is in the voting booth.
Teenagers who become mothers have grim prospects for the future. They are much more likely to leave of school; receive inadequate prenatal care; rely on public assistance to raise a child; develop health problems; or end up divorced.
Like any other difficult situation, abortion creates stress. Yet the American Psychological Association found that stress was greatest prior to an abortion, and that there was no evidence of post-abortion syndrome.
Sources:
The Minnesota Family Council
Alcorn, Randy. Pro-Life: Answers to Pro-Choice Arguments. Multnomah Books 1994.
Messerli, Joe. "Should Abortion Be Banned (Except in Special Circumstances Like Saving the Mother's Life)?" Balanced Politics 17 March 2007.
"Legal Abortion: Arguments Pro & Con." Westchester Coalition for Legal Abortion
"Placing children." Adoption.com
National Abortion Federation
- Politics & Women
- Reproductive Rights and Issues
- Workplace Issues Facing Women
- Girls, Teens, and Young Women
- International Women's Rights - Global Women's Rights
- Inside & Out - Body Issues & Relationship Issues
- Parenting, Caregiving & Family Relationships
- Money Matters for Women
- Life Transitions & Starting Over
- Media on Women - Images and Influences
- Feminist Movement & Equal Rights
- Violence Against Women
- The Arts and Creativity
- Influential Women
- Community & Connection - Women's Groups
- Women and Criminal Justice
- Women in Politics