T1	Sentence 0 103	"I believe we need to protect access to safe and legal abortion, not just in principle but in practice.
T2	Sentence 104 195	Any right that requires you to take extraordinary measures to access it is no right at all.
T3	Sentence 196 306	Not when patients and providers have to endure harassment and intimidation just to walk in to a health center.
T4	Sentence 307 438	Not when, not when making an appointment means taking time off from work, finding child care and driving halfway across your state.
T5	Sentence 439 559	Not when providers are required by state law to recite misleading information to women in order to shame and scare them.
T6	ArgumentFor 560 698	And not as long as we have laws on the book like the Hyde Amendment, making it harder for low-income women to exercise their full rights."
T7	Sentence 699 794;795 868	Source: C-Span, “Hillary Clinton Calls for Ending Hyde Amendment, www.c-span.org, Jan. 10, 2016 "As Governor, Johnson never advocated abortion or taxpayer funding of it.
T8	ArgumentFor 869 1001	However, Gov. Johnson recognizes that the right of a woman to choose is the law of the land today, and has been for several decades.
T9	Sentence 1002 1107	That right must be respected, and ultimately he believes this is a very personal and individual decision.
T10	ArgumentFor 1108 1202	He feels that each woman must be allowed to make decisions about her own health and well-being
T11	ArgumentFor 1204 1485	Further, Gov. Johnson feels strongly that women seeking to exercise their legal right must not be subjected to persecution or denied access to health services by politicians in Washington or elsewhere who are insistent on politicizing such an intensely personal and serious issue."
T12	Sentence 1486 1591;1592 1609	Source: Gary Johnson 2016, "Abortion and the Right to Life," garyjohnson2016.com (accessed Apr. 20, 2016) [Chris Matthews:]
T13	Sentence 1610 1634	How do you ban abortion?
T14	Sentence 1635 1661	How do you actually do it?
T15	Sentence 1662 1670	[Trump:]
T16	Sentence 1677 1788	You go back to a position like they had where people will perhaps go to illegal places but you have to ban it..
T17	Sentence 1790 1801	[Matthews:]
T18	Sentence 1802 1871	Do you believe in punishment for abortion, yes or no, as a principle?
T19	Sentence 1872 1880	[Trump:]
T20	Sentence 1881 1939	The answer is that there has to be some form of punishment
T21	Sentence 1941 1952	[Matthews:]
T22	Sentence 1953 1967	For the woman?
T23	Sentence 1968 1976	[Trump:]
T24	Sentence 1983 2010	There has to be some form."
T25	Sentence 2011 2128;2129 2559	Source: Maggie Haberman, "Pressed on Abortion Ban, Donald Trump Sees a Penalty for Women," nytimes.com, Mar. 30, 2016 [Editor's Note: Shortly after the above Mar. 30, 2016 quote was released to the media, Trump's campaign released a statement: "If Congress were to pass legislation making abortion illegal and the federal courts upheld this legislation, or any state were permitted to ban abortion under state and federal law, the doctor or any other person performing this illegal act upon a woman would be held legally responsible, not the woman.
T26	Sentence 2560 2622	The woman is a victim in this case as is the life in her womb.
T27	Sentence 2623 2704	My position has not changed — like Ronald Reagan, I am pro-life with exceptions."
T28	Sentence 2705 3043	In Sep. 2016, Donald Trump's campaign released a letter to the Susan B. Anthony List, stating that he was committed to: "nominating pro-life justices to the U.S. Supreme Court," "signing into law the Pain-Capable Unborn Child Protection Act, which would end painful late-term abortions nationwide," and making the Hyde Amendment permanent
T29	Sentence 3045 3250	"I believe that it is an issue that is best left to the individual conscience of women, and in the meantime, as a society, we should do those things that we can to advance the dignity of every human life."
T30	Sentence 3251 3365;3366 3389	Source: George P. Matysek, Jr., "O'Malley Touts 'Progressive' Agenda for State," catholicreview.org, Oct. 27, 2010 "We are not going back.
T31	Sentence 3390 3474	Not only are we not going to retreat on women's rights, we are going to expand them.
T32	Sentence 3475 3509	We are going forward, not backward
T33	ArgumentFor 3511 3609	We are not returning to the days of back-room abortions, when countless women died or were maimed.
T34	ArgumentFor 3610 3730	The decision about abortion must remain a decision for the woman, her family and physician to make, not the government."
T35	Sentence 3731 3823;3824 3964	Source: Bernie Sanders, "United against the War on Women," huffingtonpost.com, June 30, 2012 "Today marks the dark anniversary of Roe v. Wade, the Supreme Court decision that overturned a Texas law that prohibited abortion on demand.
T36	ArgumentAgainst 3965 4042	Since that 1973 ruling, more than 55 million lives have been lost to abortion
T37	Sentence 4044 4168	Defending life, at its core, includes protecting both the unborn child and his or her mother from an irreversible injustice…
T38	Sentence 4169 4401	I have been honored to defend the dignity of human life, helping successfully defend the federal Partial Birth Abortion Act, state parental notification laws, and Texas's law prohibiting state funds for groups that provide abortions
T39	ArgumentAgainst 4403 4566	No right is more precious and fundamental than the right to life, and any just society should protect that right at every stage, from conception to natural death."
T40	Sentence 4567 4673;4674 4689	Source: Ted Cruz, "Sen. Cruz Statement on 40th Anniversary of Roe v. Wade," cruz.senate.gov, Jan. 22, 2013 [Editor's Note:
T41	ArgumentAgainst 4690 5108	In addition to the above CON statement, Ted Cruz also made the following CON statement, as quoted in an Oct. 31, 2012 Houston Chronicle article, "Ted Cruz Says Questions about Mourdock Rape Comment Are 'An Unfortunate Distraction from the Issues that Matter,'" available at blog.chron.com: "I think that every human life is a precious gift from God and should be protected in law from conception until natural death."]
T42	Sentence 5109 5204	"Science is helping those of us who believe in the sanctity of life to make our case every day.
T43	Sentence 5205 5269	Science is why more and more young people are becoming pro-life.
T44	Sentence 5270 5418	We know, for example, that the DNA that is present in the zygote is precisely the same DNA that each person possesses at the moment of their death..
T45	Sentence 5420 5498	We know now that a life in utero dreams, and can feel pain after five months..
T46	Sentence 5500 5566	Every human life has potential, and every human life is precious."
T47	Sentence 5567 5680;5681 5696	Source: "Welcoming Every Life: Choosing Life after an Unexpected Prenatal Diagnosis," heritage.org, Jan. 20, 2015 [Editor's Note:
T48	Sentence 5697 5936	In a Jan. 2010 radio interview, as quoted in the article "Right Fight" by Connie Bruck, published June 7, 2010 in the New Yorker, Carly Fiorina stated, "I absolutely would vote to overturn Roe v. Wade if the opportunity presented itself."]
T49	Sentence 5937 6010	"Since 1973, 60 million unborn children have died in their mother's womb.
T50	Sentence 6011 6115	If I'm president I will invoke the Fifth and 14th Amendment of the Constitution and protect unborn life…
T51	Sentence 6116 6315	What we need to be doing… with abortion, and the destruction of innocent human life, what Lincoln and the early Republican Party did with slavery 130 years ago, and that is bringing abolition to it."
T52	Sentence 6316 6431;6432 6452	Source: Maya Kliger, "Mike Huckabee Compares Actions of ISIS to Abortion," www.desmoinesregister.com, Aug. 26, 2015 "I am 100% pro-life.
T53	Sentence 6453 6549	I believe life begins at conception and that abortion takes the life of an innocent human being.
T54	ArgumentAgainst 6550 6649	It is the duty of our government to protect this life as a right guaranteed under the Constitution.
T55	ArgumentAgainst 6650 6733	For this reason, I introduced S. 583, the Life at Conception Act on March 14, 2013.
T56	ArgumentAgainst 6734 6836	This bill would extend the Constitutional protection of life to the unborn from the time of conception
T57	ArgumentAgainst 6838 6920	It is unconscionable that government would facilitate the taking of innocent life.
T58	ArgumentAgainst 6921 7057	I have stated many times that I will always support legislation that would end abortion or lead us in the direction of ending abortion."
T59	Sentence 7058 7152;7153 7160	Source: Rand Paul, "Advocating for Sanctity of Life," paul.senate.gov (accessed Aug. 19, 2015) "KELLY:
T60	Sentence 7161 7205	You don't favor a rape and incest exception?
T61	Sentence 7206 7236	RUBIO: I have never said that.
T62	Sentence 7237 7269	And I have never advocated that.
T63	ArgumentAgainst 7270 7411	What I have advocated is that we pass law in this country that says all human life at every stage of its development is worthy of protection.
T64	Sentence 7412 7453	In fact, I think that law already exists.
T65	Sentence 7454 7506	It is called the Constitution of the United States..
T66	Sentence 7508 7530	And let me go further.
T67	ArgumentAgainst 7531 7643	I believe that every single human being is entitled to the protection of our laws, whether they can vote or not.
T68	Sentence 7644 7674	Whether they can speak or not.
T69	Sentence 7675 7713	Whether they can hire a lawyer or not.
T70	Sentence 7714 7759	Whether they have a birth certificate or not.
T71	Sentence 7760 7934	And I think future generations will look back at this history of our country and call us barbarians for murdering millions of babies who we never gave them a chance to live."
T72	Sentence 7935 8007;8008 8023	Source: CBS News, "Transcript of the 2015 GOP Debate (9pm), Aug. 7, 2015 [Editor's Note:
T73	Sentence 8024 8333	In addition to the above Con statement, Marco Rubio also made the following CON statement on his campaign website page "Protecting Life at Every Stage," available at marcorubio.com: "I believe that Roe v. Wade was not only morally wrong, but it was a poorly decided legal precedent and should be overturned."]
T74	Sentence 8334 8420	"Here's my record: As governor of the state of Florida, I defunded Planned Parenthood.
T75	Sentence 8421 8463	I created a culture of life in our state..
T76	Sentence 8465 8538	We were the only state to appropriate money for crisis pregnancy centers.
T77	Sentence 8539 8613	We expand dramatically the number adoptions out of our foster care system.
T78	Sentence 8614 8648	We did parental notification laws.
T79	Sentence 8649 8681	We ended partial-birth abortion.
T80	ArgumentAgainst 8682 8765	We did all of this and we were the first state to do a 'choose life' license plate.
T81	ArgumentAgainst 8766 8900	Now 29 states have done it, and tens of millions of dollars have gone to create a culture where more people, more babies are adopted..
T82	Sentence 8902 8921	My record is clear.
T83	Sentence 8922 8973	My record as a pro-life governor is not in dispute.
T84	Sentence 8974 9052	I am completely pro-life, and I believe that we should have a culture of life.
T85	Sentence 9053 9102	It's informed by my faith from beginning to end..
T86	Sentence 9104 9203	And I did this not just as it related to unborn babies; I did it at the end of life issues as well.
T87	Sentence 9204 9252	This is something that goes way beyond politics.
T88	ArgumentAgainst 9253 9352	And I hope one day we get to the point where we respect life in its fullest form across the board."
T89	Sentence 9353 9440;9441 9780	Source: CBS News, "Transcript of the 2015 GOP Debate (9 pm)," cbsnews.com, Aug. 7, 2015 [Editor's Note: Because our question specifically asks whether abortion should be "legal," if a candidate states that they are pro-life but does not advocate changing the law to make abortion illegal (for example, by overturning Roe v. Wade or supporting a constitutional amendment to do so), then we list them as “Not Clearly Pro or Con.”
T90	ArgumentAgainst 9781 10063;10064 10106	In addition to the Not Clearly Pro or Con (NC) statement above, Jeb Bush also made the following (NC) statement during his 1998 gubernatorial bid, as quoted in the Jan. 30, 2015 article "Jeb Bush's Conservative Bid For Governor: The Highlight Reel," available at huffingtonpost.com: "I believe that life begins at conception.
T91	Sentence 10107 10300	I do not believe that the question of abortion will be solved until there is a broad consensus on the subject and until that time it is inappropriate to be advocating constitutional amendments.
T92	Sentence 10301 10431	This is a typical wedge issue that politicians use today to strike fear that somehow people's rights are going to be taken away...
T93	Sentence 10432 10510	I will not be leading the charge to overturn the constitution on this issue."]
T94	Sentence 10511 10551	"I am proud to be a pro-life Republican.
T95	Sentence 10552 10643	I believe that every life is an individual gift from God, and that no life is disposable...
T96	Sentence 10644 10731	One proposal that brings Americans together is the Pain-Capable Unborn Child Protection
T97	Sentence 10732 10860	Act which would protect unborn children beginning at 20 weeks, or five months of pregnancy, based on their ability to feel pain.
T98	Sentence 10861 10948	America is one of just seven countries that permits elective abortions past this point.
T99	Sentence 10949 10980	We can do far better than this.
T100	Sentence 10981 11043	I urge Congress to take swift action on this important issue."
T101	Sentence 11044 11180;11181 11196	Source: Susan B. Anthony List, "Gov. Chris Christie Announces Support of Popular Five-Month Abortion Limit," sba-list.org, Mar. 30, 2015 [Editor's Note:
T102	Sentence 11197 11263	Our question specifically asks whether abortion should be "legal."
T103	Sentence 11264 11518	Thus, if a candidate states that they are pro-life but does not advocate changing the law to make abortion illegal (for example, by overturning Roe v. Wade or supporting a constitutional amendment to do so), then we list them as "Not Clearly Pro or Con."
T104	Sentence 11519 11610	"There is no part of society more vulnerable and in need of our protection than the unborn.
T105	ArgumentAgainst 11611 11714	The inalienable right to life of every innocent human being is an essential element of a civil society.
T106	Sentence 11715 11886	From authoring the Unborn Victims of Violence Act to co-sponsoring the Partial Birth Abortion ban, Lindsey Graham has a long history of leadership on pro-life legislation.
T107	Sentence 11887 11942	The Graham 20-Week Pain Capable Unborn Child Protection
T108	Sentence 11943 11999	Act is the most important federal pro-life effort today.
T109	Sentence 12000 12138	This essential legislation, authored and sponsored by Lindsey Graham, bans abortions at 20 weeks, when a fetus is capable of feeling pain.
T110	ArgumentAgainst 12139 12279	It represents a critical step toward reaffirming the sanctity of life and preserving the basic constitutional rights of the most vulnerable.
T111	ArgumentAgainst 12280 12369	We need a president who always has, and always will, fight for the rights of the unborn."
T112	Sentence 12370 12477;12478 12493	Source: Lindsey Graham 2016, "Protect the Sanctity of Life," www.lindseygraham.com (accessed Aug. 19, 2015) [Editor's Note:
T113	Sentence 12494 12560	Our question specifically asks whether abortion should be "legal."
T114	Sentence 12561 12816	Thus, if a candidate states that they are pro-life but does not advocate changing the law to make abortion illegal (for example, by overturning Roe v. Wade or supporting a constitutional amendment to do so), then we list them as "Not Clearly Pro or Con."]
T115	Sentence 12817 12985	"There are restrictions that we put in, we've done a lot of things in Ohio effecting abortions after 20 weeks, but until that law [Roe v. Wade] changes, that's the law.
T116	ArgumentAgainst 12986 13134	If the court makes a ruling, they make a ruling, but I think there are absolutely legitimate and constitutional restrictions that can be put on it."
T117	Sentence 13135 13247;13248 13330	Source: Michael Warren, "Kasich: Roe v. Wade 'Law of the Land' (Updated)," www.weeklystandard.com, Aug. 20, 2015 [Editor's Note: Our question specifically asks whether abortion should be "legal."
T118	Sentence 13331 13586	Thus, if a candidate states that they are pro-life but does not advocate changing the law to make abortion illegal (for example, by overturning Roe v. Wade or supporting a constitutional amendment to do so), then we list them as "Not Clearly Pro or Con."]
