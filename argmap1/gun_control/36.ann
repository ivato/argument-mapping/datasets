T1	ArgumentAgainst 0 116	Gun ownership is both a basic American right and one of the most contentious social and political issues of the day.
T2	Sentence 117 329	There are about 300 million privately-owned firearms in the US&dash;which works out to roughly one gun for every man, woman and child in America&dash;with nearly a third of the population owning at least one gun.
T3	ArgumentAgainst 332 535	Many of these firearms were bought with home protection in mind, which makes sense: in the right circumstances, a gun can be your first and most effective line of defense against intruders and criminals.
T4	Sentence 536 570	But is it the best choice for you?
T5	Sentence 571 729	For many gun owners, it's a simple calculus: having a gun gives them at least a fighting chance in the event that a burglar or home invader manages to get in.
T6	Sentence 730 1065	And while definitive numbers on the frequency of defensive gun use in the US are hard to come by&dash;they range from a low of 55,0002 to a high of over two million3 per year&dash;none of that will matter if you are one of the thousands of American gun owners who uses a firearm in self defense against a criminal or intruder each year
T7	ArgumentAgainst 1067 1269	Criminals don't like finding themselves on the business end of a gun barrel any more than the rest of us do, which is why 74% of them actively try to avoid breaking into houses when the owners are home.
T8	ArgumentAgainst 1272 1382	In other words, just the fear of being shot is often enough to dissuade criminals from targeting certain homes
T9	Sentence 1384 1465	Unfortunately, guns can't discriminate between criminals and innocent bystanders.
T10	ArgumentFor 1466 1603	Studies have shown that unintentional shootings are four times as common as occurrences of gun use in legitimate home defense situations.
T11	ArgumentFor 1606 1726	You'd actually be more likely, statistically speaking, to shoot someone by accident than you are to shoot a home invader
T12	ArgumentFor 1728 1862	Having a gun in the house also increases your own chances of becoming the victim of a firearm-related homicide or suicide in the home.
T13	Sentence 1865 1990	Researchers have found that this holds true regardless of the type of gun you own, how you store it, or how many guns you own
T14	ArgumentFor 1992 2120	Finally, if you have children, you should take into account how the presence of a firearm in the home might affect their safety.
T15	ArgumentAgainst 2121 2233	Most gun-owning parents take precautions to keep their children from finding and handling the family's firearms.
T16	ArgumentFor 2234 2485	However, despite these efforts, children often handle guns in the home without their parents' knowledge; in one study, 22% of parents who believed that their children had never handled the guns in the home were contradicted by the children themselves.
T17	ArgumentFor 2488 2622	Further, when a child or teen is killed by a firearm, the gun that killed them comes from their own home fully 72 percent of the time.
T18	Sentence 2625 2652	There is no "right" answer.
T19	Sentence 2653 2752	Whether or not a gun is the best choice for your home protection needs is a very personal decision.
T20	ArgumentAgainst 2753 2921	You are the best person to realistically assess the tradeoff between the risks and rewards of gun ownership and decide what the best course of action is for your family
T21	Sentence 2923 2940	Do you own a gun?
T22	Sentence 2941 2957	Is it in a safe?
T23	Sentence 2958 2991	Are you against guns in the home?
T24	Sentence 2992 3047	Join in the discussion below:comments powered by Disqus
