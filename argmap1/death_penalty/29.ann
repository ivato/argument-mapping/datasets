T1	Sentence 0 189	PRO: "The crimes of rape, torture, treason, kidnapping, murder, larceny, and perjury pivot on a moral code that escapes apodictic [indisputably true] proof by expert testimony or otherwise.
T2	Sentence 190 348	But communities would plunge into anarchy if they could not act on moral assumptions less certain than that the sun will rise in the east and set in the west.
T3	ArgumentAgainst 349 501	Abolitionists may contend that the death penalty is inherently immoral because governments should never take human life, no matter what the provocation.
T4	Sentence 502 547	But that is an article of faith, not of fact.
T5	ArgumentFor 548 744	The death penalty honors human dignity by treating the defendant as a free moral actor able to control his own destiny for good or for ill; it does not treat him as an animal with no moral sense."
T6	Sentence 745 842;843 943;944 1192	Bruce Fein, JD Constitutional Lawyer and General Counsel to the Center for Law and Accountability "Individual Rights and Responsibility - The Death Penalty, But Sparingly," www.aba.org June 17, 2008 CON: "Ultimately, the moral question surrounding capital punishment in America has less to do with whether those convicted of violent crime deserve to die than with whether state and federal governments deserve to kill those whom it has imprisoned.
T7	ArgumentAgainst 1193 1342	The legacy of racial apartheid, racial bias, and ethnic discrimination is unavoidably evident in the administration of capital punishment in America.
T8	ArgumentAgainst 1343 1484	Death sentences are imposed in a criminal justice system that treats you better if you are rich and guilty than if you are poor and innocent.
T9	ArgumentAgainst 1485 1683	This is an immoral condition that makes rejecting the death penalty on moral grounds not only defensible but necessary for those who refuse to accept unequal or unjust administration of punishment."
T10	Sentence 1684 1703;1704 1757;1758 1903	Bryan Stevenson, JD Professor of Law at New York University School of Law "Close to Death: Reflections on Race and Capital Punishment in America," from Debating the Death Penalty: Should America Have Capital Punishment?
T11	Sentence 1904 1950;1951 1955;1956 2277	The Experts on Both Sides Make Their Best Case 2004 PRO: "Simply because an execution method may result in pain, either by accident or as an inescapable consequence of death, does not establish the sort of 'objectively intolerable risk of harm' [quoting the opinion of the Court from Farmer v. Brennan, 511 U. S. 825, 842, 846 (1994)] that qualifies as cruel and unusual...
T12	Sentence 2278 2400	Kentucky has adopted a method of execution believed to be the most humane available, one it shares with 35 other States...
T13	Sentence 2401 2539	Kentucky's decision to adhere to its protocol cannot be viewed as probative of the wanton infliction of pain under the Eighth Amendment...
T14	Sentence 2540 2688	Throughout our history, whenever a method of execution has been challenged in this Court as cruel and unusual, the Court has rejected the challenge.
T15	ArgumentFor 2689 2791	Our society has nonetheless steadily moved to more humane methods of carrying out capital punishment."
T16	Sentence 2792 2885;2886 2899;2900 3010	Baze v. Rees(529 KB) US Supreme Court, in a decision written by Chief Justice John G. Roberts Apr. 16, 2008 CON: "Death is... an unusually severe punishment, unusual in its pain, in its finality, and in its enormity...
T17	Sentence 3011 3174	The fatal constitutional infirmity in the punishment of death is that it treats 'members of the human race as nonhumans, as objects to be toyed with and discarded.
T18	Sentence 3175 3398	[It is] thus inconsistent with the fundamental premise of the Clause that even the vilest criminal remains a human being possessed of common human dignity.' [quoting himself from Furman v. Georgia, 408 U.S. 238, 257 (1972)]
T19	Sentence 3399 3608	As such it is a penalty that 'subjects the individual to a fate forbidden by the principle of civilized treatment guaranteed by the [Clause].' [quoting C.J. Warren from Trop v. Dulles, 356 U.S. 86, 101 (1958)]
T20	Sentence 3609 3733	I therefore would hold, on that ground alone, that death is today a cruel and unusual punishment prohibited by the Clause...
T21	Sentence 3734 3837	I would set aside the death sentences imposed... as violative of the Eighth and Fourteenth Amendments."
T22	Sentence 3838 3942	PRO: "Common sense, lately bolstered by statistics, tells us that the death penalty will deter murder...
T23	Sentence 3943 3979	People fear nothing more than death.
T24	ArgumentFor 3980 4082	Therefore, nothing will deter a criminal more than the fear of death... life in prison is less feared.
T25	Sentence 4083 4210	Murderers clearly prefer it to execution -- otherwise, they would not try to be sentenced to life in prison instead of death...
T26	Sentence 4211 4283	Therefore, a life sentence must be less deterrent than a death sentence.
T27	ArgumentFor 4284 4406	And we must execute murderers as long as it is merely possible that their execution protects citizens from future murder."
T28	Sentence 4407 4431;4432 4485;4486 4525;4526 4539;4540 4663	Ernest Van Den Haag, PhD Late Professor of Jurisprudence at Fordham University "For the Death Penalty," New York Times Oct. 17, 1983 CON: "[T]here is no credible evidence that the death penalty deters crime more effectively than long terms of imprisonment.
T29	ArgumentAgainst 4664 4776	States that have death penalty laws do not have lower crime rates or murder rates than states without such laws.
T30	ArgumentAgainst 4777 4887	And states that have abolished capital punishment show no significant changes in either crime or murder rates.
T31	ArgumentAgainst 4887 4929	The death penalty has no deterrent effect.
T32	ArgumentAgainst 4930 5053	Claims that each execution deters a certain number of murders have been thoroughly discredited by social science research."
T33	ArgumentFor 5054 5131	PRO: "Society is justly ordered when each person receives what is due to him.
T34	ArgumentFor 5132 5293	Crime disturbs this just order, for the criminal takes from people their lives, peace, liberties, and worldly goods in order to give himself undeserved benefits.
T35	ArgumentFor 5294 5437	Deserved punishment protects society morally by restoring this just order, making the wrongdoer pay a price equivalent to the harm he has done.
T36	ArgumentFor 5438 5530	This is retribution, not to be confused with revenge, which is guided by a different motive.
T37	Sentence 5531 5636	In retribution the spur is the virtue of indignation, which answers injury with injury for public good...
T38	Sentence 5637 5701	Retribution is the primary purpose of just punishment as such...
T39	Sentence 5702 5800	[R]ehabilitation, protection, and deterrence have a lesser status in punishment than retribution."
T40	Sentence 5801 5975;5976 6186	J. Budziszewski, PhD Professor of Government and Philosophy at the University of Texas at Austin "Capital Punishment: The Case for Justice," OrthodoxyToday.org Aug./Sep. 2004 CON: "Retribution is just another word for revenge, and the desire for revenge is one of the lowest human emotions — perhaps sometimes understandable, but not really a rational response to a critical situation.
T41	ArgumentAgainst 6187 6348	To kill the person who has killed someone close to you is simply to continue the cycle of violence which ultimately destroys the avenger as well as the offender.
T42	Sentence 6349 6415	That this execution somehow give 'closure' to a tragedy is a myth.
T43	Sentence 6416 6485	Expressing one’s violence simply reinforces the desire to express it.
T44	Sentence 6486 6538	Just as expressing anger simply makes us more angry.
T45	Sentence 6539 6562	It does not drain away.
T46	ArgumentAgainst 6563 6670	It contaminates the otherwise good will which any human being needs to progress in love and understanding."
T47	Sentence 6671 6693;6694 6772;6773 6792;6793 6805;6806 6892	Raymond A. Schroth, SJ Jesuit Priest and Community Professor of the Humanities at St. Peter's College Email to ProCon.org Sep. 5, 2008 PRO: "...No system of justice can produce results which are 100% certain all the time.
T48	Sentence 6893 6973	Mistakes will be made in any system which relies upon human testimony for proof.
T49	Sentence 6974 7031	We should be vigilant to uncover and avoid such mistakes.
T50	Sentence 7032 7115	Our system of justice rightfully demands a higher standard for death penalty cases.
T51	Sentence 7116 7381	However, the risk of making a mistake with the extraordinary due process applied in death penalty cases is very small, and there is no credible evidence to show that any innocent persons have been executed at least since the death penalty was reactivated in 1976...
T52	Sentence 7382 7554	The inevitability of a mistake should not serve as grounds to eliminate the death penalty any more than the risk of having a fatal wreck should make automobiles illegal..."
T53	Sentence 7555 7576;7577 7622;7623 7678;7679 7691;7692 7835	Steven D. Stewart, JD Prosecuting Attorney for Clark County Indiana Message on the Clark County Prosecutor website accessed Aug. 6, 2008 CON: "...Since the reinstatement of the modern death penalty, 87 people have been freed from death row because they were later proven innocent.
T54	ArgumentAgainst 7836 7920	That is a demonstrated error rate of 1 innocent person for every 7 persons executed.
T55	Sentence 7921 8056	When the consequences are life and death, we need to demand the same standard for our system of justice as we would for our airlines...
T56	Sentence 8057 8200	It is a central pillar of our criminal justice system that it is better that many guilty people go free than that one innocent should suffer...
T57	Sentence 8201 8249	Let us reflect to ensure that we are being just.
T58	ArgumentAgainst 8250 8317	Let us pause to be certain we do not kill a single innocent person.
T59	Sentence 8318 8378	This is really not too much to ask for a civilized society."
T60	Sentence 8379 8396;8397 8414;8415 8478;8479 8493;8494 8703	Russ Feingold, JD US Senator (D-WI) introducing the "National Death Penalty Moratorium Act of 2000" April 26, 2000 PRO: "Many opponents present, as fact, that the cost of the death penalty is so expensive (at least $2 million per case?), that we must choose life without parole ('LWOP') at a cost of $1 million for 50 years.
T61	Sentence 8704 8760	Predictably, these pronouncements may be entirely false.
T62	Sentence 8761 8886	JFA [Justice for All] estimates that LWOP cases will cost $1.2 million-$3.6 million more than equivalent death penalty cases.
T63	Sentence 8887 9009	There is no question that the up front costs of the death penalty are significantly higher than for equivalent LWOP cases.
T64	Sentence 9010 9138	There also appears to be no question that, over time, equivalent LWOP cases are much more expensive... than death penalty cases.
T65	Sentence 9139 9235	Opponents ludicrously claim that the death penalty costs, over time, 3-10 times more than LWOP."
T66	Sentence 9236 9384;9385 9528	Dudley Sharp Director of Death Penalty Resources at Justice for All "Death Penalty and Sentencing Information," Justice for All website Oct. 1, 1997 CON: "In the course of my work, I believe I have reviewed every state and federal study of the costs of the death penalty in the past 25 years.
T67	Sentence 9529 9575	One element is common to all of these studies:
T68	Sentence 9576 9686	They all concluded that the cost of the death penalty amounts to a net expense to the state and the taxpayers.
T69	ArgumentAgainst 9687 9818	Or to put it differently,the death penalty is clearly more expensive than a system handling similar cases with a lesser punishment.
T70	Sentence 9819 9957	[It] combines the costliest parts of both punishments: lengthy and complicated death penalty trials, followed by incarceration for life...
T71	Sentence 9958 10055;10056 10079	Everything that is needed for an ordinary trial is needed for a death penalty case, only more so: • More pre-trial time..
T72	Sentence 10081 10097	• More experts..
T73	Sentence 10099 10126	• Twice as many attorneys..
T74	Sentence 10128 10211	• Two trials instead of one will be conducted: one for guilt and one for punishment
T75	Sentence 10213 10323	• And then will come a series of appeals during which the inmates are held in the high security of death row."
T76	Sentence 10324 10408;10409 10568;10569 10573	Richard C. Dieter, MS, JD Executive Director of the Death Penalty Information Center Testimony to the Judiciary Committee of the Colorado State House of Representatives regarding "House Bill 1094 - Costs of the Death Penalty and Related Issues" Feb.
T77	Sentence 10574 10581;10582 10817	7, 2007 PRO: "[T]he fact that blacks and Hispanics are charged with capital crimes out of proportion to their numbers in the general population may simply mean that blacks and Hispanics commit capital crimes out of proportion to their numbers.
T78	Sentence 10818 10862	Capital criminals don’t look like America...
T79	Sentence 10863 10925	No one is surprised to find more men than women in this class.
T80	Sentence 10926 11020	Nor is it a shock to find that this group contains more twenty-year-olds than septuagenarians.
T81	Sentence 11021 11278	And if — as the left tirelessly maintains — poverty breeds crime, and if — as it tiresomely maintains — the poor are disproportionately minority, then it must follow — as the left entirely denies — that minorities will be 'overrepresented' among criminals."
T82	Sentence 11279 11294;11295 11346;11347 11429;11430 11443;11444 11625	Roger Clegg, JD General Counsel at the Center for Equal Opportunity "The Color of Death: Does the Death Penalty Discriminate?,” National Review Online June 11, 2001 CON: "Despite the fact that African Americans make up only 13 percent of the nation’s population, almost 50 percent of those currently on the federal death row are African American.
T83	ArgumentAgainst 11626 11770	And even though only three people have been executed under the federal death penalty in the modern era, two of them have been racial minorities.
T84	ArgumentAgainst 11771 11847	Furthermore, all six of the next scheduled executions are African Americans.
T85	ArgumentAgainst 11848 12144	The U.S. Department of Justice’s own figures reveal that between 2001 and 2006, 48 percent of defendants in federal cases in which the death penalty was sought were African Americans… the biggest argument against the death penalty is that it is handed out in a biased, racially disparate manner."
T86	Sentence 12145 12329	PRO: "The next urban legend is that of the threadbare but plucky public defender fighting against all odds against a team of sleek, heavily-funded prosecutors with limitless resources.
T87	Sentence 12330 12537	The reality in the 21st century is startlingly different... the past few decades have seen the establishment of public defender systems that in many cases rival some of the best lawyers retained privately...
T88	Sentence 12538 12750	Many giant silk-stocking law firms in large cities across America not only provide pro-bono counsel in capital cases, but also offer partnerships to lawyers whose sole job is to promote indigent capital defense."
T89	Sentence 12751 12892;12893 12940	Joshua Marquis, JD District Attorney of Clatsop County, Oregon "The Myth of Innocence,” Journal of Criminal Law and Criminology Mar. 31, 2005 CON: "Who pays the ultimate penalty for crimes?
T90	Sentence 12951 12978	Who gets the death penalty?
T91	ArgumentAgainst 12989 13145	After all the rhetoric that goes on in legislative assemblies, in the end, when the net is cast out, it is the poor who are selected to die in this country.
T92	ArgumentAgainst 13146 13191	And why do poor people get the death penalty?
T93	Sentence 13192 13250	It has everything to do with the kind of defense they get.
T94	ArgumentAgainst 13251 13279	Money gets you good defense.
T95	Sentence 13280 13337	That's why you'll never see an O.J. Simpson on death row.
T96	ArgumentAgainst 13338 13430	As the saying goes: 'Capital punishment means them without the capital get the punishment.'"
T97	Sentence 13431 13448;13449 13507;13508 13557;13558 13562;13563 13683	Helen Prejean, MA Anti-death penalty activist and author of Dead Man Walking "Would Jesus Pull the Switch?,” Salt of the Earth 1997 PRO: "Defense attorneys... routinely file all manner of motions and objections to protect their clients from conviction.
T98	Sentence 13684 13860	Attorneys know their trial tactics will be thoroughly scrutinized on appeal, so every effort is made to avoid error, ensuring yet another level of protection for the defendant.
T99	Sentence 13861 14012	They [death penalty opponents]... have painted a picture of incompetent defense lawyers, sleeping throughout the trial, or innocent men being executed.
T100	Sentence 14013 14117	Their accusations receive wide media coverage, resulting in a near-daily onslaught on the death penalty.
T101	Sentence 14118 14227	Yet, through all the hysteria, jurors continue to perform their responsibilities and return death sentences."
T102	Sentence 14228 14491	CON: "[A] shocking two out of three death penalty convictions have been overturned on appeal because of police and prosecutorial misconduct, as well as serious errors by incompetent court-appointed defense attorneys with little experience in trying capital cases.
T103	ArgumentAgainst 14492 14657	How can we contend that we provide equal justice under the law when we do not provide adequate representation to the poor in cases where a life hangs in the balance?
T104	Sentence 14658 14744	We, the Congress, must bear our share of responsibility for this deplorable situation.
T105	Sentence 14745 14888	In short, while others, like Governor Ryan in Illinois, have recognized the flaws in the death penalty, the Congress still just doesn't get it.
T106	Sentence 14889 14912	This system is broken."
T107	Sentence 14913 14934;14935 14956;14957 15104;15105 15118;15119 15251	John Conyers, Jr., JD US Congressman (D-MI) Hearing for the Innocence Protection Act of 2000 before the Subcommittee on Crime of the Committee on the Judiciary of the House of Representatives June 20, 2000 PRO: "Accepting capital punishment in principle means accepting it in practice, whether by the hand of a physician or anyone else...
T108	Sentence 15252 15363	If one finds the practice too brutal, one must either reject it in principle or seek to mitigate its brutality.
T109	Sentence 15364 15704	If one chooses the latter option, then the participation of physicians seems more humane than delegating the deed to prison wardens, for by condoning the participation of untrained people who could inflict needless suffering that we physicians might have prevented, we are just as responsible as if we had inflicted the suffering ourselves.
T110	Sentence 15705 15863	The AMA [American Medical Association] position should be changed either to permit physician participation or to advocate the abolition of capital punishment.
T111	Sentence 15864 15982	The hypocritical attitude of 'My hands are clean — let the spectacle proceed' only leads to needless human suffering."
T112	Sentence 15983 16007;16008 16072;16073 16186	Bruce E. Ellerin, MD, JD Doctor of Oncology Radiation at Sierra Providence Health Network Response letter to the New England Journal of Medicine regarding an article titled "When Law and Ethics Collide —
T113	Sentence 16187 16249;16250 16262;16263 16481	Why Physicians Participate in Executions," by Atul Gawande, MD July 6, 2006 CON: "The American Medical Association's policy is clear and unambiguous... requiring physicians to participate in executions violates their oath to protect lives and erodes public confidence in the medical profession.
T114	Sentence 16482 16553	A physician is a member of a profession dedicated to preserving life...
T115	Sentence 16554 16746	The use of a physician's clinical skill and judgment for purposes other than promoting an individual's health and welfare undermines a basic ethical foundation of medicine — first, do no harm.
T116	Sentence 16747 16869	The guidelines in the AMA Code of Medical Ethics address physician participation in executions involving lethal injection.
T117	ArgumentAgainst 16870 17135	The ethical opinion explicitly prohibits selecting injection sites for executions by lethal injection, starting intravenous lines, prescribing, administering, or supervising the use of lethal drugs, monitoring vital signs, on site or remotely, and declaring death."
