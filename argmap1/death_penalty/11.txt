Arguments for and against capital punishment in the
Contents. If you are looking for information on the
situation in the
Is capital
punishment ethically acceptable?
Death versus deterrence
Background.
Capital punishment is the lawful infliction of death as a punishment and since ancient times it has been used for a wide variety of offences. The Bible prescribes death for murder and many other crimes including kidnapping and witchcraft. By 1500 in
Reform of the death penalty began in Europe by the 1750’s and was championed by academics such as the Italian jurist, Cesare Beccaria, the French philosopher, Voltaire, and the English law reformers, Jeremy Bentham and Samuel Romilly. They argued that the death penalty was needlessly cruel, over-rated as a deterrent and occasionally imposed in fatal error. Along with Quaker leaders and other social reformers, they defended life imprisonment as a more rational alternative.
By the
1850’s, these reform efforts began to bear fruit.
The USA, together with China, Japan and many Asian and Middle Eastern countries, plus some African states still retain the death penalty for certain crimes and impose it with varying frequency. Click here for a detailed list of abolitionist and retentionist countries.
Is capital punishment ethically acceptable?
The state clearly has no absolute right to put its subjects to death although, of course, almost all countries do so in some form or other (but not necessarily by some conventional form of capital punishment). In most countries, it is by arming their police forces and accepting the fact that people will from time to time be shot as a result and therefore at the state's behest.
A
majority of a state's subjects may wish to confer the right to put certain
classes of criminal to death through referendum or voting in state elections
for candidates favouring capital punishment. Majority opinion in some
democratic countries, including the
It is reasonable to assume that if a majority is in favour of a particular thing in a democracy, their wishes should be seriously considered with equal consideration given to the downside of their views.
A fact that is conveniently overlooked by anti-capital punishment campaigners is that we are all ultimately going to die. In many cases, we will know of this in advance and suffer great pain and emotional anguish in the process. This is particularly true of those diagnosed as having terminal cancer. It is apparently acceptable to be "sentenced to death" by one's family doctor without having committed any crime at all but totally unacceptable to be sentenced to death by a judge having been convicted of murder or drug trafficking (the crimes for which the majority of executions worldwide are carried out) after a fair and careful trial.
Let us examine the merits of both the pro and anti arguments.
Arguments for the death penalty.
Incapacitation of the criminal.
Capital punishment permanently removes the worst criminals from society and should prove much safer for the rest of us than long term or permanent incarceration. It is self evident that dead criminals cannot commit any further crimes, either within prison or after escaping or after being released from it.
Cost.
Money is not an inexhaustible commodity and the government may very well better spend our (limited) resources on the old, the young and the sick etc., rather than on the long term imprisonment of murderers, rapists, etc.
Anti-capital punishment campaigners in the
Retribution.
Execution is a very real punishment rather than some form of "rehabilitative" treatment, the criminal is made to suffer in proportion to the offence. Although whether there is a place in a modern society for the old fashioned principal of "lex talens" (an eye for an eye), is a matter of personal opinion. Retribution is seen by many as an acceptable reason for the death penalty according to my survey results.
Deterrence.
Does the death penalty deter? It is hard to prove one way or the other because in most retentionist countries the number of people actually executed per year (as compared to those sentenced to death) is usually a very small proportion. It would, however, seem that in those countries (e.g.
Anti-death penalty campaigners always argue that death is not a deterrent and usually site studies based upon American states to prove their point. This is, in my view, flawed and probably chosen to be deliberately misleading. Let us examine the situation in three countries.
The rates for unlawful killings in
A total of 29 people released after being convicted of murder and six people convicted of manslaughter, killed again between 2000/1 and 2010/11, according to figures released by the Home Office. 13 of these committed murder, the most serious homicide offence, and 16 committed manslaughter.
Some 6,300 people are currently serving sentences of “life in prison” for murder. Figures released in 2009 show that since 1997, 65 prisoners who were released after serving life were convicted of a further crime. These included two murders, one suspected murder, one attempted murder, three rapes and two instances of grievous bodily harm. The same document also noted that 304 people given life sentences since January 1997 served less than 10 years of them, actually in prison.
Statistics were kept for the first five years that capital punishment was suspended in
In most states, other than
Interestingly, the murder rate in the
It is dangerously simplistic to say that the rise in executions is the only factor in the reduction of homicides in the
As stated above,
In 1980 alone, 2,392 people died by homicide, giving it a murder rate of 16.88 for every 100,000 of the population. (The
In 2000,
Singapore always carries out death sentences where the appeal has been turned down, so its population knows precisely what will happen to them if they are convicted of murder or drug trafficking - is this concept deeply embedded into the sub-consciousness of most of its people, acting as an effective deterrent?
In 1995, Singapore hanged an unusually large number of 7 murderers with 4 in 1996, 3 in 1997 and only one in 1998 rising to 6 in 1999 (3 for the same murder). Singapore takes an equally hard line on all other forms of crime with stiff on the spot fines for trivial offences such as dropping litter and chewing gum in the street, caning for males between 18 and 50 for a wide variety of offences, and rigorous imprisonment for all serious crimes.
Arguments against the death penalty.
There are a number of incontrovertible arguments against the death penalty.
The most important one is the virtual certainty that genuinely innocent people will be executed and that there is no possible way of compensating them for this miscarriage of justice. There is also another significant but much less realised danger here. The person convicted of the murder may have actually killed the victim and may even admit having done so but does not agree that the killing was murder. Often the only people who know what really happened are the accused and the deceased. It then comes down to the skill of the prosecution and defence lawyers as to whether there will be a conviction for murder or for manslaughter. It is thus highly probable that people are convicted of murder when they should really have only been convicted of manslaughter. Have a look at the cases of James McNicol and Edith Thompson and see what you think.
A second reason, that is often overlooked, is the hell the innocent family and friends of criminals must also go through in the time leading up to and during the execution. It is often very difficult for people to come to terms with the fact that their loved one could be guilty of a serious crime and no doubt even more difficult to come to terms with their death in this form. One cannot and should not deny the suffering of the victim's family in a murder case but the suffering of the murderer's family is surely valid too.
There
must always be the concern that the state can administer the death penalty
justly, most countries have a very poor record on this. In
It must be remembered that criminals are real people too who have life and with it the capacity to feel pain, fear and the loss of their loved ones, and all the other emotions that the rest of us are capable of feeling. It is easier to put this thought on one side when discussing the most awful multiple murderers but less so when discussing, say, an 18 year old girl convicted of drug trafficking. (Singapore hanged two girls for this crime in 1995 who were both only 18 at the time of their offences and China shot an 18 year old girl for the same offence in 1998.)
There is no such thing as a humane method of putting a person to death irrespective of what the state may claim (see later). Every form of execution causes the prisoner suffering, some methods perhaps cause less than others, but be in no doubt that being executed is a terrifying ordeal for the criminal. What is also often overlooked is the mental suffering that the criminal suffers in the time leading up to the execution. How would you feel knowing that you were going to die tomorrow morning at ?
There may be a brutalising effect upon society by carrying out executions - this was apparent in this country during the 17th and 18th centuries when people turned out to enjoy the spectacle of public hanging. They still do today in those countries where executions are carried out in public. It is hard to prove this one way or the other - people stop and look at car crashes but it doesn't make them go and have an accident to see what it is like. It would seem that there is a natural voyeurism in most people.
The death penalty is the bluntest of "blunt instruments," it removes the individual's humanity and with it any chance of rehabilitation and their giving something back to society. In the case of the worst criminals, this may be acceptable but is more questionable in the case of less awful crimes.
Will
Support for the death penalty in
Successive free votes on the issue in the House of Commons during the 1980’s failed to get anywhere near a majority for restoration. Politically it would be impossible now, given our membership of the EU and our commitment to the European Convention on Human Rights, both of which totally prohibit capital punishment. The EU contains no member states that practice it and will not allow retentionist states to join. The present Labour government is implacably opposed to capital punishment and has removed it from the statute book for the few remaining offences for which it was still theoretically allowed. The Conservatives and Liberal Democrats are also against reintroduction. There is no doubt that capital punishment is a very emotive issue and there is a strong anti-death penalty lobby in this country who would put every obstacle in the way of its return should it ever become likely.
There is concern at the number of convictions that are being declared unsafe by the Courts, particularly for the most serious offences such as murder and terrorism.
Yet we live in a time of ever rising serious crime despite what the government tells us.
Will people become so fed up with escalating levels of crime and what they see in, most cases, as derisory punishments that they will support anything that appears likely to reduce crime and redress the balance? Or do they see the return of capital punishment as a return to barbarity?
Should capital punishment be re-introduced in
There are very real issues of human rights that will effect us all if it were to be reintroduced.
Will the government introduce laws that are just and contain sufficient safeguards and will the judiciary administer them properly?
We are all potentially capable of murder (a lot of domestic murders, where one partner murders the other during a row, are first time crimes) and, therefore, we must each consider whether we and our loved ones are more at risk of being murdered or being executed for committing murder.
We must also consider what the likelihood is of innocent people being executed - it is inevitable that it will happen sooner or later.
Can the police, the courts, and the system generally be trusted to get things right on every occasion? They never have been able to previously.
Will juries be willing to convict in capital cases? Would you like to have to make the decision as to whether the person in the dock should live or die?
Will the government really be willing to carry out death sentences or will they find every excuse for not doing so, thus returning to the injustices of earlier centuries?
Will executions really prove to be the deterrent that some supporters of capital punishment expect them to be? It is unlikely the very worst murderers would be deterred because they are typically psychopaths or of such dubious sanity that they are incapable of rational behaviour (sometimes taking their own lives immediately after the crime, as in the Hungerford and Dunblane massacres) Certain criminals, e.g. drug traffickers, may be deterred because they have a clear option with defined risks but would the person who has a violent argument with their partner give a second thought to what will happen to them when in the heat of the moment they pick up the carving knife?
It is unlikely that a handful of executions a year will have any real deterrent effect particularly on the people whom society would most like to be deterred, e.g. serial killers, multiple rapists and drugs barons. Yet these particular criminals are the least likely to be executed, the serial killers will be found insane and the drug barons will use any means to avoid conviction, e.g. intimidation of witnesses. So we go back to the situation where only "sane" murderers can be executed. Thus a modern day Ruth Ellis might also hang because she was sane, whilst Beverley Allitt, who murdered 4 small children, would be reprieved because she has Munchausen's Syndrome by Proxy or so she and certain psychiatrists claim.
Can
these scenarios ever be seen as justice?
Should we only execute people for the most awful multiple murders as a form of compulsory euthanasia rather than as a punishment or should we execute all murderers irrespective of the degree of guilt purely as a retributive punishment for taking another person's life and in the hope of deterring others?
What about crimes such as violent rape, terrorism and drug trafficking - are these as bad as murder? How should we punish such offences?
Should executions be carried out in such a way as to punish the criminal and have maximum deterrent effect on the rest of us, (e.g. televised hangings). Would this be a deterrent or merely become a morbid show for the voyeuristic?
Or should they be little more than a form of euthanasia carried out in such a way as to remove from the criminal all physical and as much emotional suffering as possible?
Does it make any sense to imprison someone for the rest of their life or is it really more cruel than executing them, particularly if they are young?
If we do not keep them in prison for life, will they be released to commit other dreadful crimes? A small but significant number do.
What is the cost to society of keeping people in prison? (£700.00 per week at present for an ordinary prisoner which is around £550,000 for a typical life sentence for murder with a minimum tariff of 15 years).
These questions need to be thought about carefully and a balanced opinion arrived at. How do you feel about them? If you wish to share your thoughts with me send me an email (Please include your name and age)
If the general conclusion is that capital punishment is desirable, then the first step toward restoration is for the government to present a fully thought out set of proposals that can be put to the people in a referendum stating precisely what offences should carry the death penalty, how it should be carried out, etc., and what effect on crime is expected to follow from reintroduction.
If such a referendum produced a clear yes vote, the government would have a genuine mandate to proceed upon and could claim the support of the people, thus substantially reducing the influence of the anti-capital punishment lobby. There should be another referendum about 5 years later so that the effects of reintroduction could be reviewed and voted on again. Referenda have the advantage of involving the public in the decision making process and raising awareness through the media of the issues for and against the proposed changes.
The alternatives.
What are the realistic alternatives to the death penalty?
Any
punishment must be fair, just, adequate and most of all, enforceable. Society
still views murder as a particularly heinous crime which should be met with the
most severe punishment. Whole life imprisonment could fit the bill for the
worst murders with suitable gradations for less awful murders. Some 44 people are currently serving whole
life tariffs in the
I am personally against the mandatory life sentence for murder as it fails, in my view, to distinguish between really dreadful crimes and those crimes which, whilst still homicide, are much more understandable to the rest of us. Therefore, it is clearly necessary to give juries the option of finding the prisoner guilty but in a lower degree of murder, and to give judges the ability to pass sensible, determinate sentences based upon the facts of the crime as presented to the court.
Imprisonment, whilst expensive and largely pointless, except as means of removing criminals from society for a given period, is at least enforceable upon anyone who commits murder (over the age of 10 years). However, it appears to many people to be a soft option and this perception needs to be corrected.
In modern times, we repeatedly see murderers being able to "get off" on the grounds of diminished responsibility and their alleged psychiatric disorders or by using devices such as plea bargaining. This tends to remove peoples' faith in justice which is very dangerous.
Are there
any other real, socially acceptable, options for dealing with murderers? One
possible solution (that would enrage the civil liberties groups) would be to
have everyone's
"Life without parole" versus the death penalty.
Many opponents of capital punishment put forward life in prison without parole as a viable alternative to execution for the worst offenders, and surveys in
However, there are drawbacks to this:
It is
argued by some that LWOP is in fact a far more cruel punishment that
death. This proposition was put forward
in a
Death clearly permanently incapacitates the criminal and prevents them committing any other offence. LWOP cannot prevent or deter offenders from killing prison staff or other inmates or taking hostages to further an escape bid - they have nothing further to lose by doing so and there are instances of it happening in the USA.
However good the security of a prison, someone will always try to escape and occasionally will be successful. If you have endless time to plan an escape and everything to gain from doing so, it is a very strong incentive.
We have
no guarantee that future governments will not release offenders, who were
imprisoned years previously, on the recommendations of various professional
"do-gooders" who are against any punishment in the first place.
Twenty or thirty years on it is very difficult to remember the awfulness of an
individual's crime and easy to claim that they have reformed.
Myra Hindley is a prime example of this phenomenon - whilst I am willing to believe that she changed as a person during her 37 years in prison and probably did not present any serious risk of re-offending, one has absolutely no guarantee of this and it does not obviate her responsibility for her crimes. Fortunately, she died of natural causes before she could obtain the parole which I am sure she would have eventually been granted.
The Numbers Game "death versus deterrence".
If we are, however, really serious in our desire to reduce crime through harsher punishments alone, we must be prepared to execute every criminal who commits a capital crime irrespective of their sex, age (above the legal minimum) alleged mental state or background. Defences to capital charges must be limited by statute to those which are reasonable. Appeals must be similarly limited and there can be no reprieves. We must carry out executions without delay and with sufficient publicity to get the message across to other similarly minded people. This is similar to the situation which obtains in China and would, if applied in Britain, undoubtedly lead to a large number of executions to begin with until the message got through. I would estimate at least 2,000 or so in the first year if it were applied for murder, aggravated rape and drug trafficking. This amounts to more than 7 executions every day of the year Monday through Friday.
Are we,
as a modern western society, willing to do this or would we shy away from it
and return to just carrying out the occasional execution to show that we still
can without any regard for natural justice? These events will be seized upon by
the media and turned into a morbid soap opera enjoyed by a (large?) proportion
of the population. (Note the popularity in the American media of capital murder
trials there.) It is doubtful whether executions carried out on this basis will
deter others from committing crimes.
For capital punishment to really reduce crime, everyone of us must realise that we will personally and without doubt be put to death if we commit particular crimes and that there can be absolutely no hope of reprieve.
One wonders if as many people would be willing to vote for this scenario in a referendum when they realised the full consequences of their action.
I have no doubt that if we were to declare war on criminals in this fashion, we would see a rapid decline in serious crime but at what cost in human terms? There will be a lot of innocent victims - principally the families of those executed.
"Mad or Bad".
Are criminals (particularly murderers as we are discussing capital punishment) evil or sick? This is another very important issue as it would seem hardly reasonable to punish people who are genuinely mentally ill but more reasonable to use effective punishment against those who are intentionally evil. As usual, as a society, we have very confused views on this issue - there are those, notably some social workers and psychiatrists, who seem to believe that there is no such thing as evil whilst the majority of us do not accept that every accused person should be let off, (i.e. excused any responsibility for their actions) due to some alleged mental or emotional condition. Will advances in mapping the human genome over the next couple of decades allow us to predict those people who are prone to committing violent and murderous crimes and so prevent them before they happen?
It would seem that whilst legally and technically "sane" many criminals are in some way abnormal and their thought processes are not like those of the rest of us. Ruth Ellis was, in my view, a perfect case in point. She lived at a time when the death penalty was mandatory for murder and was known to be in favour of it herself. She had two small children and yet neither factor stopped her committing a murder which she made no attempt to escape from or deny responsibility for, and for which she knew that she would probably be hanged. We can only conjecture why she did murder David Blakely, the man she loved at all, and particularly in the way she did which was much more likely to result in her execution. Home Office psychiatrists who examined her in the condemned cell found her to be sane according to their definition, and I have no doubt that we would also have considered her to be sane if we had interviewed her - but she was obviously not "normal." For a detailed account of her case and subsequent appeal Click here.
In America the judicial system seems, on the whole, less concerned about the mental state of condemned prisoners and are willing to execute them. Child killer, Westley Alan Dodd, is a case in point – reading his diary it is clear that he was very abnormal. There are many other cases to choose from where the defendant's deeds are not those of a normal person. The typical psychopath is often a person of above average intelligence but is presently incurable and will continue to present a severe risk to society.
Will we ever find an answer to the "mad or bad" question and be able to find effective treatment for those who turn out to be "mad?" Should we worry about the alleged mental state of our worst criminals? These are the people who are least likely to benefit from imprisonment or care in institutions (or worse still the community) and are most likely to re-offend. It could, therefore, be argued that killing these people would be a very good thing.
Capital punishment and the media.
Three hundred years ago there was no media. Newspapers first started in England around 1725 and were expensive and of very limited circulation. In any case few people could read at that time. So public executions were vital to show that justice had been done and provide a deterrent to others. In particularly heinous cases of murder the execution could be carried out near the scene of the crime so that the local people could see the murderer punished, or the criminal could be gibbeted near the scene to remind people of the punishment. By 1800 newspapers were more widespread and public execution was abolished in England, Scotland and Wales in 1868. Reporters were still allowed to witness some executions for some years afterwards, but by the 20th century, typically newspapers would merely state that so and so was executed yesterday for the murder of … at such and such prison. No details of the execution were made available and so the story would be two paragraphs unless there was some special feature such as a protest outside the prison. Radio and later television news would also carry a similar brief report.
In the USA reporters are always permitted to attend
executions and they receive a lot of coverage at state level. However the media's
attitude to executions varies widely depending on the age and sex of the
criminal, the type of crime and method of execution.
Middle aged men being executed by lethal injection in say Texas for "ordinary" murders hardly rate a paragraph in the press of other states, nowadays and do not get a mention in the U.K. media at all. But, a woman convicted of double murder and being injected on the same gurney gets tremendous worldwide media attention at all levels (Karla Faye Tucker). Equally, a man being hanged in Washington or Delaware or shot by a Utah firing squad makes international news (Westley Allan Dodd, Billy Bailey and John Taylor). And yet (non white) women being hanged in Jordan and Singapore, the large number of people publicly beheaded in Saudi Arabia and men and women executed by the hundred in China make very little news. However, when a white woman is hanged in Africa, (Mariette Bosch in
Why is this? Is it a form of racism or do we not care if the execution takes place in a Middle Eastern or Far Eastern Country? Are their criminals somehow perceived as lesser people with less rights? The media obviously does not judge many of these stories to be newsworthy although they are aware of them through the news wires from those countries (which is how I know about them).
During
the late 70's and early 80's when executions were rare in America, every
execution by whatever means, attracted a great deal of media interest and yet
now they are more frequent (normally averaging over one per week), the
authorities sometimes have difficulty in finding sufficient official witnesses.
They also used to attract pro and anti-capital punishment protesters in large
numbers, but these seem to have dwindled down to just a few in most cases.
I tend to think that if executions were televised, they would soon reach the same level of dis-interest amongst the general public unless it fitted into a "special category," i.e. a first by this or that method or a particularly interesting criminal.
In Kuwait criminals have been hanged in the yard of Nayef Palace in recent years and once the prisoners were suspended the press and the public were allowed in to view the hanging bodies. Photography was also allowed and photographs of the executions appeared in the Kuwaiti media. One wonders what the deterrent effect of this. Have a look at the article on Kuwait to learn more.
Is media
coverage of executions just a morbid sideshow for some people, who deprived of
public hangings, etc., lap up every detail the media has to offer whilst the
majority ignore the not very interesting criminals who are executed by lethal
injection?
Lethal injection, as my own survey has shown, is perceived by most respondents as the least cruel method - probably because it is the least gruesome method. The less the public interest, the easier the process becomes - a state of affairs that suits governments of many countries and states in America very well.
Probably the majority of people don't much care either way and would rather watch football! They may vaguely support capital punishment but do not wish to be or feel involved.
The Future.
I wonder if in another hundred years we will, as a world still have capital punishment at all or for that matter prisons, or whether we will have evolved technological means of detecting and correcting potential criminals before they can actually commit any crime. It seems to me that we must first find this technology and then educate public opinion away from its present obsession with punishment by demonstrating that the new methods work, pointing out the futility and waste of present penal methods, especially imprisonment and execution.
Punishment will remain popular with the general public (and therefore politicians) as long as there are no viable alternatives and as long as crime continues its present inexorable rise. Logically, however, punishment (of any sort) cannot be the future - we must progress and therefore we will.
Until this utopian point is reached, which I believe it ultimately will be, I think that we will see the use of the death penalty continuing and its reintroduction in countries that had previously abolished it. Most of the Caribbean countries are trying to get it re-introduced.
It is clear that in strict penal societies such as Singapore, that the crime rate is much lower than in effectively non-penal societies such as Britain. It is, therefore, logical to assume that Singaporean style policies are likely to be adopted by more countries as their crime rates reach unacceptable proportions.
I do not believe that the majority of people who support capital punishment or other severe punishments, do so for sadistic reasons but rather out of a feeling of desperation that they and their families are being overwhelmed by the rising tide of crime which they perceive the government is doing too little to protect them from. I think there would, in the long term, be sufficient support for non-penal methods of dealing with criminals if these were proved to be effective.
A
particular danger in our society is that we continue to do little or nothing
effective about persistent juvenile offenders. If the death penalty were re
introduced, we may be consigning many of these to their death at the age of 18,
having never previously given them any discipline whatsoever. Surely execution
should not be both the first and last taste of discipline a person gets and yet
as we allow so many youngsters to run wild and commit ever more serious crimes
unpunished, public opinion and thus political expediency makes it more and more
likely. Nicholas
Ingram, who went to the electric chair in the American state of
We should start by introducing stricter discipline from "the bottom up," i.e. start with unruly children at school and on the streets and progress through young thugs and older thugs before we think about restoring capital punishment. This way, we might bring up a generation or two of disciplined people who might not need the threat of execution to deter them from committing the most serious crimes.
It is noticeable that whilst Singapore retains and uses the death penalty, it also has severe punishments for all other offences, including caning for many offences committed by young men who are usually the most crime prone group. Thus, Singapore provides discipline at all levels in its society and has the sort of crime figures that most countries can only dream of.
Pain and suffering – is the
death penalty a cruel and unusual punishment?
The Eighth Amendment to the American Constitution prohibits the imposition of "cruel and unusual punishments" and the "infliction of unnecessary pain in the execution of the death sentence". Whilst this would seem reasonable it never intended this amendment to guarantee a pain free death. When the Constitution was written execution by hanging was specified and at the time this meant the short or no drop method as the concept of a measured drop hadn't been invented. In the Supreme Court case of Rees v Baze in 2007, Ralph Baze challenged the lethal injection procedure in the state of Kentucky which was found to be constitutional by the court because it did not intentionally cause pain.
Obviously one cannot be inside the brain of a person
as they are being out to death to know what, if any, pain they are
feeling. All we can do is to observe
their reaction to the process and carry out an autopsy afterwards. If for instance in a measured drop hanging,
there is no obvious struggling or movement after the drop and the autopsy finds
that the neck has been broken and the spinal cord severed then it is reasonable
to conclude that the person died a pain free death. In lethal injection if the person appears to
lapse into unconsciousness within seconds of the commencement of the injection
of the fast acting barbiturate that is normally the first chemical injected in
the US we conclude the same.
It is equally clear that when any form of execution is bungled the prisoner often exhibits signs of great suffering.
The time taken in the actual preparations prior to the
execution, (e.g. insertion of the catheters or the shaving of the head and legs
for electrocution), must also cause great emotional suffering which again may
far outweigh the physical pain of the actual moment of death which at least has
an end. Remember that in 20th century
We have looked at the pain caused by execution but
what of the suffering?
One issue rarely addressed is the length of time prisoners spend in the condemned cell or on death row in tiny cells in virtual solitary confinement prior to execution and the uncertainty of eventual execution as various stays are granted and then overturned (particularly in America, where it is an average of over 12 years in 2006, the last year for which statistics are available but can sometimes be over twenty years, as is the case in California).
In
Can capital punishment ever be
"humane"?
I have never personally believed that any form of death, let alone execution, is either instant or painless, so which method of capital punishment should a modern "civilised" society use?
Should our worst criminals be given a completely pain free death even if the technology exists to provide one, or should a degree of physical suffering be part of the punishment?
Whatever method is selected should have some deterrent value whilst not deliberately causing a slow or agonising death.
British style, hanging is an extremely quick process that is designed to cause instant and deep unconsciousness and also benefits from requiring simple and thus quick preparation of the prisoner. It also seems to have substantial deterrent value.
Lethal injection may appear to be more humane than other methods to those who have to administer and witness it, but it is a very slow process. It is essential that the catheter actually goes into a vein rather than through it or round it if the prisoner is to die a pain free death. If it doesn’t, then the person may suffer a great deal of pain but will be unable to communicate this due to the paralysing effects of the second drug. The biggest single objection to lethal injection is the length of time required to prepare the prisoner, which can take from 20 to 45 minutes depending on the ease of finding a vein to inject into.
The gas chamber seems to possess no obvious advantage as the equipment is expensive to buy and maintain, the preparations are lengthy, adding to the prisoner's agonies, and it always causes a slow and cruel death. It is also dangerous to the staff and witnesses.
Electrocution can cause a quick death when all goes well, but seems to have a greater number of technical problems than any other method, often with the most gruesome consequences. (This may in part be due to the age of the equipment - in most case 70-90 years old!)
Shooting by a single bullet in the back of the head seems greatly preferable to shooting by a firing squad in that it is likely to cause instant unconsciousness followed quickly by death rather than causing the prisoner to bleed to death, often whilst still conscious.
It is easy to condemn capital punishment as barbaric, but is spending the rest of one's life in prison so much less cruel to the prisoner or is it merely a way of salving society's conscience and removing the unpleasantness for the staff and officials?
For a full description of each of these methods click on the hyperlinks above.
Conclusion.
At the end of the debate, we would seem to be left with three options.
1) Not to have the death penalty and the genuine problems it causes and continue to accept the relatively high levels of murder and other serious crimes that we presently have.
2) Reintroduce capital punishment for just the "worst" murderers which would at least be some retribution for the terrible crimes they have committed and would permanently incapacitate them. It would also save a small amount of money each year which could, perhaps, be spent on the more genuinely needy. This option is unlikely to reduce overall crime levels.
3) Reintroduce the death penalty in the really strict format outlined above and see a corresponding drop in serious crime whilst accepting that there will be a lot of human misery caused to the innocent families of criminals and that there will be the occasional, if inevitable, mistakes.
A personal view from a victim’s stance.
Click here to read a personal view from the stance of a victim, Ali Stein whose blog is linked to with her permission.
Ultimately the choice is yours!
Have your say by taking part in the on-line survey
Back to Contents page More thoughts on crime and punishment in general Thoughts on human rights