Theory of Deterrence-Pro/Con
There are two common arguments in support of capital punishment: that of deterrence and that of retribution.
According to Gallup, most Americans believe that the death penalty is a deterrent to homicide, which helps them justify their support for capital punishment. Other Gallup research suggests that most Americans would not support capital punishment if it did not deter murder.
Does capital punishment deter violent crimes? In other words, will a potential murderer consider the possibility that they might be convicted and face the death penalty before committing a murder?
The answer appears to be "no."
Social scientists have mined empirical data searching for the definitive answer on deterrence since the early 20th century.
And "most deterrence research has found that the death penalty has virtually the same effect as long imprisonment on homicide rates." Studies suggesting otherwise (notably writings of Isaac Ehrlich from the 1970s) have been, in general, criticized for methodological errors. Ehrlich's work was also criticized by the National Academy of Sciences - but it is still cited as a rationale for deterrence.
A 1995 survey of police chiefs and country sheriffs found that most ranked the death penalty last in a list of six options that might deter violent crime. Their top two picks? Reducing drug abuse and fostering an economy that provides more jobs. (cite)
Data on murder rates seem to discredit the deterrence theory as well.
The region of the county with the greatest number of executions -- the South -- is the region with the largest murder rates. For 2007, the average murder rate in states with the death penalty was 5.5; the average murder rate of the 14 states without the death penalty was 3.1.
Thus deterrence, which is offered as a reason to support capital punishment ("pro"), doesn't wash.
Theory of Retribution-Pro/Con
In Gregg v Georgia, the Supreme Court wrote that "[t]he instinct for retribution is part of the nature of man..."
The theory of retribution rests, in part, on the Old Testament and its call for "an eye for an eye." Proponents of retribution argue that "the punishment must fit the crime." According to The New American: "Punishment -- sometimes called retribution -- is the main reason for imposing the death penalty."
Opponents of retribution theory believe in the sanctity of life and often argue that it is just as wrong for society to kill as it is for an individual to kill.
Others argue that what drives American support for capital punishment is the "impermanent emotion of outrage." Certainly, emotion not reason seems to be the key behind support for capital punishment.
What About Costs?
Some supporters of the death penalty also contend it is less expensive than a life sentence. Nevertheless, at least 47 states do have life sentences without the possibility of parole. Of those, at least 18 have no possibility of parole. And according to the ACLU:
The most comprehensive death penalty study in the country found that the death penalty costs North Carolina $2.16 million more per execution than a non-death penalty murder case with a sentence of life imprisonment (Duke University, May 1993). In its review of death penalty expenses, the State of Kansas concluded that capital cases are 70% more expensive than comparable non-death penalty cases.
Also see Religious Tolerance.
Where It Stands
More than 1000 religious leaders have written an open letter to America and its leaders:
We join with many Americans in questioning the need for the death penalty in our modern society and in challenging the effectiveness of this punishment, which has consistently been shown to be ineffective, unfair, and inaccurate....
With the prosecution of even a single capital case costing millions of dollars, the cost of executing 1,000 people has easily risen to billions of dollars. In light of the serious economic challenges that our country faces today, the valuable resources that are expended to carry out death sentences would be better spent investing in programs that work to prevent crime, such as improving education, providing services to those with mental illness, and putting more law enforcement officers on our streets. We should make sure that money is spent to improve life, not destroy it....
As people of faith, we take this opportunity to reaffirm our opposition to the death penalty and to express our belief in the sacredness of human life and in the human capacity for change.
In 2005, Congress considered the Streamlined Procedures Act (SPA), which would have amended the Anti-Terrorism and Effective Death Penalty Act (AEDPA). AEDPA placed restrictions on the power of federal courts to grant writs of habeas corpus to state prisoners. The SPA would have imposed additional limits on the ability of state inmates to challenge the constitutionality of their imprisonment through habeas corpus.