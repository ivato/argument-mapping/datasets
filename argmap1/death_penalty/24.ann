T1	Sentence 0 279	Lawyers representing Dylann Roof, the white man accused of killing nine black members of a historic Charleston, S.C., church last year, filed a motion challenging the constitutionality of the death penalty after federal prosecutors declined his offer to serve life without parole
T2	Sentence 281 447	In the 34-page motion filed late Monday in Federal District Court in Charleston, Mr. Roof’s lawyers acknowledged that “the facts of this case are indisputably grave.”
T3	ArgumentAgainst 448 626	But they argued that the death penalty itself is unconstitutional, and that therefore “no one can be lawfully sentenced to death or executed under it, no matter what his crimes.”
T4	Sentence 627 831	The challenge to the system stemmed from the government’s refusal to accept guilty pleas from Mr. Roof in exchange for multiple sentences of life in prison without a possibility of parole, the filing said
T5	Sentence 833 1027	“Should the government’s death notice be withdrawn at any point in the future,” the filing said, “Mr. Roof will withdraw this motion and plead guilty as charged to all counts in the indictment.”
T6	Sentence 1028 1259	The defense team — which includes David Bruck, a nationally renowned death penalty lawyer — drew on academic research and earlier court cases to argue that the death penalty is unreliable, excessive and undermined by lengthy delays
T7	Sentence 1261 1415	The lawyers, who declined a request for comment, cited a dissenting opinion in a landmark death penalty case in the United States Supreme Court last year.
T8	Sentence 1416 1575	The 5-to-4 decision upheld the use of lethal injection drugs, turning aside claims by three death row inmates that the drugs caused unconstitutional suffering.
T9	ArgumentAgainst 1576 1746	But in the 46-page dissent, Justice Stephen G. Breyer, joined by Justice Ruth Bader Ginsburg, said it was time to reconsider whether capital punishment was constitutional
T10	Sentence 1748 1969	Justice Breyer wrote that “it is highly likely” that the death penalty violated the Eighth Amendment, which bars cruel and unusual punishments, and said there was evidence suggesting that innocent people had been executed
T11	Sentence 1971 2186	Legal scholars said the Roof filing was notable for the high-powered defense team behind it, as well as its timing a year after the dissent by Justices Breyer and Ginsburg in the death penalty case, Glossip v. Gross
T12	Sentence 2188 2363	Mr. Bruck, one of three lawyers who signed Mr. Roof’s filing on Monday, has played a role in hundreds of death penalty cases and argued seven of them before the Supreme Court.
T13	Sentence 2364 2434	He helped defend Dzhokhar Tsarnaev in the Boston Marathon bomber trial
T14	Sentence 2436 2584	Whether a challenge like Mr. Roof’s could prevail in front of the Supreme Court, should it rise there on appeal, is a subject of intense speculation
T15	Sentence 2586 2736	The defense team is clearly “reading the tea leaves,” said Evan Mandery, the author of “A Wild Justice,” a history of challenges to capital punishment
T16	Sentence 2738 2852	“You have two odd events,” added Mr. Mandery, who is also a professor at the John Jay College of Criminal Justice.
T17	ArgumentAgainst 2853 3077	“It’s that it’s a federal death penalty case in the context of a hate crime — so you have this ambitious use of the death penalty — after Glossip, when Breyer signaled that he thought the death penalty was unconstitutional.”
T18	Sentence 3078 3156	Peter Carr, a Justice Department spokesman, declined to comment on the filing.
T19	Sentence 3157 3214	“We’ll respond at the appropriate time in court,” he said
T20	Sentence 3216 3294	Monday’s motion did not address the crimes for which Mr. Roof, 22, is accused.
T21	Sentence 3295 3407	Officials say Mr. Roof, a high school dropout from a broken home, sat in a Bible study session at Emanuel A.M.E.
T22	Sentence 3408 3553	Church, which has a predominantly black congregation, on June 17, 2015, before pulling out a gun and shooting people ranging in age from 26 to 87
T23	Sentence 3555 3698	Last year, a federal grand jury indicted Mr. Roof on 33 counts, including hate crimes, weapons charges and obstructing the practice of religion
T24	Sentence 3700 3781	In May, the Justice Department announced that it would seek Mr. Roof’s execution.
T25	Sentence 3782 3931	Attorney General Loretta Lynch said in a statement at the time that “the nature of the alleged crime and the resulting harm compelled this decision.”
T26	ArgumentFor 3932 4076	Prosecutors cited nine aggravating factors that included Mr. Roof’s expressed hatred of African-Americans and lack of remorse over the killings.
T27	Sentence 4077 4128	His federal trial is scheduled to begin in November
T28	Sentence 4130 4296	The State of South Carolina has also charged Mr. Roof with murder, and the state prosecutor, Scarlett A. Wilson, said last year that she would seek the death penalty.
T29	Sentence 4297 4339	That trial is expected to start in January
T30	Sentence 4341 4416	Whether Mr. Roof should be put to death has been debated in South Carolina.
T31	Sentence 4417 4524	Some relatives of the victims publicly forgave him during an emotional hearing two days after the massacre.
T32	Sentence 4525 4592	Gov. Nikki R. Haley, a Republican, has called for the death penalty
T33	Sentence 4594 4761	Only three federal defendants, including Timothy J. McVeigh, the Oklahoma City bomber, have been executed since 1988, when the government reinstated the death penalty.
T34	Sentence 4761 4792	Continue reading the main story
