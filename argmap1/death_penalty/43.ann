T1	Sentence 0 56;57 73;74 137;138 206;207 265;266 423	Americans: "Eye for an Eye" Top Reason for Death Penalty Story Highlights - "Save taxpayers money" also named as a top reason for support - "Wrong to take a life" cited as top reason to oppose death penalty - Fewer Americans mention "deterrence" as reason to oppose WASHINGTON, D.C. -- Americans who favor the death penalty most often cite "an eye for an eye" as the reason they hold their position, with 35% mentioning it.
T2	ArgumentFor 424 567	"Save taxpayers money" and "they deserve it" tie as the second-most-popular reasons Americans volunteer in this open-ended measure, at 14% each
T3	ArgumentFor 569 737	A solid majority of Americans (63%) still favor using the death penalty in murder convictions, generally consistent with the level of support found over the last decade
T4	Sentence 739 876	This is the fourth time Gallup has probed Americans to state, in their own words, why they hold the position they do on the death penalty
T5	Sentence 878 1126	Americans who say they support the death penalty have given a variety of responses over the years, but the biblical phrase "an eye for an eye," or retaliation, consistently has been named as the No. 1 reason why the death penalty should be applied.
T6	Sentence 1127 1284	However, this reason's pre-eminence has waned since Gallup first asked this question in 1991, when half of Americans who favor the death penalty mentioned it
T7	Sentence 1286 1352	The No. 2 response to this question has fluctuated over the years.
T8	Sentence 1353 1469	Originally in 1991, the idea of the death penalty as a way to stop someone from repeating a crime was second at 19%.
T9	ArgumentAgainst 1470 1615	That concept has seemingly lost favor over the years, and this year, only 7% say deterrence is their top reason for supporting the death penalty.
T10	ArgumentFor 1616 1790	Instead, "save taxpayers money" and simply, "they deserve it," have emerged as the second-most-popular responses to why the death penalty should be used in murder convictions
T11	Sentence 1792 2078	Several practical reasons including the belief that the prisoners "cannot be rehabilitated," that "life sentences don't always mean life in prison," and that the death penalty "relieves prison overcrowding" are mentioned less frequently, by between 2% and 3% of death penalty proponents
T12	Sentence 2080 2152	Those Opposed to Death Penalty Cite "Wrong to Take a Life" as Top Reason
T13	Sentence 2153 2341	While a majority of Americans tilt in favor of the death penalty, the one in three Americans who oppose it also have a diversity of views as to why the ultimate penalty should not be used.
T14	ArgumentAgainst 2342 2439	"Wrong to take a life" has been the top reason for opposing it since 1991, by comfortable margins
T15	ArgumentAgainst 2441 2714	In two of the three times Gallup has asked this question, "persons may be wrongly convicted" has been the No. 2 justification Americans give for opposing the death penalty, along with reasons grounded in religious beliefs, including that "punishment should be left to God."
T16	Sentence 2715 2816	Yet "wrong to take a life" is still the most popular open-ended response by a more than 2-to-1 margin
T17	ArgumentFor 2818 2972	As with the reasons for favoring the death penalty, subjective attitudes often outrank practical concerns in terms of why people oppose the death penalty.
T18	ArgumentAgainst 2973 3173	Some of the practical reasons offered for death penalty opposition include the "possibility of rehabilitation," the cost of keeping a prisoner on death row, and unfair application of the death penalty
T19	Sentence 3175 3186;3187 3497	Bottom Line Americans' top reasons for supporting the death penalty, including most prominently that it is necessary to take "an eye for an eye" when a murder is committed, demonstrate that Americans are less concerned with using the tool as a deterrent for future crimes and more so with using it as a means of punishment
T20	ArgumentAgainst 3499 3621	For those who oppose the death penalty, moral and religious reasons such as it is "wrong to take a life" are most popular.
T21	ArgumentAgainst 3622 3846	Considering the numerous accounts of death-row inmates being found innocent through the use of DNA evidence, it may be surprising that wrongful conviction is only tied for second as a reason cited by death penalty opponents.
T22	Sentence 3847 4013	Practical reasons like the cost of keeping a prisoner on death row -- for decades, potentially -- does not factor as much into why Americans oppose the death penalty.
T23	ArgumentAgainst 4014 4186	Additionally, few opponents cite cruelty as a reason for their beliefs, despite recent news stories of botched executions in which lethal injections did not work as planned
T24	Sentence 4188 4367	Throughout the 23 years Gallup has asked Americans why they hold their views on the death penalty, the responses have been generally consistent, with a few changes at the margins.
T25	Sentence 4368 4564	For those studying the death penalty, these beliefs suggest many Americans have not only made up their minds about it, but they also have coalesced around defined reasons for holding those beliefs
T26	Sentence 4566 4580;4581 4787	Survey Methods Results for this Gallup poll are based on telephone interviews conducted Oct. 12-15, 2014, with a random sample of 1,017 adults, aged 18 and older, living in all 50 U.S. states and the District of Columbia.
T27	Sentence 4788 4928	For results based on the total sample of national adults, the margin of sampling error is Â±4 percentage points at the 95% confidence level.
T28	ArgumentFor 4929 5085	For results based on the total sample of 634 who favor the death penalty, the margin of sampling error is Â±5 percentage points at the 95% confidence level.
T29	ArgumentFor 5086 5242	For results based on the total sample of 335 who oppose the death penalty, the margin of sampling error is Â±7 percentage points at the 95% confidence level
T30	Sentence 5244 5417	Each sample of national adults includes a minimum quota of 50% cellphone respondents and 50% landline respondents, with additional minimum quotas by time zone within region.
T31	Sentence 5418 5502	Landline and cellular telephone numbers are selected using random-digit-dial methods
T32	Sentence 5504 5568	View survey methodology, complete question responses, and trends
T33	Sentence 5570 5626	Learn more about how the Gallup Poll Social Series works
T34	Sentence 5628 5659;5660 5672;5673 5694	Get Articles in Related Topics: Gallup Daily |No updates March 28.
T35	Sentence 5695 5717;5718 5736	Next update March 29.| |Real Unemployment
T36	Sentence 5736 5746;5747 5778;5779 5806;5807 5836;5837 5855	||9.7%||-| |Gallup Good Jobs||46.6%||-0.1| |Engaged at Work||32.8%||-| |Economic Confidence||-12||-| |Consumer Spending
T37	Sentence 5855 5865;5866 5891;5892 5929	||$96||-9| |Obama Approval||50%||-1| Worldwide, Where Do People Feel Safe?
T38	Sentence 5930 5950;5951 6113	Not in Latin America A review of Gallup data on Americans' attitudes toward police, race and criminal justice provides a public opinion context for the events of the last several days
T39	Sentence 6204 6275;6276 6291	Gallup World Headquarters, 901 F Street, Washington, D.C., 20001, U.S.A +1 202.715.3030
