T1	Sentence 0 21;22 174	By Wesley Coopersmith Earlier this month, President Barack Obama called for a federal increase of the minimum wage in an effort to help struggling workers across the country.
T2	Sentence 175 304	The President delivered his speech in Anacostia, Washington D.C., a poverty-stricken neighborhood plagued with high unemployment.
T3	Sentence 305 422	Originally planned to be voted on this month, the Senate will wait until January to take up this piece of legislation
T4	ArgumentAgainst 424 549	Although the President may have good intensions, raising the minimum wage does not help low-skilled and unemployed Americans.
T5	ArgumentAgainst 550 629	Rather, it does more harm than good by increasing the cost of low-skilled labor
T6	Sentence 631 736	For a government policy to be effective, it must actually benefit the individuals it is supposed to help.
T7	ArgumentFor 737 833	In his speech, the President stated, “If you work hard, you should be able to support a family.”
T8	ArgumentAgainst 834 900	But many minimum wage workers are not working to support a family.
T9	ArgumentFor 901 1044	According to The Economic Policy Institute, a left-leaning think tank, less than 50% of those receiving the minimum wage are full-time workers.
T10	Sentence 1045 1131	This is because many minimum wage workers are teenagers or part time college students.
T11	Sentence 1132 1196	In fact, 50% of minimum wage workers are 25 years old or younger
T12	Sentence 1198 1342	While advocates of minimum wage hikes claim they benefit low-income individuals, many minimum wage workers do not come from low-income families.
T13	Sentence 1343 1476	Of those affected by an increase in the minimum wage to $9/hour, just over half come from families who make $40,000 or less per year.
T14	Sentence 1477 1585	Attributing to this statistic is the many teenagers/young adults who come from generally better off families
T15	Sentence 1587 1682	In short, an increase in the minimum wage only affects half of those it is attempting to reach.
T16	Sentence 1683 1761	But, does a minimum wage hike positively affect the half it is trying to help?
T17	Sentence 1762 1883	In this same speech, the President claimed, “We all know the arguments that have been used against a higher minimum wage.
T18	ArgumentAgainst 1884 1974	Some say it actually hurts low-wage workers — businesses will be less likely to hire them.
T19	Sentence 1975 2044	But there’s no solid evidence that a higher minimum wage costs jobs.”
T20	Sentence 2045 2067	But is his claim true?
T21	Sentence 2068 2155	The Washington Post Fact Checker didn’t think so and gave Obama’s claim two Pinocchio’s
T22	ArgumentAgainst 2157 2368	In fact, the most current research shows that a higher minimum wage does increase unemployment; however, the more important economic reality is that a higher minimum wage increases the cost of low-skilled labor.
T23	Sentence 2369 2420	Businesses pass this cost off in a variety of ways.
T24	ArgumentAgainst 2421 2612	The higher cost of labor may translate into higher unemployment for low-skilled workers, reduced hours, loss of benefits, a higher work demand, or even higher prices for the goods they supply
T25	Sentence 2614 2763	In each of these ways, it is not the wealthy who are hurt, but rather low-skilled workers who are already employed or who are looking for employment.
T26	Sentence 2764 2956	It is the minimum wage worker who gets laid off, whose hours is cut, whose work hour-flexibility is taken away, or who pays higher prices at cheaper stores that they are more likely to shop at
T27	ArgumentAgainst 2958 3072	One real life example of the negative effects of raising the cost of labor can be seen at the local grocery store.
T28	Sentence 3073 3188	More and more grocery stores are discovering that self-check-out machines are less expensive than hiring a cashier.
T29	ArgumentAgainst 3189 3297	Increasing the cost of labor only further incentivizes businesses to invest in technology instead of people.
T30	ArgumentAgainst 3298 3443	It’s no surprise that the government cannot simply wave its magic wand and force businesses to pay higher wages without any negative side effects
T31	Sentence 3445 3538	Even Chairwoman of President Obama’s Council of Economic Advisor, Christina D. Romer, agrees.
T32	ArgumentAgainst 3539 3665	In a recent New York Times article, Christina argues that raising the minimum wage may harm the people it is supposed to help.
T33	ArgumentAgainst 3666 3900	Discussing the fact that higher labor costs often cause higher prices, Christina writes “Often, the customers paying those prices — including some of the diners at McDonald’s and the shoppers at Walmart — have very low family incomes.
T34	ArgumentAgainst 3901 3990	Thus this price effect may harm the very people whom a minimum wage is supposed to help.”
T35	Sentence 4008 4214	Sowell sums it up well: “The net economic effect of minimum wage laws is to make less skilled, less experienced, or otherwise less desired workers more expensive — thereby pricing many of them out of jobs.”
T36	ArgumentAgainst 4215 4302	A minimum wage hike is simply an ineffective way to help Americans rise out of poverty.
T37	ArgumentAgainst 4303 4382	It only reaches half of those it is intended to help and not in a positive way.
T38	ArgumentFor 4383 4526	Increasing employment opportunity gives low-skilled workers the best chance of finding employment and climbing up the economic mobility ladder.
T39	ArgumentAgainst 4527 4577	Raising the minimum wage does more harm than good.
