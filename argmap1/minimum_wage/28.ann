T1	Sentence 0 43	America's Complicated Minimum Wage Argument
T2	Sentence 44 112	The president wants to raise the federal minimum wage to $9 an hour.
T3	Sentence 113 146	Does it make good economic sense?
T4	Sentence 147 287	In his State of the Union address, President Barack Obama proposed raising the minimum wage from $7.25 to $9 per hour, indexed to inflation.
T5	Sentence 288 517	The proposal has sparked a national conversation about the costs and benefits of boosting the pay for America's poorest workers, but the debate over the federally mandated pay level has been going on among economists for decades.
T6	Sentence 518 671	Below, an examination of exactly why policy wonks can't agree about when, why, and by how much to raise the wage paid to some of America's lowest earners
T7	ArgumentFor 673 760	If we raise the minimum wage to $9, is it going to make McDonald's cashiers super rich?
T8	Sentence 765 776	Not at all.
T9	Sentence 777 858	At $7.25 per hour, someone working 40 hours a week earns around $15,100 per year.
T10	Sentence 859 907	Nine dollars per hour puts that total at $18,700
T11	Sentence 909 925	That's not much.
T12	Sentence 926 969	How about we just raise it to $15 per hour?
T13	Sentence 970 1013	It sounds great on the surface, doesn't it?
T14	ArgumentFor 1014 1244	It means more money for poor Americans, who tend to spend extra more readily than higher earners, as they spend a larger share of their income on basic necessities (toothpaste costs $3 a pop whether you're rich or poor, after all)
T15	Sentence 1246 1294	[READ: GOP Forced to Choose: Deficit or Defense]
T16	Sentence 1295 1332	It sounds like there's a "but" coming
T17	ArgumentAgainst 1343 1531	It means higher pay for low-wage Americans, but some people argue that even raising it to $9 would ultimately boost unemployment, as employers will be able to afford to hire fewer workers.
T18	Sentence 1532 1590	Plus there's the question of how much that labor is worth.
T19	ArgumentAgainst 1591 1775	Some economists say raising the minimum wage to too high of a level could create a disconnect, as workers are only worth paying if they're able to produce at least as much as they earn
T20	Sentence 1777 1807	I mean, I make $60,000 a year.
T21	Sentence 1808 1873	It doesn't affect me anyway whether the minimum wage is $9 or $19
T22	Sentence 1875 1926	Please try not to be so fantastically shortsighted.
T23	Sentence 1927 2001	It's true that relatively few Americans are paid the federal minimum wage.
T24	Sentence 2002 2260	According to the Bureau of Labor Statistics, only 2.3 percent of hourly paid workers are paid at the minimum wage—around 1.7 million people (however, it's worth mentioning that 18 states and D.C. have minimum wages above the federal minimum, as of January 1.
T25	Sentence 2261 2322	Qualifying workers in those states are paid the higher rates)
T26	Sentence 2324 2397	Let's say that those relatively few people are suddenly paid $19 an hour.
T27	ArgumentAgainst 2398 2518	One potential effect could be higher prices at those businesses that now must pay their workers twice as much as before.
T28	ArgumentAgainst 2519 2645	Businesses in the retail and hospitality industries may suddenly have to hike their prices to be able to afford their workers.
T29	ArgumentAgainst 2646 2774	That means higher prices for anyone stopping at the mall or Burger King—price bumps that could spread further out in the economy
T30	ArgumentAgainst 2776 2890	Using $19 is an extreme example, but plenty of people argue that even a small bump could have detrimental effects.
T31	ArgumentAgainst 2891 3009	After the president's speech, House Speaker John Boehner argued that raising the minimum wage would raise unemployment
T32	Sentence 3011 3057	[NEWMAN: 7 Economic Reforms Obama Is Ignoring]
T33	ArgumentAgainst 3058 3116	Isn't having a minimum wage, period, causing unemployment?
T34	Sentence 3117 3148	Well, yes, to a certain extent.
T35	Sentence 3149 3229	As we all learned in Economics 101, the minimum wage is a "price floor" on labor
T36	Sentence 3231 3259	I didn't take Economics 101.
T37	Sentence 3260 3325	I dropped it to take "Gender Dynamics in the Modern Horror Film."
T38	Sentence 3326 3341	You missed out.
T39	ArgumentAgainst 3342 3462	According to basic economics, a price floor creates a surplus, meaning that the labor supply is greater than the demand.
T40	ArgumentAgainst 3463 3624	Translated out of econo-speak, that means that the price for labor is set artificially high, with more people willing to work than business will be able to hire.
T41	ArgumentAgainst 3625 3679	It creates a certain amount of structural unemployment
T42	Sentence 3681 3747	So we should bring the wage down to whatever people will work for?
T43	ArgumentFor 3748 3835	Then you're back to the problem of paying millions of Americans less than a living wage
T44	Sentence 3837 3879;3880 3886	[ENJOY: Political Cartoons on the Economy] Right.
T45	Sentence 3887 3926	Well, then, we just shouldn't raise it?
T46	Sentence 3927 3959	That's one way of looking at it.
T47	Sentence 3960 4085	But the real world is more complicated than Economics 101 or price floors or supply and demand curves would have you believe.
T48	ArgumentFor 4086 4205	First of all, while the minimum wage is occasionally bumped up, it is lower now in real terms than it was 40 years ago.
T49	ArgumentFor 4206 4304	The $1.60 hourly minimum wage of 1968 would be around $10.50 if it had been indexed for inflation.
