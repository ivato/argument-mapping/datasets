Updated 4 p.m. | Posted 10:51 a.m.
Attorneys for the city of Minneapolis and supporters of a $15 minimum wage squared off before the Minnesota Supreme Court Tuesday.
They were there to ask the court whether or not a proposed city charter change setting a new minimum wage should go on the ballot Nov. 8. The arguments focused on the mechanism of the proposed change, not the idea of minimum wage regulation.
Supporters have been trying to get the Minneapolis City Council to raise the minimum wage, but were turned down earlier this summer. They sued, asking a judge to put the question on the ballot anyway, and won in Hennepin County District Court.
The city appealed the decision, and the Supreme Court granted an emergency hearing given the fast-approaching general election this fall.
• Aug. 23: Mpls. officials appeal minimum wage ruling as supporters plot huge voter drive
"Is this the proper mechanism to accomplish what this ordinance is trying to accomplish?" attorney Charles Nauen asked Tuesday on behalf of the city. "It's really an ordinance, rather than a charter amendment."
He told the justices that the city doesn't have an initiative process, a provision that allows petitioners to ask voters directly if they want a city ordinance. Ordinances are the province of the City Council, he argued.
"There is no direct initiative process," Nauen said, talking about the nature of the charter Minneapolis adopted in 1920. "These attributes contribute to this day ... There have been 96 years of representative democracy in Minneapolis."
Bruce Nestor, an attorney representing supporters of the proposal to put the charter change to voters, told the justices the measure should go on the November ballot.
Nestor told justices that his clients weren't seeking an ordinance, but to change the way city government works, adding that the function of a city includes the responsibility to look out for the general welfare of the people in a city.
"In order to carry out the general welfare functions, it's necessary to regulate the activities of private parties," Nestor said. "This is a particular concern in Minneapolis, given the high cost of living in the city."
Justices raised pointed questions about the nature of the city charter in Minneapolis, and what exactly a city "function" involved — trying to differentiate between specific city activities and a local government's general responsibilities. Justice G. Barry Anderson asked if the court was essentially being asked to grant a new right to the people of Minneapolis — and possibly other cities.
"There's no initiative and referendum in the city of Minneapolis, per the charter, and given those restrictions and given that a minimum wage ordinance looks very much like ordinance activity," Anderson said. "If you were to prevail here, haven't we effectively created a process of initiative for the citizens of Minneapolis?"
And Justice Lorie Skjerven Gildea asked about the city's role in employment law, and whether the fact that people might live in another city, but work in Minneapolis, might play a role in the city's authority to set wages.
"What business is it of the Minneapolis city council, what people in St. Louis Park make?" she asked.
The justices heard arguments for about an hour Tuesday morning, but said only that they would offer an opinion "in due course."
After the hearing, Nestor said he believes allowing a charter amendment to address the minimum wage would not lead to a flood of other ballot initiatives.
He also said the minimum wage proposal is consistent with the use of charter amendments in the state for the last century.
"When special interests have captured a city council, and when the city council won't listen to the voice of the people, this is what Minnesota law and the Constitution provides as an alternative," Nestor said.
Ginger Jentzen, an organizer with 15 Now Minnesota, said they're prepared to continue their campaign regardless of the court's decision.
"We're building a movement to win not only 15 in Minneapolis, but to win workers rights in general," Jentzen said.
Correction (Aug. 31, 2016): An earlier version of this story misidentified the justice asking about the jurisdiction of minimum wage laws outside the city of Minneapolis.