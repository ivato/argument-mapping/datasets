T1	Sentence 0 177	I don’t believe that’s because economists care less about the plight of the poor — many economists are perfectly nice people who care deeply about poverty and income inequality.
T2	ArgumentAgainst 178 320	Rather, economic analysis raises questions about whether a higher minimum wage will achieve better outcomes for the economy and reduce poverty
T3	Sentence 322 382	First, what’s the argument for having a minimum wage at all?
T4	ArgumentFor 383 503	Many of my students assume that government protection is the only thing ensuring decent wages for most American workers.
T5	ArgumentAgainst 504 641	But basic economics shows that competition between employers for workers can be very effective at preventing businesses from misbehaving.
T6	Sentence 642 821	If every other store in town is paying workers $9 an hour, one offering $8 will find it hard to hire anyone — perhaps not when unemployment is high, but certainly in normal times.
T7	Sentence 822 954	Robust competition is a powerful force helping to ensure that workers are paid what they contribute to their employers’ bottom lines
T8	ArgumentFor 956 1053	One argument for a minimum wage is that there sometimes isn’t enough competition among employers.
T9	Sentence 1054 1162	In our nation’s history, there have been company towns where one employer truly dominated the local economy.
T10	Sentence 1163 1238	As a result, that employer could affect the going wage for the entire area.
T11	ArgumentFor 1239 1383	In such a situation, a minimum wage can not only make workers better off but can also lead to more efficient levels of production and employment
T12	Sentence 1385 1474	But I suspect that few people, including economists, find this argument compelling today.
T13	Sentence 1475 1648	Company towns are largely a thing of the past in this country; even Wal-Mart Stores, the nation’s largest employer, faces substantial competition for workers in most places.
T14	Sentence 1649 1761	And many employers paying the minimum wage are small businesses that clearly face strong competition for workers
T15	Sentence 1763 1870	Instead, most arguments for instituting or raising a minimum wage are based on fairness and redistribution.
T16	ArgumentAgainst 1871 2006	Even if workers are getting a competitive wage, many of us are deeply disturbed that some hard-working families still have very little.
T17	ArgumentFor 2007 2165	Though a desire to help the poor is largely a moral issue, economics can help us think about how successful a higher minimum wage would be at reducing poverty
T18	Sentence 2167 2202	An important issue is who benefits.
T19	Sentence 2203 2348	When the minimum wage rises, is income redistributed primarily to poor families, or do many families higher up the income ladder benefit as well?
T20	ArgumentAgainst 2349 2516	It is true, as conservative commentators often point out, that some minimum-wage workers are middle-class teenagers or secondary earners in fairly well-off households.
T21	Sentence 2517 2699	But the available data suggest that roughly half the workers likely to be affected by the $9-an-hour level proposed by the president are in families earning less than $40,000 a year.
T22	ArgumentFor 2700 2863	So while raising the minimum wage from the current $7.25 an hour may not be particularly well targeted as an anti-poverty proposal, it’s not badly targeted, either
T23	Sentence 2865 2987	A related issue is whether some low-income workers will lose their jobs when businesses have to pay a higher minimum wage.
T24	ArgumentFor 2988 3146	There’s been a tremendous amount of research on this topic, and the bulk of the empirical analysis finds that the overall adverse employment effects are small
T25	ArgumentFor 3148 3303	Some evidence suggests that employment doesn’t fall much because the higher minimum wage lowers labor turnover, which raises productivity and labor demand.
T26	ArgumentFor 3304 3428	But it’s possible that productivity also rises because the higher minimum attracts more efficient workers to the labor pool.
T27	ArgumentAgainst 3429 3627	If these new workers are typically more affluent — perhaps middle-income spouses or retirees — and end up taking some jobs held by poorer workers, a higher minimum could harm the truly disadvantaged
T28	ArgumentFor 3629 3780	Another reason that employment may not fall is that businesses pass along some of the cost of a higher minimum wage to consumers through higher prices.
T29	Sentence 3781 3926	Often, the customers paying those prices — including some of the diners at McDonald’s and the shoppers at Walmart — have very low family incomes.
T30	ArgumentAgainst 3927 4014	Thus this price effect may harm the very people whom a minimum wage is supposed to help
T31	ArgumentAgainst 4016 4167	It’s precisely because the redistributive effects of a minimum wage are complicated that most economists prefer other ways to help low-income families.
T32	Sentence 4168 4272	For example, the current tax system already subsidizes work by the poor via an earned-income tax credit.
T33	Sentence 4273 4374	A low-income family with earned income gets a payment from the government that supplements its wages.
T34	Sentence 4375 4492	This approach is very well targeted — the subsidy goes only to poor families — and could easily be made more generous
T35	ArgumentFor 4494 4588	By raising the reward for working, this tax credit also tends to increase the supply of labor.
T36	Sentence 4589 4630	And that puts downward pressure on wages.
T37	ArgumentFor 4631 4726	As a result, some of the benefits go to businesses, as would be the case with any wage subsidy.
T38	ArgumentFor 4727 4894	Though this mutes some of the direct redistributive value of the program — particularly if there’s no constraining minimum wage — it also tends to increase employment.
T39	ArgumentFor 4895 4988	And a job may ultimately be the most valuable thing for a family struggling to escape poverty
T40	Sentence 4990 5080	What about the macroeconomic argument that is sometimes made for raising the minimum wage?
T41	ArgumentFor 5081 5171	Poorer people typically spend a larger fraction of their income than more affluent people.
T42	ArgumentFor 5172 5358	So if an increase in the minimum wage successfully redistributed some income to the poor, it could increase overall consumer spending — which could stimulate employment and output growth
T43	Sentence 5360 5421	All of this is true, but the effects would probably be small.
T44	ArgumentFor 5422 5519	The president’s proposal would raise annual income by $3,500 for a full-time minimum-wage worker.
T45	Sentence 5520 5594	A recent analysis found that 13 million workers earn less than $9 an hour.
T46	ArgumentAgainst 5595 5761	If they were all working full time at the current minimum — and a majority are not — the income increase from the higher minimum wage would be only about $50 billion.
T47	ArgumentAgainst 5762 6038	Even assuming that all of that higher income was redistributed from the wealthiest families, the difference in spending behavior between low-income and high-income consumers is likely to translate into only about an additional $10 billion to $20 billion in consumer purchases.
T48	Sentence 6039 6080	That’s not much in a $15 trillion economy
T49	Sentence 6082 6117	SO where does all of this leave us?
T50	ArgumentAgainst 6118 6229	The economics of the minimum wage are complicated, and it’s far from obvious what an increase would accomplish.
T51	ArgumentFor 6230 6323	If a higher minimum wage were the only anti-poverty initiative available, I would support it.
T52	ArgumentFor 6324 6428	It helps some low-income workers, and the costs in terms of employment and inefficiency are likely small
T53	Sentence 6430 6500	But we could do so much better if we were willing to spend some money.
T54	Sentence 6501 6633	A more generous earned-income tax credit would provide more support for the working poor and would be pro-business at the same time.
T55	Sentence 6634 6801	And pre-kindergarten education, which the president proposes to make universal, has been shown in rigorous studies to strengthen families and reduce poverty and crime.
T56	Sentence 6802 6903	Why settle for half-measures when such truly first-rate policies are well understood and ready to go?
