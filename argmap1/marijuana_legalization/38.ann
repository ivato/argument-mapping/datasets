T1	Sentence 0 22;23 37;38 239	Editors’ Note Appended To the Editor: Much of the country — with The New York Times regrettably in the vanguard — is advocating the reckless addition of a third drug, marijuana, to two drugs currently legal for adults: alcohol and tobacco.
T2	Sentence 240 307	These two legal drugs are the leading causes of preventable illness
T3	Sentence 309 367	The legal status of a drug has dramatic impact on its use.
T4	Sentence 368 498	In the last 30 days, 52 percent of Americans 12 and older used alcohol, 27 percent used tobacco and only 7 percent used marijuana.
T5	ArgumentAgainst 499 589	The dramatically lower level of marijuana use reflects its illegal status, not its appeal.
T6	Sentence 590 652	Why is it in our nation’s interest to see marijuana use climb?
T7	ArgumentAgainst 653 701	Since when is smoking a program that we promote?
T8	ArgumentAgainst 702 793	The best policy to protect public health is one that reduces, not increases, marijuana use.
T9	ArgumentAgainst 794 935	There are plenty of ways to achieve this goal, including a strong public education effort focused on the negative health effects of marijuana
T10	ArgumentAgainst 937 1050	There are reasons why employers, including the United States government, prohibit marijuana use in the workplace.
T11	ArgumentAgainst 1051 1219	There are reasons why marijuana emergency room admissions are reported at the rate of 1,250 a day and 455,000 a year, and why highway crashes double for marijuana users
T12	Sentence 1221 1461	We cannot ignore the negative effects that legalization would have on under-age use and addiction, highway safety, treatment costs, mental health problems, emergency room admissions, workplace accidents and productivity, and personal health
T13	Sentence 1463 1478;1479 1495;1496 1518;1519 1608	PETER BENSINGER ROBERT L. DuPONT Chicago, July 30, 2014 Mr. Bensinger was administrator of the Drug Enforcement Administration from 1976 to 1981.
T14	Sentence 1609 1744	Mr. DuPont, president of the Institute for Behavior and Health, was director of the National Institute on Drug Abuse from 1973 to 1978.
T15	Sentence 1745 1888	They are co-founders of Bensinger, DuPont & Associates, which provides employee assistance program, gambling helpline and drug-testing services
T16	Sentence 1890 1904;1905 2079	To the Editor: I applaud the editorial board for highlighting the disastrous social costs of marijuana prohibition and for taking a courageous position in calling for national legalization.
T17	Sentence 2080 2170	I became a mother in 1971, the same year that President Nixon declared the “war on drugs.”
T18	ArgumentFor 2171 2332	Twenty years later my son was arrested for marijuana possession, which began a decade of cycling through the criminal justice system for nonviolent drug offenses
T19	Sentence 2334 2452	The outlawing of marijuana translated into a war that has been waged against families like mine for over four decades.
T20	ArgumentFor 2453 2551	Prohibition has robbed children of their futures, while building a huge prison-industrial complex.
T21	ArgumentFor 2552 2673	It has caused countless casualties, wasted taxpayer dollars, promoted discrimination and taken away basic human liberties
T22	Sentence 2675 2857	Mothers were instrumental in ending alcohol prohibition in the 1930s, and once again moms are uniting to end marijuana prohibition for the sake of our children and future generations
T23	Sentence 2882 2918	Spring Valley, Calif., July 28, 2014
T24	Sentence 2919 3026	The writer is co-founder and executive director of a New PATH (Parents for Addiction Treatment and Healing)
T25	Sentence 3028 3042;3043 3125	To the Editor: I am concerned by the editorial board’s stance in favor of marijuana legalization.
T26	Sentence 3126 3255	It has been only six months since retail marijuana sales began in Colorado, and just weeks since the rollout in Washington State.
T27	Sentence 3256 3430	A robust, objective analysis of outcomes in these two states is the only way to determine the best policy on this issue, but for now, it is too early to make a sound judgment
T28	ArgumentAgainst 3432 3533	What we do know is that marijuana is harmful, particularly for the still-developing adolescent brain.
T29	ArgumentAgainst 3534 3672	As we’ve seen with alcohol and tobacco, imposing a minimum age will probably not be enough to prevent a spike in teenage use and addiction
T30	ArgumentAgainst 3674 3879	Considering this likely consequence, not to mention the economic burden of regulating the drug and treating new problematic users, we may find that the societal costs of legalization outweigh the benefits.
T31	Sentence 3880 3965	However, if we make pronouncements before we see the research, we’re jumping the gun.
T32	Sentence 3966 4006	Let’s let the facts speak for themselves
T33	Sentence 4008 4026;4027 4050	HOWARD P. MEITINER New York, July 28, 2014
T34	Sentence 4051 4153	The writer is president and chief executive of Phoenix House, a drug and alcohol rehabilitation center
T35	Sentence 4155 4169;4170 4317	To the Editor: As your July 27 editorial “Repeal Prohibition, Again” says: “There are legitimate concerns about marijuana on the development of adolescent brains.
T36	ArgumentAgainst 4318 4392	For that reason, we advocate the prohibition of sales to people under 21.”
T37	ArgumentAgainst 4393 4446	Our concern about legalization is its effect on kids.
T38	ArgumentAgainst 4447 4589	Society may not do much better at enforcing this restriction on sale and marketing of marijuana to kids than we have with alcohol and tobacco.
T39	ArgumentAgainst 4590 4754	Research shows that use of any of these drugs in adolescence — especially early adolescence — significantly heightens risks of substance use disorders in later life
T40	ArgumentFor 4756 4905	We need to provide a much better prevention and treatment infrastructure, which expanded access to marijuana and increasing teenage use will require.
T41	ArgumentAgainst 4906 5066	That begins with limiting marijuana marketing that kids will be exposed to, and equipping parents with information about the very real health risks of early use
T42	Sentence 5068 5135	These are not details to be sorted later, but vital considerations.
T43	ArgumentFor 5136 5266	These are the considerations that matter most to us, and to most parents, including — research shows — those favoring legalization
T44	Sentence 5268 5281;5282 5311;5312 5342;5343 5366	STEVE PASIERB President and Chief Executive Partnership for Drug-Free Kids New York, July 29, 2014
T45	Sentence 5367 5497	The letter from Peter Bensinger and Robert L. DuPont has been revised to add their company affiliation to the identification line.
