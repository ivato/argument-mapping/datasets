T1	ArgumentFor 0 186	Legalized marijuana or weed for recreational use is already existent in several states in the US, including Colorado and Washington, where voters passed ballot initiatives to realize it.
T2	Sentence 187 335	As expected, considering that weed is considered as a dangerous drug, the move has raised plenty of questions and has become a hot topic in debates.
T3	Sentence 336 369	So, is this a wise or wrong move?
T4	Sentence 370 459	Here are the pros and cons of legalizing weed to help you come up with an informed answer
T5	Sentence 461 492;493 495	List of Pros of Legalizing Weed 1.
T6	ArgumentFor 514 676	As governments struggle with decreased revenue and rising costs, they look for creative ways to boost income to fund projects, such as new parks and road repairs.
T7	ArgumentFor 677 821	Now, some people believe that the legalization of weed could be revenue generator in the form of new taxes applied to its sale and distribution.
T8	ArgumentFor 822 930	For instance, in Colorado, analysts recommend that taxing the drug could raise millions of dollars each year
T9	Sentence 932 986	2. More Effective Criminal Justice and Law Enforcement
T10	ArgumentFor 988 1144	Legalization advocates claim that the move will provide police officers more money and time to go after criminals committing other crimes, such as violence.
T11	ArgumentFor 1145 1335	They even argue that it would create wiggle room in the criminal justice system, which allows judges and prosecutors to focus more on violent crimes, while crowded prisons are freed of space
T12	Sentence 1337 1377	3. Less Money to Support Organized Crime
T13	ArgumentFor 1379 1483	It is believed that legalizing recreational weed will cut off a revenue stream for illegal drug traders.
T14	ArgumentFor 1484 1622	Advocates contend that making the substance less profitable for criminals will result to a decrease in violence associated with the trade.
T15	ArgumentFor 1623 1691	This would save lives while taking the weight off from law enforcers
T16	ArgumentFor 1713 1833	When a person buys weed off the street, there is no way he would exactly know what dangerous substances are cut into it.
T17	ArgumentFor 1834 1998	On the other hand, current legalization efforts create a framework for a safety control system, which will work to get rid of the risk coming from smoking marijuana
T18	Sentence 2000 2033	5. Wider Access for Medicinal Use
T19	ArgumentFor 2035 2257	People inside and outside the medical field argue that marijuana is effective in treating a range of health conditions, including Crohn’s disease, epilepsy, multiple sclerosis (MS) and posttraumatic stress disorder (PTSD).
T20	Sentence 2258 2388	In many states, including Massachusetts, California and New Jersey, it is legal to use medical marijuana under certain guidelines.
T21	ArgumentFor 2389 2473	This means more people will be free to use the drug for its believed health benefits
T22	Sentence 2475 2514	6. Medical Benefits for Cancer Patients
T23	ArgumentFor 2516 2593	As previously mentioned, marijuana brings about a number of medical benefits.
T24	ArgumentFor 2594 2693	However, the most notable of them all is the treatment of patients who are undergoing chemotherapy.
T25	ArgumentFor 2694 2837	This is the reason why certain states, such as California, have implemented initiatives for the legalization of the drug for medicinal purposes
T26	ArgumentFor 2860 2931	By limiting the use of marijuana, we are intruding on personal freedom.
T27	ArgumentFor 2932 3057	Even if the drug is shown to have harmful effects, it is still the right of every individual to decide what harms him or not.
T28	ArgumentFor 3058 3179	Smoking weed is generally considered as a “victimless crime”, where only the user is at risk of experiencing the effects.
T29	Sentence 3180 3275	However, morality could not be legislated when people disagree about what is considered “moral”
T30	Sentence 3277 3327	8. Reduced Street Justice Related to Drug Disputes
T31	ArgumentFor 3329 3438	Currently, you cannot call the police or any lawyer to litigate if someone in the drug trade screws you over.
T32	Sentence 3439 3539	Instead, you must settle the dispute yourself, which often leads to a cycle of retaliatory violence.
T33	ArgumentFor 3540 3611	But with legalization, proper means to settle disputes would be created
T34	ArgumentFor 3613 3672	9. Loss of Business for Drug Dealers (Including Terrorists)
T35	ArgumentFor 3674 3775	It is quite ironic, but perhaps the worst opponents of weed legalization are drug dealers themselves.
T36	ArgumentFor 3776 3873	Due to the absence of competition and high street prices, they are making enormous sums of money.
T37	ArgumentFor 3874 4043	But through legalization, there will be open competition and lower prices, so drug cartels, which might include terrorists, would lose some or even all of their business
T38	Sentence 4045 4076;4077 4096	List of Cons of Legalizing Weed 1. Addictive Nature
T39	Sentence 4098 4317	Marijuana legalization supporters argue that weed is not as addictive as other harder drugs, such as cocaine and heroin, but addiction treatment specialists have seen firsthand that long-term use does lead to addiction.
T40	ArgumentAgainst 4318 4402	Research found that as many as a tenth of users have developed dependence over time.
T41	Sentence 4403 4541	Like in other substances of abuse cases, stopping marijuana use will lead to withdrawal symptoms, which range from anxiety to irritability
T42	ArgumentAgainst 4566 4759	Considering that marijuana is a drug, which by definition changes the way the human body works, its use would lead users to experience a high that alters their perception while under influence.
T43	ArgumentAgainst 4760 4857	For instance, low to moderate doses distort perception that is enough to cause traffic accidents.
T44	ArgumentAgainst 4858 4977	In fact, study shows that weed is one of the most prevalent illegal drugs found in fatally injured and impaired drivers
T45	Sentence 4979 5001	3. Gateway Drug Status
T46	ArgumentAgainst 5003 5149	Addiction treatment specialists believe that weed is a gateway drug that potentially introduces users to more serious illegal substances of abuse.
T47	ArgumentAgainst 5150 5244	Research even suggests that its use might be linked to a higher risk of prescription drug use.
T48	ArgumentAgainst 5245 5418	A group of teenage boys who abused weed, along with alcohol and cigarettes, were found to be 2-3 times more likely to abuse prescription drugs when they become young adults.
T49	ArgumentAgainst 5419 5561	Marijuana legalization, then, could increase societal and financial costs for the treatment of those introduced to heavier drugs by smoking it
T50	Sentence 5563 5610	4. Increase in Stoned Driving and Related Cases
T51	ArgumentAgainst 5612 5791	Contrary to what is stated above, marijuana use cannot be considered a truly “victimless crime”, knowing all the crimes that might be committed when users are under the influence.
T52	ArgumentAgainst 5792 6009	While drunk driving is a major problem in society, despite all the stiff penalties and education, equally dangerous is “driving high”, but this is more difficult to detect, unless the user has been smoking in the car.
T53	ArgumentAgainst 6010 6120	Other harder crimes, such as robbery and rape, can also be caused by the lapse in judgment due to smoking weed
T54	ArgumentAgainst 6122 6189	5. Increased Chances of the Drug Falling into the Hands of Children
T55	ArgumentFor 6191 6303	Even unhealthy items that are legal, such as alcohol and cigarettes, are prohibited from being sold to children.
T56	ArgumentAgainst 6304 6516	The primary reason for this is because children, in general, do not exhibit the same responsibility, reasoning and judgment of adults, and their bodies are not as equipped to handle the intake of such substances.
T57	ArgumentAgainst 6517 6628	The problem would be even worse for marijuana use, which can deal serious blows to developing minds and bodies.
T58	ArgumentAgainst 6629 6721	So, any time marijuana use is made legal, the drugs accessibility to children will increase.
T59	ArgumentAgainst 6722 6893	While all too often children and teenagers are getting their hands on cigarettes and alcohol, it is stressed that authorities should think twice about legalizing this drug
T60	Sentence 6895 6939	6. Danger of Second-Hand Smoke to Bystanders
T61	ArgumentAgainst 6941 7211	The dangers of second-hand smoke from cigarettes are well publicized, so common sense would just tell us that more widespread use of marijuana will increase the likelihood that other people would suffer the same damage of inhaling smoke, and this time from a heavy drug.
T62	ArgumentAgainst 7212 7271	Public places, such as bars, would expose innocent patrons.
T63	ArgumentAgainst 7272 7396	Even smoking weed at home will allow the drug to make its way to others, especially if it is a multi-level apartment complex
T64	Sentence 7398 7420	7. Damage to the Brain
T65	ArgumentAgainst 7422 7604	The humorous stories about pot users having a horrible memory are not an urban legend that is simply put forward by everyone, from Hollywood films to treatment centers for addiction.
T66	ArgumentAgainst 7605 7767	In fact, one study has shown that blood vessels in the brain of a marijuana smoker experience restricted flow, which can continue even after a month of abstinence
T67	Sentence 7769 7788	8. Poor Lung Health
T68	ArgumentAgainst 7790 7986	While tobacco and cigarettes have a nasty reputation for pumping carcinogens into the lungs, marijuana is estimated to have levels of carcinogens that are almost double of that from tobacco smoke.
T69	ArgumentAgainst 7987 8164	This is because many pot smokers tend to inhale more deeply than cigarette smokers, which increases the amount of time the lungs are being exposed to chemicals that cause cancer
T70	Sentence 8166 8198	9. Risk of Getting Heart Disease
T71	ArgumentAgainst 8200 8417	Marijuana use raises the heart rate from 20 to 100 percent for up to three hours after it has been smoked, increasing the risk of getting other health problems, such as arrhythmia, heart palpitations and heart attack.
T72	ArgumentAgainst 8418 8551	The effects on the cardiovascular system can make smoking weed a high-risk activity for seniors or for people with cardiac conditions
T73	Sentence 8553 8575	10. Poor Mental Health
T74	ArgumentAgainst 8577 8689	Research has discovered a link between marijuana use and mental illnesses, such as schizophrenia and depression.
T75	ArgumentAgainst 8690 8920	Though researchers are not sure yet if the drug really triggered such conditions or if smokers turned to the drug to mediate the symptoms on their own, but it is clear that smoking weed plays a role in the picture of mental health
T76	Sentence 8933 8998	The movement to legalize weed is already a reality in some areas.
T77	Sentence 8999 9278	Though it has it pros and cons, the important thing here is for local governments, judicial systems, law enforcement officials and addiction treatment specialists to work together to create communities that will be free from marijuana addiction and its other unfavorable effects.
