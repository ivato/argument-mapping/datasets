Did You Know?
Scientists created a working guitar the size of a red blood cell to illustrate the possible uses of nanotechnology. more...
get widget
This Day in History
August 27 , 1859 : The first successful oil well in the US was drilled. more...
get widget
Learn something new every day
More Info... by email
The pros and cons of cloning is a huge subject, which scientists and ethicists have not fully uncovered. One issue when discussing this matter is the different types of things cloned. People might see more benefit in cloning plants or animals than they do in cloning whole people, for instance.
Some pros of cloning plants are cited fairly often. For instance cloning could help reproduce plants that are more disease-resistant. Reproducing superior plants, especially those with nutritional superiority, could help address world hunger issues. Cloned plants also are more predictable, which could help save millions of dollars in farming costs, and plants near extinction could be saved through the right cloning programs.
Similar pros apply to the cloning of domesticated animals, like those providing food sources. Cloning could help produce superior food, create more disease resistant animals and address issues of world hunger. Rare animals might be saved from extinction, especially those animals that do not reproduce well in changing environmental circumstances.
Many people see fewer deeply conflicted pros and cons of cloning plants and animals, but there are some cons to consider. First, efforts to genetically engineer or fully clone plant and animal species could result in lack of needed DNA diversity. Diversity helps to improve survivability in the future, especially when unpredictable things come along. Scientists cannot predict potential development of viruses or other agents of destruction to which a cloned species might need to react in the future.
For example, perhaps scientists decide to clone all the rice in the world. They gradually produce only one type, much more nutritional than other kinds. Other rice is no longer produced and its DNA variants disappear. Some time in the future, a disease hits the rice crop and completely destroys it, and the world suddenly lacks rice.
This is perhaps the biggest “con” to cloning, and the one most frequently cited. Cloning can underestimate the possibility of the need for genetic variation in the future, under unpredictable circumstances. Similar issues could arise for cloned domesticated animals, particularly if they fully replaced animals that created genetic variation through normal breeding methods.
Another of the cons of cloning animals is potential cost. Presently, it is far more expensive to clone than to reproduce animals by other means. Failure rate remains high, though this is likely to be reduced, in addition to cost, if cloning is undertaken on a wider scale. Cost affects another of the pros and cons of cloning food supply animals. Some people feel great reluctance to eat cloned meat, which might lower the value of animals that are cloned.
Pros and cons of cloning humans are more complex. Potential benefits of cloning certainly exist. Among these is the possibility of cloning parts of humans, like vital organs to be used in transplants, which would be likely to nullify organ rejection issues. Some people feel that cloning humans unable to have their own children or who lose children at a very young age is among the “pros” of developing human cloning.
Cons include the methods for cloning, which when they involve fertilized embryos, are considered morally repugnant by some. Others feel that the idea of cloning humans is “playing God.” Another fear exists if people decide to genetically engineer super kids. What would happen to the average person not produced by cloning methods? This is fodder for legitimate scientific, legal, and ethical debate and plenty of science fiction films.
Cost is an issue too, and it would be hard to know if a health insurance company would pay for a person to have an organ cloned, or if these prices would be so prohibitive as to make the process unaffordable or available only to a few. Again, the issue of genetic diversity is an important one. Would cloning eliminate a gene or a piece of DNA today that doesn’t seem important, but might be in a different world some time in the future?
Discussions of the pros and cons of cloning are expected to continue with many people conflicted on what is the best way to proceed. Some cloning already exists, and more is likely to as science continues to develop this technology. Once cloning technology reaches its full capacity, humans will still be left with the question of when and how to apply it.
|
anon990635
|
Post 100
Honestly I don't know what to believe about cloning. Maybe it is a good thing and maybe it is not.
|
anon989912
|
Post 99
I feel as if some of you don't even do your homework before posting about a topic, in regard to this. I feel like cloning is a very good thing in the livestock side of things. As far as humans go, I don't think it is okay to clone somebody else. That is defying the rules of god. And most people who actually pay that large amount of money to clone an animal, most of them are show animals, so you wouldn't see an ounce of that meat. I also don't know one person that is going to spend the money that it costs to clone an animal just to butcher it.
|
anon950663
|
Post 89
I think we should clone animal organs, separately, so we don't have to kill them anymore.
|
anon942023
|
Post 87
If we lived in a perfect society where we knew the boundaries when it came to science, I would say that cloning is a miracle. The thing is, we are a degenerative race. We will eventually destroy ourselves because we don't know limits.
If we could stop at the point of cloning vital organs, cloning could be a spectacular advance in the medical field, but it is inevitable that it will go too far.
|
anon937276
|
Post 85
I think it is absolutely hilarious how diverse and extremely opinionated people are on here. Especially the posts that have nothing to do with cloning, and everything to do with their views on religion, which is fine, but just very off topic. I also think it's funny that a lot of people have brought up "super children" and "we are gods", yet no one has even mentioned Mary Shelley's book, "Frankenstein." It would definitely make you more confused, it relates to this so well.
Honestly, I don't think cloning could ever be right or wrong. It is tied between the number of pros and cons. So where do we draw the line you say? Probably when intentions are no longer
for the well being of others. I know that that probably doesn't help at all. But seriously, if you only clone to prevent world hunger, save an endangered species, or replace someone's liver, then you're in the right. I know there are problems that could arise, so focus on solving them. Don't try to bring back dead people, or go off on a Jurassic Park tangent; that's when bad things start to happen.
view entire post
|
anon925916
|
Post 83
I believe that we all know that there are many consequences while trying to clone another human or animal. Dolly the sheep wasn't the only animal clone. Noah the Gaur was an endangered species ox. You don't know what you are talking about - leave it to the professionals.
|
anon356391
|
Post 80
Cloning just means more mouths to feed.
|
anon350391
|
Post 79
Wow! This site was great for my eighth grade science debate on the pros of cloning. I do understand both the pros and cons of cloning, and I'm not sure if I'm for or against it myself yet, but with some more research it looks like I'm leaning more towards the for side.
Cloning could really help our world, and help us understand the power of it. We could definitely make changes to where it isn't as harmful when the animal and or baby is born if we get to know more about cloning. Reading up on cloning, has really put me on a different perspective about it. The article and comments helped me. Thanks!
|
anon335705
|
Post 77
Wouldn't the clones be just like us, invoking a potential torrent of activists that argue for clones to have autonomy? If we ever create clones, shouldn't our first priority be to make laws that govern clones?
|
anon330456
|
Post 74
@Post 41: Cloning is a bad idea I agree, but your IQ doesn't naturally change. You say you are in seventh grade and researching this for fun. Good for you. That means that you should be using your intellect to think. Cloning a human is a very personal and emotional decision, but that doesn't mean cloning itself is an abomination. We could end strife and suffering in the world with cloned plants to make mass production easier. We also could save endangered species, even revive extinct ones. Think on that a while.
|
anon329217
|
Post 73
Is cloning safe for the future or will they overrun us?
|
anon329211
|
Post 72
Intriguing. I'm surprised gene therapy was not addressed. It's also tailor made molecular treatment and has founded the practice of molecular medicine with secondary antibodies and 3D protein folding graphic technology.
|
anon327413
|
Post 70
I also see the pros and cons of cloning. I am undecided at this point but to everyone else, let people have their own opinion even if that includes what religion says. We all have our own mind and not everyone will agree on the same thing.
Yes, you may state your own opinion but do not harshly judge other people and say they are wrong. No one truly knows the answer to anything. We may have facts to support them, but we will never fully understand anything. Like Socrates said "I know that I know nothing".
|
anon282317
|
Post 65
Here's some more information to digest.
Whole body transplants of one head to another chimpanzee's body have already been carried out successfully in the 50's. They only lasted a few days, but the medical techniques have advanced quite a long way since then. The research was done to provide a possible surgical procedure to make quadriplegics mobile once again.
This same procedure could be used today to clone your head to a cloned body and you'd be doing handstands in your new 20 year old body in a few months.
The only remaining hurdle is the improvement of the already amazing nerve grafts that are already being used. Also, research into nerve regeneration for people with paralyzing injuries is going on at an ever accelerating rate since actor Christopher Reeve gave it a huge push before he died.
Immortality is just around the corner for those who can afford it.
|
Bolrog
|
Post 64
One word. Immortality. Another word. Ethics. Another word. Morality.
With or without a belief in God or gods, there will always be a yearning for immortality in humankind as a race. Also because of this yearning; there will be some who condone any means to that end: immortality. Others will recoil apprehensibly at any such idea.
As to the legal ramifications; there is no law on the books concerning legal rights of a cloned individual. There are court precedents though; a cancer cell line was developed and very profitable to a pharmaceutical laboratory and the lawsuit that the patient brought to bear was heard and the result? The cells belonged to the donor and were a part of
his own body to do with as he saw fit. The pharmaceutical company had to pay damages and fines as a result.
So, bottom line. You own all your cells in your body even if they are cloned in vitro. So, a clone is legally owned by the donor and can be used in what ever way he or she chooses.
view entire post
|
anon261858
|
Post 63
@post 54: Would their personality not change since the clone and the human who has been cloned do not have the same experiences as each other and would then continue on their own path in life as separate people. Like you said about the twins, they develop different personalities only because they live separate lives and have different experiences from one another.
|
anon258618
|
Post 60
I will pose this question. Why do we put the "religious" aspect on this? I believe you mean "Christian" because I have not heard any Buddhist or any other religious view. Why should we allow one religion to dictate the rules over everyone? Cloning should be based on scientific facts of the possible good and bad things that come from cloning. Just my two "sense."
|
anon248191
|
Post 59
Religion is not the issue here.
|
anon244280
|
Post 58
I'm not so sure what I think. I guess if I had to pick, I would not want to do cloning. I know it may help cure diseases and all, but if we've survived all these years without doing cloning, then it's really not mandatory.
|
anon242682
|
Post 57
Only God can breathe life into someone and these scientists are not God, and no, these cloned people will not have souls. They will be man made, not God made! God says for he even knew us in our mother's womb.
|
anon235966
|
Post 56
Cloning is the future. All you people who quote the Bible say it is wrong but we must advance.
|
anon205948
|
Post 54
This is for the person who said twins have different personalities, which is true, but we are talking about cloning, which is reproducing the same person, so that same person should have the same personality, that is, if personality comes only from genetics. But throw in environmental factors and experiences later on that the first did not experience, and that same personality could become a different one based on the stress caused from those issues. Anyway, it's the same personality from the start.
|
anon183966
|
Post 53
Economic PoV: Cloning is expensive, very expensive in fact. It costs $150,000 dollars to clone a dog. Sure plants may be (although not guaranteed) cheaper, but animals? Not by a long shot.
Genetic PoV: Sure, creating some super plant or animal sounds like a good idea now but when looked at over the years, it can be very risky. With no diversity, we loose our ability to adapt and evolve to our surroundings which could ultimately lead to mass extinction. Also Anon 34 brought up an excellent point when he mentioned that creating a bug-resistant plant(s) could lead to the extinction of that bug, causing a butterfly effect. Lets say we accidentally kill off bees. Everyone knows how
important bees are don't they?
Religious PoV: I find it funny when people say "We are Gods". God made "humans" in his "image" (pay attention to the quotation marks), he didn't make other gods. Let's say God (an apple) made humans (oranges) look like him (oranges that look like an apples). Just because we (oranges) look like god (apple), that doesn't mean we are made like God (we only have the apple's image, not its flesh). Last time I checked, I'm not omnipresent nor did I make the world in seven days (well six days, then rested on the seventh). There is also the question of whether clones receive souls or not. As far as I'm concerned, creating life should not be in the hands of humans. There is a good and a bad to everything.
Want an example? Physics to the atomic bomb.
To sum things up, I say just leave cloning alone. We survived without it in the past, we can survive without it in the future.
view entire post
|
mad256
|
Post 51
Cloning is wrong! Who can tell if the clones are going to take over the real person's life, by killing him? If you do clone you are playing god and he will be angry!
|
bchakalis9
|
Post 48
I think that cloning isn't something that should happen too often. I believe in the cloning of a particular plant to help with our farming needs. To help make a massive amount of crops and animals to help feed hunger. But you can't over due it.
Like the person who wrote this essay said, if you use cloning, and eventually get rid of the diversity, our unpredictability of viruses and bacteria evolving will come back and slap us in the face. I don't think it is right though to clone humans, or any other organism for a purpose other than food, (hopefully not a human for food) If a recent family member died, or is very sick, it isn't right
to bring them back, or even try to. If a family member is sick from a bad heart, and is close to death, you might think, If i clone her, I can take a heart from the clone, and put it in her. But technically, clones do have feelings, nerves, thoughts, and emotions. So you are going to save her, by killing her in the same equation. That doesn't make sense. Everyone should just let things happen as they do.
view entire post
|
anon165374
|
Post 46
i believe that there is a God, and i totally disagree with this cloning process, because animals and plants are naturally a part of nature and nature was created by God.
|
anon162914
|
Post 43
Great article. Tons of diversity on opinion. Cloning should be limited to therapeutic cloning rather than reproductive cloning.
|
anon158590
|
Post 42
Cloning can bring end to many plagues that are killing the people of our planet. Wouldn't God want us to save our people? Think about that 24 26 27.
|
anon149289
|
Post 41
Hmmm. I have to agree with person 39. God does say in the Bible that He created us in His image. Now, I have some questions for anyone who reads this to think about.
A) Would I be willing to give a cell for another me?
B) Will cloning disrupt the natural order of things?
C) Would this "clone" have a soul?
D) Would the clone do exactly what I did\do?
E) What are the benefits of cloning? Do I agree with them?
F) What are the cons of cloning? Do I agree with them?
G) Would the clone think exactly like me?
I believe that this would be an intensely personal decision, but if I had to give my
opinion, I would be on the border.
Now, I am just a seventh grade kid with the IQ of a 20 year old, researching this on my own time for fun. I want you guys to think about those questions and try to think on this subject for a while. Thank you for reading this.
view entire post
|
anon143347
|
Post 39
OK, I am doing a report on cloning and this essay helped me a ton! Thanks! As for me, I believe that God doesn't want us to clone because he says in the Bible that He created us in His image and we should not mess that up and if we start creating other human people we will be God and that is messed up because there is only one God! And other thing, will these cloned people have souls? I don't think so, but I am not sure. Someone please answer this question!
|
anon141907
|
Post 37
clones will still be unique. They may look the same and have the same DNA, but there fingerprint and personality will be unique. people are shaped around experience, and a clone's will be different from anyone else's. twins don't have the same personality.
|
anon137380
|
Post 35
If god created us in his image, and we are trying to create life in our own image then isn't that trying to play god? He made us for a reason, and that reason is not to repeat his mistakes. That is why we are here.
|
anon133867
|
Post 34
I am dead set against cloning. Sure, it has its benefits with ending world hunger by cloning plants. But one tiny mistake in human cloning, and we just ruined a person. Or an animal.
Genetic engineering sounds great, right? We could make plants resistant to pollution and bugs. But where will that take us? We will just make more insects extinct! We need to figure out a natural not life threatening to any animal or human safe way to end hunger. Why not gardens?
And if we don't have a house, and live in an apartment (like me), we could just grow plants in whatever containers we have from seeds of fruits.
|
anon119752
|
Post 32
cloning animals and plants is one thing, but when it comes to people, it's just gone way too far. When it comes to the human body there are just so many things that can go wrong with something like this. With so many diseases and disabilities in the world, do we really want to be the cause of more diseases and disorders?
|
anon119505
|
Post 31
If we lived in a perfect society where we knew the boundaries when it came to science, I would say that cloning is a miracle. The thing is,we are a degenerative race. We will eventually destroy ourselves because we don't know limits. If we could stop at the point of cloning vital organs, cloning could be a spectacular advance in the medical field, but it is inevitable that it will go too far.
We as human beings are curious and we like to take things to the point of no return. Sometimes this brings us benefits, but many times it cripples us. Human cloning is one of those times where I believe it will cripple us, and it could possibly
bring life to an end.
There are far too many risks. We risk overpopulation of a planet that is struggling to fuel it's inhabitants as we speak. We are using up everything on this earth and the number of people that it has to house continues to rise.
The sad thing is that death is a part of nature, and it is essential in a balanced world. Unless we wish to be the destruction of ourselves, cloning is something that must not be tampered with.
The worst part is, no matter how many warnings are given, they will be ignored. Unless cloning is ruled illegal, it will occur and there will be repercussions.
Science has many wonderful things to offer, but we need to know when to let something go. The day human cloning happens will be glorious for some and the day that human cloning takes over will be end for much more.
view entire post
|
anon116138
|
Post 29
I am doing a cloning debate in my class and i am against cloning. I need one point that is against cloning from the Bible and on point against cloning from scientists. If God created us in his own image and we are cloning people or anything then they (the clone) is created in the image of man or something else. I need more than that because i need to win this debate we are having. thanks.
|
anon103447
|
Post 26
This is for person 24. How do you know for sure there is no "god"? Have you searched every plane of existence, or died? If you're wrong, what happens to all of us? Do we just disappear? Where do you get your information that there is no "god"?
Personally, I see nothing wrong with cloning, as long as it's used in the right parameters. Clone extinct animals and body parts, yes. Super babies, not so much. With great power comes great responsibility. This was, is, and always will be true. We must tread carefully.
|
anon95732
|
Post 24
You are all just quoting the bible. There is no god to test us or try us. We are god now!
|
anon92637
|
Post 22
OK. 1) Thanks for this. Like a thousand other people, I'm doing a speech on cloning for school.
2)I have and will always be a Catholic, but I really don't have a problem with cloning. It's us, the people, who will determine what cloning is used for and when it's seems appropriate. If we can play our cards right, cloning might actually be a good thing.
|
anon89038
|
Post 21
i have opposing thoughts on the discussion that is being held. I find that both 12 and 20 have lot to offer but with extreme bias.
First. were i lean more towards 12's ideas i have to disagree with the get rid of religion altogether idea. There are so many people in the world that to do so is not possible. I am not of any faith myself, simply because of the fact that there are so many religions in the world with their own ideas of what's real or not and what/who god is.
The way i see it is that there can only be one right answer, if there is one at all. Also i feel people hide
behind their religion as something to blame their problems on or to explain the unexplainable.
I disagree with the statement made by 12 saying "religion has brought nothing of use to mankind but a belief in death." i disagree with this because death is not a belief, but a fact. What religion has brought was what they believe is the answer to life after death.
My view on that is that we are going to die so why waste our time living while worrying about the unchangeable outcome?
view entire post
|
anon88715
|
Post 20
For person 12 and his comment; here is a response. God puts evil on this world to test us, to build our own views, to have a voice in what we believe, to form ourselves in experiences and tests he puts in front of you to see how we do, and to be able to think for ourselves. he wants us to follow him.
He does not want harm for us; we are all his children. He wants us to learn right from wrong; good from bad. He has a plan for each of us. And some people ask me, 'then why do people die', 'why do we have devastating tragedies to the human race? That's evil; if he loves
us and wants us to be happy and follow him, why does He do these horrible things to innocent people?'.
God knows how our lives will turn out way before our moms knew they were pregnant. He knows everything; if you take a wrong turn he fixes it to make it a learning lesson.
God knows that if someone passes away, He knows they lived their life well and pleased Him. As for us who have to grieve with the loss, it may be hard, but something good always is the end of something bad.
So He doesn't do bad things; he puts people in our lives because they will teach us something that we may not know at the time or even ever know or realize.
You're right: God personally did not write The Bible but he had disciples and others who thought he was a fool; he was a joke; he was wanting attention; he was tricking everyone into believing he was the Holy One. But they told their stories, experiences, thoughts, they told the stories through generations of families, they wrote them and passed the pages down to their kids or grandchildren and so forth. But no the Bible was made through time from generations.
As for human cloning, think about this: if there’s another replica of another human the clone begins to develop different experiences from the second it opens its eyes. Which leads to different personalities. So although they may have the same blood, DNA, fingerprint, etc., they experience different obstacles and realities than you may have. So then they're not really a ‘clone’ anymore, they have their own mind and their own opinions.
And if they get in trouble with the law, you may be the one who gets blamed even though you didn't do the crime. So having human cloning may make this world a criminal scene or something. Also, isn't the U.S known for our diversity and our originality in how not one person is the same? Well if we allow this that won't be the case and we pride ourselves in that.
view entire post
|
anon88183
|
Post 19
humans need challenges in life to continue learning and survive and grow stronger. in a perfect world of cloning, the population would increase incredibly and every one would be bored to death.
|
anon86467
|
Post 18
To say that religion is a problem is a lie. If we are able to create clones, then god must have meant for it to be possible. If he didn't want cloning to be possible it wouldn't be possible at all!
|
anon84584
|
Post 16
I must first say one thing. I, like several people so far, am using this site as a source for a report that I am doing.
Having said that, anon80343 really seems to have missed the point. The debate on cloning should not be an excuse for an over-used and repugnant anti-religion tirade. I'm not religious, by any standards, but to completely miss the point of the discussion is just irresponsible.
There is a valid religious side to this debate and to unabashedly put any follower in religion down will only widen the growing chasm in this issue.
We must ask ourselves one important question before we decide to put our two-cents in: Can we discuss the issue in an intelligent and respectable manner? I'm not saying either side is any more correct than the other; what I'm saying is that for anything to get done, we must not hinder the lines of communication.
|
anon84299
|
Post 15
cloning livestock won't work too well, because growing them will still require energy.
|
anon81793
|
Post 14
I am doing cloning as a report, but I am supporting it. But I see both sides.
|
anon81037
|
Post 13
This subject is so interesting. A lot of people think it is bad but clones can save people. Also this helps a lot because I have to write a essay on cloning. Thanks to whoever wrote this!
|
anon80343
|
Post 12
Religion, religion, religion, science. Christianity is about 53 percent in the modern world so I'll focus on them. For those who look at most religions of the world. It's been many many years since the cross was waved from sea to sea and those who don't believe were left dead in its wake so if we are created in his image, replicas of god then playing god is us.
Playing god? We are god! We are the last of the titans, the most powerful of them all, but god feared us so much that he took away our godhood at a young age, leaving a hole in our heads and our lust for knowledge is so much that god is
gone. If God willing, but not able? Then he is not omnipotent.
If he is able, but not willing? Then he is malevolent.
If he both able and willing? Then why is there evil?
If he neither able nor willing? Then why call him God? god fears what he created (us). we make our world what we see fit and we change what must be changed we teach what must be taught.
science is like a bullet and any intelligent person would not stand in the way, but for the life of me I can't understand this. But why does religion, especially god faith religions, get in the way all the time and all the time science puts holes in religion and in responses?
It makes a new testament to counter the science to put god back in favor but through all its efforts the holy books are not made by god but man so dying and the age of religion is coming to an end vary fast so just let it die and all the better of it. for religion has brought nothing of use to mankind but a belief in death. It is a very, very powerful kind of influence and that is all they have nowadays.
view entire post
|
anon79392
|
Post 10
perfect website. i'm doing a report on cloning. thanks!
|
anon77528
|
Post 9
This is a really good essay. I am also on the fence about cloning. Genetic diversity being the biggest issue will probably stop cloning. Even if we clone the last animal of an endangered species, one disease will destroy all of them.
|
anon68891
|
Post 7
who would ever want the "perfect child"? i think that the human race has so many issues with our teens and children today, that we need to work with what we have now, not neglect it, and be trying to create a "better" human. that to me, is just sick and messed up.
if we have "perfect" people, then why do we even need to have people at all? life is about learning, and i believe so much learning comes from dealing with the troubles in your life, and coming out of them, hopefully stronger and wiser.
we will never have a perfect world. ever. and i am very thankful for that.
i do think that being able to solve
world hunger could be the best thing humans could do. but, i don't think that cloning is the answer to solving world hunger. i think if we are smart enough to be able to clone, we are smart enough to figure out a natural, less risky way to solve it.
view entire post
|
anon62831
|
Post 5
I must disagree with one topic in your article. I do not agree with the sentence that states that it is repugnant to use fertilized embryos. I believe that it is just a way of bettering our world.
What if someday we could find a way to clone a human and mix around some genes to make the "perfect" child! Think of what this world would become. Also, some people might think that cloning an animal embryo is animal abuse. But I think of it as a way of creating more livestock to end world hunger.
World hunger is "human abuse". Children are dying, adults and seniors are left to mourn their dead children while starving themselves. The cloned livestock would be a source of food for many hungry countries.
|
anon62828
|
Post 4
This article is really good. It was one of the only websites that I used for my research paper for school.
|
frankjoseph
|
Post 2
I'm totally on the fence about cloning; I mean, I see both the pros and cons of cloaning. On the one hand, I completely see the potentially _huge_ benefits that cloning can bring, on the other hand it freaks me out -- there's such potential for bad results if a person with bad goals or intentions gets a hold of it.
One of our editors will review your suggestion and make changes if warranted. Note that depending on the number of suggestions we receive, this can take anywhere from a few hours to a few days. Thank you for helping to improve wiseGEEK!