For First Time, Majority in U.S. Oppose Nuclear Energy
Story Highlights
- 54% of Americans oppose nuclear energy, 44% in favor
- First time in Gallup's trend that majority oppose nuclear energy
- Both major parties less likely to favor nuclear energy than in 2015
WASHINGTON, D.C. -- For the first time since Gallup first asked the question in 1994, a majority of Americans say they oppose nuclear energy. The 54% opposing it is up significantly from 43% a year ago, while the 44% who favor using nuclear energy is down from 51%.
Gallup asks Americans as part of its annual Environment poll if they favor or oppose the use of nuclear energy as one way to provide electricity. Each year from 2004 to 2015, a majority of Americans said they favored the use of nuclear energy, including a high of 62% in 2010.
In 2011, Gallup conducted its annual Environment poll a few days before the Fukushima nuclear plant disaster in Japan, and at that time, 57% of Americans were in favor of nuclear energy. The next time the question was asked in 2012, a similar majority still favored the use of nuclear energy.
And although there have not been any major nuclear incidents since Fukushima in 2011, a majority of U.S. adults now oppose nuclear energy. This suggests that energy prices and the perceived abundance of energy sources are the most relevant factors in attitudes toward nuclear power, rather than safety concerns prompted by nuclear incidents.
Lower gasoline prices over the past year are likely driving greater opposition toward the use of nuclear power. As Americans have paid less at the pump, their level of worry about the nation's energy situation has dropped to 15-year-low levels. This appears to have resulted in more Americans prioritizing environmental protection and fewer backing nuclear power as an alternative energy source.
Democrats and Republicans Less Likely to Favor Nuclear Energy
Republicans continue to be more likely than Democrats and independents to be in favor of nuclear energy. Still, support for the use of nuclear energy among Republicans and Democrats has declined in comparison to 2015. A slight majority of Republicans, 53%, are in favor of nuclear energy, down significantly from 68% last year. One in three Democrats, 34%, favor it, down from 42% in 2015. Independents' support is essentially unchanged from last year, but is down from the high Gallup found in 2010.
In previous years, as many as three in four Republicans were in favor of nuclear energy, peaking at 76% in 2009. The percentage of Democrats favoring nuclear energy also reached its high the same year, at 54%. Independents have typically been less likely than Republicans to favor nuclear energy but have usually been more likely than Democrats to favor it, particularly in recent years.
Bottom Line
Gas prices have been relatively low over the past year, likely because of the sharp decline in oil and natural gas prices and the apparent glut of oil around the world. This seems to have lessened Americans' perceptions that energy sources such as nuclear power are needed. The increased opposition to nuclear power does not seem to result from a fear of it, as there have been no major nuclear disasters anywhere in the world since 2011.
Nuclear power plants are expensive to build, often costing billions upfront, although they require relatively low maintenance costs once they are running. And nuclear energy has lower greenhouse gas emissions than other power sources, especially coal, so it is considered a clean provider of electricity. Still, nuclear energy is a bet that the cost over time of coal or natural gas to power an electric plant will be higher than the upfront cost of building a nuclear reactor. And at a time when oil prices are low, it seems Americans are not in favor of making that bet.
Historical data are available in Gallup Analytics.
Survey Methods
Results for this Gallup poll are based on telephone interviews conducted March 2-6, 2016, with a random sample of 1,019 adults, aged 18 and older, living in all 50 U.S. states and the District of Columbia. For results based on the total sample of national adults, the margin of sampling error is Â±4 percentage points at the 95% confidence level. All reported margins of sampling error include computed design effects for weighting.
Each sample of national adults includes a minimum quota of 60% cellphone respondents and 40% landline respondents, with additional minimum quotas by time zone within region. Landline and cellular telephone numbers are selected using random-digit-dial methods.
Learn more about how the Gallup Poll Social Series works.
Get Articles in Related Topics:
Gallup Daily
|No updates March 28. Next update March 29.|
|Real Unemployment||9.2%||-0.1|
|Gallup Good Jobs||45.1%||+0.1|
|Engaged at Work||35.4%||-0.1|
|Economic Confidence||18||+1|
|Consumer Spending||$81||-7|
|Trump Approval||46%||-|
U.S. leadership gets highest approval ratings, Russia lowest
Hillary Clinton's margin over Donald Trump in the national popular vote will be close to two percentage points, making the 3.3-point Clinton margin in the pre-election national poll average remarkably accurate.
Gallup http://www.gallup.com/poll/190064/first-time-majority-oppose-nuclear-energy.aspx
Gallup World Headquarters, 901 F Street, Washington, D.C., 20001, U.S.A
+1 202.715.3030