## Co-dependência semântica

Através de elementos coesivos e até como premissa inicial de nossa pesquisa, afirmamos que as sentenças não ganham significação isoladamente, mas em contexto: desta maneira as evidências e conclusões tornam co-dependentes semanticamente. Como uma continuação do conceito de correferência, que se apresenta como um elemento de conectividade formal (morfossintática e lexical) relacionada à coesão entre as sentenças, temos esta co-dependência como um elemento de coerência: constituído sobre conectividade de conteúdo (semântica e argumental).

Tome por exemplo os argumentos a seguir: 

>1a So what is responsible for this disconnect between popular opinion and medical reality? 
>
>1b A big part of it may have to do with the fact that marijuana today is much stronger than it was in previous generations. 
>
>1c The average THC level in today’s marijuana is approximately three times that of 1990, with some experts saying it’s up to six times more potent. (Marijuana legalization/15)
  

Julgando as duas sentenças isoladamente, tendo em vista a ausência de uma colocação clara na sentença 1a, que apenas apresenta um questionamento sobre as motivações entre percepção pública e objetividade médica, não teríamos 1a como parte constituinte do argumento em 1a:c, sendo rotulado como apenas uma sentença visto que é avaliado como mero artifício retórico. Contudo, ao analisar o mecanismo de chamada e resposta (question hook) e encontrar um posicionamento claro em 1b e 1c, 1a retorna para o rol das premissas; em outras palavras: quando vistos em contexto, exemplos como 1a tornam-se constituintes de uma premissa  devido ao seu contexto.

A dependência de sentidos demonstradas pela relação 1a→1b em nosso dataset é comumente rotulado como “group”, indicando este “sentido incompleto” em uma das sentenças isoladamente, apenas completando-o quando duas sentenças (ou mais) são vistas como um grupo.

## Agrupamento por equivalência 
Neste exemplo a seguir, faz-se o agrupamento de duas sentenças devido à sua equivalência de sentido: na prática, ambas sentenças dizem, essencialmente, a mesma coisa. 

### Exemplo
Linguisticamente, sentenças como as duas a seguir, que possuem equivalência de sentido são chamadas de paráfrase.
> “The volume of death represented by abortion is staggering."> 
> <p style="text-align:center">←Group (paráfrase)→</p>
> ”Abortion is killing on a genocidal scale.”

| T           | Large quantity of Abortions in the United states      |
|:-----------:|------------------------------------------------------ |
| I           |     Ø                                                 |
| Conclusão   | staggering/ genocidal scale (Is bad because is large) |


## Agrupamento por codependência
As codependências semânticas, como mencionado acima, resultam de uma incompletude de sentido de uma sentença isolada, fazendo com que ela busque um tópico ou uma conclusão em outra sentença para concluir seus sentidos. Devido a essa codependência é bastante comum que sentenças codependentes criem agrupamentos devido às incompletude de uma das sentenças envolvidas ou de ambas.

### Exemplo
No exemplo a seguir temos uma sentença inicial que se basta, afirmando que o risco de uma mulher de ter câncer de mama (tópico) aumenta após após um aborto(Qdat - dado/comentário sobre o tópico). No entanto, temos a segunda sentença afirmando que, caso este aborto seja feito na adolescênciam, os riscos aumentam: repare que, ainda que possamos inferir que "os riscos" se referem  aos "riscos de câncer de mama", não temos esta construção auto-susufiente na frase uma vez que o tópico completo não está presente.
Desta forma, a segunda aparição do tópico é dependente da primeira ao recuperá-la para especificá-la mais além. Desta maneira, a segunda sentença constrói uma relação de dependência da primeira e, portanto, formará um agrupamento

> (Qdat) A woman’s risk of developing breast cancer is 50% higher if she had had an abortion.
><p style="text-align:center">←Group (co-dependência)→</p>
> (Qdat) If the abortion is done in teenage years then the risk was 100% higher.

| Tópico                   | A woman’s risk of developing breast cancer| 
| :----------------------: | ----------------------------------------- |
| Indicador                | Ø                                         |
| Qdat                     | 50% higher if she had had an abortion     | 
| Tópico <br> (repetição)  | T + in teenage years                      |  
| Qdat                     | then the risk was 100% higher.            |

## Questões-gancho
Questões-gancho são construções com base em uma pergunta que funciona para enquadrar um assunto seguida por uma sentença que responde o tópico trazido a tona pela frase anterior. 

### Exemplo
Assim como podemos observar na questão-gancho a seguir, é possível observar como a questão, por si só, é capaz de trazer colocar resgatar o tópico-geral, sendo o uso da maconha, e coloca um tópico mais específico em voga: "a distorção na opinião pública (sobre o uso de maconha)". Entretanto, neste artifício retórico, apesar de criar-se a expectativa de uma conclusão, esta conclusão está ausente na pergunta, apenas sendo postulada na resposta ao afirmar que o tópico específico (distorção na opinião pública) deve-se a "retratação negativa pela mídia e ao fundo racial" relacionados ao tópico.
Desta forma, reconhecendo que a pergunta em si não declara uma conclusão, essa sentença sozinha fica deficiente e, ao mesmo tempo, ela serve como tópico para a sentença seguinte.
Sendo assim, **questões-gancho**, por conta de suas relações de dependência semântica, formarão **agrupamentos**.

> So what is responsible for this disconnect between popular opinion and medical reality?
><p style="text-align:center"> ←Group (co-dependência)→</p>
> The media's negative depiction and of it the paired with its racial background create a breeding ground for untruthful beliefs 
  
| **Tópico**                    | **Indicador** | **Conclusão**                  |
|-------------------------------|---------------|--------------------------------|
| ... is responsible for this disconnect between popular opinion and medical reality | So what/?         | Ø |

Da mesma maneira, encontramos a conclusão na sentença 1b. Faz-se possível afirmar que a codependência consistiria na deficiência de TÓPICO ou da CONCLUSÃO em uma ÚNICA sentença, tornando-a dependente de uma outra onde recuperará o constituinte que a falta. Delimitamos, em especial, duas configurações semânticas que necessitarão da relação de "group".
