## 3 - Rótulos (Entidades e atributos)

Nas [seção 2.4](schema.md#24-exemplos-de-evid%C3%AAncias-independentes-de-contexto), foram fornecidos exemplos de evidências independentes de contexto. Nesta seção, apresentaremos Conclusão Dependente de Contexto (CDC) e Evidências Dependentes de Contexto (CDE), ou seja, as suas classificações dependem do contexto que estão inseridas. Para um CDC, o contexto é um tópico e para as CDEs, o contexto é formado por um tópico e uma conclusão. 

## 3.1 Rótulos do estágio 0
O estágio 0, direcionado à segmentação e classificação de tópicos. Sendo assim, temos as subseções a seguir: 

### Rótulos de segmentação
Para a segmentação, o objetivo se torna recortar e subdividir os textos entre assuntos pertinentes, assim como é feito na imagem a seguir:

![alt_text](images/topic_segmentation.drawio.svg)

Repare que o texto inicial é separado entre dois subgrupos que recebem o rótulo de "Topic", sendo atríbuitos três rótulos de tópicos cada, o primeiro contando com a atributos _"Safety and security"_; _"research and science"_ e _"accidents"_ ao passo em que discursa sobre como o desenvolvimento tecnológico da energia nuclear se relaciona com questões de segurança e, subsequentemente, da relação de ambos com acidentes nucleares. Da mesma maneira, o segundo grupo recebe os atributos em comum _"Safety and security"_ assim como _"accidents"_ e, no entanto, como abandona o assunto do desenvolvimento tecnológico para se centrar na discussão de um acidente em específico, o atributo _"research and science"_ dá lugar ao atributo _"Fukushima"_ ao passo em que este acidente radiológico se torna o foco da nova seção. A mesclagem de tópicos forma um contínuo entre subtópicos que flui gradualmente para novos temas.

??? question "Contínuo de tópicos"
    ![alt_text](images/topic_segmentation_venn_diagram.drawio.svg)

Há de se observar que os tópicos comumente não são interamente abandonados nas viradas de tópico, e sim, percebe-se empiricamente que novos tópicos geralmente portam atributos remanescentes das seções anteriores, como um resquício e consequência do poder de coesão dos textos.

Note também que é frequente a manifestação de palavras relacionadas aos atributos de tópico, como está demonstrado na imagem pelas palavras em negrito, ou seja, palavras chave como _"Safety"_, _"develop"_; _"radiation"_ ou _"accidents"_ ou palavras relacionadas a atributos específicos são claros indicativos da necessidade de sua atribuição.

Em sequência, é perceptível como os trechos de fechamento de um tópico comumente tecem referência a sentenças anteriores, o que pode ser utilizado como indicativo do aparecimento e então necessidade de marcação de um novo tópico.

Outros aspectos formais do textos também assinalam a possível secção do texto como:
- Bullet points
- Estrutura de parágrafos
- Mudança nas palavras chave manifestadas

Para compreender a anotação desta função observe os rótulos pertinentes a seguir:

### Topic
O rótulo de tópico é destinado para detecção de introdução de novos tópicos ou a entrada em um subtópico mais específico 

|                    |               |
| :----------------- | :----------------------------------- |
| Topic              | A segmentação de tópicos visa detectar a introdução de subtópicos no texto, uma especificação do assunto principal que se apresenta. Ao visualizar a entrada de assuntos tangentes porém pertinente, por vezes marcado por bullet points, o anotador deve atribui o valor de "tópic" à sentença.  |
| Out-Of-Topic       | De maneira oposta, o atributo de "out_of_topic" pode ser atribuido a sentenças que, claramente, não falam sobre o tópico geral, simbolizando uma sentença "fora de tópico", informando que sentença seria irrelevante para o debate do tópico inicial. |

<TODO: dar exemplos de trechos para Out-Of-Topic. citar exemplos como do N00>


### Rótulos de classificação
Os rótulos de classificação são responsáveis por indicar o assunto do conteúdo nas partes segmentadas de um texto. Cada tópico contém cerca de 16 atributos de classificação baseados em pontos-chave. Estas classificações se subdividem ainda entre:

|||
|:-:|:-:|
|Palavras-chave|descrevem de maneira sintética um conteúdo relevante do texto. Estão marcadas na imagem a seguir com contorno laranja|
|Entidades nomeadas| referem-se a objetos do mundo real que podem ser nomeados. Indicam acontecimentos, pessoas, lugares ou instituições. Estão marcadas na imagem a seguir com contorno verde|

![alt_text](images/print_topic_entity_attributes.png)

Estes atributos de tópico deverão ser assinalados em todas as sentenças marcadas como "Topic", sendo possível a utilização de um a três rótulo, sendo desejado que se utilize sempre o máximo de atributos possíveis. Na ausência de qualquer rótulo cabível, há a possibilidade de indicação de um novo rótulo na seção de comentários como pode ser visto a seguir.

![alt_text](images/print_topic_entity_attributes_hiroshima_e_nagasaki.png)

Neste exemplo, podemos ver como o anotador, apesar de marcar os atributos "weapon" e "politics and ethics", considerou imprescindível a inclusão da _entidade nomeada_ "Hiroshima e Nagasaki" que, no momento, não se encontrava disponível dentre os rótulos. Para tanto, a inseriu na barra de comentários.

### Boas práticas
- É preferível que o anotador atribua sempre o máximo de rótulos de tópico a uma sentença. Ainda que a abordagem seja um pouco tangencial, o uso de mais rótulos assegura maior definição do tópico utilizado em uma sentença, assim como maior concordância entre anotadores.
- Caso um novo tópico tenha todas as classificações de tópico idênticas a um tópico precedente, é preferível que não haja a segmentação a não ser que, através desta, alguma nova rotulação possa ser atribuida.
- Na manifestação de um novo tópico no texto que, caso segmentado, geraria um nó de tópicos com nenhum argumento dentro de si, não é indicável a segmentação ou isolamento do mesmo, excluindo casos de absoluta necessidade.
- Caso haja correferências, evite separá-las. Marcadores de correferência são indicadores valiosos de continuidade de um tópico. Na duvida sobre segmentar um trecho ou não, confira se, nas sentenças logo adiante, não existem correferências ao tópico inicial que seria segmentado. 
- Priorize entidades nomeadas. Caso haja a possibilidade de rotulação com mais de três atributos de tópico, evite deixar de fora uma entidade nomeada.
- Evite ao máximo recorrer à seção de comentários para a classificação de tópicos. A barra de comentários deve ser utilizada apenas ocasionalmente como um "último recurso".

TODO: corrigir numeração nos títulos das subseções
## Rótulos do estágio 2
### 3.2 - Conclusão Dependente de Contexto (CDC)

De Levy et al (2014), apresentamos a seguir uma tabela com exemplos de Conclusão Dependente de Contexto (CDC):

|**Tópico** |The sale of violent video games to minors should be banned||
| :-: |---|--|
|**S1**| Violent video games can increase children’s aggression|**V**|
|**S2**|Video game publishers unethically train children in the use of weapons|**V**|
|**S3**|Violent games affect children positively.|**V**|
|**S4**| Video game addiction is excessive or compulsive use of computer and video games that interferes with daily life|**X**|
|**S5**|Violent TV shows just mirror the violence that goes on in the real world.|**X**|
|**S6**| Violent video games should not be sold to children|**X**|
|**S7**|“Doom” has been blamed for nationall covered school shooting <br/> Este candidato falha no critério da generalidade, uma vez que foca em um video game em específico.|**X**|

Tabela 3.1: Exemplos e contraexemplos de conclusões dependentes de contexto (CDC). Note nas setenças S1, S2 e S3, que as conclusões não precisam ser necessariamente factuais. A sentença S4 define um conceito relevante ao tópico e não exatamente uma conclusão. A sentença S5 não é relevante o bastante para o tópico. A setença S6 repete o tópico e, portanto, não cria um CDC válido. A sentença S7 poderia servir como CDE para um CDC mais generalista.

No nosso trabalho, referimos as CDCs como todos subtipos da superclasse Claim. Foram definidos na [seção 2.3](schema.md#23-entidades-para-anotar).

Cada Claim pode ter dois tipos de atributos, sendo as categorias de _SupportedClaim_, uma claim com suporte para suas conclusões ou _Rebuttal_, onde uma declaração inicial é contestada dentro da mesma sentença. Discorremos sobre estas funções a seguir.

### 3.3 - ComposedClaim
Supported claims se apresentam como sentenças mistas: sendo sentenças com conclusões secundárias; justificativas ou explicações dentro de si. Desta maneira, o rótulo SupportedClaim nomeia uma sentença composta, comumente, por uma conclusão e uma evidência, ou, então, uma conclusão principal e uma secundária.


|**Tópico** |The sale of violent video games to minors should be banned||
|-----------|----------------------------------------------------------|---|
|**S1**| Violent video games can increase children’s aggression|**Claim**|
|**S1 [com suporte (Claim)]**| "Violent video games can increase children’s aggression due to a recurrent and unrestricted exposition to gore and violence" <br/> <br/> Note que a segunda parte da sentença opera como um justificativa, ou, para nossa aplicação, uma conclusão ou uma __Claim__ secundária|**SupportedClaim**|
|**S2**|Video game publishers unethically train children in the use of weapons|**Claim**|
|**S2 [com suporte (Event)]**|"Video game publishers unethically train children in the use of weapons as it could be directly linked to the school shooting in Oregon last year" <br/> <br/> Neste caso, há a busca por um suporte em uma evidência anedótica, operando como um __Event__ interno|**SupportedClaim**|
|**S3**|Violent games affect children positively.|**Claim**|
|**S3 [com suporte (Qdat)]**|"Violent games affect children positively because many studies present a significant reduction of violent tendencies on teenagers after being provided with a way to vent." <br/> <br/> Neste caso, repare que a sentença busca suporte em um dado quantitativa, semelhante a um __Qdat__ interno|**SupportedClaim**|
|**S4**|"Violent video games can make violence seem banal."|**Claim**|
|**S4 [com suporte (Def)]**|"Violent video games can make violence seem banal as senseless utilization of certain content is the exact definition of banalization" <br/> <br/>Note que a segunda parte da sentença apresenta uma definição da banalidade apresentada na sentença original, assim, configurando um __Def__ interno.|**SupportedClaim**|

### 3.3.1 - SupportedClaim em uma única sentença
SupportedClaims poderão ser representadas em uma única sentença ao apresentar uma extrutura composta como supracitado. Observe como as estruturas PIC ou TIC são expressas nos seguintes exemplos:
![alt_text](images/print_supportedclaim.png)

#### Exemplo 1
No exemplo a seguir temos uma conclusão composta: primeiramente há a afirmação de que muitas mulheres procuram pelo aborto e que a maioria dessas mulheres têm uma motivação em comum. Em seguida, temos duas conclusões sobre essa mesma premissa de tal forma que a segunda conclusão constrói uma elaboração sobre a primeira: conclui-se que a motivação destas mulheres é o desejo de postergar maternidade e, como motivação para esta conclusão, postula-se que este atraso deve-se ao fato das mulheres serem ainda muito jovens. 
Observe, a seguir, a esquematização:

> (Spclf) One of the most common reasons why some women want to have an abortion is because they have the desire to delay childbearing because they themselves are young.” 

| Premissa      | One of the most common reasons why some women want to have an abortion |
| ----------- | ------------------------|
| Indicador   | Because                 |
| Conclusão 1 | Women want to delay childbearing |
| Indicador   | Because                 |
| Conclusão 2 | They are young          |

#### Exemplo 2
O exemplo a seguir, assim como o anterior, exibe uma estrutura de conclusões composta. Primeiramente afirma-se que as crianças planejadas teriam melhores chances na vida, conclusão esta que é justificada por uma conclusão secundária: estas melhores chances devem-se ao melhor preparo dos responsáveis sobre estas crianças.

> (Spclfv) “Planned children often have better life prospects because caregivers are better prepared to support the child physically, emotionally, and financiall”

| Tópico         | Planned children                  |
| -------------- | --------------------------------- |
| Indicador      | Ø                                 |
| Conclusão 1    | have better life prospects        |
| Indicador      | because                           |
| Tópico         | T1 + caregivers                   |
| Conclusão 2    | are better prepared to support T1 |

### 3.3.2 - ComposedClaim por agrupamento 
Assim como faz-se possível a construção de uma SupportedClaim em uma única sentença, no caso de haver o agrupamento de duas ou mais sentenças por codependência, também haverá a possibilidade de se construção de uma estrutura composta como se as sentenças, agora juntas, operassem como uma apenas.

#### Exemplo 1
Observando o exemplo a seguir, temos a premissa (afirmação com valor de verdade) de que os grupos pró-vida colocam-se como a favor de leis sobre o aborto mais restritivas, contudo, não se tem aqui ainda a declaração de um posicionamento, declaração esta que requisita a conclusão apenas exposta na segunda sentença ao se mencionar que __os fetus têm direito à vida__. Estrutura esta que justifica não somente o agrupamento das sentenças por codependência assim como seus status como como SupportedClaims.

> (Spcla) In the United States, pro-life groups favor greater legal restrictions on abortion, or even the complete prohibition of it.”
>  <p style="text-align:center">← Group →</p>
> (Spcla) “They argue that a human fetus is a human being with a right to live, so abortion is similar to murder.” 

| Premissa    | Pro life groups favor more restrictive laws on abortion |
| ----------- | ---------------------------------------------------------------------------------- |
| Indicador   | They argue that                                                                    |
| Conclusão   | Fetuses have the right to life                                                     |

Por SupportedClaim, consideramos como sendo uma superclasse dos rótulos SupportedClaimAgainst (SpClAg) e SupportedClaimFavor (SpClFv). A diferenciação entre os dois rótulos anteriores, é definido simplesmente pelo posicionamento a favor ou contra o tópico.

### 3.4 - Rebuttal
![alt_text](images/print_rebuttalclaim.png)

Claims com o rótulo de "Rebuttal" indicam refutações ou sentenças de contestação em que uma declaração inicial é colocada para, ainda na mesma sentença, ser impugnada ou desafiada por um argumento oposto. 

### Exemplo 1
Podemos observar no exemplo a seguir como o subtópico da "energia nuclear como fonte de energia limpa", declarando que esta poderia ser uma conclusão válida, contudo, logo após temos um ponto contra essa possibilidade, o problema de que a energia nuclear ainda produziria lixo nuclear, contradizendo a possibilidade inicial.

> (Claim [Rebuttal]) Nuclear power could be a source for cleaner energy but the production of nuclear waste counteracts this claim.

| Topic              | Nuclear power as clean energy                            |
| -------------------| -------------------------------------------------------- |
| Conclusão          | could be a source for cleaner energy                     |
| Indicador          | but                                                      |
| Conclusão contrária| the production of nuclear waste counteracts this claim   |


### 3.5 - Evidência dependente de contexto (CDE)
Como par das conclusões dependentes de contexto, teremos também as evidências dependentes de contexto. Em oposição às _Claims_, as evidência se identificam como sentenças com __dados__ que possam apoiar os argumentos postos no texto, mas, não seriam, em si, conclusões ou conteriam declarações de explicitas de opinião. 
![alt_text](images/print_Evid.png)

O rótulo _Evid_ contém quatro atributos possíveis: Definition, QuantitativeData, Event e EvidenceOther, os quais serão detalhados nos sub-itens a seguir.

!!! warning
    É obrigatória a indicação de um atributo de evidências. É vedado, no entanto, a indicação de mais de um dos rótulos acima na mesma sentença. 

### 3.5.1 - Atributo: Definition (DEF)

|**Tópico**   |Abortion||
| :---------: |---|--|
|**Conclusão**| Women have the right to choose what to do with their body|
|**S1**|By definition, an abortion is the conclusion of a pregnancy through  <br /> the removal of the fetus from the womb, which directly results in its death.|**V**|
|**S2**|Every person has an inherent and undeniable right over their own bodies, <br /> and as such, it is their choice to do with it as they please.|**V**|
|**S3**|The general principle is that no one can be compelled to invade his  <br /> body against his will.|**V**|
|**S4**|The body rights is the backbone for every argument relating to a person’s rights, <br /> such as the right to life and liberty, or the right to freedom of speech and expression.|**X**|
|**S5**|In 1973, the U.S. Supreme Court declared abortion a “fundamental right” in Roe v. Wade.|**X**|
|**S6**|Cases where complications in the pregnancy lead to significantly higher risk  <br /> to the woman’s life are more frequent than we wish they would be.|**X**|


Tabela 5: Exemplos e contraexemplos de definições dependentes de contexto: A sentença S1 é uma definição válida e suporta a conclusão pois associa o termo aborto contido no tópico. As sentenças S2 e S3 são paráfrases e implicitamente definem o direito ao corpo presente na conclusão. A sentença S4 não é uma definição, mas sim uma comparação entre o direito ao corpo com outros direitos. A sentença S5 não define um termo, na verdade é um evento. A sentença S6 tem relação com o tópico, mas não tem relação direta com a conclusão e também nem é uma definição.

### 3.5.2 - Atributo: Event (EVT)

| **Tópico** |Nuclear energy ||
| :-: |:-: |-|
|**Conclusão**| The use of nuclear energy poses an environmental risk|
|**S1**|On 6 August 1945 a single nuclear bomb devastated the Japanese city of Hiroshima.|**V**|
|**S2**|Environmental risk is the probability and consequence of an unwanted accident. Because of deficiencies in waste management, waste transport, and waste treatment and disposal, several pollutants are released into the environment, which cause serious threats to human health along their way.|**X**|
|**S3**|The Indian government used products from its 'civilian' nuclear research programme to produce a nuclear explosion in 1974|**V**|
|**S4**|Nuclear power was attractive to governments and state bureaucracies for several reasons|**X**|
|**S5**|When corporations are confronted with the environmental pollution, concern for profitability dictates that efforts will be made to merely clean up the mess, rather than change the structures responsible for the pollution|**X**|
|**S6**|However, by 1974 sections of the Australian Labor Party (ALP) and the public generally were becoming concerned with the hazards of uranium mining and the impact of mining on traditional Aboriginal communities.|**V**|


Tabela 6: Exemplos e contraexemplos de eventos dependentes de contexto: a sentença S1 é um exemplo de evento, pois tem um marco temporal e efeitos bem definidos. A sentença S2 define o que é risco ambiental contido na conclusão, mas não é um evento, portanto pode ser uma definição. A sentença S3 tem marco temporal, mas parece não ter um efeito definido, uma vez que utiliza ironia para ilustrar a contradição entre ‘uso civil’ e a explosão nuclear, portanto há um efeito implícito. A sentença S4 não possui um marco temporal e nem efeito claro. A sentença S5 apesar de conter a palavra ‘When’, ela só exprime causa e efeito, mas sem um marco temporal claro. A sentença S6 tem um marco temporal e causa e efeito bem definidos.

### 3.5.3- Atributo: Quantitative Data (QDAT) 

Dado quantitativo é um tipo de evidência que reforça sua conclusão associada, aumentando sua verossimilhança. Nessa categoria é comum citar dados estatísticos (percentuais, números, etc) ou comparações de valores (maioria, maior parte de, etc). Assim, a hipotese apresentada na conclusão ganha credibilidade através do fornecimento de uma noção quantitativa.

|**Tópico**| Guns control||
| :-: |-|-|
|**Conclusão**| Greater access to weapons increases the level of gun violence|
|**S1**|According to a survey by the Gun Violence Archive, the United States had, until May 2021, an average of three shooting attacks every two days. There were 194 records until May 10th.|**V**|
|**S2**|Studies which link gun laws to rates of gun violence only demonstrate a correlation or relationship. They do not establish any kind of causality.|**X**|
|**S3**|Cross-analyzing data on firearm deaths per State with number of firearm regulations per State shows that increasing regulations does decrease firearm deaths.|**V**|
|**S4**|Empirical evidence indicates that there is a strong and significant link between weak gun laws and high rates of gun violence.|**X**|
|**S5**|“The debate on guns is very much fueled by the issue of public massacres, but there is a distortion, because most homicides by guns do not happen in schools, workplaces, etc., and the fact is that none of these control measures of guns, for example, is done where most gun homicides are taking place,” says Professor Purdy.|**X**|s
|**S6**|According to Texas officials, it was 11:28 am when the truck the gunman was driving slammed into a ditch behind the school and exited the vehicle carrying an AR-15-style rifle.|**X**|


Tabela 7: Exemplos e contraexemplos de referências dependentes de contexto: a sentença S1 faz uma referência a uma pesquisa de uma entidade, citando dados numéricos. A sentença S2 versa sobre estudos relacionados, mas não envolve dados quantitativos. A sentença S3 não cita números diretamente, mas faz uma análise da correlação entre dados quantitativos. A sentença S4, apesar de citar um dado quantitativo - high rates of gun violence - envolve principalmente dados qualitativos como parte da evidência - weak gun laws. As sentenças S5 e s6 não apresentam dados quantitativos. 

### 3.5.4- Atributo: EvidenceOther
Este atributo é reservado para qualquer sentença de evidência que não se encaixem nos outros três atributos listados acima. Frente às evidências tipologicamente heterogêneas de nosso dataset, este se torna um atributo "guarda-chuva", para as evidências excedentes aos outrous três tipos listados e mais comuns.
Selecione este rótulo através de uma estratégia eliminatória. 
