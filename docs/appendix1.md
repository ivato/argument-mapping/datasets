## Anexo I - Correferência

A correferência consiste na referência ao mesmo índice (pessoa ou entidade do texto) por duas ou mais frases nominais diferentes.
Neste contexto, a referência a uma mesma "coisa" em duas circunstâncias no texto representa uma correferência, seja essa referência tecida por um substantivo, artigo, pronome ou expressão de antecedência ([ver anáfora](##Anáfora)) ou consecutividade ([ver catáfora](##Catáfora)).
 
 A árvore sintática a seguir demonstra, em uma sentença, a funcionalidade comum de uma correferência:
![alt_text](images/syntaxTree_1a.svg)

A título de exemplo, a sentença __S__ com o sujeito "Many women" (muitas mulheres), se refere novamente a elas com o pronome __THEY__ fazendo de NPi (Many women) o índice da sentença e do pronome __THEY__, em NPi-N, um correferente de NPi. 

Uma das consequências da correferencialidade, como pode ser visto, é a perda do sentido do correferente. No exemplo dado, como previsto, __THEY__ perde completamente o sentido sem o NPi (Many women) 

Outro aspecto comum das correferências é a concordância, feminino ou neutro) e pessoa (primeira, segunda ou terceira pessoas), o que também pode ser vizualizado no exemplo dado, como é possível ver a diante:

|            | Número | Gênero | Pessoa    |
|------------|--------|--------|-----------|
| Many women | plural | neutro | 3ª pessoa |
| They       | plural | neutro | 3ª pessoa |


## Anáfora
Correferências por anáfora se constituem como uma referência a um tema anterior no texto, como é possível ver abaixo:

> Unsafe abortions occur when they are performed by individuals without adequate medical skills or in an unhygienic environment. __SUCH__ situations occur globally, and result in about 70,000 maternal deaths and 5 million maternal disabilities per year

Como visto, a expressão "SUCH" reintroduz o tema abordado na primeira sentença, voltando a se referir a "Unsafe abortions". Neste caso, "SUCH situtations" é um correferente de "Unsafe abortions". Para testar a validade da correferência, substitua o índice pelo referente: note que ao trocar "SUCH situations" por "Unsafe abortions", o sentido da sentença é mantido.

Anáforas podem ser comumente introduzidas por expressões de antecedência, como exemplificado a seguir:

- As such
- Such as 
- Per se
- And so
- As well

Mais exemplos de antecedência e expressões "pró-forma" podem ser vistos neste [link](https://en.wikipedia.org/wiki/Pro-form#:~:text=In%20linguistics%2C%20a%20pro%2Dform,is%20recoverable%20from%20the%20context.)

<details>
<summary> Antecedência </summary>
Antecedência, em gramática, são elementos que "ligam" orações seguintes a índices já mencionados. 
</details>

## Catáfora 
Como contraparte da anáfora, expressões catafóricas antecipam um tema a ser abordado posteriormente no texto, como no exemplo abaixo:

> To obtain this visa, the worker has to comply with the   __FOLLOWING__ requirements: to hold a passport valid for more than 6 months

Catáforas podem, da mesma maneira, ser introduzidas por expressões comuns:

- Following
- As follows
- Hence
- Thus
- In sequence
- Then

As correferências podem ser aplicadas de três formas comuns, como delimitado abaixo:

### Nominalização
A repetição do índice ou utilização de sinônimos, hiperônimos ou hipônimos pode ser uma forma de utilização da correferência, como pode ser observado a seguir:

> __POODLES__ have become one of the most famous breeds through the past years. __ANIMALS__ like these are always beloved by families due to their gentle nature. 

<details>
<summary>Hiperônimos e Hipônimos</summary>

<p>Hiperônimo é uma relação hierárquica em que um item está contido em um conjunto maior. Como, por exemplo, a troca de qualquer nome nos conjuntos azuis por Dog.</p>

<p>Hipônimos se referem a sinônimos mais específicos, contidos pelo índice inicial. Como, por exemplo, seria a correferência a um cachorro (dog) chamando pelos nomes nos conjuntos azuis abaixo</p>

<img src='images/vennGraph_hipernym.drawio.svg' alt='Venn diagram of hipernym relation'/>

</details>

### Pronominalização
O mesmo mecanismo de repetição pode ser encontrado com o uso de pronomes no lugar do nome em si, como se observa no seguinte exemplo:

> In the United States, __PRO-LIFE GROUPS__ favor greater legal restrictions on abortion, or even the complete prohibition of it. __THEY__ argue that a human fetus is a human being with a right to live, so abortion is similar to murder.

- Note que o pronome "THEY", na seguda sentença, se refere ao sujeito "PRO-LIFE GROUPS" da primeira sentença, criando, desta forma, uma correferência ao sujeito.

O mesmo pode ocorrer com pronomes demonstrativos (This/That/These/Those):

> The truth: Hitler said that if you tell a lie often enough, loud enough and long enough, people will believe it. __THIS__ seems to be the tactic used by the pro death crowd, particularly in these "reasons" for abortion

- O demonstrativo "THIS" não recupera apenas o sujeito, mas o argumento por completo da sentença neste caso. 

### Tabela de marcadores
A correferencialidade regularmente conta com expressões específicas que as estruturam no texto, como poderemos ver a seguir:

<table>
  <tr>PRONOMES</tr>
  <tr>
    <th>Subject pronouns</th>
    <td>I; You; He; She; It; We; They</td>
  </tr>
  <tr>
    <th>Object pronouns</th>
    <td>Me; Him; Her; Us; Them</td>
  </tr>
  <tr>
    <th>Demonstratives</th>
    <td>This; That; These; Those</td>
  </tr>
</table>

<details>
<summary> Pronomes </summary>
Em especial quando colocados em inícios de sentenças e em referências a índices estabelecidos em sentenças anteriores, os seguintes exemplos podem ser marcadores de correferência.
</details>

<table>
    <tr>ANAFÓRICOS</tr>
  <tr>
    <th>Exemplificação</th>
    <td>For example; For illustration; As such; Such as; E.g</td>
  </tr>
  <tr>
    <th>Adição</th>
    <td>Like; likewise; Moreover; Additionally; Also; Furthermore; And so; As well; Also</td>
  </tr>
  <tr>
    <th>Similaridade</th>
    <td>Similarly; Like; likewise</td>
  </tr>
   <tr>
    <th>Ênfase</th>
    <td>Indeed; Certainly; Undoubtedly; Granted</td>
  </tr>
  <tr>
    <th>Retorno</th>
    <td>Still; Nevertherless; Even so; Again</td>
  </tr>
</table>

<table>
    <tr>CATAFÓRICOS</tr>
  <tr>
    <th>Sequenciação</th>
    <td>First; In sequence; Next; Following; As follows; As will be seen; Below</td>
  </tr>
</table>

<details>

<summary>Expressões</summary>

Via de regra, as expressões anafóricas se encontraram na última cadeia de sentenças se referindo às anteriores enquanto, as catafóricas estarão nas iniciais em referência de uma sentença vinda a seguir
</details>
