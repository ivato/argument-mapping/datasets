## 10 - Considerações Finais
Com este estudo, esperamos contribuir com o avanço da ciência na área de automação da diagramação de argumentos em grande volume de textos. O dataset anotado terá uma lista de todos os anotadores que contribuíram com o projeto e assim serão considerados autores. O trabalho será liberado por uma licença cc-by-sa de forma a aumentar o alcance e contribuir com o maior número de pessoas interessadas.

Para fins de garantir a privacidade dos anotadores, cada dado anotado terá a identificação do anotador anonimizada no dataset disponibilizado publicamente.

