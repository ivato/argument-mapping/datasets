## 4 - Relações

Com um conjunto de entidades anotadas, podemos rotular as relações entre elas. Seguem os 3 tipos de relações possíveis. 

**Convergência ou divergência:** Quando as entidades conclusões (ClaimFavor, SupportedClaimAgainst, SupportedClaimFavor   ou ClaimAgainst) e possuem mesmo posicionamento, diz-se que é uma relação _converge_ (convergência), caso contrário, é uma relação _diverge_(divergência).

**Agrupamento**: é um tipo de relação que agrupa entidades de mesmo tipo, geralmente evidenciada por uma relação de [co-dependência](appendix3.md#co-depend%C3%AAncia-sem%C3%A2ntica) ou [equivalência](appendix3.md#agrupamento-por-equival%C3%AAncia) semântica. A relação utilizada é _group_ (grupo) e na prática, agrega duas sentenças como se fossem uma única entidade agrupada, composta de uma ou mais sentenças. 


### 4.1 - Converge


|**Entidade de origem**|**Sentença origem**|**Entidade  de destino**|**Sentença destino**|
| :-: | :-: | :-: | :-: |
|**Ex1**|ClaimFavor|Life may take forms other than those currently understood by humanity, increasing the possibility of alien life.|ClaimFavor|The resilient and adaptable nature of life on Earth suggests that it can thrive under a variety of conditions. |
|**Ex2**|SupportedClaimAgainst|<p>If life existed elsewhere in the universe, then it should be obvious to humanity by now, even if solely evidenced by</p><p>second order effects.</p>|ClaimAgainst|Galactic resources have not been depleted in the way they would have been if complex alien civilisations existed.|
|**Ex3**|ClaimFavor|A UBI would improve the lives of many people.|ClaimFavor|A UBI promotes social justice  by improving the distribution of wealth, opportunities, and privileges within a society.|


Tabela 4.1: Quanto ao exemplo tem-se o exemplo de duas entidades ClaimFavor as quais convergem para ideia de existência de vida alienígena. No segundo exemplo ainda sobre o contexto de vida alienígena, tem-se a possibilidade do converge com as entidades SupportedClaimAgainst e ClaimAgainst. Já o terceiro exemplo, trata-se de convergência de ideias sobre a renda mínima universal de duas ClaimFavor.

### 4.2 - Diverge


|**Entidade de origem**|**Sentença origem**|**Entidade  de destino**|**Sentença destino**|
| :-: | :-: | :-: | :-: |
|**Ex1**|ClaimAgainst|Humans have not yet had any form of contact with aliens, despite repeated attempts to reach out to them.|SupportedClaimFavor|Governments and organizations often suppress information until the right time arises to release it, this may be occurring in the case of alien life.|
|**Ex2**|ClaimFavor|Being immortal would result in more net happiness.|ClaimAgainst|If we want immortality to happen because it results in more net happiness, then by the same standard, the mistreatment of others would be justified if the pleasure we receive outweighs the suffering of others.|
|**Ex3**|ClaimFavor|Immortality would be beneficial to human scientific and technological development.|ClaimAgainst|Being immortal, people may relax more knowing they have more time to accomplish their goals and reach their true potential.|


Tabela 4.2: Os exemplos fornecidos para esse tipo de relação foram pares de ClaimAgainst com SupportedClaimFavor, ClaimFavor e ClaimAgainst e ClaimFavor com ClaimAgainst.

### 4.3 - Group

Vide: exemplos de [co-dependência](appendix3.md).
