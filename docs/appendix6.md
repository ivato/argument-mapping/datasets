### Anexo VI - Curadoria de frases-chave
O conjunto final de frases-chave de cada tópico surge da seleção criteriosa de candidatos extraidos automaticamente. Neste dataset as ferramentas ChatGPT (60 candidatos), KeyBERT (40 candidatos)e Yake (40 candidatos) geraram ao todo 140 candidatos. Os candidatos extraidos foram agrupados, filtrados e selecionados conforme os padrões de qualidade contidos em Firoozeh(2019) conforme a tabela a seguir:

TODO: traduzir tabela 
| Quality Criteria for keywords set to classify text documents (Firoozeh2019) | :-:  | :-: |
|--------------|-----------------|-----------------|
| Criterios    |Descrição        | Explicação/Exemplos                                            |
| Exhaustivity | The set of keywords should cover all the subjects of the studied document, which have potential information value. This principle tends to add implicit keywords to the set of keywords that do not occur in the studied document but target a subject of it. | The number of keywords is sufficient to cover all subjects?|
| Especificidade| Keywords are considered as key elements of a document by contrast with other documents that could belong to other domains.| For instance, in legal documents, common law and disclosure statement are rather common terms and give little information on the content of the documents beyond categorizing them as legal ones.                                                                                                                    |
| Minimalidade  | Keywords should differ from each other. The set of keywords has to include unique keywords with different meanings.| In Figure 1, quality of service and service quality in the author-assigned keywords refer to the same meaning and so the set is not minimal.|
| Imparcialidade| Keywords should be as objective as possible by reflecting the informational content of the document without involving personal sentiment or opinion.| Does the set of keywords have a bias towards some subjects? For example, are the keywords balanced to label pro and con opinions in argumentative texts?|
| Representatividade| Keywords are considered as key elements of a document by contrast with other words or phrases, which reflect minor aspects| While keywords such as end-to-end quality of service and distributed real-time embedded systems in Figure 1 are representative, phrases like experiment, researchers, and resource availability would not represent the subject of the text.|
| Estruturação|  is important when the extracted elements are presented to humans. It is essential that keywords are well-formed words or phrases.| Truncated forms, such as emphasi instead of emphasis, incomplete noun phrases such as right to instead of legal right, onshore wind instead of onshore wind farm are prohibited forms|
| Inflexão| refers to the linguistic form of the keyword that should be retained.| Keywords appear in the text under inflectional forms, but only the form without inflection should be kept.|
| Conformidade| Each domain has its proper terminology, that is specific terms naming its concepts.| Thesauri and domain-specific terminologies provide lists of terms that are recommended to be used as keywords|
| Homogeneidade | Several synonymic forms are often available that refer to the same topic| such as wind farm and wind power farm in the renewable energy domain.|
| Univocidade| refers to the unambiguity of the keyword| In a specialized domain, keywords are expected to be less ambiguous than common words. For instance, in aeronautics, (flight) recorder always refers to the same type of electronic devices; any domain expert knows what the term means and prefers it to black box, which is more col- loquial but more ambiguous. |

A curadoria foi realizada por especialistas TODO: terminar

A tarefa de curadoria de frases-chave é composta por três subtarefas, as quais se ilustra no gráfico a seguir:
![alt_text](images/pipelineargmap.png) 

A somatória de todos candidatos foi primeiramente agrupada de acordo com suas proximidades semânticas em 32 clusters, destinando um 33º para frases-chave mal-formadas ou consideradas irrelevantes. Na segunda subtarefa, ocorre a filtragem dos 32 agrupamentos de frases-chave prezando pela minimalidade, representatividade e homogeneidade de um conjunto de 16 agrupamentos. Finalmente, na terceira subtarefa, escolhe-se uma ou duas frases-chave como representantes de seus agrupamentos, nesta etapa, se objetiva a estruturação formal (Well-formedness) e representatividade do termo final.  
