# Autores
* Marcelo Akira Inuzuka
* Romário Fernando Castro Azevedo Magalhães
* Alexandre Medeiros de Oliveira
* Victor Melo Brandão

# Instituição
* Instituto de Informática
* Universidade Federal de Goiás
* Information Visualization, Algorithms,Theory and Optimization Group
* http://ivato.inf.ufg.br

# Licença
* Creative-Commons-Share-Alike
* Copyright: Setembro 2022
