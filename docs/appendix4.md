## Anexo IV - Sumarização de sentenças
As sentenças ou agrupamentos de sentenças devem ser resumidos devido a um excesso de caracteres e/ou à quebra de sentido da enunciação construída. Desta forma, para que haja padronização dos elementos resumidos, o seguinte passo-a-passo deve ser seguido de acordo com essas duas situações possíveis.

### Excesso de caracteres
As sentenças ou grupo de sentenças, afim de manter compreensibilidade e concisão, devem obedecer um limite de 200 caracteres. Essa versão resumida, porém, deve buscar a preservação do sentido original das sentenças.

Tome por exemplo o seguinte grupo de sentenças de nosso dataset:

![alt_text](images/Fig_11.png)

Este agrupamento, com 246 caracteres, para além de se excessivamente complexo para seu uso em aprendizado de máquinas, criará uma tag ilegível no mapa argumental, ou, para nossos casos, gerará a seguinte mensagem:

![alt_text](images/Fig_12.png)
 
Para fins de manutenção de um modelo comum para a sumarização e para evitar esta complexidade excessiva, colocamos os seguintes passos:

- Retirada ou alteração de adjetivações:
- 
A retirada ou alteração de adjetivos, advérbios ou qualquer outra informação não-essencial  mostra se como uma forma simples de sumarizar sem a perda do sentido primário, tomemos o grupo de sentenças novamente:

> Unsafe abortions occur when they are performed by individuals without __adequate medical skills__ or in an __unhygienic environment__. Such situations occur globally, and result in about 70,000 maternal deaths and 5 million maternal disabilities per year

As duas partes em negrito (__"adequate medical skills"__ e  __"unhygienic environment"__) são exemplos que indicam a característica de __irregularidade__ ou __inadequação__ comuns aos profissionais e ambiente cirúrgico destes abortos insalubres. Ambas ideias podem ser, por exemplo, representadas por um adjetivo apenas, resultando no seguinte:

> Unsafe abortions occur when they are performed by __inappropriate individuals or inappropriate environments__. Such situations occur globally, and result in about 70,000 maternal deaths and 5 million maternal disabilities per year

- Agrupamento semântico
-  
Neste mesmo exemplo, como os indivíduos e ambientes cirúrgicos englobam uma situação, eles podem ser agrupados em uma definição comum, como é visto a diante:

> Unsafe abortions occur when they are performed in  __inappropriate circumstances__. Such situations occur globally, and result in about 70,000 maternal deaths and 5 million maternal disabilities per year

Agrupamentos semânticos, portanto, exercem uma alteração de termos por um terceiro que contenha todos os sentidos em si.

- Retirada de correferências e conectivos
-
Retomando a mais recente permutação de nosso exemplo, o trecho __"Such situations occur globally"__ exerce um papel conjuntivo entre as duas sentença o que, ao afirmar que as sentenças já forma um agrupamento, torna-se desnecessário para a visualização do correferência das sentenças, gerando a versão final que vemos abaixo.

> Unsafe abortions occur when they are performed in  inappropriate circumstances, ~~Such situations occur globally, and~~ __Resulting__ in about 70,000 maternal deaths and 5 million maternal disabilities __globally__ per year 

Retirada esta que tanto simplifica o entendimento da frase quanto permite que haja redução para menos de 180 caracteres.

### Quebra de sentido

Em demais casos em que o sentido da conexão das sentenças seja afetado por seu agrupamento ou retirada do contexto de um artigo, será cabível a sumarização associada a uma breve edição do texto para aperfeiçoar sua coesão. Tome por exemplo o seguinte agrupamento

![alt_text](images/Fig_13.png)

Agrupamento que resultará no seguinte nó argumental:

![INSERIR_NÓ_ARGUMENTAL_ABORTION13-46](INSERIR_NÓ_ARGUMENTAL_ABORTION13-46)

No momento de sumarização destas sentenças indica-se a colocação de conectivos para preservar sua conexão, como é possível ver a seguir: 

>``Abortions`` may affect the social life of a woman who makes abortions ``because``  their pears may disaprove of this action and can cause family problem or guilt may cause social withdrawal.
