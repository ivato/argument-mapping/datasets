## 9 - Perguntas e respostas
**Q1:** Todos os rótulos importados do dataset utilizado devem ser trocados?

**R:** Devem ser trocados todos os rótulos ArgF e ArgA, no entanto, os argumentos rotulados como Sent podem ser ou não trocados, sendo que isso cabe ao anotador analisar o contexto do documento e decidir se é pertinente a troca.

**Q2:** Comecei a anotar o documento e não consegui realizar a anotação. O que pode ser?

**R:** Nesse caso pode ter ocorrido duas situações: o anotador não estar devidamente logado e estar na branch selecionada para edição pode estar errada (por exemplo, argmap1 é só para leitura).

**Q3:** Tenho dúvidas se Claim é uma SupportedClaim, tem algo que possa auxiliar?

**R:** O anotador pode realizar uma consulta ao anexo II para verificar se existe algum um indicador de premise na claim a qual poderia induzir ao rótulo de SupportedClaim.

**Q4:** Os rótulos ArgA e ArgF importados do dataset utilizado devem ser convertidos, obrigatoriamente, em Claim?

**R:** Não, eles podem ser alterados para qualquer um dos rótulos listados na seção 2.1.2 .

**Q5:** O que fazer quando uma sentença possui claramente um evento, mas também na mesma sentença uma conclusão breve ou de cunho emotivo?

**R:** Essa estrutura é comum, ter um evento (EVT), seguida de uma conclusão que retoma o (sub)tópico, por exemplo:  "During the Second World War more than 60 million humans were ruthlessly killed: an outrageous brutality". Neste caso, a recomendação é rotular como SupportedClaim, apesar da função de evento ficar oculta.

**Q6:** O que fazer quando uma sentença for composta por um Qdat e um evento simultaneamente?

**R:** Neste caso, como o design da anotação não permitir aplicação de mais de um rótulo, a recomendação é utilizar o rótulo genérico EVID (evidence). Um exemplo deste caso seria: "Global agreements against nuclear weapons, after the bombing of Hiroshima and Nagasaki which killed over 250 thousand people,  were signed by many countries like the Treaty on the Prohibition of Nuclear Weapons on 20 September 2017"

**Q7:** Caso uma sentença possuir muito informação contextual - referência, autoria, url, lei, fonte, parte de um processo, etc - e esta possuir uma citação de terceiro, qual rótulo deve ser aplicado?

**R:** O nosso esquema de anotação abstrai ou ignora autoria ou referências, portanto, o que deve ser focado é a informação contida na sentença. Se a declaração ou afirmação é de terceiro, geralmente é delimitada por aspas. Por exemplo: On the last congress, President Biden said: **"no one has to raise a family in poverty."** He complemented **"We should lower your costs, not your wages."** - neste exemplo, deve ser considerado para fins de análise, somente as partes em negrito. 
