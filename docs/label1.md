O estágio 1 é direcionado à segmentação e classificação de tópicos. Sendo assim, temos as subseções a seguir:
![alt text](images/out_of_topic_N00.png)

### 3.1 Rótulos de segmentação

Para a segmentação, o objetivo se torna recortar e subdividir os textos entre assuntos pertinentes, assim como é feito na imagem a seguir:

![alt_text](images/topic_segmentation.drawio.svg)

Na imagem acima temos __grupos de sentenças__ que formam dois __segmentos de texto__.Repare que os segmentos recebem rótulos de "__Topic__", sendo atríbuitos três __termos-chave__ a cada, o primeiro contando com a atributos _"Safety and security"_; _"research and science"_ e _"accidents"_ ao passo em que discursa sobre como o desenvolvimento tecnológico da energia nuclear se relaciona com questões de segurança e, subsequentemente, da relação de ambos com acidentes nucleares. Da mesma maneira, o segundo grupo recebe os atributos em comum _"Safety and security"_ assim como _"accidents"_ enquanto abandona o assunto do desenvolvimento tecnológico para se centrar na discussão de um acidente em específico, desta maneira, recebe o atributo _"research and science"_ ao invés de _"Fukushima"_. A mesclagem de tópicos forma um contínuo entre subtópicos que flui gradualmente para novos temas.

??? question "Contínuo de tópicos"
    ![alt_text](images/topic_segmentation_venn_diagram.drawio.svg)

Há de se observar que os tópicos comumente não são interamente abandonados nas viradas de tópico, e sim, percebe-se empiricamente que novos tópicos geralmente portam atributos remanescentes das seções anteriores, como um resquício e consequência do poder de coesão dos textos.

A manifestação de __palavras relacionadas__ aos atributos de tópico, como está demonstrado na imagem pelas palavras em negrito, ou seja, palavras chave como _"Safety"_, _"develop"_; _"radiation"_ ou _"accidents"_ ou palavras relacionadas a atributos específicos são claros indicativos da necessidade de sua atribuição.

Os trechos de fechamento de um tópico comumente tecem referência a sentenças anteriores, o que pode ser utilizado como indicativo de marcação de um novo tópico.

Outros aspectos formais do textos também assinalam a possível secção do texto como:

- Bullet points
- Estrutura de parágrafos
- Mudança nas palavras chave manifestadas

Para compreender a anotação desta função observe os rótulos pertinentes a seguir:

### 3.1.1 TopicType: Topic

O rótulo de tópico é destinado para detecção de introdução de novos tópicos ou a entrada em um subtópico mais específico

|      |                                                                                                                                                                                                                                                                                                            |
| :---: | :---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------: |
| Topic | A segmentação de tópicos visa detectar a introdução de subtópicos no texto, uma especificação do assunto principal que se apresenta. Ao visualizar a entrada de assuntos tangentes porém pertinente, por vezes marcado por bullet points, o anotador deve atribui o valor de "topic" à sentença. |

### 3.1.2 TopicType: Ignored

|        |                                                                                                                                                                                                                                                                                                  |
| :-----: | :----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------: |
| Ignored | De maneira oposta, o atributo de "TopicType: Ignored" pode ser atribuido a sentenças que, claramente, não falam sobre o tópico geral ou elementos complementares da formatação do texto (como glossários e referências) apontando as sentenças como vazias de argumentação pertinente. |

Repare, no exemplo a seguir, como a autora inicia sua argumentação dentro do tópico de energia nuclear e, logo após, discursa e cria definições sobre um tema não correlato: feminismo e patriarcado. Não havendo conexões com o tema de energia nuclear, deve-se marcar trechos como esse como "TopicType: Ignored".

### 3.2 Rótulos de classificação

Os rótulos de classificação são responsáveis por indicar o assunto do conteúdo nas partes segmentadas de um texto. Cada tópico contém cerca de 16 atributos de classificação baseados em pontos-chave. Estas classificações se subdividem ainda entre:

|                    |                                                                                                                                                                              |
| :----------------: | :--------------------------------------------------------------------------------------------------------------------------------------------------------------------------: |
|   Palavras-chave   |                           descrevem de maneira sintética um conteúdo relevante do texto. Estão marcadas na imagem a seguir com contorno laranja                           |
| Entidades nomeadas | referem-se a objetos do mundo real que podem ser nomeados. Indicam acontecimentos, pessoas, lugares ou instituições. Estão marcadas na imagem a seguir com contorno verde |

![alt_text](images/print_topic_entity_attributes.png)

Estes atributos de tópico deverão ser assinalados em todas as sentenças marcadas como "Topic", sendo possível a utilização de um a três rótulo, sendo desejado que se utilize sempre o máximo de atributos possíveis. Na ausência de qualquer rótulo cabível, há a possibilidade de indicação de um novo rótulo na seção de comentários como pode ser visto a seguir.

![alt_text](images/segmentation_attributes.drawio.svg)

### 3.3 Maximização de concordância em classificação

A fim de recomendar uma anotação com maior concordância é necessário observar a maneira com a qual os itens as palavras relacionadas a determinados tópicos se apresentam em um trecho, tome por exemplo o seguinte parágrafo:

> "Some environmentalists are warming up to nuclear energy due to its low greenhouse gas emissions compared to fossil fuels. Nuclear power doesn't produce pollutants like sulfur dioxide and nitrogen oxides, and it offers reliable electricity unaffected by weather conditions. However, it's not entirely emission-free due to processes like power plant manufacturing and uranium mining. The debate continues over whether investing in new nuclear reactors is the most cost-effective approach to reducing emissions, with some arguing for alternative methods like efficiency measures and smaller-scale renewables."

O trecho anterior se inicia com uma sequência argumentos a favor e contra a adoção da energia nuclear da seguinte maneira:

|             |                                                                                                                                                        |
| ----------- | ------------------------------------------------------------------------------------------------------------------------------------------------------ |
| Arg Favor   | Some environmentalists are warming up to nuclear energy due to its low greenhouse gas emissions compared to fossil fuels.                              |
| Arg Favor   | Nuclear power doesn't produce pollutants like sulfur dioxide and nitrogen oxides, and it offers reliable electricity unaffected by weather conditions. |
| Arg Against | it's not entirely emission-free due to processes like power plant manufacturing and uranium mining                                                     |

Neste contexto, o autor primeiramente defende a adoção da energia nuclear dando como evidência que ela produziria menos poluição e que seria mais confiável, depois, atacando-a ao dizer que esta forma de produção energética não seria completamente livre de produzir poluentes. 

![alt_text](images/roda_de_rotulos.drawio.svg)

### 3.4 Regras de anotação

- Atributos de tópico devem ser atribuido à sentença inicial que introduz um segmento que, via de regra, possui mais de 5 sentenças. Cuidado: não atribuir rótulo de "Topic" frase a frase, os segmentos de texto devem ser atribuidos a __agrupamentos de sentenças__.
- Segmentos de texto devem possuir pelo menos uma sentença detectada como argumento. Ou seja, no segmento rotulado como 'Topic', deve haver pelo menos uma sentença rotulada como 'ArgumentFavor' (cor verde) ou 'ArgumentAgainst' (cor vermelha).
- De forma complementar à regra anterior, segmentos rotulados como 'TopicType: Ignored' não devem possuir nenhuma sentença rotulada como 'ArgumentFavor' ou 'ArgumentAgainst'.
- Os segmentos rotulados como 'TopicType: Topic' geralmente possuem mais de 5 sentenças. No entanto, existem exceções. Por exemplo, há situações em que segmentos de texto formando uma lista de argumentos, abordando vários tópicos (multi-tópico) e com cada item da lista com 2 ou 3 sentenças. Ainda não encontramos situações de segmentos com somente uma sentença.
- Os segmentos rotulados como 'TopicType: Ignored' devem possuir no mínimo 5 sentenças. Caso encontre segmentos com características apontadas na primeira regra, com quantidade de segmentos abaixo desse número, é necessário verificar se há correferências ao segmento anterior ou seguinte, caso exista, deve-se juntar os segmentos co-referenciados. Por exemplo, é comum ter segmentos no final de um segmento a fonte do texto, como por exemplo, "Fonte: Wikipédia". Nesse caso, o segmento que contém essa sentença deve ser juntado ao segmento anterior.

### 3.5 Boas práticas

- É preferível que o anotador rotule tópicos com __dois atributos__, utilizando um ou três atributos em casos de exceção.
- Caso um novo tópico tenha todas as classificações de tópico idênticas a um tópico precedente, é preferível que não haja a segmentação a não ser que, através desta, alguma nova rotulação possa ser atribuida.
- Na manifestação de um novo tópico no texto que, caso segmentado, geraria um nó de tópicos com nenhum argumento dentro de si, não é indicável a segmentação ou isolamento do mesmo, excluindo casos de absoluta necessidade.
- Caso haja correferências, evite separá-las. Marcadores de correferência são indicadores valiosos de continuidade de um tópico. Na duvida sobre segmentar um trecho ou não, confira se, nas sentenças logo adiante, não existem correferências ao tópico inicial que seria segmentado.
