## 1 - Introdução
Diversas áreas de conhecimento produzem textos argumentativos para suportar suas ideias: pesquisadores produzem artigos científicos; juízes publicam decisões judiciais; cidadãos defendem suas opiniões sobre propostas de projetos de lei; dentre outros. Estes textos essenciais ao avanço do conhecimento humano possuem um elemento em comum: o argumento, observado empiricamente expressando premissas que suportam ou atacam _conclusões_, peças centrais da argumentação que, não obstante, necessitam de _evidências_ ou outras premissas que a confiram validade [STAB e GUREVYCH (2014)]. No entanto, o avanço do conhecimento humano prescinde do revisão da literatura pertinente em busca do ‘estado da arte’. Levando em conta a velocidade da produção de documentos e urgência de resultados e um prazo para leitura é cada vez mais curto, a tarefa cresce em complexidade. Para fins de exemplificação, o conjunto de dados (dataset) [CORD-19](https://www.kaggle.com/datasets/allen-institute-for-ai/CORD-19-research-challenge) selecionou mais de 1 milhão de artigos científicos desde o início da pandemia de COVID (Dez/2019) até o momento (Jun/2022); significando que mais de 33 mil artigos científicos mensais em média foram produzidos durante o recorte. Com o objetivo de sistematizar e viabilizar uma visualização otimizada deste tipo de textos argumentativos não-estruturados é proposto a aplicação de técnicas de diagramação de argumentos na forma de mapa de argumentos, propiciando a compreensão e produção de insights para seres humanos. 

Este documento é um guia para a anotação de um conjunto de textos para a Diagramação de Argumentos, assim como a segmentação e organização de subtópicos textuais. A anotação visa em gerar exemplos sistematicamente fornecidos por humanos, a fim de gerar uma fonte adequada para o aprendizado computacional, conjunto de exemplos esse intitulado como conjunto de dados ou *dataset*. Assim,  o processo de anotação consiste na classificação de argumentos e segmentação de tópicos por anotadores humanos. A Fig. 1 ilustra uma estrutura de argumentos, com duas premissas (premises) e uma conclusão (claim), com relações de ataque (attack) e suporte (support) entre cada premissa e a conclusão.

![alt_text](images/Fig_1.png)
Fig. 1: Estrutura de argumentos, do manual (guidelines) de Stab and Gurevych (2014)

Para anotar textos eficientemente, faz-se necessária a utilização de ferramentas para a anotação, desta forma, elegimos os recursos [Brat](https://brat.nlplab.org/) e [Argdown](https://argdown.org/). A interface web _Brat_ fornece segmentação de textos em parágrafos com rótulos e relações entre si, permitindo edição graficamente observável. Na Fig. 2 é apresentado um exemplo de documento anotado com sentenças rotuladas bem como relações entre elas. TODO (colocar observação sobre fig 2 em forma de nota de rodapé)

![alt_text](images/Fig_2.png)

Fig. 2: exemplo de edição do rótulo de uma sentença na ferramenta Brat

Complementarmente, a sintaxe _Argdown_ fornece visualizações para argumentações complexas. A Fig. 3 apresenta uma visualização gerada pelo Argdown das anotações realizadas no Brat. Nosso estudo desenvolveu uma integração dessas duas ferramentas, a fim de que a anotação de textos do Brat seja apoiada pela visualização do Argdown. TODO (colocar observação sobre fig 3 em forma de nota de rodapé)

![alt_text](images/Fig_3.png)

Fig. 3: exemplo da visualização produzida pelo Argdown 


Para este estudo, aproveitamos o trabalho de Stab et al (2018) que produziram um *dataset* denominado *UKP Sentential*, no qual anotou sentenças argumentativas extraídas de 400 documentos distribuídos igualmente em 8 tópicos polêmicos (aborto, controle de armas, pena de morte, etc) aleatoriamente escolhidos em um portal de debates. Assim, cada tópico possui 50 documentos, que são em sua maioria páginas Web que foram recuperadas pelo mecanismo de busca Google e que possuíam arquivamento no portal [Wayback Machine](https://archive.org/web/). Neste *dataset*, cada sentença foi apresentada para os anotadores que os classificaram como argumento a favor (ArgFor), argumento contra (ArgAgainst) ou não-argumento (NoArg), assim realizaram uma tarefa que é denominada Busca de Argumentos (*Argument Search*). Neste estudo, vamos acrescentar um novo estudo, construindo duas novas camadas de anotação, a serem realizadas em etapas:

- Diagramação de Argumentos Monodocumento: consiste em criar um diagrama de argumentos (*argument diagram*) para cada documento, inicialmente para o tópico aborto. Cada um destes diagramas conterá unidades argumentativas (premissa e conclusão, por exemplo) e as relações entre elas (ataque ou suporte, por exemplo), formando assim uma árvore de argumentos (argument tree). Esta árvore representa a estrutura de argumentos do documento e possui o tópico localizado na raiz da árvore (tópico aborto, por exemplo). A Fig. 4 apresenta um exemplo de árvore de argumentos, no qual a raiz da árvore possui um Projeto de Lei como tópico.
- Diagramação de Argumentos Multi-documento: consiste em mesclar pares de árvores de argumentos, visando gerar um sumário geral de todos os documentos. Cada par de árvores é mesclado de forma a gerar um sumário, um mapa visual que tem como objetivo fornecer uma compreensão resumida e coerente das árvores. Uma vez que cada par de árvores gera uma nova árvore mesclada, no final, todas as árvores mescladas resultam em uma única árvore, que engloba e resume todos os documentos. 

![alt_text](images/Fig_4.png)

Fig. 4: Árvore de argumentos com um Projeto de Lei (PL5198/20) como tópico raiz


O resultado final da Diagramação de Argumentos Multi-documento é um super-árvore de argumentos, ou seja, um diagrama de argumentos geral que fornece uma visualização de qualidade que produz *insights* para humanos de forma completa, não-redundante, relevante e coerente de uma extensa fonte de informação. Na seção 2.4 serão fornecidas mais informações sobre como o anotador deve proceder para garantir uma visualização de qualidade.

O processo de anotação deste projeto envolve três estágios conforme descrito a seguir: 

- __Estágio 1__ - Segmentação e Classificação de Tópicos: consiste em duas tarefas, segmentação e classificação. A segmentação consiste na divisão do documento em segmentos de texto contíguos e coesos em um determinado assunto. A classificação consiste em aplicar palavras-chave que caracterizam os segmentos de texto. 
- __Estágio 2__ - Detecção de Componentes: consiste em detectar os componentes argumentativos de um texto, basicamente identificar conclusões e evidências bem como a relação entre elas.
- __Estágio 3__ - Mesclagem e Hierarquização de Componentes: consiste em mesclar componentes semanticamente equivalentes bem como ajustar a hierarquia de relacionamento entre eles.

TODO: Akira, criar diagrama para ilustrar parágrafo acima

Como o processo de anotação envolve vários colaboradores, é natural que cada um possua um certo nível de compreensão subjetiva das instâncias que serão classificadas. Assim é comum que ocorram divergências; no entanto, para a finalidade de aprendizado de máquina é necessário atingir um nível mínimo de *Concordância Entre Avaliadores* (Inter-rater Agreement), também conhecido basicamente por uma medida estatística denominada *Kappa*. Tipicamente o valor mínimo do Kappa aceitável é acima de 0,7 (Landis and Kock, 1977), cujo limite será monitorado durante todo o processo de anotação. O valor do Kappa pode ser afetado devido aos seguintes fatores:

- __1) Erros de anotação:__ podem ocorrer por ignorância, ambiguidade de instrução, imperícia ou falta de atenção. Para minimizar os dois primeiros fatores, será realizada uma *fase de treinamento* antes do início da anotação. A fase seguinte, conhecida como *fase de calibração*, tem como objetivo reduzir problemas de instruções ambíguas ou falhas, que podem ocorrer mudanças nas instruções do manual; esta fase é caracterizada por uma intensa resolução de dúvidas e ajustes. Após esta fase, as mudanças nas instruções do manual (guideline) serão mínimas para se evitar retrabalho e revisões de anotações anteriores. Depois das fases de treinamento e calibração, a equipe entra na *fase de acompanhamento* e o supervisor do processo acompanha erros de anotação causados por imperícia ou falta de atenção.

- __2) Subjetividade de anotação:__ esse algo normal e esperado. Um certo nível de discordância na classificação de cada informação é normal, pois cada anotador possui experiências e perfis diferentes; assim é esperado que ocorram divergências. Para produzir um resultado não-ambíguo, cada documento será anotado por pelo menos 2 colaboradores, sendo que a maioria dos votos é que vai prevalecer, podendo ser desempatado por um revisor final.