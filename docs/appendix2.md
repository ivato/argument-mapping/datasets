## Anexo II - Indicadores de Premissa e Conclusão

Abaixo listamos conectivos que regularmente encontram-se como sinalizadores de evidências ou conclusões. 
Observe que ambos poderão ser demarcados por indicadores em comum. Da mesma maneira, cada um destes indicadores estarão passiveis a serem aplicados de maneira anafórica ou catafórica (vide APPENDIX1_LINK) ao resgatar uma referência anterior ou antecipar algum argumento que está por vir.
Apesar dos indicadores evidenciarem as relações argumentais, estes não deverão ser levados ao pé da letra: devendo prevalecer o contexto e o sentido construído no texto.


### Indicadores de Claims (Conclusão)

|**Conclusão** |**Consequência**| Sumarização|
|---|---|---|
|so|as a result|to sum up|
|conclude that|hence|in short|
|thus|consequently| |
|we may deduce| | |
|in conclusion| | |
|points to the conclusions | | |
|proves that| | |
|therefore| | |
|may be inferred| | |

### Indicadores de Premissa (Evidências)

|**Contraste**|**Adição**|**Causa**|
|---|---|--|
|But|And|deduced|
|On the contrary|in addition|due to|
|However|besides|given that|
|Whereas|moreover|for|
||furthermore|since|
||what’s more|researchers found that|
|||indicated by|
|||is supported by|
|||this can be seen from|

### Indicadores para Ambos (Evidências e Conclusões)

Há indicadores que tanto podem sinalizar a presença de uma conclusão quanto de uma evidência. Geralmente anunciam uma exemplificação ou uma explicação, como poderemos ver a seguir:

|**Explicação**|**Exemplificação**|
|---|---|
|implies|for example|
|because|or instance|
|assuming that|as|
|in light of|as indicated by|
|in that|as shown|
|in view of|derived from|
|accordingly|follows that|
|clearly|entails|
|it should be clear||
|it is highly probable that||
|indicates that||
|it follows that||
|shows that||
|suggests that||

### Tabela de estrutura de indicadores

Geralmente uma premissa (ou evidência) aparece muito próxima a uma conclusão associada. Quem faz a associação entre as duas é o indicador, que pode vir no início ou nome das duas, formando uma ou duas sentença.

A tabela a seguir exemplifica diversos indicadores, coletados por Gao et al (2022) em um corpus de textos extraídos de artigos científicos. Os resultados deste trabalho, apontou 6 tipos de estruturas de argumentos mais recorrentes no texto:

-  PIC (P → I → C): uma sentença contendo premissa (P) seguida por um indicador (I) e uma conclusão (C).
- CIP (C → I → P): uma sentença contendo conclusão (C) seguida por um indicador (I) e uma premissa (P). <!-- TODO: add examples? -->
- IPC (I, P → C): uma sentença contendo um indicador (I) seguida de uma premissa (P) e uma conclusão (C). <!-- TODO: add examples? -->
- ICP (I → C → P): Uma sentença contendo um indicador (I) seguida de uma conclusão (C) e uma premissa (P). <!-- TODO: add examples? -->
- P.IC (P. I → C): Duas sentenças contendo uma sentença com uma premissa (P) seguida de um indicador (I) e uma conclusão (C). <!-- TODO: add examples? -->
- C.IP (C. I → P): Duas sentenças contendo uma sentença com uma conclusão (C) seguida de um indicador (I) e uma premissa (P). <!-- TODO: add examples? -->

Nesta mesma tabela, os 5 indicadores mais recorrentes para conclusão foram marcados em negrito (in short, assuming that, indeed, in conclusion e in fact), em ordem decrescente de frequência. Os 5 indicadores mais recorrentes para evidência foram marcados em destaque ou delimitado por '== ==' (indicating that, for these reason, so that, indeed e as a consequence), também em ordem decrescente de frequência.

### Estrutura com Indicador para uma sentença 
| **PIC (P → I → C)**             | **CIP (C → I → P)**                 | **IPC: I, P → C**                   | **ICP (I → C → P)**|
|---------------------------------|-------------------------------------|-------------------------------------|--------------------|
| P can cause C.                  | C, as indicated by P.               | As indicated by P, C.               | Here is why C: P.  |
| P demonstrates that C.          | **C, assuming that P.**             | As shown from P, C.                 | In support of C, P.|
| P guarantees that C.            | C, because P.                       | **Assuming that P, C.**             |
| P implies that C.               | C can be derived from P.            | Because P, C.                       |
| P indicates that C.             | C, considering P.                   | Convinced by the fact that P, C.    |
| P justifies that C.             | C, due to P.                        | Due to P, C.                        |
| P proves that C.                | C, due to the reason that P.        | Due to the reason that P, C.        |
| P signifies that C.             | C, follows from P.                  | Granted that P, C.                  |
| P suggested that C.             | C, for the reason that P.           | **In fact that P, C.**              |
| P, consequently C.              | C, giving that P.                   | In light of the fact that P, C.     |
| P, entails that C.              | ==C, if P.==                        | In view of the fact that P, C.      |
| P, from which it follows C.     | C, in view of the fact that P.      | Inasmuch as P, C.                   |
| P, in other words, C.           | C, insofar as P.                    | Now that P, C.                      |
| P, in that case C.              | C is based on P.                    | On account of the fact P, C.        |
| P, indicating that C.           | C is supported by P.                | On account of the reason that P, C. |
| P, indicating that C.           | C may be deduced from P.            | On the basis of P, C.               |
| P, means that C.                | C may be derived from P.            | On the grounds that P, C.           |
| P, resulting in C.              | C may be inferred from P.           | On the hypothesis that P, C.        |
| P, shows that C.                | C, on account of the fact P.        | Owing to P, C.                      |
| P, so that C.                   | C, on account of the reason that P. | Seeing that P, C.                   |
| P, thereby showing that C.      | C, on the basis of P.               | Supposing that P, C.                |
| P, therefore C.                 | C, on the grounds that P.           |                                     |
| P, thus C.P establishes that C. | C, on the hypothesis that P.        |                                     |
| P, wherefore C.                 | C, owing to P.                      |                                     |
| P, which allows us to infer C.  | C, seeing that P.                   |                                     |
| P, which implies C.             | C, since P.                         |                                     |
| P, which leads credence to C.   | C, supposing that P.                |                                     |
| P, which leads to C.            |                                     |                                     |
| P, which points to C.           |                                     |                                     |
| P, which shows that C.          |                                     |                                     |

### Estrutura de indicador para duas sentenças

| P.IC: P. I → C                     | C.IP (C. I → P)          |
|------------------------------------|--------------------------|
| P. Accordingly, C.                 | C. Its proof is that P.  |
| P. As a result, C.                 | C. The reason is that P. |
| P. As conclusion, C.               | C. This comes from P.    |
| P. Evidently, C.                   | C. This is shown by P.   |
| ==P. For this reason, C.==         |                          |
| P. From this it follows that C.    |                          |
| P. From this we can deduce that C. |                          |
| P. Hence, C.                       |                          |
| **P. In conclusion, C.**           |                          |
| **P. In consequence, C.**          |                          |
| **P. In fact, C.**                 |                          |
| **P. In short, C.**                |                          |
| P. In sum, C.                      |                          |
| P. In view of that, C.             |                          |
| ==**P. Indeed, C**==               |                          |
| P. Obviously, C.                   |                          |
| P. On this account, C.             |                          |
| P. One can conclude that C.        |                          |
| P. One can deduce that C.          |                          |
| P. One can infer that C.           |                          |
| P. Therefore, C.                   |                          |
| P. This is being so C.             |                          |
| P. This proves that C.             |                          |

### Premissa versus Tópico

No nosso caso, nem sempre encontraremos a mesma estrutura de Gao (2022) citada na tabela anterior. É bastante comum encontrar uma estrutura onde a premissa é substituída por um tópico, que é uma referência ao tópico geral. Geralmente é uma frase ou um trecho, que tem a função de contextualizar um sub-tópico, um enquadramento do que se quer falar sobre.  

Para a análise e compreensão desta incompletude semântica, observe a representação das seguintes sentenças (GAO, 2022)

>2a Abortions done later in life can offer more risks to the woman.

A sentença, para a construção de argumentos, possuirão, via de regra, três partes essenciais: o tópico (T) referente ao tema tratado; um indicador (I) que sinaliza uma relação lógica entre as partes da sentença e uma conclusão (C), apresentando a ideia final de um argumento, organizado o exemplo da seguinte maneira:

| **Tópico**                    | **Indicador** | **Conclusão**                  |
|-------------------------------|---------------|--------------------------------|
| Abortions done later in life  | can           | offer more risks to the woman. |

Nem todas sentenças, contudo, possuirão todos os três elementos (T; I e C) contidos em si, como é o exemplo de 1a, sendo notável que, ainda que ela traga o tópico da legalização do uso da maconha, ela contém, em si, uma conclusão, como vemos a seguir:

<table>
<tr>
  <th>T. I C</th>
  <td>(P. I → C)</td>
  <td> Duas sentenças contendo uma sentença com uma Tópico (T) <br/> seguida de um indicador (I) e uma conclusão (C).</td>
</tr>
<tr>
  <th>C. I T</th>
  <td style="margin: auto">(C. I → T)</td>
  <td> Duas sentenças contendo uma sentença com uma conclusão (C) <br/> seguida de um indicador (I) e um tópico (T).</td>
</tr>
</table>
