## Referências
CARDOSO, R.; GARATTONI, B.Energias renováveis devem superar o carvão já em 2025, diz relatório.**Superinteressante**, 2022. Disponível em: <https://super.abril.com.br/carbono-zero/energias-renovaveis-devem-superar-o-carvao-ja-em-2025/> . Acesso em: 28 de dezembro de 2022.

GAO, Yingqiang; et al. 2022. Do Discourse Indicators Reflect the Main Arguments in Scientific Papers?. In Proceedings of the 9th Workshop on Argument Mining, pages 34–50, Online and in Gyeongju, Republic of Korea. International Conference on Computational Linguistics.

GIVEN, Lisa M. (2008). The SAGE Encyclopedia of Qualitative Research Methods. Los Angeles: SAGE Publications. ISBN 978-1-4129-4163-1.

LANDIS, J. R.; KOCH, G. G. The Measurement of Observer Agreement for Categorical Data. Biometrics, v. 33, n. 1, p. 159–174, 1977. 

LEVY, Ran, et al. "Context dependent claim detection." Proceedings of COLING 2014, the 25th International Conference on Computational Linguistics: Technical Papers. 2014.

RINOTT, Ruty, et al. Show me your evidence-an automatic method for context dependent evidence detection. In: Proceedings of the 2015 conference on empirical methods in natural language processing. 2015. p. 440-450.

STABB, C. et al. Cross-topic Argument Mining from Heterogeneous Sources. Proceedings of the 2018 Conference on Empirical Methods in Natural Language Processing. Anais... Em: EMNLP 2018. Brussels, Belgium: Association for Computational Linguistics, out. 2018. Disponível em: <https://aclanthology.org/D18-1402>. Acesso em: 7 dez. 2021

STAB, C.; GUREVYCH, I. Annotating Argument Components and Relations in Persuasive Essays. Proceedings of COLING 2014, the 25th International Conference on Computational Linguistics: Technical Papers. Anais...Dublin, Ireland: Dublin City University and Association for Computational Linguistics, ago. 2014. Disponível em: <https://aclanthology.org/C14-1142>. Acesso em: 24 nov. 2021

TSENG, Tina; STENT, Amanda; MAIDA, Domenic. Best Practices for Managing Data Annotation Projects. arXiv preprint arXiv:2009.11654, 2020.
