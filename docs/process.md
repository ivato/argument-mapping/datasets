## 8 - Processo de Anotação


### 8.1 - Equipe

O processo de anotação de dados envolve uma equipe de anotadores, divididos em dois grupos: 

- Anotadores experts: são aqueles que possuem o domínio do processo de anotação de argumentos e eventualmente podem participar no processo de anotação. São responsáveis pelo treinamento, suporte e acompanhamento sob a coordenação de um supervisor.  Para os anotadores experts serão atribuídos 10% do dataset (40 documentos) para anotação por duplas e [adjudicado](https://medium.com/@jorgecp/the-adjudication-process-in-collaborative-annotation-61623c46b700) pelo supervisor. Anotações realizadas pelos experts geralmente passam por um processo de verificação em reuniões de discussão e portanto são consideradas padrão _gold_ ou simplesmente _anotações gold_.
- Anotadores contratados: são aqueles que foram recrutados e selecionados exclusivamente para anotação dos dados, podem ser voluntários ou não. Recebem treinamento e suporte para capacitação. No contexto do nosso projeto, serão realizadas duas anotações não-experts por documento, de não-experts  silver_ (geralmente na literatura são anotações realizadas automaticamente por algoritmos, não é o caso). 

### 8.2 - Fases de anotação

Uma vez que as equipes estiverem montadas, o processo de anotação seguirá quatro fases:

- Fase de treinamento: período em que os anotadores contratados recebem treinamento dos anotadores expert. Nesta fase é apresentado um plano, com prazos, desempenho e resultados esperados.
- Fase de calibração: nesta fase, há uma comunicação intensa em que os anotadores realizam anotações com a finalidade de validação do planejamento do processo. Nesta altura, o volume de dados anotados é baixo e os ajustes no esquema de anotação e no *guideline* são viáveis, pois o retrabalho de anotação ainda é pequeno.
- Fase de anotação: nesta fase, a comunicação é menos intensa e o ritmo de anotação é acelerado conforme se avança com a prática. Nesta altura, os ajustes no esquema de anotação e no manual de anotação são pontuais e não geram impactos de retrabalho. A fim de manter a qualidade do processo, as métricas de avaliação são monitoradas e reuniões de acompanhamento individual são realizadas.
- Fase de conclusão: nesta fase, a anotação de dados já foi finalizada e os resultados são divulgados e publicados.

### 8.3 - Avaliação de qualidade

A avaliação de qualidade de anotação é avaliada principalmente por meio de [medição da concordância entre anotadores (IAA)](https://towardsdatascience.com/inter-annotator-agreement-2f46c6d37bf3) através da métrica [Cohen's Kappa](https://en.wikipedia.org/wiki/Cohen%27s_kappa) ou simplesmente, kappa. Quando o kappa de um anotador é comparado com a anotação gold, chamamos simplesmente de _kappa gold_. Para cada documento, apenas duas anotações serão marcadas O kappa será utilizado em todas fases:

- Fase de treinamento: para se considerar que um anotador foi aprovado e está pronto para anotar, deve obter pelo menos 3 anotações independentes em um determinado estágio, com kappa comparado a anotação gold, acima de 0,7.
- Fase de calibração: nesta fase, o kappa silver e gold será monitorado para realizar ajustes neste guideline e também para alertar sobre não-conformidades com o guideline para os anotadores.
- Fase de anotação: nesta fase, o kappa gold é monitorado para alertar os anotadores quando houver alguma não-conformidade com o guideline.

### 8.4 - Boas práticas

A fim de manter a qualidade dos resultados do processo de anotação, é recomendável seguir algumas boas práticas:

- Comunicação assertiva e proativa: expressar de forma sincera e ágil sempre que encontrar problemas ou dificuldades. Para agilizar a comunicação, a equipe utilizará comunicação em canais individuais ou em grupo por meio do mensageiro instantâneo Telegram.
- Reuniões de acompanhamento: regularmente serão marcadas reuniões individuais ou em grupo para resolver dúvidas, apresentar alterações no manual de anotação, ajustes no processo de anotação e outras demandas de comunicação síncrona. 
