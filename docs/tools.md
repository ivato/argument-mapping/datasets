## 7 - Utilização das ferramentas
Para anotar os dados de forma eficiente, adotamos as ferramentas de anotação de textos [Brat](https://brat.nlplab.org/) e visualização de argumentos [Argdown](https://argdown.org/). Nesta seção, as operações básicas de cada ferramenta serão apresentadas. Para maiores detalhes, consulte a documentação específica de cada ferramenta.
### 7.1 Abrir um documento
Para abrir um documento, é necessário abrir o endereço da Web no qual o Brat foi instalado. No nosso caso, os documentos são organizados em coleções, divididos em tópicos e numerados de 00 a 49. Para selecionar um documento para anotar, você deve respeitar uma lista de prioridades que será apresentada na seção 4. A Animação 1 apresenta os passos 1 a 5 a seguir:

- **Passo 1:** abra o Brat acessando o seguinte endereço [https://argmap.inf.ufg.br](https://argmap.inf.ufg.br/login)
- **Passo 1:** efetue login 
- **Passo 2:** selecione uma coleção (inicialmente será texts2.1)
- **Passo 3**: selecione o tópico (inicialmente será abortion)
- **Passo 4:** selecione o documento disponível na fila de prioridades

![alt_text](images/Gif_1.gif)

Animação 1: Abrindo documento e efetuando login na ferramenta Brat.


### 7.2 Alterar rótulo
Para alterar o rótulo de uma sentença, siga os passos apresentados na animação 2, a seguir:

- **Passo 1:** selecione uma sentença
- **Passo 2:** clique duas vezes no rótulo da sentença
- **Passo 3:** escolha o novo rótulo da lista
- **Passo 4:** clique no botão Ok

![alt_text](images/Gif_2.gif)

Animação 2: Alterando rótulo de uma sentença


### 7.3 Atribuir relação
Para atribuir uma relação entre duas entidades, é necessário identificar um rótulo de origem, um rótulo de destino e uma relação. Na animação 3 é apresentado um exemplo de atribuição de relação de suporte (support) entre uma premissa (PRE) e uma conclusão a favor (ClFv). Seguem os passos necessários:

- **Passo 1:** altere o rótulo de uma sentença, ex: premissa.
- **Passo 2:** clique uma vez e mantenha pressionado no argumento de origem e arraste a seta de relação para o argumento de destino.
- **Passo 3:** selecione o tipo de relação e clique em Ok

![alt_text](images/Gif_3.gif)

Animação 3: Alterando uma relação entre duas sentenças


### 7.4 Visualização do diagrama de argumentos
Para visualizar o diagrama de argumentos resultante da anotação realizada no Brat, é necessário abrir outra aba com a visualização produzida pelo Argdown. A Animação 4 ilustra os passos a seguir:

- **Passo 1:** clique no botão **Diagram**

![alt_text](images/Gif_4.gif)

Animação 4: Como visualizar o diagrama de argumentos de um documento


### 7.5 Visualização em janelas lado a lado
Uma forma alternativa à visualização por abas é a visualização em janelas lado a lado. A Animação 5 demonstra como isso pode ser feito, conforme os passos seguintes:

- **Passo 1:** clique na aba da janela de visualização e selecione a opção destacar aba em nova janela
- **Passo 2:** ajuste as janelas lado a lado

![alt_text](images/Gif_5.gif)

Animação 5: Visualizando janelas lado a lado


### 7.6 Visualizando alteração
Para acompanhar o efeito de cada anotação realizada no Brat basta realizar uma alteração de rótulo, criar uma relação de convergência ou divergência para uma claim (claim ou supported claim) ou para uma evidência que esteja relacionada a uma claim (claim ou supported claim) direta ou indiretamente. A Animação 6 apresenta como isso é feito através dos seguintes passos:

- **Passo 1:** realize uma anotação no documento que modifique o diagrama

![alt_text](images/Gif_6.gif)

Animação 6: Visualizando alterações em tempo real


### 7.7 Aplicar zoom e arrastar
Quanto mais entidades e relações são anotadas, mais o diagrama de argumentos aumenta e se torna necessário aplicar operações de zoom ou arrastar para visualizar os detalhes ou a forma geral da estrutura de argumentos. A Animação 7 demonstra tais operações, executando os passos a seguir:

- **Passo 1:** clique duas vezes na janela de visualização para minimizá-la.
- **Passo 2:** clique duas vezes no diagrama para habilitar o zoom
- **Passo 3:** use o botão de rolagem do mouse para aumentar e diminuir o zoom
- **Passo 4:** clique e arraste a figura para melhor visualização da parte desejada

![alt_text](images/Gif_7.gif)

Animação 7: Aplicar zoom e arrastar em uma visualização de documento.


### 7.8 Atualização automática do diagrama
Na seção 3.6 podemos ver que o diagrama é atualizado automaticamente após uma anotação no documento que modifica o diagrama. Por padrão, a caixa de seleção da atualização automática (Automatic) está marcada. Mas também temos a opção de atualizar manualmente o diagrama executando os passos a seguir:

- **Passo 1:** realize uma anotação no documento que modifique o diagrama.
- **Passo 2:** desmarque a caixa de seleção Automatic.
- **Passo 3:** realize uma anotação no documento que modifique o diagrama e ou desmarque qualquer uma das outras caixas de seleção.
- **Passo 4:** clique no botão Refresh para visualizar as alterações no diagrama

![alt_text](images/Gif_8.gif)

Animação 8: Atualização manual e automática de diagrama


### 7.9 Visualização de sentenças agrupadas
Quando duas ou mais sentenças estão relacionadas com o rótulo group elas podem ser visualizadas como um nó único na árvore de argumentos. Por padrão, a caixa de seleção de sentenças agrupadas (grouped) está checada. Mas temos a escolha de visualizarmos como nós separados executando os passos a seguir:

- **Passo 1:** realize uma anotação de relação group no documento que modifique o diagrama se ainda não tiver feito.
- **Passo 2:** desmarque a caixa de seleção grouped
- **Passo 3:** se a caixa de seleção Automatic estiver desmarcada, clique no botão refresh.

![alt_text](images/Gif_9.gif)

Animação 9: Visualizando sentenças agrupadas e desagrupadas


### 7.10 Visualização de sentenças resumidas
Uma árvore de argumentos com muitos nós e sentenças grandes pode dificultar a visualização do anotador, então por padrão a caixa de seleção de sentenças resumidas está checada, tornando as sentenças menores. Mas temos a escolha de visualizarmos o texto das sentenças por completo.

- **Passo 1:** desmarque a caixa de seleção shorted
- **Passo 2:** se a caixa de seleção Automatic estiver desmarcada, clique no botão refresh

![alt_text](images/Gif_10.gif)

Animação 10: Visualizar sentenças no modo resumido ou extenso