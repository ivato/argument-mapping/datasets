## 2 - Esquema
Como foi dito anteriormente, a anotação é uma forma de expressar o conhecimento humano na forma de exemplos para aprendizado computacional. Neste estudo, o processo de anotar  consiste basicamente em atribuir uma classe a um exemplo. Além disso, cada classe também é chamada de *entidade* e o conjunto de classes formam um esquema de entidades. Assim, a especificação de um conjunto de classes e suas relações entre elas formam um esquema de anotação.

Para exemplificar, considere uma tarefa de classificação de um conjunto de imagens de animais. Segue um exemplo de esquema simples:

![alt_text](images/Fig_5.png)

Fig. 5: Esquema de entidades. Figuras sob licença CC By Heikki Siltala e domínio público, respectivamente.


Sabemos que _gato_ e _cachorro_ são _animais_, por isso podemos dizer que são subclasses ou classes específicas de animais. Neste caso, a classe genérica é animal, que por sua vez, poderia abranger outras subclasses como rato ou leão. Caso o conjunto de imagens seja na maioria composta de gatos e cachorros, não há necessidade de um esquema complexo. Neste esquema, ratos, leões ou outras exceções poderiam ser classificados simplesmente na classe animal e, portanto, poderiam não ser de interesse para os objetivos do aprendizado de máquina.

Outro conceito importante será a diferenciação entre entidades e atributos. Enquanto entidades nomeiam um objeto ou conceito que pode ser distinguido dos demais, criando uma unidade virtual assim como uma classe, os atributos conferem qualidades a eles. Como exemplo temos as duas frases a seguir.

> Essa pessoa é o João 
> 
> João é professor, é casado e é alto.

Para fins pragmáticos de nosso estudo, pessoa seria uma classe enquanto João seria uma _instância_ ou _entidade_ ao passo que um ente distinto. No mesmo interim, a classe pode ter vários atributos como profissão, estado civil ou altura, os quais João possui os atributos com valores de professor, casa e alto respectivamente.

Em nossa classificação um objeto pode possuir apenas uma classe ou subclasse e define uma relação de "É um", por exemplo João "É um" pessoa. O mesmo objeto pode ter vários atributos que o caracterizam em uma relação de posse por exemplo, João possui profissão como _professor_, estado civil como _casado_ e possui altura considerada _alta_.

![alt text](images/attributes_ilustration_man.drawio.svg)

Nas seções seguintes, será apresentado o esquema de anotação previamente desenhado experimentalmente. Para que o anotador faça uma boa classificação das sentenças que lhe serão apresentadas, é necessário o domínio do esquema de anotação.

### 2.1 - Introdução à anotação
Como foi dito, a anotação atual visa a diagramação de argumentos e aproveitamos o trabalho do dataset UKP Sentential que localizou sentenças argumentativas, o qual denominamos entidades importadas, explicadas detalhadamente na seção 2.2. A Fig. 6 apresenta o esquema de entidades aplicada na ferramenta Brat.

![alt_text](images/Fig_6.png)

Fig. 6: Entidades importadas e para anotar aplicadas na ferramenta Brat. 


A Fig. 6 apresenta uma sentença previamente rotulada como Sentence (SE). As intervenções possíveis na anotação são: manter como SE; alterar para Evidence ou alguma subclasse dela (Def, Evt ou QDat);  ou alterar para algum tipo de Conclusão (Claim) - ClFv ou ClAg.

Na prática da anotação, classificamos entidades e atributos, ou seja, aplicamos rótulos a objetos. Rótulos consistem em valores que aplicamos a entidades, atributos ou relações entre entidades. Desta forma, conseguimos identificar e classificar objetos e suas relações entre si por meio da rotulação. 

### 2.2 Entidades importadas

O dataset original UKP Sentential utilizou um esquema de 3 entidades: ArgFavor (argumento a favor), ArgAgainst (argumento contra) e NoArg (não é argumento). Para a finalidade do nosso estudo, importamos os dois primeiros rótulos e ignoramos o último. Uma vez que a anotação do dataset original não garantiu a cobertura de avaliação de todas sentenças de todos documentos, não foi conveniente importar os rótulos NoArg.  Desta forma, nosso dataset inicialmente terá todas sentenças previamente anotadas com os seguintes rótulos:

**ArgFavor (ArgF):** Sentenças rotuladas como argumentos que expressam uma posição a favor relacionada ao tópico em questão.

**ArgAgainst (ArgA):** Sentenças rotuladas como argumentos que expressam uma posição contra relacionada ao tópico em questão.

**Sentence (SE):** Sentenças que podem ser argumentativas ou não. A segmentação das sentenças foi realizada automaticamente por *software*.

Por convenção da ferramenta, as entidades possuem um nome longo e um nome curto. Os nomes longos ArgFavor, ArgAgainst e Sentence possuem os nomes curtos ArgA, ArgF e SE, respectivamente.

### 2.3 Entidades para anotar

Como todas as sentenças estarão inicialmente rotuladas como ArgA, ArgF e SE, o anotador terá que: 1) substituir cada rótulo antigo por outro novo: ClFv (conclusão favor), ClAg (conclusão contra), SpClFv (conclusão suportada a favor) e SpClAg (conclusão suportada contra), Evid (evidência), Def(definição), Evt (evento) ou QDat (dados quantitativos); 2) alterar o rótulo antigo (ArgA ou ArgF) por SE; ou 3) simplesmente manter como SE. A Fig. 6 lista as entidades disponibilizadas na ferramenta e a Seção 2.1.3 mostra uma sugestão de fluxograma de alteração de rótulos. As entidades do novo dataset são:

**ClaimFavor (ClFv):** sentença argumentativa que expressa posicionamento do autor ou de um terceiro a favor em relação ao tópico abordado.

**ClaimAgainst (ClAg):** sentença argumentativa que expressa posicionamento do autor ou de um terceiro contra em relação ao tópico abordado.

**Evidence (Evid):**  sentença argumentativa que suporta uma conclusão (Claim). 

As entidades apresentadas de forma resumida acima  serão detalhadas e exemplificadas nas seções seguintes.
### 2.4 Atributos de tópicos 

### 2.5 Aributos de Claims
Estes atributos podem ser dados a sentenças marcadas como ClaimFavor ou ClaimAgainst, sendo disponível apenas  a marcação uni-rótulo.

**SupportedClaim:** é um tipo específico de *Claim* que possui uma evidência  ou conclusão secundária na mesma sentença e que expressa posicionamento de maneira em que elas se suportem e concordem entre si. 

**Rebuttal:** é um tipo específico de *Claim* que possui a introdução de um ponto de vista para que ele seja refutado ou oposto na mesma sentença. 

### 2.5 Atributos de Evidências 
Os atributos também serão de rótulo único, sendo vedada a atribuição de mais de um simultaneamente.

**Definition (Def)**: é uma evidência que expressa o significado de um termo, geralmente de forma concisa.

**Event (Evt)**: é uma evidência que possui relação de causa e efeito com a conclusão e se referem a um acontecimento que ocorreu no passado.

**Quantitative Data (QDat)**: é uma evidência que agrega valor à conclusão, por meio de registro de dados numéricos ou estatísticos de um fenômeno observado.

**EvidenceOther**: este não é um rótulo que representa um valor específico, ele é um rótulo utilizado para indicar evidências fora dos conjuntos anteriores ou indicam uma composição de um ou mais dos atributos acima. Rótulo ad hoc ou guarda-chuva.

### 2.4 Exemplos de evidências independentes de contexto

Nessa seção, apresentamos exemplos das 3 subclasses de evidência, as quais são definições, eventos e dados quantitativos. Esses exemplos estão em situações independentes de contexto, ou seja, não são associadas a nenhum tópico e conclusões específicas.

Se for o caso, uma evidência pode ser identificada por uma classe mais específica (subclasses). Há 3 rótulos que podemos utilizar para esta finalidade:

**Definition (DEF):** Uma definição é um enunciado que explica o significado de um termo. Pode envolver significados diferentes dependendo do ponto de vista. Para não confundir com o rótulo Evento (EVT),  definições não devem envolver marcos temporais, mas podem evoluir para se tornar mais abrangente ou mais preciso ao longo do tempo. Por exemplo, a definição de força de gravidade para a Teoria da Relatividade é mais abrangente que a Teoria Mecânica de Newton.

|Nº|Sentença|Validade|
| :- | :- | :-: |
|**S1**|A hipotenusa é o maior lado de um triângulo retângulo|**V**|
|**S2**|A vida começa a partir da fecundação|**V**|
|**S3**|A vida  se inicia quando começam as atividades cerebrais,  <br />por volta do 2º mês de gestação|**V**|
|**S4**|﻿O Artigo 2 da Lei nº 10.406 atribui direitos a nascituros|**X**|
|**S5**|A lei 10.406 definiu o conceito de nascituro e estabeleceu seus direitos|**V**|
|**S6**|Segundo Newton, duas partículas se atraem com forças cuja intensidade <br /> é diretamente proporcional ao produto de suas massas e inversamente  <br />proporcional  ao quadrado da distância que as separa|**V**|
|**S7**|A partir das descobertas de Einstein em 1905, a gravidade foi definida como a  <br /> influência  que a curvatura do espaço-tempo produz sobre objetos que estejam nele.|**V**|


Tabela 2: Exemplos e contraexemplos de definições: As sentenças S1, S2 e S3 são definições válidas, sendo que a primeira é uma definição (matemática) independente de ponto de vista e a segunda e a terceira são dependentes de pontos de vista. As sentenças S4 e S5 poderiam referenciar as mesmas informações e portanto assumirem o mesmo rótulo, no entanto a sentença S4 cita uma lei, enquanto que a sentença S5 refere-se a uma definição da mesma lei; portanto, os rótulos das sentenças S4 e S5 são respectivamente EVID e DEF. A sentença S6 parece ser meramente uma descrição de algo, porém há uma frase oculta: “A gravidade é…” e se esta estivesse explícita seria obviamente uma definição. A sentença S7 poderia ser classificada como  evento, porém no nosso esquema a definição tem prioridade sobre ele.

**Event (EVT):** É um evento ou conjunto de eventos que ocorreram em um determinado período de tempo no passado, com início bem definido e que causam efeitos no presente. O período de tempo possui um marco temporal (time frame), definido explicitamente ou compreendido implicitamente (senso comum). O efeito encerrado ou prolongado no presente - dias, meses ou anos - tem que ser bem claro e evidente.


|Nº|Sentença|Validade|
| :- | :- | :-: |
|**S1**|O atentado terrorista de 11/09 teve impactos profundos  <br />nas relações internacionais|**V**|
|**S2**|A Guerra dos 100 Anos influenciou os nacionalismos <br />francês e inglês|**V**|
|**S3**|Nos últimos anos, as viagens de longa distância diminuíram  <br />devido à pandemia de COVID|**V**|
|**S4**|A Lei Maria da Penha contribuiu com a criminalização da   <br />violência contra a mulher|**V**|
|**S5**|Ao longo desses anos, a violência contra as mulheres tem  <br />diminuído devido a aplicação de leis mais rígidas|**X**|
|**S6**|Segundo a declaração recente do presidente, a saúde pública  <br /> é um direito de todos cidadãos e dever do estado|**X**|
|**S7**|Ontem, o presidente fez uma declaração que foi um alento  <br /> para a população carente: declarou que é a favor de  <br />uma saúde pública garantida pelo estado |**V**|


Tabela 3: Exemplos e contraexemplos de eventos:  as sentenças S1 e S2 são eventos históricos bem conhecidos e portanto seu marco histórico é implícito. A sentença S3 é semelhante às anteriores e o seu início e efeitos são bem conhecidos, apesar do seu término ainda não ter sido determinado oficialmente em 2022. A sentença S4 não possui explicitamente uma data, mas é consensual que as leis possuem uma data de promulgação e eventualmente provocam efeitos na sociedade. Por outro lado, a sentença S5, apesar de informar um efeito, é vaga com relação ao acontecimento (lei) ou acontecimentos (leis) que provocaram mudanças. A sentença S6, apesar de descrever um acontecimento (declaração de um presidente) em um determinado tempo (recente), não há clareza quanto ao efeito da ação; essa sentença também poderia ser confundida como uma definição por conta do verbo ‘é’, no entanto, não tem a função de definir o termo “saúde pública”. Por outro lado, a sentença S7, parecida com a sentença S6, é considerada um evento, pois possui um efeito evidente. 

**Quantitative data (QDAT):** Dados quantitativos são quaisquer dados que estejam em forma numérica, como estatísticas, porcentagens, etc… (Given, 2008, tradução nossa). No nosso caso assumimos referências a dados quantitativos não expressados de forma detalhada, por exemplo, pode ser utilizado a palavra “maioria” em vez de citar o dado numérico mínimo (maior que 50%).


|Nº|Sentença|Validade|
| :- | :- | :-: |
|**S1**|De acordo com Gallup, a maioria dos americanos acredita  <br />que a pena de morte é um impedimento ao homicídio,  <br /> o que os ajuda a justificar seu apoio à pena capital|**V**|
|**S2**|"A arma mais forte contra discurso de ódio não é a repressão; <br /> é mais discurso.", disse Barack Obama|**X**|
|**S3**|Logo após o resultado das eleições foram identificados que cerca de  <br />um terço das mensagens nas redes sociais eram discurso de ódio|**V**|
|**S4**|O Brasil é o 5º país em tamanho territorial no mundo|**V**|
|**S5**|O Brasil é o maior país da América Latina|**V**|
|**S6**|A Lei Maria da Penha contempla as violências contra as mulheres, que acontecem  <br /> no convívio doméstico, no âmbito familiar ou em relações íntimas de afeto.|**X**|
|**S7**|A partir do vigor da Lei Maria da Penha, houve um aumento no registro de ocorrências  <br /> nas delegacias especializadas.|**X**|


Tabela 4: Exemplos e contraexemplos de referências: o S1 cita o resultado de uma pesquisa estatística (do Instituto Gallup), mas não informa diretamente um número, mas sim uma noção quantitativa de maioria que pode ser acima de 50%, portanto é um dado quantitativo. A S2 não trata de um dado quantificável, no entanto a S3 que trata de um assunto semelhante apresenta um dado quantitativo. A S4 é obviamente um exemplo de dado que expressa uma ordem quantitativa. A S5 não expressa uma quantidade diretamente, mas possui uma comparação quantitativa (maior em extensão territorial). O exemplo S6 cita a Lei Maria da Penha, informando o que é contemplado nela, mas não expressa nenhum dado quantitativo; o exemplo S7 também cita a mesma lei, no entanto, há evidências de efeito, portanto prioritariamente, por convenção deste estudo, é um evento.
