## 6 - Fluxogramas de anotação

Nesta seção são apresentados fluxogramas de anotação para cada estágio de anotação, bem como um fluxograma geral de anotação. O objetivo de dividir a anotação em estágios é facilitar a compreensão do processo de anotação e a divisão de tarefas entre os anotadores. Com uma boa compreensão e divisão de tarefas, espera-se que a qualidade da anotação seja melhorada.

### 6.1 - Estágio 1: Segmentação e classificação de tópicos

O estágio um inclui duas atividades:
- Segmentação de tópicos 
A segmentação de tópicos divide o texto em trecho ou unidades coesivas de um mesmo subtópico. Via de regra, estes tópicos são expressos como estruturas textuais como seções, parágrafos ou listas. 

- Classificação de tópicos 
A classificação de tópicos consiste na aplicação de palavras-chave que caracterizam o assunto ou assuntos tratados no subtópico, por exemplo: dado um tópico _aborto_, seria possível aplicar as três seguintes palavras-chave em um subtópico: Mulher, Ética, Saúde.

No fluxograma a seguir, realizaremos as duas atividades anteriores concomitantemente:

* __Passo P1__: Identifique a primeira sentença do documento de acordo como um dos dois rótulos acima. Indique também de um a três palavras-chave à sentença inicial.
* __Passo P2__: Avalie a sentença seguinte de acordo com seu pertencimento ao assunto/subtópico da sentença anterior. Caso ela pertença,a este, repita este passo até encontrar uma mudança de assunto/subtópico e siga para o passo seguinte.
* __Passo P3__: Assim que a mudança de assunto for detectada no passo anterior, marque a primeira sentença que faça parte de um novo subtópico com um dos dois rótulos disponíveis.
* __Passo P4__: Assim que identificado e rotulado o início de um subtópico, indique de um a três palavras-chave que melhor representem o subtópico analisado. Dê preferência por sempre indicar o máximo de atributos de tópico o possível à sentença.
* __Passo P5__: Caso a sentença analisada não faça parte da argumentação acerca do tópico geral, confira o atributo "Out of topic" a essa sentença. Após as atribuições devidas, repita o passo P2 até o final do documento.

Observações: é comum ter a primeira sentença dos documentos como um título geral que expressa o assunto geral do documento. Neste caso, apesar dos subtópicos a seguir se constituirem como "descendentes" ou pertencentes ao assunto geral, ainda devem ser sinalizados simplesmente com o rótulo "Topic". Adotamos, neste trabalho, uma segmentação linear e não hierárquica.

!!! Question "Topic e Out-Of-Topic"
    Refira-se à seção de [rótulos (entidades e atributos) - Rotulos do estágio 0](entity.md#31-rótulos-do-estágio-0) para ler mais a fundo sobre os rótulos, suas utilizações e boas práticas.

### 6.2 - Estágio 2: Detecção de componentes

No estágio 1, é realizada a alteração dos [rótulos migrados](schema.md#22-entidades-importadas) do dataset _UKP Sentential_ para os [rótulos atuais](schema.md#23-entidades-para-anotar). Na figura 5.1, é exibido um fluxograma com os passos necessários para executar o estágio 1.

### 6.2.1 - Atividade: Agrupamento de Sentenças

A atividade de agrupamento de sentenças tem como objetivo agrupar sentenças que possuem relações de co-dependência entre si. Para isso, o anotador deve detectar as relações de co-dependência (vide [Anexo III](appendix3.md)) . Caso sim, o anotador deve agrupá-las em um mesmo bloco utilizando a relação __group__.

A atividade de agrupamento de anotações é realizada durante os dois primeiros estágios de anotação.

!!! Question "Composed claim por agrupamento"
    Refira-se à seção de [rótulos (entidades e atributos) - SupportedClaim por agrupamento](entity.md#332---supportedclaim-por-agrupamento) para ler sobre um caso comum que demandará agrupamentos de sentenças.

### 6.2.2 - Atividade: Sumarização de Sentenças

A atividade de sumarização de sentenças ou grupo de sentenças tem como objetivo sumarizar sentenças que ultrapassam 200 caracteres ou que precisam de ser parafraseadas para melhor compreensão. Para isso, o anotador deve detectar as sentenças que precisam ser sumarizadas e criar uma nova sentença que contenha a mesma informação de forma resumida. Veja o [Anexo IV](appendix4.md) sobre os detalhes de como realizar a sumarização de sentenças.

### 6.2.3 - Atividade: Detecção de componentes
O fluxograma da figura 9 apresenta uma sugestão de fluxo de atividades para a anotação de relações. Inicialmente, o anotador deve escolher um par de argumentos e avaliar se as entidades (argumentos) são do tipo conclusão (Claim) ou não. Em ambos os casos, deve-se avaliar se há relação de correferência entre as entidades, caso sim, a relação a ser utilizada é group. Caso as entidades (argumentos) tenham o mesmo posicionamento, então a relação é converge, ou caso contrário, é uma relação diverge.

TODO: revisar esta seção

### 6.2.4 - Fluxograma do estágio 2

O fluxograma da figura 5.1, é explicado algoritimicamente a seguir por meio dos seguintes em passos:

* __Passo P1__: Começe analizando o primeiro argumento migrado (i=1) (com rótulo ArgF ou ArgA) e o segundo argumento migrado seguinte (i=2). Verifique se os dois argumentos vizinhos A<sub>i</sub> e A<sub>i+1</sub> são livres de co-dependência. Caso sejam, Altere o rótulo de um ou ambos A<sub>i</sub> e A<sub>i+1</sub> para o mesmo tipo de rótulo de entidade e aplique a relação group; depois continue no passo P1.1; caso contrário, siga o passo P1.2.
* __Passo P1.1__: Verifique na visualização de diagrama se os argumentos agrupados (A<sub>i</sub> e A<sub>i+1</sub>) possuem mais de 200 caracteres. Caso sim, sumarize a sentença.
* __Passo P1.2__: Verifique na visualização de diagrama se o argumento A<sub>i</sub> possui mais de 200 caracteres. Caso sim, sumarize a sentença; caso contrário, siga direto para o passo P2.
* __Passo P2__: Analise se sentença é realmente argumentativa e tem relação direta com o assunto abordado no tópico. Caso sim, continue no passo seguinte. Caso contrário, houve alguma falha na detecção de rótulo importado do _dataset_ anterior (falso positivo), provavelmente causada por problemas de falta de contexto. Neste caso, a sentença deve ser rebaixada e portanto, altere o rótulo para __Sentence (SE)__.
* __Passo P3__: Analise se a sentença expressa um posicionamento à favor ou contra o tópico. Caso sim, a sentença é algum tipo de conclusão (claim) e continue no passo 4. Caso contrário, a sentença é algum tipo de evidência e siga no passo 3.1.
* __Passo P3.1__: a sentença define algum termo ou conceito? Caso sim, altere o rótulo para __Definition (DEF)__. Caso contrário, continue no passo seguinte.
* __Passo P3.2__: analise a sentença e verifique se trata de algum evento que provocou algum efeito e possui algum marco temporal de início (inclusive supostamente presumido por senso comum). Caso sim, altere o rótulo da sentença para *Event (EVT)*. Caso contrário, continue no passo seguinte.
* __Passo P3.3__: analise a sentença e verifique se trata de resultados de uma observação documentada através de dados quantitativos, como uma estatística ou comparação numérica. Caso sim, altere o rótulo desta sentença para __Quantitative Data (QDAT)__. Caso contrário, não foi encontrado um tipo específico que encaixe nos rótulos anteriores, portanto é um tipo genérico de evidência, portanto altere o rótulo da sentença para __Evidence (EVID)__.
* __Passo P4__: analise a sentença e verifique se além de conter uma conclusão, também possui uma evidência que a apoia. Caso sim, é uma conclusão suportada por uma evidência e portanto, continue no passo seguinte. Caso contrário, siga o passo P4.2.
* __Passo P4.1__: analise a sentença e verifique se há um posicionamento à favor do tópico. Caso sim, altere o rótulo da sentença para __SupportedClaimFavor (SpClFv)__. Caso contrário, altere o rótulo da sentença para __SupportedClaimAgainst (SpClAg)__.
* __Passo P4.2__: analise a sentença e verifique se há um posicionamento à favor do tópico. Caso sim, altere o rótulo da sentença para __ClaimFavor (ClFv)__. Caso contrário, altere o rótulo da sentença para __ClaimAgainst (ClAg)__.

O fluxograma do algoritmo apresentado anteriormente é apresentado na figura 5.1. Como resultado final deste estágio, você deve verificar os seguintes requisitos, antes de marcar o estágio como status finalizado (S1-finalizado):

* Nenhuma sentença marcada como __Sentence (SENT)__ foi escalada para outro rótulo atual ou migrado.
* Não resta mais nenhum rótulo migrado (__ArgA__ ou __ArgF__) pendente de alteração.
* Toda sentença com um dos 8 rótulos atuais (ClFv, ClAg, SpClFv, SpClAg, Evid, Def, Evt e QDat) que possui [co-dependência semântica](appendix3.md#co-depend%C3%AAncia-sem%C3%A2ntica) com outra sentença deverão estar associadas entre si por meio do rótulo *group*; e portanto, as duas sentenças devem possuir o mesmo rótulo. Assim, as sentenças pertencentes a um grupo não possuem classificações distintas.
* Toda sentença ou grupo de sentenças com mais de 200 caracteres [deve estar sumarizada](appendix4.md#anexo-iv-sumariza%C3%A7%C3%A3o-de-senten%C3%A7as).

![alt_text](images/fluxograma-estagio1.png)

Fig. 5.1: fluxograma do estágio 1 - alteração de rótulos

TODO: juntar o diagrama do estagio 1 e estagio 2

![alt_text](images/fluxograma-estagio2.png)

Fig. 5.2: fluxograma sugerido de anotação de relação entre entidades


### 6.3 Estágio 3: Mesclagem e Hierarquização de Conclusões

TODO: criar fluxograma

### 6.4 Fluxograma geral

A fase de anotação será dividida em três etapas: pré-anotação, anotação e pós-anotação; sendo que cada etapa possui atividades as quais estão detalhadas no fluxograma a seguir:

![alt_text](images/Fig_10.png)

Fig. 10: fluxograma de anotação sugerido

### 6.4.1 Fases de anotação 
> Alerta: essa seção está sendo revisada
 
- **A1.1 - Atribuição e escolha do documento:**  Escolha um documento para anotar dentre o lote que lhe foi atribuído. A atribuição de lotes é realizada pela coordenação e comunicada através de planilha.
- **A1.2 - Leitura breve de parágrafos com argumentos detectados:** O objetivo da leitura breve é para se ter uma noção do conteúdo  argumentativo do documento. Considere fazer algumas anotações  em um papel ou bloco de notas identificando cada parágrafo pelo seu número identificador. Note que todas sentenças vão estar pré-rotuladas, sendo que boa parte foi migrada a partir do dataset  ukp\_sentential. Os rótulos ArgA e ArgF denotam argumentos contra e a favor do tópico (Aborto, por exemplo) e merecem maior atenção.  O rótulo SE denota uma sentença comum.
- **A2.1 - Troca de rótulos de entidades:**  Baseado em suas anotações, faça alteração dos rótulos conforme o fluxograma da seção 2.1.5. Aguarde o resultado da fase seguinte para avançar para a atividade A2.3. Marque na planilha que a anotação desta atividade foi finalizada.
- **A2.2 - Adjudicação da subfase A2.1:** Nesta atividade um dos anotadores expert revisa o resultado da atividade A2.1 de pelo menos dois anotadores e adjudica a favor de um deles ou decide mesclar o resultado de ambos.
- **A2.3 - Atribuição de relações entre entidades:** A partir de um resultado adjudicado, faça as anotações de dados conforme a seção 2.2.1. Aguarde o resultado da fase seguinte para avançar para a atividade A2.5. Marque na planilha que a anotação desta atividade foi finalizada.
- **A2.4 - Adjudicação da subfase A2.3:** Nesta atividade um dos anotadores expert revisa o resultado da atividade A2.3 de pelo menos dois anotadores e adjudica a favor de um deles ou decide mesclar o resultado de ambos.
- **A2.5 - Atribuição de relações entre conclusões:** Em construção
- **A2.6 - Adjudicação da subfase A2.5:** Em construção
- **A3.1 - Acompanhamento de anotação:** Após cada lote de documentos concluídos pelo anotador, será comunicado um relatório de qualidade de anotação, seja por meio oral ou de forma automática.
