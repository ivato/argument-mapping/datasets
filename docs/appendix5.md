## Anexo V - Dicionários de palavras-chave

A seguir temos uma lista de palavras-chave utilizadas para a classificação dos tópicos. As palavras-chave foram definidas a partir de uma análise exploratória dos dados e foram agrupadas por tópicos. 

Para cada palavra-chave é apresentada uma definição e uma lista de palavras similares. As palavras-chave similares são utilizadas para a expansão do vocabulário, ou seja, para a busca de palavras que possuem um significado muito parecido, mas que não foram incluídas no dicionário. 

Todas palavras-chave foram escritas em inglês, pois a base de dados utilizada para a classificação foi escrita nesse idioma. As palavras-chave iniciadas em letra maiúscula são entidades nomeadas, ou seja, nomes de pessoas, organizações, locais, etc. E as demais, escritas com letras iniciais minúsculo, são termos comuns.

### Abortion

|Palavra-chave|Definição|Similares|
|---------------|-----------|-----------|
|abortion_distress|Sintomas e condições psicológicas ou emocionais, em uma mulher grávida, relacionadas a situações pré ou pós aborto |regret, coersion, depression, pressure|
|conception_and_contraception|Conceitos ou posicionamentos frente a concepção e a contracepção|abstinence, prevention, morning-after pill, contraceptive|
|education_and_career|Educação sexual e impactos da gravidez indesejada na carreira dos pais|sex education, young/youth, teenager|
|ethics_and_moral_values|Valores e normas pessoais ou reflexões filosóficas sobre o aborto|right, ideal, principle, virtue|
|health|Aspectos ou impacto na saúde física da mãe decorrentes de procedimentos abortivos|death, fertility, cancer, maternal mortality|    
|human_life_and_personhood|Início da vida, visão do feto como pessoa e aspectos do desenvolvimento pré natal|life, feeling, brain stem, term abortions|
|killing_and_infanticide|Associação do aborto ao homicídio de crianças e bebês|death, murder, genocide|
|law_and_politics|Legislações, projetos de lei ou repercussões politicas sobre o aborto e assuntos correlatos|Abortion cases, legislation, legal and ilegal abortion|
|parenthood|Responsabilidades e impactos pessoais e sociais da paternidade/maternidade, assim como planejamento familiar|Responsability, family planning, conception and contraception|
|Pro-Life_Organization|Organizações que participam do movimento anti-aborto do Estados Unidos||
|Pro-Choice_Organization|Organizações que participam do movimento em defesa do aborto como direito humano no Estados Unidos||
|religion_and_church|Motivações religiosas ou teológicas para o posicionamento acerca do aborto|god, bible, commandments|
|safety|Aspectos relacionados a procedimento do aborto seguro|Safe abortion, abortificants|
|Supreme_Court|Suprema Corte do Estados Unidos|High Court, Supreme, United States|
|unborn_rights|Direitos humanos do feto e suas aplicações|late term abortion, conciousness, fetus right, birthright|
|women_rights|Direitos humanos da mulher e suas aplicações|Reproductive rights, autonomy, feminism and equality, privacy, sexual abuse and incest|

## Nuclear Energy
|Palavra-chave|Definição|Similares|
|-------------|---------|---------|
|accident|Acidentes nucleares em projetos de energia ou engenharia atômica|disaster, disaster preparedness|
|Chernobyl|Acidente nuclear ocorrido na usina de Chernobyl, USSR, em 1986||
|climate_change|Alterações de longo prazo nos padrões climáticos do planeta|climate change mitigation, anthropogenic, carbon emission, air pollution, environmental impacts, carbon emission|
|economy|Aspectos econômicos da energia nuclear, incluindo custo-benefício e impacto no mercado energético|subsidies, cost, industry, labour and jobs, job creation, energy independence|
|Fukushima|Acidente nuclear ocorrido na usina de Fukushima, Japão, em 2011||
|health|Impactos na saúde relacionados à energia nuclear|radiation exposure, health effects, public health concerns|
|mining|Extração de minerais e utilização de minérios específicos para a engenharia nuclear|nuclear fuel, uranium mining, plutonium mining|
|Nuclear_Regulatory_Commission|Comissão Reguladora Nuclear (NRC) dos Estados Unidos||
|politics_and_ethics|Relacionado às questões de governo, tomada de decisões e princípios morais.|corruption, management, activism, democratization, promotion|
|radioactive_waste|Materiais perigosos e radioativos resultantes de atividades nucleares.|waste disposal, nuclear waste, radioactivity, pollution|
|renewable_energy|Fontes de energia sustentáveis e não esgotáveis, como solar e eólica.|fossil fuel, coal, energy integration, non-renewable energy source, resource depletion, sustainability, natural resources|
|safety_and_security|Medidas para prevenir riscos e garantir a proteção de pessoas e bens.|security threats, national security, safety, prevention and reliability, safety risks|
|regulation|Regulamentações da industria de energia nuclear|law, international cooperation, decommissioning|
|research_and_science|Pesquisa e desenvolvimento científico relacionados à energia nuclear.|technology and innovation, fission and fusion, education|
|terrorism|Vulnerabilidade e riscos de instalações nucleares contra ataques terroristas|antiterrorism, terrorist attacks|
|weapon|Um instrumento usado para causar dano físico ou influenciar eventos.|weapon proliferation, nuke, disarmament|