|Nuclear Power ProCon.org||BACK|
|Pro||Con|
The world has vast reserves of coal, which are more widely distributed than oil and gas reserves.1
The coal industry is well-established, with adequate technologies for conventional mining.2
Coal's energy is easily released.3
Coal is easily stored, transported, and controlled in large volumes.4
There are enough sites, coal mines, engineering talent and materials to construct coal plants on schedule.5
In areas of the United States where coal is abundant, coal-fired power plants have a significant economic advantage over oil-fired and nuclear plants.6
Coal power plants and the coal fuel cycle are not subject to low-probability, high-consequence accidents or sabotage. The hazards of coal can be made relatively predictable, given sufficient research on such matters as the health effects of coal-derived air pollutants.7
Coal burning in utilities does not raise the problem of nuclear weapons proliferation and related safeguards.8
The lead time for planning coal-burning power plants is less than that for nuclear plants.9
The combustion of coal is the largest single source of sulfur oxide pollution worldwide.10
Carbon dioxide, an unavoidable by-product of coal combustion, causes a warming of the earth which can lead to global climatic changes.11
Toxic metals and carcinogenic compounds are released through the combustion of coal.12
Coal combustion produces solid wastes such as boiler ash and flyash, and causes water pollution.13
Strip-mining produces soil waste problems, acid drainage, unproductive land, and ugly terrain. Soil erosion pollutes streams.14
Coal mining causes black lung disease.15
100,000 men lost their lives through coal mining in this century; more than 1 million have been permanently disabled.16
The sulfur dioxide and nitrogen oxide wastes released when coal is burned can have serious adverse effects on agriculture. Acid rain is caused by sulfur dioxide and nitrogen oxide.17
|AMA Commentary|
Coal is the United States' and the world's most abundant fossil fuel.18 In the past decade, the use of coal has increased dramatically in the West, with the U.S. alone using 250 million more tons of coal in 1985 than in 1974.19
More than 55 percent of the electricity generated in the U.S. is coal-fired.20 Even greater coal use is limited by the extraordinary industrial and regulatory difficulties of mining and burning it in an environmentally acceptable and economically competitive manner.21
The capital costs of coal-burning plants and equipment (including the installation and maintenance of emission control devices) are high. The average capital cost of bringing a new coal-fired plant on line is approximately $1,000 (1984 dollars) per kilowatt (compared to about $2,600 per kilowatt for a nuclear plant).22 Because coal is bulky, it is expensive to move and store.23
An array of adverse occupational and environmental hazards are associated with the coal fuel cycle (including air pollution that leads to acid rain, mining-related land degradation, and lung disease among miners).24 A growing body of evidence also indicates that the atmospheric accumulation of carbon dioxide, a major by-product of coal combustion, can alter global climatic patterns.25
A variety of advanced methods of limiting the harmful emissions from coal combustion (generally referred to as "clean-coal technologies") are currently in use, and others are being developed. A number of new approaches to electricity generation using coal, aimed at improved efficiency, lower costs, and reduced environmental impacts, are also under development.26
1 Medard Gabel, Energy, Earth, & Everyone, (New York: Anchor Press/Doubleday, 1980), p. 62.
2 James E. Gander and Fred W. Belaire, Energy Futures for Canadians, (Canada: Minister of Supply and Services, 1978), p. 120.
3 Gabel, op. cit.
4 Ibid.
5 Roger W. Sant, Bennis W. Bakke, and Roger R. Naill, Creating Abundance, (New York: McGraw Hill Book Co., 1984), p. 98.
6 Daniel Deudney and Christopher Flavin, Renewable Energy: The Power to Choose, (New York: W.W. Norton & Co., 1983).
7 National Academy of Sciences, Committee on Nuclear and Alternative Energy Systems, Energy in Transition 1985-2010, op. cit., pp. 15-15. All subsequent references to this work will be cited at Energy in Transition.
8 Ibid.
9 Gabel, op. cit.
10 Ibid.
11 Ibid.
12 Ibid.
13 Ibid.
14 Ibid.
15 Ibid.
16 Ibid.
17 Ibid., p. 63.
18 Energy in Transition, op. cit., p. 19.
19 U.S. Department of Energy, Energy Security: A Report to the President of the United States, op. cit., p. 162. All subsequent references to this report will be cited as Energy Security.
20 Ibid., p. 171.
21 Energy in Transition, op. cit. p. 20.
22 Charles Komanoff, Komanoff Energy Associates, private communication, October 15, 1987.
23 Energy Security, op. cit., p. 171.
24 Ibid., p. 172.
25 John Holdren, "Energy and the Human Predicament," in Earth and the Human Future, (Boulder: Westview Press, 1986), pp. 145-146.
26 Energy in Transition, op. cit., pp. 161-171.
TOP BACK