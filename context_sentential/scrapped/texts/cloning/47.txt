Advantages and Disadvantages of Cloning Animals
Cloning is a highly controversial practice that has begun taking off in the animal world. There are benefits as well as potential drawbacks in cloning animals. With today’s advanced technologies, it is much easier to clone animals. Still, the industry is continuously developing and the future creations are quite promising. Discover the pros and cons of animal cloning, and decide if it’s the best solution to today’s decreasing number of certain animal species.
The Advantages
The following are the advantages of cloning animals:
1. The creation of identical copies of animals – through cloning, it is possible to create genetically identical animals for organ or tissue transplantation; and the creation of identical copies of only the best animal population
2. Repopulate the world with endangered or even extinct animals – with the modern technology nowadays, it is possible to address the problem of endangered species, or recreate the extinct species of animals
Just imagine being able to bring back an extinct animal into today’s world! Big movies like that of Jurassic Park and other similar themed cinema hits showed the recreation of dinosaurs through the preserved DNA in mosquitoes. It could completely change the way that we look at the world.
The Disadvantages
The following are the disadvantages of cloning animals:
1. Mutations – in cloning, somatic cells play a significant role; when these cells contains mutations, the cloned animal can be weak or it could have lethal effects on the subject
2. Death – death can happen when the donor egg’s mitochondria doesn’t match with the somatic cell’s mitochondria
3. Early death or premature aging – when somatic cells undergo telomeric shortening during cloning, the results can be early death of the animal or premature aging
4. Expensive – cloning is expensive because a great number of eggs are required for a single viable clone and this may seem wasteful since it is much easier to breed animals naturally
5. Genetic diversity is reduced – since it is possible to create identical copies of animals, this aspect is reduced greatly
6. Scientific limitation – until now, cloning is still experimental and it warrants further research
As mentioned earlier, cloning is at its best in movies. In the real world, it still remains experimental and comes with a lot of scientific limitations. With continued research, it may be possible to create identically similar animals, or even humans! For now, scientists and researchers are still conducting in-depth studies to perfect the method of cloning.
Not everyone agrees to cloning, and in fact some people claim that it is unethical and immoral. It can also be the source of a new world scheme wherein there are superior and inferior animals. Finding donors of compatible somatic cells is vital to the success of cloning.
Knowing the advantages and disadvantages of cloning animals will enlighten your knowledge and appreciation of life. Some things can and cannot be done. It can be said that cloning is still in its development stage, and with continued studies or research, efficient and effective methods can be created for best results.