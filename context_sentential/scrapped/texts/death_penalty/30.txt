Criminal Justice: Capital Punishment Focus
Background
The formal execution of criminals has been used in nearly all societies since the beginning of recorded history. Before the beginning of humane capital punishment used in today’s society, penalties included boiling to death, flaying, slow slicing, crucifixion, impalement, crushing, disembowelment, stoning, burning, decapitation, dismemberment and scaphism. In earlier times, the death penalty was used for a variety of reasons that today would seem barbaric. Some cultures used it as punishment for magic, violation of the Sabbath, blasphemy, a variety of sexual crimes including sodomy and murder. Today, execution in the US is used primarily for murder, espionage and treason. In China, human trafficking and serious cases of corruption are punishable by death, and several militaries around the world impose the death penalty for desertion, mutiny and even insubordination. In middle-eastern countries, rape, adultery, incest and sodomy carry the death penalty as does apostasy (the act of renouncing the state religion). While most industrialized countries utilize lethal injection or the electric chair for capital punishment, many others still use hanging, beheading or stoning. In some states in the US, death by firing squad is also still used.
- History of the Death Penalty Provides a history of capital punishment as well as news on recent developments.
- Capital Punishment in US History Describes how capital punishment has played a role in US history.
- Capital Punishment in China Provides a history of capital punishment in China.
- Death Penalty in Iraq Provides current information on the death penalty as used by the Iraqi government.
- World History of the Death Penalty Provides information on the death penalty throughout recorded history.
The Death Debate
The fight between those who support capital punishment and those who oppose it is rather simple compared to many other debates. Those in support of capital punishment believe it deters crimes and, more often than not believe that certain crimes eliminate one’s right to life. Those who oppose capital punishment believe, first and foremost, that any person, including the government, has no right to take a life for any reason. They often believe that living with one’s crimes is a worse punishment than dying for them, and that the threat of capital punishment will not deter a person from committing a crime. They also believe that the risk of executing an innocent person is too high. The debate between these two sides is often heated, with both sides protesting outside court houses and jails during high profile cases. However, a worldwide poll conducted in 2006 indicates that 52% of the world’s population supports the death penalty. In the US, that number is 65%.
- In Support of the Death Penalty An article detailing the argument for the death penalty.
- Three Good Reasons Details the three main reasons why people support the death penalty.
- Pro Death Penalty Explains the reasoning behind the death penalty.
- The Case Against the Death Penalty A detailed article showing the reasons for opposition to the death penalty.
- Abolish the Death Penalty An organization dedicated to abolishing the death penalty in the US.
- Anti Capital Punishment A world organization working to eliminate the death penalty in every country.
Costs and Procedures
On average, it costs $620,932 per trial in federal death cases, which is eight times higher than that of a case where the death penalty is not sought. When including appeals, incarceration times and the actual execution in a death penalty case, the cost is closer to $3 million per inmate. However, court costs, attorney fees and incarceration for life only totals a little over $1 million. Recent studies have also found that the higher the cost of legal counsel in a death penalty case the less likely the defendant is to receive the death penalty, which calls the fairness of the process into question.
A capital punishment case begins with a trial in front of a grand jury (typically 23 people) where the prosecutor makes it known before the trial that they are seeking the death penalty. The first part of the trial is the guilt phase, where both sides of the case is presented and the jury determines whether the defendant is guilty of the crime they are charged with. Following a charge of guilty, the next phase of the trial is the penalty trial. Both sides again present their case for punishment in front of the jury, and the jury makes a recommendation and the judge pronounces the sentence. In some states, the judge does not have to follow jury recommendation, though in most he or she does. Following the sentencing, the decision must go through direct review and state review, which acts as an appeal process for the convict. If the sentence makes it past all of the reviews, the inmate sentence is set in stone barring involvement of the President. The prisoner typically stays on death row for many years before their sentence is carried out, and in many states less than half of those sentenced to death actually receive their punishment before dying of natural causes.
- Freed from Death Row An article discussing the lives of those released from death row.
- Death Row Survivors Includes personal stories of those who survived death row.
- Wrongful Convictions Provides statistics on wrongful convictions in the US.
- Executing the Innocent An article detailing what happens when an innocent person is executed.
- Cases of Wrongful Execution A list of cases that the defendant was wrongfully accused, convicted and executed.
- Not Executing the Innocent An article arguing against the claim that the US has a 68% failure rate in execution.
- The Justice Project An organization dedicated to ensuring no innocent person is executed.
- How Many Innocent are Executed Provides information on how many innocent people are executed.
- Cost of the Death Penalty Provides information on the financial cost of the death penalty.
- Deterrent and Cost Details how capital punishment deters crime and whether it is cost effective.
- Cost Comparison A cost comparison between Texas and Connecticut regarding the death penalty.
- Maryland Death Penalty Describes the cost of the death penalty in Maryland.
- Capital Punishment Process Describes the process used to sentence someone to death.
- The Death Penalty Appeals Process Provides information on how to appeal a death penalty in Alabama.
Famous Cases, Statistics and Futher Reading
- Capital Punishment Statistics Provides statistical information on capital punishment in the US.
- Death Penalty Statistics Provides information on the death penalty in 37 states.
- Simple Answers Provides easy to read statistics on capital punishment in the US.
- Pros and Cons of the Death Penalty Provides a simple explanation of both sides of the debate.
- Death Penalty Arguments Details arguments for and against the death penalty.
- Giles Corey Provides information on this famous death penalty case from the Salem Witch Hunts.
- Timothy McVeigh A news article detailing the death by lethal injection of Timothy McVeigh.
- Karla Faye Tucker One of the few women put to death, she sparked several debates over capital punishment.
- Charles Guiteau Details on the execution of the assassination of President Garfield in 1881.
- Famous Cases Provides information on some of the most famous capital punishment cases in the US.
Criminal Justice Information by State
- Select One
- Alabama
- Alaska
- Arizona
- Arkansas
- California
- Colorado
- Connecticut
- Delaware
- Florida
- Georgia
- Hawaii
- Idaho
- Illinois
- Indiana
- Iowa
- Kansas
- Kentucky
- Louisiana
- Maine
- Maryland
- Massachusetts
- Michigan
- Minnesota
- Mississippi
- Missouri
- Montana
- Nebraska
- Nevada
- New Hampshire
- New Jersey
- New Mexico
- New York
- North Carolina
- North Dakota
- Ohio
- Oklahoma
- Oregon
- Pennsylvania
- Rhode Island
- South Carolina
- South Dakota
- Tennessee
- Texas
- Utah
- Vermont
- Virginia
- Washington
- Washington D.C.
- West Virginia
- Wisconsin
- Wyoming