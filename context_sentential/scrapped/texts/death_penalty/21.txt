DEATH
PENALTY ARGUMENTS:
This
Paper in Memoriam of Sean Burgado
My
Precious Nephew - Murdered
June 7, 1969 to May 21, 1997
Ě
DEATH PENALTY
ARGUMENTS:
Deterrent
or Revenge
(Pros
and Cons)
INTRODUCTION
What is Capital
punishment? Capital punishment is
the death penalty. It is used today
and was used in ancient times to punish a variety of offenses. Even the bible
advocates death for murder and other crimes like kidnapping and witchcraft.
When the word death penalty is used,
it makes yelling and screaming from both sides of extremist.
One side may say deterrence, while the other side may say,
but you may execute an
innocent man.
Today, one of the most debated issues in the Criminal Justice System
is the issue of capital punishment or the death penalty.
Capital punishment was legal until 1972, when the Supreme Court declared
it unconstitutional in Furman v. Georgia stating that it violated the Eight and
Fourteen Amendments citing cruel and unusual punishment.
However, in 1976, the Supreme Court reversed itself with Gregg v. Georgia
and reinstated the death penalty but not all states
have the death penalty.
Thirteen states do not have the death penalty:
Alaska, District of Colombia, Hawaii, Iowa, Main, Massachusetts,
Michigan, Minnesota, North Dakota, Rhode Island, Vermont, West Virginia, and
Wisconsin.
AGAINST THE
DEATH PENALTY (CONS)
Death Penalty Fails to Rehabilitate
What
would it accomplish to put someone on death row?
The victim is already dead-you cannot bring him back.
When the opponents feel “fear of death” will prevent one from committing murder, it is not true because
most murders are done on the “heat of passion” when a person cannot think
rationally. Therefore, how can one
even have time to think of fear in
the heat of passion (Internet)?
ACLU and Murderers Penniless
The American Civil Liberty Union (ACLU) is working for a moratorium on
executions and to put an end to state-sanctioned murder in the United States.
They claim it is very disturbing to anyone who values human life.
In the article of the ACLU Evolution Watch,
the American Bar Association said the quality of the legal representation
is substantial. Ninety-nine percent
of criminal defendants end up penniless by the time their case is up for appeal.
They claim they are treated unfairly.
Most murderers who do not have any money, receive the death penalty.
Those who live in counties pro-death penalty are more likely to receive
the death penalty. (Internet).
Death Penalty Failed as a Deterrent
Some
criminologist claim they have statistically proven that when an execution is
publicized, more murders occur in the day and weeks that follow. A good example is in the Linberg kidnapping.
A number of states adopted
the death penalty for crime like this, but figures showed kidnapping increased.
Publicity may encourage crime instead of preventing it (McClellan, G.,
1961).
Death is one penalty which makes error irreversible and the chance of
error is inescapable when based on
human judgment . On the contrary,
sometimes defendants
insist on execution. They feel it is an act of kindness to them. The argument here is - Is life imprisonment a crueler fate?” Is there evidence supporting the usefulness of the death penalty securing the life of the citizens (McClellan, G. 1961)?insist on execution. They feel it is an act of kindness to them. The argument here is - Is life imprisonment a crueler fate?” Is there evidence supporting the usefulness of the death penalty securing the life of the citizens (McClellan, G. 1961)?
Does
the death penalty give increased protection against being murdered?
This argument for continuation of the death penalty is most likely a
deterrent, but it has failed as a
deterrent. There is no clear
evidence because empirical studies
done in the 50’s by Professor Thorsten Sellin, (sociologist)
did not give support to deterrence (McClellan, G., 1961).
Does not Discourage Crime
It is noted that we need extreme penalty as a deterrent to crime.
This could be a strong argument if it could be proved that the death
penalty discourages murderers and kidnappers.
There is strong evidence that the death penalty does not discourage crime
at all (McClellan, G., 1961).
Grant McClellan (1961) claims:
In 1958 the10 states that had the fewest murders –fewer
than
two a year per 100,000 population -were New Hampshire
Iowa,
Minnesota, Massachusetts, Connecticut, Wisconsin,
Rhode
Island, Utah, North Dakota and Washington.
Four of
these 10 states had abolished the death penalty.
The
10 states, which had the most murderers from eight to
fourteen killings per100,000 population were Nevada,
Alabama,
Georgia, Arkansas, and Virginia - all of them
enforce
the death penalty. The fact is that
fear of the
death
penalty has never served to reduce the crime rate (p. 40).
Conviction of the Innocent Occurs
The states that have the death penalty should be free of murder, but
those states have the most murders, and the states that abolished the
death penalty has less. Conviction of the innocent does occur and death makes a
miscarriage of justice irrevocable. Two
states Maine and Rhode Island abolished the death penalty because of public
shame and remorse after they discovered they executed
some innocent men.
Fear of Death
Does not Reduce Crime.
The fear of the death penalty has never reduced crime. Through most of history
executions were public and brutal.
Some criminals were even crushed to death slowly under heavy weight.
Crime was more common at that time than it is now.
Evidence shows execution does not act as a deterrent to capital
punishment.
Motives for Death Penalty
- Revenge
According to Grant McClellan (1961), the motives for the death penalty
may be for revenge. Legal vengeance
solidifies social solidarity against law breakers and is the alternative to the
private revenge of those who feel harmed.
FOR THE
DEATH PENALTY (PROS)
Threat of Death Penalty Rate of Homicide Decreases
Frank
Carrington (1978) states- is there any way one can tell whether the death
penalty deters murders from killing? There
is no way one can tell whether the death penalty deters murderers from killing.
The argument goes on that proponents of capital punishments should not
have to bear the burden of proving deterrence by a
reasonable doubt. Nor should
the abolitionist have to prove deterrence by a reasonable doubt -neither side
would be able to anyway.
Frank Carrington (1978) claims common sense supports the inference that
if, the threat of the death penalty decreases, the rate of murders increases
than it may be true. But
if the threat had increased, the homicide rate may decrease.
Justice Stewart held in the Supreme Court in Gregg v. Georgia:
Although
some of the studies suggest that the death
penalty
may not function as a significantly greater
deterrent than lesser penalties, there is no convincing
empirical
evidence supporting or refuting this view.
We
may nevertheless assume safely there are murders,
such as those who act in passion, for whom the threat of
death has little or no deterrent effect. But for many others,
the death penalty undoubtedly, is
a significant deterrent.
There
are carefully contemplated murders, such as murder
for hire, where the possible penalty of death may well enter
the cold calculus that precedes the decision to act
( as cited in Carrington, 1978. p. 87).
J.
Edgar
Hoover, late director of Federal
Bureau of Investigations, asks the
following questions: “Have you ever thought about how many criminals escape
punishment, and yet, the victims never have a chance
to do that? Are crime victims in
the United States today the forgotten people of our time?
Do they receive full measure of justice (as cited in Isenberg, 1977, p.
129)?
A
criminal on death row has a chance to prepare his death, make a will,
and make his last statements, etc. while
some victims can never do it.
There are many other crimes where people
are injured by stabbing, rape, theft, etc.
To some degree at least, the victims right to freedom and pursuit of
happiness is violated.
When the assailant is apprehended and charged, he has the power of the
judicial process who protects his constitutional rights. What about the victim? The
assailant may have compassion from investigating officers, families and friends.
Furthermore, the criminal may have organized campaigns of propaganda to
build sympathy for him as if he is the one who has been sinned against.
These false claims are publicized, for no reason, hence, protecting the
criminal (Isenberg, I., 1977).
The former Theodore L. Sendak, Attorney General of Indiana delivered a
speech to Law enforcement officials in Northern Indiana on
May 12, 1971 (as cited in Isenberg, 1977):
“Our system of criminal law is to minimize human
suffering by works or order primarily to forestall
violence or aggression. In the question of the death
penalty, we must ask ourselves which action will
serve the true humanitarian purpose of criminal law.
We
should weigh the death of the convicted murders
against
the loss of life of his victims and the possibility
of
potential victims to murder (p. 129)
In
arguments of the death penalty, there are two lives to think about.
Too much emphasis is placed on the convicted murderer, the one being
executed, and the victim is all forgotten.
Crime Rate Increases
Millions
are being killed and will be killed because our justice system is not working.
Millions have already been killed and will be killed every year.
According to Time Magazine, there
are 2,000,000 people beaten in the United States.
Some are knifed, shot, or assaulted (Internet).
Crime
growth has been going up in the past because of
too much leniency going hand
in hand with the increased rate of people
being victimized. There are many
loop holes devised for offenders, and because of that
crime rate has increased drastically.
Between l960 to 1968 crime rate increased 11 times.
More and more people are being murdered,
raped, assaulted, kidnapped, and robbed, etc. (Isenberg, I., 1997).
Free Will
When you commit a felony, it is a
matter of free will. No one is
compelled to commit armed robbery, murder, or rape.
The average citizen does not have a mind or intentions
to become a killer or being falsely accused of murder.
What he is worried about is being a victim.
Deterrent in 27 States
Opponents
argue that there is no deterrent effect by using the death penalty. According to Baily, who did a study from l967 to l968, the
death penalty was a deterrent
in 27 states. When
there was a moratorium on Capital Punishment in the United States, the study
showed murder rates increased by 100%. The study also reviewed 14 nations who abolished the death
penalty. It (the study)
claimed murder increased by 7% from five years before the abolition period to
the five years after the abolition (Internet).
Studies were made by Professor Isaac Erlich
between the period of 1933 and 1969.
He concluded “An additional execution per year may have resulted in
fewer murders (Bedau, 1982, p. 323)”.
The
number of years on the average spent in death row is 10 years.
It is known, with all the appeals, the death penalty is not swift!
In fact, most murderers feel they most likely will never be put to death.
If the death penalty was swift and inevitable, there certainly would be a
decrease in homicide rates. (Internet).
Death Feared
Most
people have a natural fear of death- its a trait man have to think about what
will happen before we act. If we
don’t think about it consciously, we will think about it unconsciously. Think- if every murderer who killed someone died instantly,
the homicide rate would be very low because no one likes to die.
We cannot do this, but if the Justice system can make it more swift and severe, we could
change the laws to make capital punishment faster and make appeals a shorter
process. The death penalty is
important because it could save the lives of thousands of potential victims who
are at stake (Bedau, H., 1982).
In
a foot note Bedau (1982) cites, “Actually being dead is no different from not
being born, a (non) experience we all had before being born.
But death is not realized. The
process of dying which is a different matter is usually confused with it.
In turn, dying is feared because death is expected, even though death is
feared because it is confused with dying (p. 338)”.
Death
is an experience that cannot be experienced and ends all experience.
Because it is unknown as it is certain, death is universally feared.
“The life of a man should be sacred to each other (Bedau, H., 1982, p.
330)”.
Innocent Executed -
no Proof
Opponents
claim lots of innocent man are wrongly executed.
There has never been any proof of an innocent man being executed!!
A study by Bedau-Radlet
claimed there were 22 cases where the defendant have been wrongly executed.
However, this study is very controversial.
Studies like Markman and Cassell find that the methodology was flawed in
l2 cases. There was no substantial
evidence of guilt, and no evidence of innocence.
Moreover, our judicial system takes extra precautions to be sure the
innocent and their rights are protected. Most
likely an innocent person would not be executed (Internet).
Death Penalty Saves Lives
The question is whether or not execution of an innocent person is strong
enough to abolish the death penalty. Remember,
the death penalty saves lives. Repeat
murders are eliminated and foreseeable murders are deterred.
You must consider the victim as well as the defendant.
Hugo Bedau (1982) claims:
The
execution of the innocent believed guilty is a
miscarriage
of justice that must be opposed whenever
detected.
But such miscarriage of justice do not
warrant
abolition at the death penalty. Unless
the
moral drawbacks of an activity practice, which include
the possible death of innocent lives that might be saved
by it, the activity is warranted.
Most human activities like
medicine, manufacturing, automobile, and air traffic, sports,
not to mention wars and revolutions, cause death of
innocent bystanders. Nevertheless,
advantages outweigh
the disadvantages, human activities including the penal
system
with all its punishments are morally justified ( p. 323).
Wesley Lowe states, “As for the penal system, accidentally executing an
innocent person, I must point out that in this imperfect world, citizens are
required to take certain risks in exchange for safety.”
He says we risk dying in an accident when we drive a car, and it is
acceptable. Therefore, risking that someone might be wrongfully executed
is worth saving thousand’s of
innocent people who may be the next victim of murder (Internet).
Death Penalty - Right to Live
Opponents say the State is like a murder himself.
The argument here is, if execution is murder, than killing someone in war
is murder. Our country should stop
fighting wars. On the contrary, is
it necessary to protect the rights
of a group of people. Hence, the
death penalty is vital to protect a person’s right to live! Is arresting someone same as kidnapping someone?
In the same, executing someone is not murder, it is punishment by society
for a deserving criminal.
Bible Quotes
Huggo A. Bedau (1982) states one popular objection to Capital punishment
is that it gratifies the desire for revenge regarding as unworthy. The bible
quotes the Lord declaring “Vengeance is mine” (Romans 12:19). He thus legitimized vengeance and reserved it to Himself.
However, the Bible also enjoins, “The murderer shall surely be put
to death” (Numbers 35:16-18), recognizing that the
death penalty can be warranted whatever the motive. Religious tradition certainly suggest no less (p. 330).
All religions believe having life is sacred.
If we deprive someone else life, he only suffers minor inconvenience;
hence, we cheapen human life—this is where we are at
today.
Death Penalty Deterrent Effect
If we do not know whether the death penalty will deter others, we will be
confronted with two uncertainties . If
we have the death penalty and achieve no deterrent effect, than, the life of
convicted murderers has been expended in vain (from a deterrent point of
view)—here is a net loss. If we
have the death sentence, and deter future murderers, we spared the lives of
future victims-(the prospective murderers gain, too; they are spared punishment
because they were deterred). In
this case, the death penalty is a gain, unless the convicted murderer is valued
more highly than that of the unknown victim, or victims (Carrington, F., l978).
Capital
Punishment is not excessive, unnecessary punishment, for those who knowingly and
intentionally commits murder in
premeditation, to take lives of others.
Even though capital
punishment is not used so often, it still is
a threat to the criminal.
Justice
Justice requires punishing the guilty even if only some can be punished
and sparing the innocent, even if all are not spared.
Morally, justice must always be preferred to equality.
Justice cannot ever permit sparing some guilty person, or punishing some
innocent ones, for the sake of equality—because others have been spared or
punished. In practice, penalties
could never be applied if we insisted that they can be inflicted on only
a guilty person unless we are able to make sure that they are equally applied to all other guilty persons. Anyone familiar with the law enforcement knows that punishments can be inflicted only on an unavoidable “shudder” selection of the guilty (Bedau, H., 1977).a guilty person unless we are able to make sure that they are equally applied to all other guilty persons. Anyone familiar with the law enforcement knows that punishments can be inflicted only on an unavoidable “shudder” selection of the guilty (Bedau, H., 1977).
Irwin
Isenberg (1977) said, when you kill
a man with premeditation, you do something different than stealing from him.
“I favor the death penalty as a matter of justice and human dignity
even apart from deterrence. The penalty must be appropriate to the seriousness of the
crime (p. 135).
Life is Sacred
In
an interview with Professor van den Haag, a psychoanalyst and adjunct professor
at New York University, was
questioned, “Why
do you favor the death penalty?” His
answer was that the Federal prison had a man sentenced to Life who, since he has
been in prison committed three more murders on three separate occasions .They
were prison guards and inmates. There’s no more punishment he can receive, therefore, in
many cases, the death penalty is the only penalty that can deter.
He went on saying “I
hold life sacred, and because I hold it sacred, I feel that anyone who takes
some one’s life should know that thereby he forsakes his own and does not just
suffer an inconvenience about being put into prison for sometime (as cited in
Isenberg, 1977, p. 135)
An Eye for an Eye
Some
people argue that the capital punishment tends to brutalize and disregards
society. Do you agree? Some people say the that penalty is legalized murder because
it is like “an eye for an eye”. The
difference between punishment and the crime is that one is legalized and the
other is not! People are more
brutalized by what they see on T.V. daily.
People are not brutalized by punishments they are brutalized by our failure to serious punish,
the brutal acts.
Could
the same effect be achieved by putting the criminal in prison for life? “Life in prison” means in six months the parole board can
release the man to 12 years in some states.
“But even if it were real life imprisonment, it’s deterrent effect
will never be as great as that of the death penalty. The death penalty is the only actually irrevocable penalty.
Because of that, it is the one that people fear the most (Isenberg, I.,
1977).
The framers of the constitution clearly believed that Capital punishment
was an acceptable mess of protecting society form “wicked dissolute men”
Thomas Jefferson liked to talk about it (Carrington, F., 1978).
CONCLUSION
My research on issues on the
death penalty is one of the most
debatable in the criminal justice system. Today,
there are many pros and cons to this death penalty issues.
However, if people weigh the arguments properly, and have empathy for the victims, they will be more inclined to favor capital punishment. As a matter of fact, most people in the U.S. today are in favor of it. But we need more states to enforce the death penalty.However, if people weigh the arguments properly, and have empathy for the victims, they will be more inclined to favor capital punishment. As a matter of fact, most people in the U.S. today are in favor of it. But we need more states to enforce the death penalty.
As you may have read in the arguments, the death penalty help to curtail
future murderers, thus, we can save more lives.
The chances of murdering an innocent man is very minute.
My Opinion
In my opinion, I am in favor of the death penalty, because we can save
innocent lives. Life to me is
scared as Professor Haag stated.
My innocent nephew, Sean Burgado, who
was brutally murdered by a shot gun
to the chest, did not have a choice to make a last statement or make a will before he died.
The people on death row can watch T.V. and enjoy their lives for another
20 years before they are executed. They
can prepare their death by making a
will and a last statement. Sean’s
murder is still unsolved, and the killer is enjoying his life somewhere.
The murderer(s) will
probably murder another person some
day.
I heard on the news last month, February 2000, where a 62 year-old
grandmother, Betty Beets, was
pleading for her life because she was on death row and was going to be executed.
At first, I felt very sorry for her, but after doing research on her, I
learned she had five husbands. She
had already killed the fourth one, and served a prison sentence for
murder, and she got out of prison early. She murdered the fifth husband-she shot him, and buried him in her back yard. Betty Beets was imprisoned a second time, and now was pleading for her life? It has been proven these killers do it again and again. The rate of recidivism is high for people who commit murder and crimes. I feel murderers should be executed the first time because chances are they will come out of prison and kill another innocent person again. We need stricter laws and swift death penalty.murder, and she got out of prison early. She murdered the fifth husband-she shot him, and buried him in her back yard. Betty Beets was imprisoned a second time, and now was pleading for her life? It has been proven these killers do it again and again. The rate of recidivism is high for people who commit murder and crimes. I feel murderers should be executed the first time because chances are they will come out of prison and kill another innocent person again. We need stricter laws and swift death penalty.
I belong to a group called Parents of Murdered Children (POMC).
One of the woman came forward and told me how her husband shot and killed
her five year-old daughter which she witnessed on her birthday.
He attempted to kill the two-year old son, too, but fortunately, the gun
he was using didn’t go off a second time, because it was too old and the
son’s life was sparred. Her
husband’s intention was to kill the two children, and himself on her
(the wife’s) birthday. He said,
if I can’t have my children you
won’t either. Everything to her
is still a nightmare.
He (the husband) was sentenced to death, but committed suicide in prison.
She recently learned that prior to the killing he had contracted someone
$5,000 to burn their house while she and the kids were inside.
She said she would have gone to see her husband being executed if he
lived because she didn’t want him out again.
She said, “To me, I think for the most part, I didn’t care what
happened to him. I just didn’t
want him to be out again after what he did.
I told the District Attorney
that I was afraid that he would get out and try to finish what he started”
(Email, personal communication- March 31, 2000).
There are too many stories like these where
people deserve the death penalty for killing other people.
If they are released from prison,
they will kill other innocent lives again.
I believe life is sacred, therefore, one who takes a life should have his
own life taken away, too. The Lord
said in Exodus “Thou shalt not kill!”. It is one of the Ten Commandments.
The laws today are too lenient.
If there is no death penalty in your
state, and a criminal kills
someone, it is because he felt
he could get out in 10 years
or less from prison. There is no fear of death for him. They
see other murderers in the state get away with murder,
so they, too, can get away
with it. They don’t have to
fear the death penalty. In
fact, I read where a husband intentionally moved to a non death penalty state,
so he could murder his wife and get away with it.
Many murders are premeditated.
People in the “heat of passion” should make it a point to evade the argument or the environment. Remember
it could be one of your loved ones. Can
you imagine what it would be like to have your loved one murdered?
There are no words that can explain the loss of your loved one to murder.
Call your state legislature representatives
today to enforce the death penalty in your state!
Lori
Ornellas
Abstract
The purpose of this paper is to look at both sides of the arguments of
the death penalty-the pros and cons, and how our criminal justice system makes
legislatures, courts, and the U.S. Supreme Court chose to resolve issues.
Interesting issues are brought up like the fear of the death penalty,
bible quotes, how life is sacred, and the execution of the innocent. You will
note too much emphasis is placed on the convicted murderer and not on the
victim. The murderers get out of
prison early and murder again. There are evidence to both sides of the argument in whether
the death penalty is a deterrent or not. In
question of the death penalty, I ask you to
weigh both sides of the argument carefully and make your decision based on the
action that will serve the best humanitarian purpose of criminal law.
Page visited
times since 5/3/01