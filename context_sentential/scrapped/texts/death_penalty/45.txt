Pros
Those who favor keeping the death penalty for juveniles make the following arguments:
- State legislatures should determine whether or not juveniles should be executed for capital crimes, not the courts.
- Juries should determine the culpability of juveniles on a case-by-case basis, on the nature of the crime and the maturity level of the individual juvenile.
- In a society, which is experiencing an increase in violence by juveniles, banning the death penalty would remove a much-needed deterrent.
- What other countries do concerning executing juveniles should not be relevant to the court's consideration of what the United States Constitution demands.
Cons
Those who oppose the death penalty for juveniles make these arguements:
- Executing children is immoral and uncivilized.
- Scientific research shows that juveniles are underdeveloped and immature, particularly in the areas of the brain that dictate reason, impulse control and decision-making, and therefore should not be held culpable.
- A high percentage of juveniles on death row have suffered from mental abuse, physical abuse, sexual abuse, drug addiction, abandonment and severe poverty.
- The execution of juveniles is expressly forbidden in the International Covenant on Civil and Political Rights, the American Convention on Human Rights, the Geneva Convention Relative to the Protection of Civilian Persons in Time of War, and the United Nations Convention on the Rights of the Child.
- With the exception of Somalia, the United States is the only country in the world that still executes juveniles.
Where It Stands
The U.S. Supreme Court heard oral arguments on the juvenile death penalty (Roper v. Simmons) on Oct. 13, 2004 and appeared to be deeply divided on the issue.
The U.S. Supreme Court later voted 5-4 to outlaw the death penalty for juveniles who were under the age of 18 at the time of the crimes, calling the execution of children unconstitutionally cruel.
See Also: Supreme Court Strikes Down Juvenile Death Penalty
- What Is Your Opinion?
Should juveniles be executed if they are convicted of capital crimes just like adults, or should they be excluded from the death penalty. Take our poll and let us know what you think.