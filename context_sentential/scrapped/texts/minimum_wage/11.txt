Labor advocates cheered moves earlier this week by the governors of California and New York to bump the states’ minimum wages up to $15 in coming years.
But since Texas seems unlikely to make a similar change any time soon, the boost in minimum wages may send coastal employers into the embrace of a state whose leaders have boasted about its business-friendly regulatory environment, said Pia Orrenius, an economist at the Federal Reserve Bank of Dallas.
“This intensifies the business climate differential between Texas and these other large states,” she said, adding that roughly a third of Texas’ domestic migrants come from California and New York.
Manufacturers or other employers who pay many of their workers minimum wage might be lured, Orrenius said.
Aside from that, things may not change much — at least, not for Texans, who are subject to the federal minimum wage of $7.25 an hour, said Cheryl Abbot, a regional economist for the U.S. Bureau of Labor Statistics in Dallas.
“Most jobs that pay minimum wage or less are in leisure and hospitality, or eating and drinking,” she said. “They tend to be more localized, local delivery — and would a business who is in local delivery consider moving to another state?”
Orrenius said that, because the federal minimum wage hasn’t been changed since 2009, it hasn’t kept pace with inflation. That means in real spending terms, the minimum wage has effectively been dropping.
In the Republican-dominated state Legislature, a proposal that would have allowed voters to decide whether to increase Texas’ minimum wage to $10.10 an hour failed resoundingly last session. It’s unlikely to be addressed again until 2017.
An increase to get closer to an inflation-adjusted minimum wage, Orrenius said, probably wouldn’t negatively affect job numbers.
“A minimum wage increase at the federal level will help even out the playing field a little bit,” she said.
However, she warned that with a jump in the minimum wage, young workers without a lot of experience will have a harder time getting a job, as employers cut positions they might have been able to afford to keep, Orrenius said.
She added that the costs of goods and services that lower-income people may rely on, such as fast food, would become more expensive.
“The problem is that the wage really should be a price at which you sell the service — it should reflect your productivity as well as supply and demand, when they’re set by the market,” Orrenius said.
Proponents of the higher minimum wage say that employees who work full time should be able to live off their pay, and in places that have high costs of living, that’s becoming increasingly difficult.
The groundswell of support from unions, as well as Democratic politicians, has made a $15 minimum wage a popular option for addressing what Orrenius described as the bigger underlying problem: growing income inequality.
“Economists are almost in agreement that there are much better tools to target those concerns,” she said. “The one we cite most commonly and has proven to be very effective is the earned income tax credit.”
Comments