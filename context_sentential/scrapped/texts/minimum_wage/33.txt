The second term of President Barack Obama will always be remembered for the intense debate prompted by minimum wage increases and adjustments. The White House proposed to increase minimum wages to $10.10. In Seattle, the City Council resolved to increase minimum wage to $15 an hour, and Massachusetts legislators resolved to raise it to $11.
Increasing minimum wages in the United States has always been a touchy issue. Many people believe that they are too low while others think that governments should not promulgate minimum wages in a free market economy. As with many other aspects of the national economy in a capitalistic society, minimum wages have advantages as well as disadvantages.
The Pros
It is important to note that no two national economies are the same; therefore, a stable foreign economy without minimum wage laws should not be used as a meas of comparison. In countries such as Costa Rica and India, minimum wages vary across professions and type of work performed.
Income inequality and fair wealth distribution are two factors of the economy that are kept in check with minimum wages. In consumer economies where the unemployment rate is set by macroeconomic conditions, minimum wages help in keeping inflation from increasing too rapidly.
Worker exploitation and underemployment can be avoided with reasonable minimum wages.
Consumer economies tend to benefit from minimum wages almost immediately trough increased business activity. In other words, workers who make more money will spend more money.
The Cons
Low wages have been a staple of business efficiency since the late 20th century. Businesses that outsource certain processes to developing nations are generally opposed to minimum wages as a matter of cost efficiency.
Some small businesses cannot afford to pay minimum wages. This situation often results in stagnant growth.
Minimum wages are not a cure for poverty. In consumer economies and capitalistic societies, businesses tend to react to minimum wage increases by raising the prices of the goods and services they provide.
Some economists believe that minimum wages prevent equilibrium wages from developing. The equilibrium wage rate is reached when the number of workers is adequate to meet job demand; this is an ideal situation insofar as economic stability, but it requires an effective social contract provided by the government.
Comments are closed.