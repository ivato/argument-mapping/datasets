Story highlights
- The country already has some of the strictest regulations on abortion
- If law passes, a woman who has an abortion could get up to five years in prison
(CNN)Millions of Polish women staged a nationwide strike Monday against a proposed ban on all abortions in the country.
Many took the day off work and school and did not do domestic chores in a day of action inspired by a 1975 women's strike in Iceland.
Some businesses had to close for the day as a result, while others were staffed by men only.
Thousands of protesters took to the streets of Warsaw in a pro-choice march to mark what organizers called "Black Monday."
According to Radio Poland, an English-language station based in Warsaw, millions of people took part in the nationwide protest.
Many at the event in Warsaw dressed in black, as part of the Black Protest movement, which has been trending on social media in recent weeks.
"I was walking around the center of Warsaw for much of the day and there are quite a few cafes and restaurants that are shut because all the staff have gone to the protests. Others are being staffed by men," Ben Stanley, a politics lecturer at SWPS University in Warsaw, told CNN.
Stanley, who is from Britain but has lived in Poland for 15 years, said while there have been several pro-democracy protests in the country in recent months, he cannot recall anything on this scale over a single issue.
He estimated that thousands of protesters congregated in Castle Square in Warsaw, and said most of them were women.
"It is tense but peaceful and I have not seen any reports of arrests or confrontations," he said.
One young man at the protest who spoke to CNN affiliate AVN/TVP said he thinks the proposed government action to ban abortions is not right.
"Every woman should have choice; deciding for women is inhuman," said the man, who didn't give his name.
Poland, a staunchly Catholic nation, already has some of the strictest regulations in Europe when it comes to abortion.
The proposed legislation, should it pass, would put the country's laws on par with Malta and the Vatican.
The punishment for terminating a pregnancy could involve up to five years in prison, and any doctors assisting in abortions would also face jail time.
Poland's current law allows abortion through the 12th week of pregnancy if the woman's health or life is endangered, if the pregnancy results from a crime, or if the fetus is irreparably damaged.
The proposed bill would make abortion illegal in all cases.
On a Facebook page set up to promote the strike -- with the hashtag #blackprotest and #czarnyprotest -- organizers wrote that "even in the cases when a woman should have a right to do abortion, she might still be refused the operation by a doctor on the grounds of conscience."
The organizers called on the international feminist community to mobilize in support of the protest in Poland. There were displays of solidarity from individuals and groups, including activities in Berlin and Chicago.
The 1975 strike in Iceland that inspired Monday's action was held to protest wage discrepancy and unfair employment practices against women.
Women were urged to not attend work if they had paying jobs, nor do any housework or child care. As many as 90% of Icelandic women participated in the protest and it essentially paralyzed the country, according to the Global Nonviolent Action Database.