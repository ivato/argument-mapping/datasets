From this selection of readings (some having www links), an
opportunity to challenge your thinking about guns, gun control, and gun rights.
"Shot Stories" by Tim Long, SPY Magazine
What it feels like to get shot. Descriptions of the experience are given by
near-famous people. The writer also offers 'the greatest song ever written
about getting shot.'