Saul Cornell, PhD, Ohio State University Professor of History, in a Jan. 2001 article titled "The Second Amendment Under Fire: The Uses of History and the Politics of Gun Control," published by the George Mason University project "History Matters," wrote:
"The debate over the Second Amendment pits supporters of an individual right to gun ownership against those who believe the Bill of Rights only protects the right of the people to maintain a well-regulated militia. Apart from a few Second Amendment absolutists, most supporters of the individual rights view believe that some restrictions on gun ownership are allowable. But such restrictions must meet a very high standard of constitutional scrutiny...The second view, the collective rights interpretation, argues that the Bill of Rights provides no protection for an individual's right to own guns..."
Sanford Levinson, JD, PhD, Professor of Law, in a Dec. 1989 article titled "The Embarrassing Second Amendment," published in the Yale Law Journal, wrote:
"...Indeed, one sometimes discovers members of the [National Rifle Association] who are equally committed members of the ACLU, differing with the latter only on the issue of the Second Amendment but otherwise genuinely sharing the libertarian viewpoint of the ACLU."
Is Gun Ownership an Individual Right Guaranteed by the Second Amendment?
PRO (yes)
CON (no)
The National Rifle Association's Institute for Legislative Action (NRAILA), in a Sep. 18, 2006 posting to its website titled "Fables, Myths and Other Tall Tales about Gun Laws, Crime and Constitutional Rights," argued that:
"Gun control supporters insist that 'the right of the people' really means the 'right of the state' to maintain the 'militia' mentioned in the amendment, and that this 'militia' is the National Guard.
Such a claim is not only inconsistent with the statements of America's early statesmen and the concept of individual rights as understood by generations of Americans, it misdefines the term 'militia.' For centuries before the drafting of the Second Amendment, European political writers used the term 'well regulated militia' to refer to the citizenry on the whole, armed with privately-owned weapons, led by officers chosen by themselves."
John Ashcroft, JD, former US Attorney General, in a May 17, 2001 letter to NRAILA Executive Director James Jay Baker, wrote:
"[L]et me state unequivocally....the Second Amendment clearly protect[s] the right of individuals to keep and bear firearms...
While some have argued that the Second Amendment guarantees only a 'collective' right of the States to maintain militias, I believe the Amendment's plain meaning and original intent prove otherwise. Like the First and Fourth Amendments, the Second Amendment protects the rights of 'the people,' which the Supreme Court has noted is a term of art that should be interpreted consistently throughout the Bill of Rights."
In US v. Emerson, in an opinion written by Justice Garwood Oct. 16, 2001, the US Fifth Circuit Court of Appeals held:
"We have found no historical evidence that the Second Amendment was intended to convey militia power to the states, limit the federal government's power to maintain a standing army, or applies only to members of a select militia while on active duty. All of the evidence indicates that the Second Amendment, like other parts of the Bill of Rights, applies to and protects individual Americans.
We find that the history of the Second Amendment reinforces the plain meaning of its text, namely that it protects individual Americans in their right to keep and bear arms whether or not they are a member of a select militia or performing active military service or training."
American Civil Liberties Union (ACLU) "Policy #47," as outlined in a Mar. 4, 2002 posting to its website, stated:
"The ACLU agrees with the Supreme Court's long-standing interpretation of the Second Amendment [as set forth in the 1939 case, U.S. v. Miller] that the individual's right to bear arms applies only to the preservation or efficiency of a well-regulated militia. Except for lawful police and military purposes, the possession of weapons by individuals is not constitutionally protected. Therefore, there is no constitutional impediment to the regulation of firearms."
John Massaro, JD, author of the 2008 book No Guarantee of a Gun: How and Why the Second Amendment Means Exactly What It Says, wrote in a Sep. 29, 2008 email to ProCon.org:
"[I]ndividual gun ownership is NOT a right
guaranteed by the Second Amendment. The Second Amendment does NOT
guarantee the right of the individual to possess and carry weapons. The
Second Amendment guarantees the right of the general populace to store weapons
and render military service as the Organized State Militia that is
known today as the National Guard."
Seth Waxman, JD, former US Solicitor General, in an Aug. 22, 2000 letter to an NRA member, wrote:
"...There is no personal constitutional right, under the Second Amendment, to own or use a gun...
That position is consistent with the view of the Amendment taken both by the federal appellate courts and successive administrations.
[The Supreme Court and eight Appellate Courts] 'have uniformly rejected arguments that it extends firearms rights to individuals independent of the collective need to ensure a well-regulated militia.'"
In United States v. Warin (1976), in a decision written by District Judge David M. Lawson, the Sixth Circuit Court of Appeals held:
"Since the Second Amendment…applies only to the right of the State to maintain a militia and not to the individual's right to bear arms, there can be no serious claim to any express constitutional right to possess a firearm."