Enter your mobile number or email address below and we'll send you a link to download the free Kindle App. Then you can start reading Kindle books on your smartphone, tablet, or computer - no Kindle device required.
To get the free app, enter your mobile phone number.
Learn more
The War on Guns: Arming Yourself Against Gun Control Lies Hardcover – August 1, 2016
Price
|New from||Used from|
Books on Limiting Government Reach
Leading up the election season, here are books on the benefits of small government. Learn more | See related books
Frequently Bought Together
Editorial Reviews
Review
—TED CRUZ, U.S. senator, Texas
"Amidst threats of terrorism, the need for John Lott's The War on Guns and a rational debate on guns has never been greater. Paris and San Bernardino show us that gun-free zones won’t stop terrorists. As this book documents, these killers consciously pick targets where they know victims will be sitting ducks. Lott carefully proves that the push for more gun control only makes the types of attacks that we fear more likely to occur. Next time you drop your child off at school, try and find the sign saying, 'Gun Free Zone.' Stop for a moment and ask yourself, 'Does this really keep my child safe?' If you still have questions, read The War on Guns."
—NEWT GINGRICH, former Speaker of the House
"John Lott is the nation's preeminent expert on guns, and in The War on Guns he has done it again. Do mass shootings occur more in the U.S. than other developed countries? No. Did Australia's gun laws make them safer? Hardly. Will background checks on private transfers of guns make us safer? No. He destroys one myth after another. John Lott is a national treasure."
—MARK LEVIN, constitutional lawyer, New York Times bestselling author, and host of the nationally syndicated radio show, The Mark Levin Show
"The war on guns will never end because the anti-gun zealots will never raise the white flag even though the research and data in this book has soundly defeated them. The key is to continue to beat back their propaganda, misinformation, manipulation of data, and even outright lies, in order to protect our God-given right to keep and bear arms."
—SHERIFF DAVID A. CLARKE JR., sheriff of Milwaukee County, Wisconsin
"John Lott is the most dogged, intellectually credible, academic defender of the right to keep and bear arms in the United States today. He is also the chief beast in the night to the gun control crowd. His works are must reading for those of us on the front lines in these debates. The War on Guns is his best work yet. In it, the reader will find well-documented all the data, statistics, practical, legal, and moral arguments one will ever need to support the natural right to self-defense. The statists will fear this book. Freedom lovers will crave it."
—HON. ANDREW P. NAPOLITANO, senior judicial analyst Fox News Channel, and Distinguished Visiting Professor of Law, Brooklyn Law School
"To paraphrase a once-famous commercial slogan, 'When John R. Lott speaks, everyone listens.' Or, at least they should when this leading expert on all matters gun control speaks out so provocatively and persuasively. In his new book, Lott delves into the myriad ways in which anti-gun 'statistics' and 'research' have been used to perpetrate utter falsehoods and misleading propaganda. If there is to be any intelligent, honest, and objective discussion of gun policy in the United States, then The War on Guns ought to be required reading."
—CHARLES J. GOETZ, Joseph M. Hartfield Professor of Law Emeritus, University of Virginia School of Law
"John R. Lott is a role model for those who do scientific research on important problems. He is objective and logical. He uses the best evidence available—data from natural experiments on the effects of gun regulations—to compare the effects of alternative policies. He fully discloses his analysis, and responds to critics by conducting further research. As a result, his findings have persuaded many people. Lott's scientific approach is the opposite of the advocacy research that fills many academic journals, and which the media delight in reporting. Lott's The War on Guns exposes and explains the deceptions used by gun-control advocates. Those who want to live in a safer world will benefit from Lott's findings. Of the many gun regulations to date, there are no scientific comparisons that have found a reduction in crime or death rates."
—PROFESSOR J. SCOTT ARMSTRONG, The Wharton School, University of Pennsylvania
"We should all thank and admire John Lott for single-handedly changing the debate on guns from ill-informed rhetoric attacking gun ownership to hard-headed empirical analysis that shows the benefits of allowing law abiding citizens to carry concealed weapons. The War on Guns continues this debate by showing that the anti-gun lobby has spent millions of dollars making false and misleading claims about guns. Once again, John Lott provides careful and rigorous empirical analysis that undermines these claims. Kudos to John Lott for having the guts to take on the anti-gun lobby."
—PROFESSOR WILLIAM M. LANDES, senior lecturer and Clifton R. Musser Professor Emeritus of Law & Economics, University of Chicago Law School
"John Lott is more responsible than anyone else for arguments that gun ownership in the U.S. increases safety and reduces crime. Arrayed against him are the entire public health establishment and much of the media. In this book, John carefully analyzes many of the arguments made against gun ownership—for example, there are more gun homicides and mass shootings in the U.S. than elsewhere, background checks reduce gun harms, "Stand Your Ground" laws harm African Americans and increase crime—and shows using both statistical and anecdotal evidence that they are incorrect. He also shows that wealthy opponents of gun ownership (such as Michael Bloomberg) finance much fallacious "public health" research on the effects of guns. Anyone interested in the gun debate should read this book, and opponents of gun ownership have an intellectual obligation to confront the arguments."
—PROFESSOR PAUL H. RUBIN, Samuel Candler Dobbs Professor of Economics, Emory University
"John Lott's new book, The War on Guns: Arming Yourself Against Gun Control Lies, does just that. Lott deals with a wide variety of claims that question the value of firearms for self-defense, providing the analysis and facts the public needs to see through the distortions of gun control advocates. This is a valuable guide to a more balanced understanding of the issue."
—PROFESSOR JOYCE LEE MALCOLM, Patrick Henry Professor of Constitutional Law and the Second Amendment, Antonin Scalia Law School, George Mason University
"John Lott's new book, The War on Guns: Arming Yourself Against Gun Control Lies, is an indispensable source of facts, insights, and cogent argument. Anyone who wants to be informed on the gun control issue has to read this book."
—CARLISLE E. MOODY, Professor of Economics, William and Mary
From the Inside Flap
They’re coming for your guns
That’s not a scaremongering phraseit’s a simple truth.
And it comes from John R. Lott Jr., Ph.D., bestselling author of the debate-changing More Guns, Less Crime.
In this, his latest and most important book, The War on Guns, Lott offers the most thorough debunking yet of the so-called facts,” data,” and arguments” of anti-gun advocates, exposing how they have repeatedly twisted or ignored the real evidence, the evidence that of course refutes them on every point.
In The War on Guns, you’ll learn:
Why gun licenses and background checks don’t stop crime
How gun-free” zones actually attract mass shooters
Why Stand Your Ground laws are some of the best crime deterrents we have
Why having background checks on private gun transfers is a bad idea
How big-money liberal foundations and the federal government are pouring hundreds of millions of dollars into public health” studies, the sole purpose of which is to manufacture false data against guns
How media bias and ignorance skew the gun debateand why it will get worse
How anti-gun activists have targeted not just the Second Amendment, but the First Amendmentall in an effort to shut down pro-gun arguments
Concerned about your gun rights? You should be. And John R. Lott Jr.’s The War on Guns is the essential book for defending your Second Amendment Rightsbefore they are taken away forever.
Don't have a Kindle? Get your Kindle here, or download a FREE Kindle Reading App.
Product Details
Would you like to update product info or give feedback on images?
Customer Reviews
Top Customer Reviews
Given how Lott has spent his career, I’m not surprised this is such an important book.
I first met John R. Lott, Jr. in 2006 at a cocktail party in Arlington, VA. I was speaking with a liberal journalist about his soon-to-be-released book on Iraq when John Lott joined us. John listened for a moment and then said to the author, “I’m curious. You say you just finished a book on the Iraq war. I always find it so hard to finish a book. I get so deep into the research I have a hard time stopping to write. I’m guessing you had a hard time leaving Iraq. There is so much to investigate and understand.”
The author said, “I didn’t go to Iraq.”
John paused with this quizzical look on his face before asking, “Oh, how did you do your research?”
The author said, “I didn’t have to do much. I mean, I already know what I think.”
An awkward silence ensued before the liberal author shrugged and moved away.
Yeah, I liked John from that moment—well, and before. I had, of course, already read and learned much from his groundbreaking book More Guns, Less Crime. That book detailed his research that found that, despite what people are often told, violent crime rates actually tend to go down when states pass “shall-issue” concealed-carry laws. (Shall-issue laws force a licensing agent to give anyone a concealed-carry permit to carry a handgun as long as the individual passing certain criteria as stipulated by law.)
As soon as More Guns, Less Crime was published a lot of academics made it their mission to prove Lott to be the fraud they were sure he must be.Read more ›
Let me emphasize: while very detailed and full of useful data, it is a lively read unlike many scholarly works that others have published.
Right now, Obama and Hillary's plan seems to be to let in tens of thousands of potential jihadi's and then scream for gun control when the inevitable happens.
The topic addressed in this book is chiefly the messaging having to do with firearms policy, so the author's two obvious goals are to first characterize those policies and effects, and then to pierce through the often heated rhetoric about how those policies or effects are characterized
to the public. This is not a dry recitation of research facts - it is a forthright description of the 'stuff' of public discourse on guns, crime and safety, made accessible to readers and fully supported in the reference lists, notes and appendices for whose who wish to check Lott's work. Because of the author's dual goals, both the research and illustrative examples from media (the "messaging" part) are included in the copious notes and citations. This work is timely as well, and I appreciate having a comprehensive inventory of topics which percolate to the top of public debate
during this political season.
As the saying goes, one should be entitled to his own opinions but not his own facts. Unfortunately when it comes to firearms there are too many assertions floating around in public forums disguised as facts, and as such these serve no useful purpose if the goal is sensible and effective consensus on ways forward as a community. Thoughtful voters who prefer a fair and balanced perspective of the science underlying assertions they hear made made in the media or by one or another candidate should arm themselves with this work so they may judge the speaker accordingly. Lott's work slices through the emotional haze and helps readers to make informed decisions for themselves.
Lott's landmark "More Guns, Less Crime" changed the way the world understands guns in the hands of lawful citizens. In "The Bias Against Guns" he exposed how media, the academy and the entertainment industry perpetuate untruths about guns that make people less safe. In "The War on Guns", John Lott has given us the best "gun book" I've ever read. This book will give you the tools you need to defend your gun rights and explain their value persuasively if you're a gun owner. If you simply want to understand the balanced, nuanced facts about guns and their impact on crime and public safety, no book yet written does as good a job as "The War on Guns". I strongly recommend this fantastic read.
Customer Images
Most Recent Customer Reviews
Required reading to separate the wheat from the chaff, the fire from the smoke and mirrors, the truth from the lies.Published 17 hours ago by David M. Gross
I enjoyed this book very much and I have been recommending it to many people. It is written with a natural biased for the argument however John Lott does a good job of presenting... Read morePublished 18 hours ago by Aeby Richard James Perez
Excellent book filled with a lot of useful information. Anyone who cares about their 2nd amendment rights should read this book.Published 2 days ago by Christopher
Professor Lott makes clear the bias of the media against the Second Amendment and uses data to support his claim.Published 2 days ago by John B.
The War On Guns is exceptionally well researched and full of details and supporting documentation if you want to know the backdrop. Read morePublished 6 days ago by Wizard