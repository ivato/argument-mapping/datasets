Summary
With a growing number of states considering the legalization of medical marijuana and, more broadly, the decriminalization of recreational marijuana, this Forum event examined the public health pros and cons. What does science tell us about the benefits and risks of using marijuana? What are the economics at play? And what impact might decriminalization make on social justice?
Part of: Policy Controversies.
Presented in Collaboration with Reuters
Image Credit: iStockphoto.com l gaspr13