At present, the U.S. government claims the right to, and does, criminalize the growing, selling and possession of marijuana in all states. The U.S. Supreme Court affirmed this federal right in two separate court cases:
- In 2001, U.S. v. Oakland Cannabis Buyers' Cooperative, which overturned California proposition 215 which, in 1996, legalized marijuana for medicinal purposes.
- In 2005, Gonzales v. Raich, which again upheld the right of the federal government to ban marijuana use in all states.
What Is Marijuana?
Marijuana is the dried blossom of cannabis sativa and cannabis indica plants, leafy annual plants with parts that are used for as herbs, animal food, medicine and as hemp for rope-making.
"The flowers... contain psychoactive and physiologically active chemical compounds known as cannabinoids that are consumed for recreational, medicinal, and spiritual purposes," per Wikipedia.
Why is Marijuana Banned in the U.S.?
Before the 20th century, cannabis plants in the U.S. were relatively unregulated, and marijuana was a common ingredient in medicines.
Recreational use of marijuana was thought to have been introduced in the U.S. early in the 20th century by immigrants from Mexico. In the 1930s, marijuana was linked publicly in several research studies, and via a famed 1936 film named "Reefer Madness," to crime, violence, and anti-social behavior.
Many believe that objections to marijuana first rose sharply as part of the U.S.
temperence movement against alcohol. Others claim that marijuana was initially demonized partly due to fears of the Mexican immigrants associated with the drug.
In the 21st century, marijuana is illegal in the U.S. ostensibly due to moral and public health reasons, and because of continuing concern over violence and crime associated with production and distribution of the drug.
Latest Developments
On June 23, 2011, a federal bill to fully legalize marijuana was introduced in the House by Rep. Ron Paul (R-TX) and Rep. Barney Frank (D-MA.) The bill would remove marijuana from controlled substance classification.
On February 25, 2009, Attorney General Eric Holder announced that "that federal agents will now target marijuana distributors only when they violate both federal and state laws."
The impact of Holder's statement is that if a state has legalized marijuana, then the Obama administration will not override state law. To date, thirteen states have decriminalized marijuana for medical purposes only.
In 2010, States Move to Loosen Marijuana LawsIn November 2010, Californians defeated a ballot proposition that "Californians over age 21 would be able to possess up to one ounce of marijuana, and grow their own plants on a plot up to 5 feet by 5 feet large."
Washington, New Hampshire and Massachusetts state legislatures are slated to consider marijuana legalization bills in 2010-2011. And more than 20 other states are considering bills to otherwise loosen criminalization of marijuana use and possession.
President Obama Avoids the Marijuana Question
When asked at a March 2009 online town hall about marijuana legalization, President Barack Obama avoided a serious answer, and laughingly demurred "I don't know what this says about the online audience. But, no, I don't think that is a good strategy to grow our economy" Wrote an irritated college newspaper columnist at State University of New York at Geneseo:
"Many people were disappointed by his reaction, as Obama did not offer any counter-arguments and completely ignored the potential medical and social benefits of ending the prohibition on marijuana.
"Yes, some who wish to see marijuana legalized use it for recreational purposes, but the other benefits cannot be ignored. Cannabis is known to ease pain disorders, including the side-effects cancer patients experience throughout treatment.
"In addition to this, legalization would strike an enormous blow to organized crime, free up the overflowing prison system and reduce violence along the Mexican-American border."
Obama Supported Decriminalization in 2004
However, in a 2004 appearance at Northwestern University, then Illinois politician Obama told a crowd, "I think the war on drugs has been a failure, and I think we need to rethink and decriminalize our marijuana laws."
(See page two of this article for specific Pros & Cons of Legalizing Marijuana.)
Background
The following are milestones of federal marijuana enforcement in U.S. history:
- Prohibition, 1919 to 1933 - As use of marijuana became popular in response to alcohol prohibition, conservative anti-drug campaigners railed against the "Marijuana Menace," linking the drug to crime, violence and other bad behaviors.
- 1930, Federal Bureau of Narcotics established - By 1931, 29 states had criminalized marijuana.
- Uniform State Narcotic Act of 1932 - Pushed states, rather than federal authorities, to regulate narcotics.
- Marijuana Tax Act of 1937 - Restricted marijuana to persons who paid an excise tax for certain medical uses of the drug.
- 1944, New York Academy of Medicine - Report finds that marijuana does not "induce violence, insanity or sex crimes."
- Narcotics Control Act of 1956 - Set mandatory prison sentences and fines for drug offenses, including for marijuana.
- 1960s Counter-Culture Movement - U.S. marijuana use grew rapidly. Studies commissioned by Presidents Kennedy and Johnson concluded that "marijuana use did not induce violence."
- 1970 in Congress - Repealed mandatory penalties for drug offenses. Marijuana was differentiated from other drugs.
Per PBS, "It was widely acknowledged that the mandatory minimum sentences of the 1950s had done nothing to eliminate the drug culture that embraced marijuana use throughout the 60s... "
- 1973, Drug Enforcement Agency - Created by President Nixon.
- 1976, Conservative Christian Groups - Led by Rev. Jerry Falwell's Moral Majority, rising conservative groups lobbied for stricter marijuana laws. The coalition grew powerful, leading to the 1980s "War on Drugs."
- Anti-Drug Abuse Act of 1986 - Pushed for and signed by President Reagan, the Act raised penalties for marijuana offenses, and established harsh mandatory "three strikes" sentencing laws.
- 1989, New "War on Drugs" - Declared by President George H.W. Bush
- 1996 in California - Voters legalized marijuana use for cancer, AIDS, glaucoma and other patients, via a doctor's prescription.
- 1996 to 2008 - 12 other states legalized medicinal marijuana use, albeit with widely varying restrictions.
Most states can't implement their programs, though, as the Bush DEA executed a series of surprise raids on marijuana clinics, arresting both sellers and patients. The White House claimed that federal law held precedence over state legislatures. MAIN SOURCE: Condensed from materials produced by PBS and WGBH/Frontline.