import re
import spacy
import stanza
import os
import pandas as pd
import stanza.server
from stanza.server import client as clientCoreNLP
from difflib import SequenceMatcher

class TextPreprocessor:

  paragraph_regex = re.compile(r"\.[\n\r]")

  def __init__(self, text, tokenizer='stanza'):
    self.text = text
    if tokenizer == 'stanza':
      if not os.path.exists('/root/stanza_resources/en/default.zip'):
        stanza.download('en')
      self.nlp = stanza.Pipeline(lang='en', verbose=False, processors='tokenize,pos')
    elif tokenizer == 'spacy':
      self.nlp = spacy.load("en_core_web_sm")
    elif tokenizer == "corenlp":
      self.nlp = clientCoreNLP.CoreNLPClient(
              annotators=['tokenize','ssplit', 'pos'], 
              memory='1G', 
              endpoint='http://localhost:9001',
              start_server=stanza.server.StartServer.TRY_START,
              be_quiet=True)
    else:
      raise RuntimeError(f"Tokenizer {tokenizer} not valid")
    self.tokenizer = tokenizer

  # split a text to spans (begin and end), replacing newlines with whitespace 
  def span_segments(text): 
    spans = [] 
    output_str = '' 
    idx_start = 0
    for i, span in enumerate(text.split('\n')): 
      span_len = len(span) 
      if span_len != 0:
        idx = text.index(span, idx_start) 
        spans.append((idx,idx+span_len))
        idx_start = idx+span_len+1
        if i == 0:
          output_str = span
        else: 
          output_str = output_str + ' ' + span 
    return (spans, output_str) 

  def del_noisy_chars(text, noisy_chars=['"','”','“']):
    pattern = '[' + ''.join(noisy_chars) + ']'    
    text = re.sub(pattern, '', text)
    return text

  def paragraphs(self):
    # return [paragraph.strip() for paragraph in self.text.splitlines()]
    return [paragraph for paragraph in TextPreprocessor.paragraph_regex.split(self.text)]

  def has_verb(self, sentence):
    result = False
    if self.tokenizer == 'spacy':
      for token in sentence:
        if token.pos_ == "VERB":
          result = True
          break
    elif self.tokenizer == 'stanza':
      for token in sentence.words:
        if token.upos == "VERB":
          result = True
          break
    elif self.tokenizer == 'corenlp':
      for i in range(len(sentence.token)):
        if sentence.token[i].pos[0:2] == 'VB':
          result = True
          break 
    else:
      raise RuntimeError(f'tokenizer ({self.tokenizer}) not exist')
    return result

  # break text into sentences
  def sentences(self, min_tokens=3, require_verb=False, del_noisy_chars=False):
    sents = []
    if del_noisy_chars:
      self.text = del_noisy_chars(self.text)
    if self.tokenizer == 'spacy':
      doc = self.nlp(self.text)
      for sent in doc.sents:
        if len(sent) > min_tokens:
          if require_verb:
            if self.has_verb(sent):
              sents.append(str(sent).strip())
          else:
            sents.append(str(sent).strip())
    elif self.tokenizer == 'stanza':
      doc = self.nlp(self.text)
      for sent in doc.sentences:
        if len(sent.words) > min_tokens:
          if require_verb:
            if self.has_verb(sent):
              sents.append(sent.text.strip())
          else:
            sents.append(sent.text.strip())
    elif self.tokenizer == 'corenlp':
      doc = self.nlp.annotate(self.text)
      index_sent_start = 0
      index_sent_end = 0            
      for i in range(len(doc.sentence)):
        sent = doc.sentence[i]
        if len(sent.token) > min_tokens:
          if require_verb:
            if self.has_verb(sent):
              for token in sent.token:
                index = self.text.index(token.word, index_sent_end)
                index_sent_end = index + len(token.word)
              sentence = self.text[index_sent_start:index_sent_end]
              sents.append(sentence)
              index_sent_start = index_sent_end+1
          else:
            for token in sent.token:
              index = self.text.index(token.word, index_sent_end)
              index_sent_end = index + len(token.word)
            sentence = self.text[index_sent_start:index_sent_end]
            sents.append(sentence)
            index_sent_start = index_sent_end+1
    return sents
  
  # direct segmentation from text
  def segment(self):
    sentences_segments = []
    sentence_start = 0
    for sentence_text in self.sentences():
      sentence_index = self.text.index(sentence_text, sentence_start)
      sentence_end = sentence_index+len(sentence_text)
      sentence_segment = (sentence_index, sentence_end)
      sentences_segments.append(sentence_segment)
      sentence_start = sentence_end
    self.sentences_segments = sentences_segments
    return self.sentences_segments

class ParagraphPreprocessor(TextPreprocessor):
  def __init__(self, text, tokenizer='stanza'):
    self.segmented_paragraphs = { "text": text }
    self.segmented = False
    super().__init__(text, tokenizer)

  def sentences(self):
    if not self.segmented:
      self.segment()
    text = self.segmented_paragraphs['text']
    text_sentences = []
    for paragraph in self.segmented_paragraphs['paragraphs']:
      for sentence in paragraph['sentences']:
        start = sentence[0]
        end = sentence[1]
        text_sentences.append(text[start:end])
    return text_sentences  

  # segment sentences in paragraphs
  def segment(self):
    paragraphs = []
    paragraph_start = 0
    for paragraph_text in self.paragraphs():
      paragraph = {}
      paragraph_index = self.text.index(paragraph_text, paragraph_start)
      paragraph['start'] = paragraph_index
      paragraph['end'] = paragraph_index+len(paragraph_text) 
      paragraph_start = paragraph['end']
      paragraph['sentences'] = []
      sentence_start = paragraph['start']
      tp_paragraph = TextPreprocessor(paragraph_text)
      for sentence_text in tp_paragraph.sentences():
        sentence_index = self.text.index(sentence_text, sentence_start)
        sentence_end = sentence_index+len(sentence_text)
        sentence = (sentence_index, sentence_end)
        paragraph['sentences'].append(sentence)
        sentence_start = sentence_end
      paragraphs.append(paragraph)
    self.segmented_paragraphs["paragraphs"] = paragraphs
    return self.segmented_paragraphs

  def enumerated_sentences(text, sentences_segments):
    sentences_enum = []
    for sentence_num, sentence in enumerate(sentences_segments):
      sentence_text = text[sentence[0]:sentence[1]].strip()
      sentences_enum.append((sentence_num, sentence_text))
    return sentences_enum

  def print_segments(text, par_segments):
    for paragraph_num, par_segment in enumerate(par_segments):
      paragraph_text = text[par_segment['start']:par_segment['end']].strip()
      print(f"{paragraph_num}: {paragraph_text}")
      sentences_enum = ParagraphPreprocessor.enumerated_sentences(text,par_segment['sentences'])
      for sentence_num, sentence_text in sentences_enum:
        print(f"{paragraph_num}.{sentence_num}: {sentence_text}")

  def print(self):
    text = self.segmented_paragraphs['text']
    par_segments = self.segmented_paragraphs['paragraphs']
    ParagraphPreprocessor.print_segments(text, par_segments)
    return self