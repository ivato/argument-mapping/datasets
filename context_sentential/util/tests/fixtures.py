import pytest
import os
from util.dataset import Dataset, UKPSententialDataset, ScrappedDataset
from util.preprocess import TextPreprocessor, ParagraphPreprocessor

@pytest.fixture
def text_from_scrapped_dataset(dataset_path=Dataset.default_ds_path('scrapped'), topic_num=0, url_num=0):
  scrapped_dataset = ScrappedDataset(dataset_path).load()
  return scrapped_dataset.dataframes[topic_num]['text'][url_num]

@pytest.fixture
def cleaned_text_from_scrapped_dataset(dataset_path=Dataset.default_ds_path('scrapped'), topic_num=0, url_num=0):
  text = text_from_scrapped_dataset(dataset_path, topic_num, url_num)
  return TextPreprocessor.del_noisy_chars(TextPreprocessor(text).text)

@pytest.fixture
def paragraph_sentences(dataset_path=Dataset.default_ds_path('scrapped'), topic_num=0, url_num=0):
  cleaned_text = cleaned_text_from_scrapped_dataset(dataset_path=Dataset.default_ds_path('scrapped'), topic_num=0, url_num=0)
  paragraph_sentences = ParagraphPreprocessor(cleaned_text).sentences()
  return paragraph_sentences

@pytest.fixture
def sentences_from_scrapped_dataset(dataset_path=Dataset.default_ds_path('scrapped'), topic_num=0, url_num=0):
  cleaned_text = cleaned_text_from_scrapped_dataset(dataset_path=Dataset.default_ds_path('scrapped'), topic_num=0, url_num=0)
  ds_sentences = TextPreprocessor(cleaned_text).sentences()
  return ds_sentences

@pytest.fixture
def urls_from_ukp_dataset(topic_num, dataset_path=Dataset.default_ds_path('complete')):
  sent_dataset = UKPSententialDataset(dataset_path).load()
  return sent_dataset.archived_urls(topic_num)

@pytest.fixture
def sentences_from_ukp_dataset(dataset_path=Dataset.default_ds_path('complete'), topic_num=0, url_num=0, clean=False):
  sent_dataset = UKPSententialDataset(dataset_path).load()
  url = sent_dataset.archived_urls(topic_num)[url_num]
  sentences = sent_dataset.lines_from_archive(topic_num, url)['sentence'].values.tolist()
  result_sentences = []
  if clean:
    for sentence in sentences:    
      sentence = TextPreprocessor.del_noisy_chars(sentence)
      result_sentences.append(sentence)
  else:
    result_sentences = sentences
  return result_sentences
