import torch
from lm_scorer.models.auto import AutoLMScorer as LMScorer

class LanguageModel:
  def __init__(self, model='gpt2', batch_size=1):
    self.model = model
    self.batch_size = 1
    self.device = "cuda:0" if torch.cuda.is_available() else "cpu"
    self.scorer = LMScorer.from_pretrained(
      self.model, 
      device=self.device, 
      batch_size=self.batch_size)

  def sentence_score(self, sentence, reduce='gmean'):
    return self.scorer.sentence_score(sentence, reduce=reduce)

  def supported_models():
    return list(LMScorer.supported_model_names())
