class Prompter:
  """Manage prompting using templates
  
  :ivar: template: a string with {Xi} marks as input and a mark {Z} as an output
  :example:
      - template example: '{X[0]} is a {Z} argument for {X[1]}'
  """

  def __init__(self, template=None):
    self.template = template
    self.prompt = None
    self.filled_prompt = None
    self.answered_prompt = None

  def fprompt(self, _input):
    """Prompting function, fills the sentence input at {X} slot and generates

    :param _input: a tuple of strings to fill a template mark {Xi}
    :type _input: vector of strings
    :return: the prompter object with prompt attribute updated
    :rtype: updated Prompter object
    """
    X = []
    for input_value in _input:
      X.append(input_value)
    Z = '{Z}'
    self.prompt = self.template.format(X=X, Z=Z)
    return self

  def fill_prompt(self, response):
    """Fill prompt with a response

    :param response: a string to fill template mark {Z} of prompt
    :type response: str
    :return: the prompter object with filled_prompt attribute updated
    :rtype:  updated Prompter object
    """
    self.filled_prompt = self.prompt.format(Z=response)
    return self

  def answer_prompt(self, answer):
    """Fill prompt with the true answer

    :param answer: a string to fill template mark {Z} of prompt with answer
    :type answer: str
    :return: the prompter object with answered_prompt attribute updated
    """
    self.answered_prompt = self.prompt.format(Z=answer)
    return self

class Template:
  pass

class Response:
  pass

