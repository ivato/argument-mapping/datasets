import trafilatura
import tika
from tika import parser
import requests
import pandas as pd
import tempfile
from pdfminer.high_level import extract_text_to_fp
from io import StringIO
from enum import Enum
from selenium import webdriver

class Downloader(Enum):
  REQUESTS_LIB = 1
  SELENIUM_CROMIUM = 2

class HTMLScrapper(Enum):
  TRAFILATURA = 1
  TIKA = 2
  def alternatives(scrapper):
    others = []
    for other in HTMLScrapper:
      if other != scrapper:
        others.append(other)
    return others  

class PDFScrapper(Enum):
  TIKA = 1
  PDFMINER = 2
  def alternatives(scrapper):
    others = []
    for other in PDFScrapper:
      if other != scrapper:
        others.append(other)
    return others  

# scrap from a URL
class URLScrapper:
  def __init__( self, 
                url = None,
                content = None,
                content_type = None,
                download_status = None,
                html_scrapper=HTMLScrapper.TRAFILATURA, 
                pdf_scrapper=PDFScrapper.TIKA,
                alternative_html_scrappers=True,
                alternative_pdf_scrappers=True,
                browser=None):
    self.html_scrapper = html_scrapper
    self.pdf_scrapper = pdf_scrapper
    self.final_scrapper = None
    self.text = None
    self.content = content
    self.content_type = content_type
    self.url = url
    self.download_status = download_status
    self.scrap_status = None
    self.alternative_html_scrappers = alternative_html_scrappers
    self.alternative_pdf_scrappers = alternative_pdf_scrappers
    chrome_options = webdriver.ChromeOptions()
    chrome_options.add_argument('--headless')
    chrome_options.add_argument('--no-sandbox')
    chrome_options.add_argument('--disable-dev-shm-usage')
    self.browser = webdriver.Chrome('chromedriver',options=chrome_options)
    self.downloader = None

  def fetch_browser(self):
    self.browser.get(self.url)
    self.content = self.browser.page_source
    self.downloader = Downloader.SELENIUM_CROMIUM
    self.download_status = 'OK'
    self.content_type = 'HTML'
    self.scrap()    
    return self

  def fetch_requests(self):
    self.downloader = Downloader.REQUESTS_LIB    
    try:
      r = requests.get(self.url)
      self.download_status = 'OK'
      self.content = r.content
      content_type_header = r.headers['content-type']
      if content_type_header.find('application/pdf') != -1:
        self.content_type = 'PDF'
      elif content_type_header.find('text/html') != -1:
        self.content_type = 'HTML'
      else:
        raise Exception(f'ERROR: Content-type {r.content} is not supported')
    except requests.exceptions.Timeout:
      self.download_status = 'TIMEOUT'
      # Maybe set up for a retry, or continue in a retry loop
    except requests.exceptions.TooManyRedirects:
      self.download_status = 'TOO_MANY_REDIRECTS'
      # Tell the user their URL was bad and try a different one
    except requests.exceptions.RequestException as e:
      self.download_status = 'ERROR'
    return self

  def fetch(self, url, downloader=None):
    self.url = url
    if downloader == Downloader.SELENIUM_CROMIUM:
      return self.fetch_browser()
    else:
      self.downloader = Downloader.REQUESTS_LIB      
      return self.fetch_requests()

  def scrap_is_ok(self, result):
    if result == None:
      self.scrap_status = 'ERROR'
    elif str(result) == '':
      self.scrap_status = 'EMPTY'
    elif len(str(result)) < 200:
      self.scrap_status = 'TOO_SHORT'
    else:
      self.scrap_status = 'OK'      
      return True
    return False

  def scrap_html_alternatives(self):
    scrappers = HTMLScrapper.alternatives(self.html_scrapper)
    for scrapper in scrappers:
      self.scrap_from_html(scrapper, try_alternatives=False)
      if self.scrap_status == 'OK':
        break

  def scrap_from_html(self, html_scrapper, try_alternatives=True):
    if html_scrapper == HTMLScrapper.TRAFILATURA:
      result = trafilatura.extract(self.content)
    elif html_scrapper == HTMLScrapper.TIKA:
      tika.initVM()
      result = parser.from_buffer(self.content)['content']
    else:
      raise Exception(f'ERROR: HTML scrapper {html_scrapper} is not valid')
    if self.scrap_is_ok(result):
      self.text = result
      self.final_scrapper = html_scrapper
    elif self.scrap_status == 'TOO_SHORT' and try_alternatives:
      print(f'Scrap Status: {self.scrap_status}, trying alternative downloader SELENIUM_CROMIUM')
      self.fetch(self.url, downloader=Downloader.SELENIUM_CROMIUM)
    elif self.alternative_html_scrappers and try_alternatives:
      self.scrap_html_alternatives()
    return self

  def scrap_pdf_alternatives(self):
    scrappers = PDFScrapper.alternatives(self.pdf_scrapper)
    for scrapper in scrappers:
      self.scrap_from_pdf(scrapper, try_alternatives=False)
      if self.scrap_status == 'OK':
        break

  def scrap_from_pdf(self, pdf_scrapper, try_alternatives=True):
    result = None
    if pdf_scrapper == PDFScrapper.PDFMINER:
      fp = tempfile.TemporaryFile()
      fp.write(self.content)
      str_io = StringIO()
      extract_text_to_fp(fp, str_io)
      result = str_io.getvalue().strip()
    elif pdf_scrapper == PDFScrapper.TIKA:
      tika.initVM()
      result = parser.from_buffer(self.content)['content']
    else:
      raise Exception(f'ERROR: PDF scrapper {pdf_scrapper} is not valid')
    if self.scrap_is_ok(result):
      self.text = result
      self.final_scrapper = pdf_scrapper      
    elif self.alternative_pdf_scrappers and try_alternatives:
      self.scrap_pdf_alternatives()
    return self

  # extract text from content
  # valid formats: HTML or PDF
  def scrap(self):
    if self.download_status is not 'OK':
      raise Exception('ERROR: the content to scrap is not available')
    if self.content_type == 'PDF':
      self.scrap_from_pdf(self.pdf_scrapper)
    elif self.content_type == 'HTML':
      self.scrap_from_html(self.html_scrapper)
    else:
      raise Exception(f'ERROR: Content-type {self.content_type} is not valid')      
    return self

# scraps from a list of URLs
class URLListScrapper:
  def __init__(self, urls, scrapped_dataset=None):
    self.urls = urls
    self.output_dataframe = pd.DataFrame()
    self.scrapped_dataset = scrapped_dataset

  # scrap list of urls to a dataframe
  def scrap(self):
    for num, url in enumerate(self.urls):
      if self.scrapped_dataset == None:
        print(f"Fetching url {num}: {url} ")
        scrapper = URLScrapper().fetch(url)
        print(f"Scrapping content of url {num}: {url} ")        
        scrapper.scrap()
      else:
        info = self.scrapped_dataset.download_info(url)
        print(f"Scrapping content of url {num}: {url} ")        
        scrapper = URLScrapper(
                                url=url,
                                content=info['content'],
                                content_type=info['content_type'],
                                downloader=info['downloader'],                                
                                download_status=info['download_status']
                  ).scrap()
      data = [  scrapper.url, 
                scrapper.content,
                scrapper.content_type,
                scrapper.text, 
                scrapper.downloader.name,                
                scrapper.download_status,
                scrapper.final_scrapper.name, 
                scrapper.scrap_status
              ]
      dataframe = pd.DataFrame(
        [data], 
        columns=[
          'url', 
          'content',
          'content-type', 
          'text', 
          'downloader',          
          'download_status',
          'final_scrapper',
          'scrap_status'
        ])
      self.output_dataframe = pd.concat([self.output_dataframe, dataframe])
    return self
  
  # save results from dataframe to tsv
  def to_tsv(self, filename):
    self.output_dataframe.to_csv(filename, sep="\t", index=False)