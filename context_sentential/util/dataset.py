import os
import pandas as pd
import csv
import re
from util.scrapper import URLListScrapper
from util.preprocess import TextPreprocessor, ParagraphPreprocessor 
from util.matcher import SentenceMatcher
import json
from io import StringIO
import contextlib


ds_root_dir=os.environ['DS_ROOT_DIR']

class Dataset:
  metadata = {}
  metadata['topic_ids'] = [0, 1, 2, 3, 4, 5, 6, 7]
  metadata['dirs'] = {
 
    # sub-directory of dataset without sentences
    'incomplete': 'incomplete', 
    
    # sub-directory of dataset with scrapped sentences from original scrapper
    'complete': 'complete',

   # sub-directory used for tests
    'tests': 'util/tests/tmp', 

    # sub-directory of dataset with content scrapped
    'scrapped': 'scrapped',

    # sub-directory of complete text scrapped from each document
    'texts': 'scrapped/texts',

    # sub-directory of segmented paragraphs and sentences from scrapped content
    'segmented': 'segmented',

    # sub-directory of annotation imported from UKP dataset
    'annotated': 'annotated',

    # sub-directory of annotation for each document
    'annotations': 'annotated/annotations',

    # matching: from sents (direct from text) to paragraph scrapped
    'matched_sent_to_par_scrapped': ( 'matched/sent_to_par_scrapped/source',
                                      'matched/sent_to_par_scrapped/target'),

    # matching: from ukp sents as source to paragraph scrapped
    'matched_ukp_to_par_scrapped': ('matched/ukp_to_par_scrapped/source',
                                    'matched/ukp_to_par_scrapped/target'),

    #  matching: from ukp sents as source to sentences scrapped directly from text
    'matched_ukp_to_sent_scrapped': ('matched/ukp_to_sent_scrapped/source',
                                     'matched/ukp_to_sent_scrapped/target')
  }

  metadata['topics'] = [
    {
      'filename': 'abortion',
      'extension': 'tsv',
      'topic': 'abortion'
    },
    {
      'filename': 'cloning',
      'extension': 'tsv',      
      'topic': 'cloning'
    },
    {
      'filename': 'death_penalty',
      'extension': 'tsv',      
      'topic': 'death penalty'
    },
    {
      'filename': 'gun_control',
      'extension': 'tsv',      
      'topic': 'gun control'
    },
    {
      'filename': 'marijuana_legalization',
      'extension': 'tsv',      
      'topic': 'marijuana legalization'
    },
    {
      'filename': 'minimum_wage',
      'extension': 'tsv',      
      'topic': 'minimum wage'
    },
    {
      'filename': 'nuclear_energy',
      'extension': 'tsv',      
      'topic': 'nuclear energy'
    },
    {
      'filename': 'school_uniforms',
      'extension': 'tsv',      
      'topic': 'school uniforms'
    }
  ]

  def default_ds_path(dataset_name, sub_dir=None):
    if sub_dir == None:
      return ds_root_dir+'/'+Dataset.metadata['dirs'][dataset_name]
    else:
      return ds_root_dir+'/'+Dataset.metadata['dirs'][dataset_name][sub_dir]

  def __init__(self, dataset_dir, quoting):
    self.quoting = quoting
    self.dataframes = []
    self.dataset_dir = dataset_dir
    self.tsv_paths = []
    self.url_topic_is_initialized = False
    self.url_topic = {}    

  def init_url_topic(self):
    for topic_id in Dataset.metadata['topic_ids']:
      for url in self.urls(topic_id):
        self.url_topic[url] = topic_id    

  def urls(self, topic_id):
    return self.unique(topic_id,'archivedUrl')

  def load_datasets(self):
    for tp in self.tsv_paths:
      if os.path.exists(tp):
        df = pd.read_csv(tp, quoting=self.quoting, sep='\t', encoding='utf-8')
      else:
        df = pd.DataFrame() 
      self.dataframes.append(df)

  def get_tsv_filenames(self, topics=metadata['topic_ids']):
    filenames = []
    for topic in topics:
      filename = Dataset.metadata['topics'][topic]['filename']
      extension = Dataset.metadata['topics'][topic]['extension']
      complete_filename = '{}.{}'.format(filename,extension)
      filenames.append(complete_filename)
    return filenames

  def load_tsv_paths(self, topics=metadata['topic_ids']):
    files = self.get_tsv_filenames(topics)
    self.tsv_paths = [self.dataset_dir+'/'+files[i] for i in range(len(files))]

  def load(self):
    self.load_tsv_paths()
    self.load_datasets()
    return self

  def to_tsv(dataframe, filename):
    result = dataframe.to_csv(filename, sep="\t", index=False)

  def columns_name(self):
    return list(self.dataframes[0].columns)

  def filename_by_id(topic_id):
    return Dataset.metadata['topics'][topic_id]['filename']

  def fullname_by_id(topic_id):
    return Dataset.metadata['topics'][topic_id]['filename'] + '.' + \
           Dataset.metadata['topics'][topic_id]['extension']

  def topic_by_id(topic_id):
    return Dataset.metadata['topics'][topic_id]['topic']

  # get head from dataframe identified by df_num
  def head(self, topic_id=0):
    return self.dataframes[topic_id].head()

  # get value from a column of one of the dataframes (df_num)
  def get_value(self, topic_id=0, column='topic', line=0):
    return self.dataframes[topic_id][column][line]

  def topics():
    return [ topic['topic'] for topic in Dataset.metadata['topics'] ]

  def unique(self, topic_id, column_name):
    return list(self.dataframes[topic_id][column_name].unique())

class UKPSententialDataset(Dataset):

  def __init__(self, dataset_dir=Dataset.default_ds_path('complete')):    
    self.dataset_dir = dataset_dir
    #quoting=csv.QUOTE_NONE is necessary because quoting is default but not used
    # in this dataset
    super().__init__(self.dataset_dir, csv.QUOTE_NONE)

  def archived_urls(self, topic_id):
    return self.unique(topic_id,'archivedUrl')

  def lines_from_archive(self, df_num, archivedUrl, set_= ['test', 'train', 'val']):
    lines = self.dataframes[df_num].loc[self.dataframes[df_num]['archivedUrl']==archivedUrl]
    return lines.loc[lines['set'].isin(set_)]

  # scrapping from urls from UKP Dataset
  # the content is fetched and a ScrappedDataset is saved in 'save_dir'
  def scrap(self, save_dir, topic_ids=Dataset.metadata['topic_ids']):
    for id in topic_ids:
      print(f"Scrapping URLs from topic {self.topic_by_id(id)}")
      urls = self.archived_urls(id)
      uls = URLListScrapper(urls).scrap()  
      print(f"Saving contents from topic {self.topic_by_id(id)}")
      uls.to_tsv(save_dir+'/'+self.filename_by_id(id))

  def sentences_from_url(self, topic_id, url):
    df = self.dataframes[topic_id]
    return df.loc[df['archivedUrl']==url]['sentence'].values.tolist()

class ScrappedDataset(Dataset):
  def __init__(self, dataset_dir=Dataset.default_ds_path('scrapped'), tokenizer='stanza'):
    super().__init__(dataset_dir, csv.QUOTE_MINIMAL)
    self.tokenizer = tokenizer

  def download_info(self, url, topic_id=None):
    info = {}
    if not self.url_topic_is_initialized:
      self.init_url_topic()
    if topic_id == None:
      topic_id = self.url_topic[url]
    df = self.dataframes[topic_id]
    rows = df.loc[df['url']==url]
    info['content'] = rows['content'].values[0]
    info['content_type'] = rows['content_type'].values[0]
    info['downloader'] = rows['downloader'].values[0]
    info['download_status'] = rows['download_status'].values[0]
    return info

  # scrapping from urls from ScrappedDataset
  # the content is reused and a updated ScrappedDataset is saved in 'save_dir'
  def scrap(self, save_dir, topic_ids=Dataset.metadata['topic_ids']):
    for id in topic_ids:
      print(f"Scrapping URLs from topic {self.topic_by_id(id)}")
      urls = self.urls(id)
      uls = URLListScrapper(urls, self).scrap()
      print(f"Saving contents from topic {self.topic_by_id(id)}")
      uls.to_tsv(save_dir+'/'+self.filename_by_id(id))

  def text(self, topic_id, url):
    df = self.dataframes[topic_id]
    return df.loc[df['url']==url]['text'].values[0]

  def total_urls_downloaded_ok(self,topic_ids=Dataset.metadata['topic_ids']):
    for topic_id in topic_ids:
      oks = len(self.dataframes[topic_id].loc[self.dataframes[topic_id]['download_status'] == 'OK'])
      topic = self.topic_by_id(topic_id)
      print(f"{topic}: {oks}")

  def total_scrap_status_ok(self,topic_ids=Dataset.metadata['topic_ids']):
    for topic_id in topic_ids:
      oks = len(self.dataframes[topic_id].loc[self.dataframes[topic_id]['scrap_status'] == 'OK'])
      topic = Dataset.topic_by_id(topic_id)
      print(f"{topic}: {oks}")

  def urls(self, topic_id):
    return self.unique(topic_id,'url')

  def segment( self, 
                save_dir=Dataset.default_ds_path('segmented'), 
                topic_ids=Dataset.metadata['topic_ids'],
                direct_from_text=True):
    for topic_id in topic_ids:
      print("Segmenting paragraphs from topic {}".format(Dataset.topic_by_id(topic_id)))
      urls = self.urls(topic_id)
      segmented_text = [] 
      for num, url in enumerate(urls):
        text = self.text(topic_id, url)
        # clean_text = TextPreprocessor.del_noisy_chars(text)
        print(f"Segmenting paragraphs from url: {num}:{url}")
        #segmented_paragraphs = ParagraphPreprocessor(clean_text, tokenizer=self.tokenizer).segment()
        segmented_paragraphs = ParagraphPreprocessor(text, tokenizer=self.tokenizer).segment()
        if direct_from_text:
          # sentences_from_text = TextPreprocessor(clean_text, tokenizer=self.tokenizer).segment()
          sentences_from_text = TextPreprocessor(text, tokenizer=self.tokenizer).segment()
        else:
          sentences_from_text = ''
        segmented_text.append({
          "url": url,
          # "text": clean_text,          
          "text": text,          
          "paragraphs": json.dumps(segmented_paragraphs["paragraphs"]),
          "sentences_from_text": sentences_from_text
        })
      df_json = pd.read_json(StringIO(json.dumps(segmented_text)))
      print(f"Saving contents from topic {Dataset.topic_by_id(topic_id)}")
      if not os.path.isdir(save_dir):
        os.makedirs(save_dir)
      df_json.to_csv(save_dir+'/'+Dataset.fullname_by_id(topic_id), index=False, sep='\t')

  def dump_texts(self):
    ukpsd = UKPSententialDataset()
    ukpsd.load()
    # ukpsd.urls(0)
    # ukpsd.dataframes[0]
    texts_dir = Dataset.default_ds_path('texts')
    for topic_id in range(0,8):
      subdir = Dataset.filename_by_id(topic_id)
      topic_subdir = '{}/{}'.format(texts_dir,subdir)
      if not os.path.isdir(topic_subdir):
        os.makedirs(topic_subdir)
      url_id = 0
      urls_filename = '{}/{}.tsv'.format(topic_subdir,'urls')
      urls_file = open(urls_filename, 'w')
      for url in ukpsd.urls(topic_id):
        text = self.text(topic_id, url)
        full_filename = '{}/{}.txt'.format(topic_subdir,url_id)
        text_file = open(full_filename, 'w')
        text_file.write(text)
        text_file.close()
        urls_file.write("{}\t{}\n".format(url_id,url))
        url_id = url_id + 1
      urls_file.close()

class SegmentedDataset(Dataset):
  def __init__(self, dataset_dir=Dataset.default_ds_path('segmented')):
    self.source_output_df = pd.DataFrame()
    self.target_output_df = pd.DataFrame()
    self.sm_sents_par = None
    self.sm_sents_ukp_par = None
    self.sm_sents_ukp_sent = None
    super().__init__(dataset_dir, csv.QUOTE_MINIMAL)

  def text(self, topic_id, url):
    df = self.dataframes[topic_id]
    return df.loc[df['url']==url]['text'].values[0]

  def paragraph_segments(self, topic_id, url):
    df = self.dataframes[topic_id]
    return json.loads(df.loc[df['url']==url]['paragraphs'].values[0])

  def sentences_segments_from_text(self, topic_id, url):
    df = self.dataframes[topic_id]
    return json.loads(df.loc[df['url']==url]['sentences_from_text'].values[0])

  def sentences_from_text(self, topic_id, url):
    sentences = []
    url_text = self.text(topic_id, url)
    for sentences_segment in self.sentences_segments_from_text(topic_id, url):
      sentences.append(url_text[sentences_segment[0]:sentences_segment[1]])
    return sentences

  def sentences_from_paragraphs(self, topic_id, url):
    sentences = []
    url_text = self.text(topic_id, url)
    par_segments = self.paragraph_segments(topic_id, url)
    for par_segment in par_segments:
      sentence_segments = par_segment['sentences']
      for sentence_segment in sentence_segments:
        start = sentence_segment[0]
        end = sentence_segment[1]        
        sentences.append(url_text[start:end])
    return sentences

  # sentences from paragraphs with indexes (start, end)
  def sentences_with_indexes(self, topic_id, url):
    sentences_with_indexes = []
    url_text = self.text(topic_id, url)
    par_segments = self.paragraph_segments(topic_id, url)
    for par_segment in par_segments:
      sentence_segments = par_segment['sentences']
      for sentence_segment in sentence_segments:
        start = sentence_segment[0]
        end = sentence_segment[1]        
        sentence_text = url_text[start:end]
        tuple = (sentence_text, start, end)
        sentences_with_indexes.append(tuple)
    return sentences_with_indexes

  def print_segmented_paragraphs(self, topic_id, url):
    url_text = self.text(topic_id, url)
    par_segments = self.paragraph_segments(topic_id, url)
    ParagraphPreprocessor.print_segments(url_text, par_segments)
    return self

  def urls(self, topic_id):
    return self.unique(topic_id,'url')    

  def concat_dfs(self, input_df, output_df_type):
    #output_df = pd.DataFrame()
    if output_df_type == 'source':
      other = 'target'
    elif output_df_type == 'target':
      other = 'source'
    else:
      raise Exception(f"Error: {output_df_type} is not valid")
    best_id = f'best_{other}_id'
    best_value = f'best_{other}_value'
    best_sentence = f'best_{other}_sentence'
    for num, row in input_df.iterrows():
      data = [
        row['topic_id'],
        row['url'],
        row['id'],
        row['sentence'],
        row['matched'],
        row[best_id],
        row[best_value],
        row[best_sentence]
      ]
      df = pd.DataFrame(
          [data], 
          columns=[
            'topic_id', 
            'url',
            'segment_id', 
            'sentence', 
            'matched',          
            best_id,
            best_value,
            best_sentence
          ])
      if output_df_type == 'source':
        self.source_output_df = pd.concat([self.source_output_df, df])
      elif output_df_type == 'target':
        self.target_output_df = pd.concat([self.target_output_df, df])

  def match_sent_to_par_scrapped( self,
                                  threshold=.84, 
                                  save_dir_source=Dataset.default_ds_path('matched_sent_to_par_scrapped', 0),
                                  save_dir_target=Dataset.default_ds_path('matched_sent_to_par_scrapped', 1), 
                                  topic_ids=Dataset.metadata['topic_ids'],
                                  urls=None):
    for topic_id in topic_ids:
      self.source_output_df = pd.DataFrame()
      self.target_output_df = pd.DataFrame()
      print(f'Matching sentences for topic {Dataset.topic_by_id(topic_id)}')      
      for num, url in enumerate(self.urls(topic_id)):
        if urls is not None and url not in urls:
          break
        sentences_from_text = self.sentences_from_text(topic_id, url)
        sentences_from_paragraphs = self.sentences_from_paragraphs(topic_id, url)
        self.sm_sents_par = SentenceMatcher(sentences_from_text, sentences_from_paragraphs)
        print(f'Matching sentences for url {num}: {url}')              
        self.sm_sents_par.match(threshold)
        self.sm_sents_par.df_source['topic_id'] = topic_id
        self.sm_sents_par.df_source['url'] = url
        self.sm_sents_par.df_target['topic_id'] = topic_id
        self.sm_sents_par.df_target['url'] = url
        print(f'Concatenating sentences matched from url {num}: {url}')
        self.concat_dfs(self.sm_sents_par.df_source, 'source')
        self.concat_dfs(self.sm_sents_par.df_target, 'target')
      print(f'Saving source and target datasets for topic {Dataset.topic_by_id(topic_id)}')
      os.makedirs(save_dir_source, exist_ok=True)
      filename = save_dir_source+'/'+Dataset.fullname_by_id(topic_id)
      Dataset.to_tsv(self.source_output_df, filename)
      os.makedirs(save_dir_target, exist_ok=True)
      filename = save_dir_target+'/'+Dataset.fullname_by_id(topic_id)
      Dataset.to_tsv(self.target_output_df, filename)
  
  def match_ukp_to_par_scrapped(  self,
                                  threshold=.84, 
                                  save_dir_source=Dataset.default_ds_path('matched_ukp_to_par_scrapped',0), 
                                  save_dir_target=Dataset.default_ds_path('matched_ukp_to_par_scrapped',1), 
                                  topic_ids=Dataset.metadata['topic_ids'],
                                  urls=None):   
    for topic_id in topic_ids:
      self.source_output_df = pd.DataFrame()
      self.target_output_df = pd.DataFrame()
      print(f'Matching sentences for topic {Dataset.topic_by_id(topic_id)}')      
      for num, url in enumerate(self.urls(topic_id)):
        if urls is not None and url not in urls:
          break
        ukp_complete_dataset = UKPSententialDataset().load()
        sentences_from_ukp = ukp_complete_dataset.sentences_from_url(topic_id, url)
        sentences_from_paragraphs = self.sentences_from_paragraphs(topic_id, url)
        self.sm_sents_ukp_par = SentenceMatcher(sentences_from_ukp, sentences_from_paragraphs)
        print(f'Matching sentences for url {num}: {url}')              
        self.sm_sents_ukp_par.match(threshold)
        self.sm_sents_ukp_par.df_source['topic_id'] = topic_id
        self.sm_sents_ukp_par.df_source['url'] = url
        self.sm_sents_ukp_par.df_target['topic_id'] = topic_id
        self.sm_sents_ukp_par.df_target['url'] = url
        print(f'Concatenating sentences matched from url {num}: {url}')
        self.concat_dfs(self.sm_sents_ukp_par.df_source, 'source')
        self.concat_dfs(self.sm_sents_ukp_par.df_target, 'target')
      print(f'Saving source and target datasets for topic {Dataset.topic_by_id(topic_id)}')
      os.makedirs(save_dir_source, exist_ok=True)
      filename = save_dir_source+'/'+Dataset.fullname_by_id(topic_id)
      Dataset.to_tsv(self.source_output_df, filename)
      os.makedirs(save_dir_target, exist_ok=True)
      filename = save_dir_target+'/'+Dataset.fullname_by_id(topic_id)
      Dataset.to_tsv(self.target_output_df, filename)      

  def match_ukp_to_sent_scrapped( self, 
                                  threshold=.84, 
                                  save_dir_source=Dataset.default_ds_path('matched_ukp_to_sent_scrapped',0), 
                                  save_dir_target=Dataset.default_ds_path('matched_ukp_to_sent_scrapped',1), 
                                  topic_ids=Dataset.metadata['topic_ids'],
                                  urls=None):
    for topic_id in topic_ids:
      self.source_output_df = pd.DataFrame()
      self.target_output_df = pd.DataFrame()
      ukp_complete_dataset = UKPSententialDataset().load()      
      print(f'Matching sentences for topic {Dataset.topic_by_id(topic_id)}')      
      for num, url in enumerate(self.urls(topic_id)):
        if urls is not None and url not in urls:
          break
        sentences_from_ukp = ukp_complete_dataset.sentences_from_url(topic_id, url)
        # sentences_from_paragraphs = self.sentences_from_paragraphs(topic_id, url)
        sentences_from_text = self.sentences_from_text(topic_id, url)
        self.sm_sents_ukp_sent = SentenceMatcher(sentences_from_ukp, sentences_from_text)
        print(f'Matching sentences for url {num}: {url}')              
        self.sm_sents_ukp_sent.match(threshold)
        self.sm_sents_ukp_sent.df_source['topic_id'] = topic_id
        self.sm_sents_ukp_sent.df_source['url'] = url
        self.sm_sents_ukp_sent.df_target['topic_id'] = topic_id
        self.sm_sents_ukp_sent.df_target['url'] = url
        print(f'Concatenating sentences matched from url {num}: {url}')
        self.concat_dfs(self.sm_sents_ukp_sent.df_source, 'source')
        self.concat_dfs(self.sm_sents_ukp_sent.df_target, 'target')
      print(f'Saving source and target datasets for topic {Dataset.topic_by_id(topic_id)}')
      filename = save_dir_source+'/'+Dataset.fullname_by_id(topic_id)
      os.makedirs(save_dir_source, exist_ok=True)
      Dataset.to_tsv(self.source_output_df, filename)
      os.makedirs(save_dir_target, exist_ok=True)
      filename = save_dir_target+'/'+Dataset.fullname_by_id(topic_id)
      Dataset.to_tsv(self.target_output_df, filename)      

class MatchedDataset(Dataset):
  def __init__(self, dataset_dir):
    super().__init__(dataset_dir, csv.QUOTE_MINIMAL)

  def print_statistics(matched_ds_source, matched_ds_target):
    for topic_id in Dataset.metadata['topic_ids']:
      print(f'\nStatistics for topic "{Dataset.topic_by_id(topic_id)}"')
      df_source = matched_ds_source.dataframes[topic_id]
      df_target = matched_ds_target.dataframes[topic_id]
      if df_source.empty or df_target.empty:
        print(f'Dataframes for topic "{Dataset.topic_by_id(topic_id)}" are empty')
      else:
        source_result = SentenceMatcher.statistcs(df_source)
        target_result = SentenceMatcher.statistcs(df_target)
        SentenceMatcher.print_statistics(source_result, target_result)

  def matched_sentence(self, topic_id, url, sentence, direction='to_source'):
    md = self.dataframes[topic_id]
    res = md.loc[(md['sentence']==sentence) & (md['url']==url) & (md['matched']==True)]
    if (len(res) != 1):
      return None
    else:
      if direction == 'to_source':
        return res['best_source_sentence'].values[0]
      else:
        return res['best_target_sentence'].values[0]

  def urls(self, topic_id):
    return self.unique(topic_id,'url')    

  # generate AnnotatedDataset
  def annotate( # self, 
                save_dir=Dataset.default_ds_path('annotated'), 
                topic_ids=Dataset.metadata['topic_ids']):
    usd = UKPSententialDataset().load()
    dataset_dir_target = Dataset.default_ds_path('matched_ukp_to_par_scrapped', 1)
    md_target = MatchedDataset(dataset_dir_target).load()
    seg_ds = SegmentedDataset().load()

    for topic_id in topic_ids:
      output_df = pd.DataFrame()      
      df_md = md_target.dataframes[topic_id]
      df_usd = usd.dataframes[topic_id]
      print(f'Importing UKP annotation for topic {Dataset.topic_by_id(topic_id)}')
      df_merged = pd.merge(df_md, df_usd, how='left', left_on="best_source_sentence", right_on="sentence")
      df_matched = df_merged.loc[(df_merged['matched']==True) & (df_merged['annotation']!='NoArgument')].copy()
      print(f'Importing {len(df_matched)} annotated arguments from topic {Dataset.topic_by_id(topic_id)}')
      urls = md_target.urls(topic_id)
      for url in urls:
        m_df_url = df_matched.loc[df_matched['url']==url]
        sentences_with_indexes = seg_ds.sentences_with_indexes(topic_id, url)
        for url, sentence, segment_id, annotation in m_df_url[['url', 'sentence_x', 'segment_id', 'annotation']].values:
          sentence_text = sentences_with_indexes[segment_id][0]
          sentence_start_index = sentences_with_indexes[segment_id][1]
          sentence_end_index = sentences_with_indexes[segment_id][2]
          if sentence_text != sentence:
            text_log = f"Not OK: {sentence_text} at ({sentence_start_index}, {sentence_end_index})"
          else:
            text_log = f"OK: {sentence_text} at ({sentence_start_index}, {sentence_end_index})"
          data = [
            topic_id,
            url,
            segment_id,
            sentence,
            sentence_start_index,
            sentence_end_index,
            annotation
          ]
          df_data = pd.DataFrame(
            [data],
            columns = [
              'topic_id',
              'url',
              'segment_id',
              'sentence',
              'start',
              'end',
              'annotation'
            ]
          )
          output_df = pd.concat([output_df, df_data])
      print(f'Saving annotated dataset for topic {Dataset.topic_by_id(topic_id)}')
      os.makedirs(save_dir, exist_ok=True)
      filename = save_dir+'/'+Dataset.fullname_by_id(topic_id)
      Dataset.to_tsv(output_df, filename)

class AnnotatedDataset(Dataset):
  def __init__(self, dataset_dir=Dataset.default_ds_path('annotated')):
    super().__init__(dataset_dir, csv.QUOTE_MINIMAL)
  
  def gen_annotation(segment_id, label, sentence_start_index, spans, clean_sentence):
    annotation = f"T{segment_id}\t{label} "
    for i, span in enumerate(spans):
      if i > 0:
        annotation += ';'
      begin = sentence_start_index + span[0]
      end = sentence_start_index + span[1]
      annotation += f"{begin} {end}"
    annotation += f"\t{clean_sentence}\n"
    return annotation

  def dump_annotations(self, topic_ids=Dataset.metadata['topic_ids'], urls=None):
    sd = SegmentedDataset().load()
    annotations_dir = Dataset.default_ds_path('annotations')
    if urls == None:
      urls_subset = False
    else:
      urls_subset = True
    for topic_id in topic_ids:
      print(f'Generating annotations for topic {Dataset.topic_by_id(topic_id)}')
      subdir = Dataset.filename_by_id(topic_id)
      annotations_df = self.dataframes[topic_id]
      topic_subdir = '{}/{}'.format(annotations_dir,subdir)
      if not os.path.isdir(topic_subdir):
        os.makedirs(topic_subdir)
      urls_filename = f'{topic_subdir}/urls.tsv'
      urls_file = open(urls_filename, 'w')
      url_id = 0
      if urls_subset == False:
        urls = sd.urls(topic_id)
      for url in urls:
        print('Generating annotation for url: '+url)              
        annotations = ""      
        annotations_df_url = annotations_df.loc[annotations_df['url']==url]        
        text = sd.text(topic_id, url)
        sentences_with_indexes = sd.sentences_with_indexes(topic_id, url)
        for segment_id, segment in enumerate(sentences_with_indexes):
          sentence_text = segment[0]
          sentence_start_index = segment[1]
          sentence_end_index = segment[2]
          (spans, clean_sentence) = TextPreprocessor.span_segments(sentence_text)          
          if annotations_df_url.loc[annotations_df_url['segment_id']==segment_id].empty:
            annotations += AnnotatedDataset.gen_annotation(segment_id+1, "Sentence", sentence_start_index, spans, clean_sentence)
          else:
            ann_df = annotations_df_url.loc[annotations_df_url['segment_id']==segment_id]
            annotation = ann_df['annotation'].values[0]
            if annotation == 'Argument_against':
              label = 'ArgumentAgainst'
            else:
              label = 'ArgumentFor'
            annotations += AnnotatedDataset.gen_annotation(segment_id+1, label, sentence_start_index, spans, clean_sentence)            
        full_filename = '{}/{:02d}.txt'.format(topic_subdir,url_id)
        ann_filename = '{}/{:02d}.ann'.format(topic_subdir,url_id)
        text_file = open(full_filename, 'w')
        text_file.write(text)
        text_file.close()
        ann_file = open(ann_filename, 'w')
        ann_file.write(annotations)
        ann_file.close()
        urls_file.write(f"{url_id}\t{url}\n")
        url_id = url_id + 1
      urls_file.close()

