from difflib import SequenceMatcher
import numpy as np
import pandas as pd

class SentenceMatcher:
  def __init__(self, source_sentences, target_sentences):
    self.df_source = pd.DataFrame(enumerate(source_sentences), columns=['id', 'sentence'])
    self.df_target = pd.DataFrame(enumerate(target_sentences), columns=['id', 'sentence'])
    self.df_source['matched'] = False
    self.df_source['best_target_id'] = None
    self.df_source['best_target_value'] = -1.0
    self.df_source['best_target_sentence'] = None    
    self.df_target['matched'] = False
    self.df_target['best_source_id'] = None
    self.df_target['best_source_value'] = -1.0
    self.df_target['best_source_sentence'] = None
    self.print_statistics = self._instance_print_statistics

  @staticmethod
  def similar(a, b):
      return SequenceMatcher(None, a, b).ratio()

  def match(self, match_level):
    for source_id, source_row in self.df_source.iterrows():
      for target_id, target_row in self.df_target.iterrows():
        match_value = SentenceMatcher.similar(source_row['sentence'], target_row['sentence'])
        if match_value > self.df_source.iloc[source_id]['best_target_value']:
          self.df_source.at[source_id,'best_target_value'] = match_value
          self.df_source.at[source_id,'best_target_id'] = target_id
          self.df_source.at[source_id,'best_target_sentence'] = target_row['sentence']
        if match_value > self.df_target.iloc[target_id]['best_source_value']:
          self.df_target.at[target_id,'best_source_value'] = match_value
          self.df_target.at[target_id,'best_source_id'] = source_id
          self.df_target.at[target_id,'best_source_sentence'] = source_row['sentence']
        if  match_value > match_level:
          self.df_target.at[target_id,'matched'] = True
          self.df_source.at[source_id,'matched'] = True

  def statistcs(df):
    result = {}
    result['matched'] = len(df.loc[df['matched']==True])
    result['total'] = len(df)
    result['percent'] = float(result['matched'])/float(result['total'])
    return result

  @staticmethod
  def print_statistics(source_result=None, target_result=None):
    if target_result == None:
      percent_matched = "{:.0%}".format(source_result['percent'])
      print('RESULTS:')
      print(f'* {source_result["matched"]}/{source_result["total"]} ({percent_matched} matched)')
    elif target_result is not None:
      percent_matched_in_source = "{:.0%}".format(source_result['percent'])
      percent_matched_in_target = "{:.0%}".format(target_result['percent'])      
      print('RESULTS:')
      print(f'* Source: {source_result["matched"]}/{source_result["total"]} ({percent_matched_in_source} matched)')
      print(f'* Target: {target_result["matched"]}/{target_result["total"]} ({percent_matched_in_target} matched)')

  def _instance_print_statistics(self):
    df_source = self.df_source
    df_target = self.df_target
    source_result = SentenceMatcher.statistcs(df_source)
    target_result = SentenceMatcher.statistcs(df_target)
    SentenceMatcher.print_statistics(source_result, target_result)

  def source_sentences_matched(self, matched=True):
    return self.df_source.loc[self.df_source['matched']==matched]

  def target_sentences_matched(self, matched=True):
    return self.df_target.loc[self.df_target['matched']==matched]