import pandas as pd
import csv
from util.matcher import SentenceMatcher

class Annotator:
  def __init__(self, annotation_file):
    self.annotation_file = annotation_file
    self.annotation_df = pd.read_csv(annotation_file, quoting=csv.QUOTE_NONE, encoding='utf-8', sep='\t', header=None, names=['id', 'segment', 'sentence'])
    
  def sentence(self, id):
    return self.annotation_df.loc[self.annotation_df['id']==id]['sentence'].values[0]
  
  def relations(self):
    df_relations = self.annotation_df.loc[self.annotation_df['id'].str.contains("R")]
    relations = []
    for rel_id, rel_values in df_relations.iterrows():
        segment = rel_values['segment'].split()
        source = segment[1].split(':')[1]
        source_sentence = self.sentence(source)
        target = segment[2].split(':')[1]
        target_sentence = self.sentence(target)
        relations.append((rel_values['id'],segment[0],(source, source_sentence),(target, target_sentence)))
    return relations    

  def relations_sentences(self):
    sentences = []
    for relation in self.relations():
        sentences.append(relation[2][1])
        sentences.append(relation[3][1])
    return sentences

  def entities(self):
    df_entities = self.annotation_df.loc[self.annotation_df['id'].str.contains("T")]
    entities = []
    for ent_seq, ent_values in df_entities.iterrows():
        ent_id = ent_values['id']
        ent_sentence = ent_values['sentence']
        entities.append((ent_id, ent_sentence))
    return entities

  def entity_id(self, sentence):
    df_entities = self.annotation_df.loc[self.annotation_df['id'].str.contains("T")]    
    return df_entities.loc[df_entities['sentence']==sentence]['id'].values[0]
    
  def sentences(self):
    sentences = []
    for entity in self.entities():
        sentences.append(entity[1])
    return sentences

  def match_dict(self, ann_target):
    relations_sentences = self.relations_sentences()
    sentences = ann_target.sentences()
    sm = SentenceMatcher(relations_sentences, sentences)
    sm.match(.84)
    match_dict = {}
    for match_id, match_values in sm.df_source.iterrows():
        matched = match_values['matched']
        sentence = match_values['sentence']
        source_id = self.entity_id(sentence)
        if matched:
            best_target_sentence = match_values['best_target_sentence']
            target_id = ann_target.entity_id(best_target_sentence)
        else:
            target_id = None
        match_dict[source_id] = target_id
    return match_dict

  def relations_matched(self, ann_target):
    relations_matched = []
    match_dict = self.match_dict(ann_target)
    for relation in self.relations():
      rel_id = relation[0]
      rel_type = relation[1]
      rel_src_id = relation[2][0]
      rel_src_matched_id = match_dict[rel_src_id]
      rel_src_matched_sent = self.sentence(rel_src_id)
      rel_tgt_id = relation[3][0]
      rel_tgt_matched_id = match_dict[rel_tgt_id]
      if rel_tgt_matched_id != None:
        rel_tgt_matched_sent = ann_target.sentence(rel_tgt_matched_id)
      else:
        rel_tgt_matched_sent = ''
      relation_matched = (
        rel_id,
        rel_type,
        (rel_src_matched_id, rel_src_matched_sent),
        (rel_tgt_matched_id, rel_tgt_matched_sent)
      )
      relations_matched.append(relation_matched)
    return relations_matched

  def export_annotations(ann_src_file, ann_tgt_file):
    ann_src = Annotator(ann_src_file)
    ann_tgt = Annotator(ann_tgt_file)
    rel_matched = ann_src.relations_matched(ann_tgt)
    annotations_add = ""
    annotations_not_add = ""
    annotation_not_add = ""
    annotation_add = ""
    for rel in rel_matched:
      rel_id = rel[0]
      rel_type = rel[1]
      rel_src_id = rel[2][0]
      rel_tgt_id = rel[3][0]
      if rel_src_id != None and rel_tgt_id != None:
        annotation_add = f"{rel_id}\t{rel_type} Arg1:{rel_src_id} Arg2:{rel_tgt_id}\t\n"
        annotations_add += annotation_add
      else:
        annotation_not_add = f"{rel_id}\t{rel_type} Arg1:{rel_src_id} Arg2:{rel_tgt_id}\t\n"
        annotations_not_add += annotation_not_add
    if len(annotations_add) != 0:
      file_ann_add = f"{ann_tgt_file}.add"
      print(f"saving new annotations in file {file_ann_add}")
      with open(file_ann_add, 'w') as f:
        f.write(annotations_add)
      f.close()
    if len(annotations_not_add) != 0:    
      file_ann_no_match = f"{ann_tgt_file}.no_match"
      print(f"saving annotations not matched in file {file_ann_no_match}")
      with open(file_ann_no_match, 'w') as f:
        f.write(annotations_not_add)
      f.close()
        