Legalization will cause a tremendous increase in marijuana use. Based on the experience elsewhere, the number of users will double or triple. This means an additional 17 to 34 million young and adult users in the United States. Legalization will mean that marijuana businesses can promote their products and package them in attractive ways to increase their market share.
Increased marijuana use will mean millions more damaged young people. Marijuana use can permanently impair brain development. Problem solving, concentration, motivation, and memory are negatively affected. Teens who use marijuana are more likely to engage in delinquent and dangerous behavior, and experience increased risk of schizophrenia and depression, including being three times more likely to have suicidal thoughts. Marijuana-using teens are more likely to have multiple sexual partners and engage in unsafe sex.
[Read the U.S. News Debate: Should Welfare Recipients Be Tested for Drugs?]
Marijuana use accounts for tens of thousands of marijuana related complaints at emergency rooms throughout the United States each year. Over 99,000 are young people.
Despite arguments by the drug culture to the contrary, marijuana is addictive. The levels of THC (marijuana's psychoactive ingredient) have never been higher. This is a major factor why marijuana is the number one drug causing young people to enter treatment and why there has been a substantial increase in the people in treatment for marijuana dependence.
Marijuana legalization means more drugged driving. Already, 13 percent of high school seniors said they drove after using marijuana while only 10 percent drove after having several drinks. Why run the risk of increasing marijuana use among young drivers?
[See a collection of political cartoons on healthcare.]
Employees who test positive for marijuana had 55 percent more industrial accidents and 85 percent more injuries and they had absenteeism rates 75 percent higher than those that tested negative. This damages our economy.
The argument that we can tax and regulate marijuana and derive income from it is false. The increased use will increase the multitude of costs that come with marijuana use. The costs from health and mental wellness problems, accidents, and damage to our economic productivity will far out strip any tax obtained. Our economy is suffering. The last thing we need is the burden that legalization will put on us.
About David G. Evans Special Adviser to the Drug Free America Foundation
Yes – Marijuana Use Should Not Be a Crime
Alison Holcomb Drug Policy Director of the American Civil Liberties Union
Yes – Marijuana Regulation Works and Prohibition Fails
Paul Armentano Deputy Director of the National Organization for the Reform of Marijuana Laws
Yes – Stop Wasting Time Pretending Marijuana Prohibition Works
Morgan Fox Communications Manager at the Marijuana Policy Project
No – There Are Smarter Ways to Deal With Marijuana Than Legalization
Kevin Sabet Former Senior Policy Advisor to President Obama's Drug Czar