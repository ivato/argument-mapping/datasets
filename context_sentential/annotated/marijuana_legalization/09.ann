T1	Sentence 0 54	Should Marijuana be Legalized under any Circumstances?
T2	Sentence 55 68;69 174	Related Links Overview/BackgroundA number of movements to legalize the use of marijuana have been gaining steam lately.
T3	ArgumentFor 175 254	There are places in California where it's already legal for medicinal purposes.
T4	Sentence 255 464	Much of the American public now believes that the drug should be legalized (40 percent according to a Rasmussen International Poll ) but others are still concerned about health damage and other adverse affects
T5	Sentence 472 556	The drug generally isn't more harmful than alcohol or tobacco if used in moderation.
T6	ArgumentFor 557 736	As you'll see by reading research studies from the related links section at the bottom of the page, the studies of the harmfulness of marijuana are inconclusive and contradictory.
T7	ArgumentFor 737 811	Most doctors would agree that it's not very harmful if used in moderation.
T8	ArgumentFor 812 875	It's only when you abuse the drug that problems start to occur.
T9	Sentence 876 930	But isn't abuse of almost any bad substance a problem?
T10	ArgumentFor 931 1034	If you abuse alcohol, caffeine, Ephedra, cigarettes, or even pizza, health problems are sure to follow.
T11	ArgumentFor 1035 1139	Would you want the government limiting how much coffee you can drink or how much cheesecake you take in?
T12	ArgumentFor 1140 1220	Most doctors believe that marijuana is no more addictive that alcohol or tobacco
T13	ArgumentFor 1222 1282	- Limiting the use of the drug intrudes on personal freedom.
T14	ArgumentFor 1283 1391	Even if the drug is shown to be harmful, isn't it the right of every person to choose what harms him or her?
T15	ArgumentFor 1392 1493	Marijuana use is generally thought of as a "victimless crime", in that only the user is being harmed.
T16	Sentence 1494 1575	You can't legislate morality when people disagree about what's considered "moral"
T17	ArgumentFor 1577 1669	- Legalization would mean a lower price; thus, related crimes (like theft) would be reduced.
T18	Sentence 1670 1788	All illegal drugs are higher in price because the production, transportation, and sale of the drugs carry heavy risks.
T19	ArgumentAgainst 1789 1903	When people develop drug habits or addictions, they must somehow come up with the money to support their cravings.
T20	ArgumentAgainst 1904 2034	Unless a person is wealthy, he or she must often resort to robbery and other crimes to generate the money needed to buy the drugs.
T21	ArgumentFor 2035 2098	Legalization would reduce the risks and thus reduce the prices.
T22	ArgumentFor 2099 2180	There would therefore be less need for the secondary crimes needed to raise money
T23	ArgumentFor 2182 2249	- There are medical benefits such as the those for cancer patients.
T24	ArgumentFor 2250 2411	As detailed in the related links section, there are a number of medical benefits of marijuana, most notably in the treatment of patients undergoing chemotherapy.
T25	ArgumentFor 2412 2467	Others believe it helps in the treatment of depression.
T26	ArgumentFor 2468 2576	Certain states like California have brought initiatives to legalize the drug for at least medicinal purposes
T27	ArgumentFor 2578 2637	- Street justice related to drug disputes would be reduced.
T28	ArgumentFor 2638 2744	Currently, if someone in the drug trade screws you over, there's no police to call or lawyers to litigate.
T29	Sentence 2745 2779	You must settle disputes yourself.
T30	Sentence 2780 2831	This often leads to cycles of retaliatory violence.
T31	ArgumentFor 2832 2889	Legalization would create proper means to settle disputes
T32	ArgumentFor 2891 2941	- It could be a source of additional tax revenues.
T33	ArgumentFor 2942 3049	An enormous amount of money is raised through government taxation of alcohol, cigarettes, and other "sins".
T34	ArgumentFor 3050 3126	The legalization of marijuana would create another item that could be taxed.
T35	ArgumentFor 3127 3202	I'm sure the government would have no problem spending all that extra money
T36	ArgumentFor 3204 3275	- Police and court resources would be freed up for more serious crimes.
T37	ArgumentFor 3276 3328	Many consider the War on Drugs an expensive failure.
T38	Sentence 3329 3405	Resources for DEA, FBI, and border security are only the tip of the iceberg.
T39	Sentence 3406 3540	You must add in the cost of police officers, judges, public defenders, prosecutors, juries, court reporters, prison guards, and so on.
T40	ArgumentFor 3541 3688	Legalization of marijuana would free up those people to concentrate on more important things like terrorism, harder drugs, rape, murder, and so on.
T41	ArgumentFor 3689 3831	In addition, an already overloaded civil court docket would be improved; thus, the wait time for other legitimate court cases would be reduced
T42	ArgumentFor 3833 3917	- Drug dealers (including some terrorists) would lose most or all of their business.
T43	ArgumentFor 3918 4000	Perhaps the biggest opponents of legalizing drugs are the drug dealers themselves.
T44	ArgumentFor 4001 4144	They make their enormous sums of money because of the absence of competition and the monstrous street prices that come from the increased risk.
T45	ArgumentFor 4145 4290	Legalization would lower prices and open competition; thus, drug cartels (that might include terrorists) would lose all or some of their business
T46	ArgumentFor 4292 4359	- The FDA or others could regulate the quality and safety of drugs.
T47	Sentence 4360 4431	Many drug users become sick or die because of poorly-prepared products.
T48	ArgumentAgainst 4432 4532	After all, there is nothing to regulate what is sold and no way to sue anyone for product liability.
T49	ArgumentFor 4533 4636	By bringing marijuana into the legitimate business world, you can oversee production and regulate sales
T50	ArgumentFor 4638 4734	- Like sex, alcohol, or cigarettes, marijuana is one of life's little pleasures for some people.
T51	Sentence 4735 4771	All of us have our guilty pleasures.
T52	Sentence 4772 4818	They are part of what makes life worth living.
T53	ArgumentFor 4819 4928	Several of these little pleasures--coffee, sex, alcohol, cigarettes, etc.--are potentially harmful if abused.
T54	ArgumentFor 4929 5030	Even legal substances like pizza and donuts can be harmful to a person if not consumed in moderation.
T55	Sentence 5031 5100	Would you want to give up all these things for the rest of your life?
T56	ArgumentFor 5101 5212	Would you want someone else telling you what you can and can't have when it is only your body that is affected?
T57	ArgumentFor 5213 5352	- Aside from recreational drug use, Cannabis has several industrial and commercial uses, as over 25,000 products can be made from the crop.
T58	ArgumentFor 5353 5547	The plant used in making marijuana has a ton of alternative uses, including construction & thermal insulation materials, paper, geotextiles, dynamite, composites for autos, and insect repellent.
T59	ArgumentFor 5548 5672	As far back as 1938, Popular Mechanics deemed it the "new billion dollar crop", as over 25,000 products can be made from it.
T60	ArgumentFor 5673 5799	Unfortunately, the lack of legality in the U.S. and other countries has squashed the growth and development of these products.
T61	ArgumentFor 5800 5899	We shouldn't limit the use of such a diverse product because one use is found objectionable by some
T62	ArgumentFor 5901 5997	- Drug busts often trap young people in a flawed system that turns them into lifelong criminals.
T63	Sentence 5998 6150	Imagine an impressionable teenager who is tired of earning minimum wage, who hates living in a poor ghetto area, or who needs to save money for college.
T64	ArgumentFor 6151 6245	He's offered the opportunity to make some decent money simply carrying some drugs across town.
T65	Sentence 6246 6263	Then he's busted.
T66	Sentence 6264 6316	He's thrown in jail as part of a mandatory sentence.
T67	Sentence 6317 6391	There, he spends his time and becomes friends with many other delinquents.
T68	Sentence 6392 6463	He gets meaner in jail since he has to defend himself in a rough crowd.
T69	ArgumentFor 6464 6589	When he gets out of prison, his job and college prospects are slammed because of a felony record and/or disruption of school.
T70	ArgumentAgainst 6590 6672	This just makes the resumption of a normal crime-free life all the more difficult.
T71	Sentence 6673 6757	Strapped for cash, he joins some of his new friends in a greater crime like robbery.
T72	Sentence 6758 6844	Suddenly, you have someone who has started down the road of being a lifelong criminal.
T73	Sentence 6845 6909	This story may seem farfetched, but it is all too real for some.
T74	ArgumentFor 6910 7041	The legalization of marijuana would remove another temptation that could lead a young impressionable individual down the wrong road
T75	ArgumentAgainst 7043 7045;7046 7148	No - Marijuana is often used as a stepping-stone drug, leading to heroin, cocaine, or other harder drugs.
T76	ArgumentAgainst 7149 7225	Studies show that marijuana use often progresses to the use of harder drugs.
T77	ArgumentAgainst 7226 7311	In other words, people experiment with what is often thought of as a "harmless" drug.
T78	ArgumentAgainst 7312 7446	Then, after using it for a while, a bigger "high" is sought; thus, users then turn to the harder stuff like heroin, LSD, cocaine, etc.
T79	ArgumentAgainst 7447 7589	This is particularly a problem since most people will not directly start abusing the harder drugs that are generally understood to be harmful.
T80	ArgumentAgainst 7590 7642	Marijuana use may simply embolden them to experiment
T81	ArgumentAgainst 7644 7698	- Stoned driving and other dangers would be increased.
T82	ArgumentAgainst 7699 7850	Marijuana use isn't truly a "victimless crime" when you consider all the crimes that may be committed when the user is under the influence of the drug.
T83	Sentence 7851 7951	Drunk driving is still a major problem in our society despite all the education and stiff penalties.
T84	ArgumentAgainst 7952 7998	"Driving high" would be even harder to detect.
T85	Sentence 7999 8107	Unless the user has been smoking in the car, there isn't as distinctive of a smell as there is with alcohol.
T86	ArgumentAgainst 8108 8238	Also, there's always the possibility that the lapse in judgment caused by drug use will lead to harder crimes like rape or robbery
T87	ArgumentAgainst 8240 8289	- Some consider use of the drug as morally wrong.
T88	ArgumentAgainst 8290 8365	Many religions and moral codes prohibit the use of intoxicating substances.
T89	Sentence 8366 8425	Marijuana is generally considered to fit into this category
T90	ArgumentAgainst 8427 8512	- Legalization would increase the chances of the drug falling into the hands of kids.
T91	Sentence 8513 8610	Even unhealthy legal items such as cigarettes and alcohol are prohibited from being sold to kids.
T92	Sentence 8611 8717	This is because kids generally don't exhibit the same reasoning, responsibility, and judgment of an adult.
T93	ArgumentAgainst 8718 8795	And their bodies aren't as equipped to handle the intake of these substances.
T94	Sentence 8796 8840	The problem is even worse for marijuana use.
T95	ArgumentAgainst 8841 8921	Developing brains and bodies can be dealt serious blows by the use of marijuana.
T96	ArgumentAgainst 8922 9000	Any time you make something legal, you increase the accessibility to children.
T97	Sentence 9001 9075	All too often kids and teenagers get their hands on alcohol or cigarettes.
T98	Sentence 9076 9129	We shouldn't let the same thing happen with marijuana
T99	ArgumentAgainst 9131 9265	- Because of drug-related arrests, people who have committed or are likely to commit more serious crimes can be taken off the streets.
T100	Sentence 9266 9391	People who produce, sell, traffic, or use illegal drugs have already established themselves as people who will break the law.
T101	ArgumentAgainst 9392 9529	Anyone who commits drug-related felonies isn't likely to be constrained in committing other felonies, such as robbery, rape, murder, etc.
T102	ArgumentAgainst 9530 9635	If such people are in prison because of drug charges, they aren't able to go out and commit other crimes.
T103	Sentence 9636 9823	Also, it often occurs that there isn't enough evidence to imprison felons for the serious crimes like murder; however, if they can be imprisoned for something, society is much better off.
T104	ArgumentFor 9824 9894	At a minimum, they will be off the streets, unable to wreak more havoc
T105	ArgumentAgainst 9896 9957	- Physical damage would be done to users that abuse the drug.
T106	ArgumentAgainst 9958 10119	Although some studies have been disputed, marijuana abuse has been tied to brain damage, cancer, lung damage, depression, amotivational syndrome, and even death.
T107	ArgumentAgainst 10120 10207	The brain damage has been shown to cause memory loss and difficulty in problem solving.
T108	ArgumentAgainst 10208 10283	It is the governments duty to protect the public from such dangerous drugs.
T109	Sentence 10284 10325	After all, that's why the FDA was created
T110	ArgumentAgainst 10327 10417	- More widespread use would increase the dangers of secondhand smoke-damage to bystanders.
T111	ArgumentAgainst 10418 10480	The dangers of secondhand cigarette smoke are well-publicized.
T112	ArgumentAgainst 10481 10643	Common sense tells us that more widespread usage of marijuana increases the likelihood that other people would suffer the damage of inhaling other people's smoke.
T113	ArgumentAgainst 10644 10698	Public places like bars would expose innocent patrons.
T114	ArgumentAgainst 10699 10784	In the home siblings, roommates, kids, and spouses would all face increased exposure.
T115	ArgumentAgainst 10785 10849	Thus, the health damage to society becomes somewhat exponential.
T116	ArgumentAgainst 10850 10951	Even marijuana smoked at home can make it's way to others, such as in multi-level apartment complexes
T117	ArgumentAgainst 10953 11063	- Legalization of marijuana could eventually lead to the legalization of harder drugs or all drugs altogether.
T118	ArgumentFor 11064 11103	Culture shifts rarely happen overnight.
T119	Sentence 11104 11185	Behaviors of society stay relatively stable, with only small incremental changes.
T120	ArgumentAgainst 11186 11283	Legalization of marijuana would further shift the culture to more of a "anything goes" mentality.
T121	ArgumentAgainst 11284 11381	Step-by-step, more drugs will gain acceptance, with advocacy of the legalization of harder drugs.
T122	ArgumentAgainst 11382 11549	Drugs like heroin, cocaine, and amphetamines, which we may view now as unacceptable for legalization may eventually be sold over the counter at every corner drug store
T123	Sentence 11551 11579;11580 11604;11605 11635;11636 11674;11675 11710;11711 11737;11738 11791;11792 11812;11813 11854;11855 11879;11880 11909	Related LinksReader Comments Marijuana Policy Project ProCon.org - Medical Marijuana Legalization of Marijuana Organization Partnership for a Drug-Free America Marijuana Research Reviews Marijuana and Medicine: Accessing the Scientific Base Don't Legalize Drugs Whitehouse Office of National Drug Policy Written by: Joe Messerli Page Last Updated: 08/06/2011
