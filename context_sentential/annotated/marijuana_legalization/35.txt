Arizonans for Mindful Regulation (AZFMR) represents a group of responsible Marijuana Consumers who live in Arizona.
AZFMR has launched our “Marijuana Consumers Against FAKE Marijuana Legalization” Campaign!
Arizonans For Mindful Regulation (AZFMR) worked directly with the Marijuana Policy Project (MPP) and the AZ Medical Marijuana Dispensary Owners who are paying for Prop. 205, in order to ensure that their 2016 Marijuana Initiative included the most important Marijuana Consumer protections.
In the end, the Marijuana Policy Project (MPP) and the AZ Medical Marijuana Dispensary Owners refused to listen to the Marijuana Consumers’ demands, and instead they wrote a Marijuana Initiative that ONLY protects the AZ Medical Marijuana Dispensaries.
After working hand-in-hand with the Prop. 205 Campaign, it became very clear that their ONLY motivation is MONEY. In other words;
THEY SIMPLY DO NOT CARE ABOUT THE MARIJUANA CONSUMERS!
Their Marijuana Initiative, known as Prop. 205, does VERY LITTLE to actually protect Arizona’s Marijuana Consumers!
After considering all of the PROS and CONS of Prop. 205, it became very obvious that Prop. 205 actually DOES MORE HARM THAN GOOD!
As such, AZFMR has now officially launched our “Marijuana Consumers Against FAKE Marijuana Legalization” Campaign.
We strongly encourage all Arizonans to Vote NO on Prop. 205 and Vote YES on our AZFMR 2018 Marijuana Legalization Initiative instead.
Over the years, we’ve learned first-hand that Arizona’s Marijuana Consumers do NOT support a Marijuana law that encompasses only TAX AND REGULATION. That’s not REAL Marijuana Legalization.
If you support REAL Marijuana Legalization in Arizona, will you please join us in supporting the Marijuana Consumers Against FAKE Marijuana Legalization Campaign?
If you would like to Volunteer or Contribute Financially, please Contact Us as soon as possible!