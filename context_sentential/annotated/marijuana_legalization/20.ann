T1	Sentence 0 154	And as Nov. 8 approaches, look for the commercial airwaves, and your social media feed, to contain more and more advertisements for and against Question 4
T2	Sentence 156 270	For the last two years, proponents of Question 4 have been amassing nearly $4 million advocating for legalization.
T3	Sentence 271 353	Last month, more than 80 percent of expenditures went straight into television ads
T4	Sentence 355 513	Campaign finance records show that most of the money for the Yes on 4 effort has come from the Washington, D.C.-based New Approach political action committee.
T5	Sentence 514 588	As of early this month, New Approach had contributed just under $3 million
T6	Sentence 590 762	Graham Boyd, the director and co-founder of New Approach, says the money is coming from grassroots fundraising — not, as opponents have charged, from the marijuana industry
T7	Sentence 764 919	"The philanthropists who are supporting the legalization and regulation of marijuana in Massachusetts are not in the marijuana industry at all," Boyd said.
T8	Sentence 920 1002	"They are people who believe that it makes no sense to treat marijuana as a crime.
T9	ArgumentFor 1003 1066	That for adults, it should be taxed, regulated and controlled."
T10	ArgumentFor 1067 1230	A review of New Approach's IRS filings confirms the vast majority of contributions to the PAC come from individuals with no apparent ties to the marijuana industry
T11	Sentence 1232 1305	The genesis of New Approach goes back to a late Ohio insurance executive.
T12	Sentence 1306 1408	Peter Lewis, who died in 2013, was a wealthy philanthropist and longtime CEO of Progressive Insurance.
T13	ArgumentFor 1409 1531	He became an advocate for medical marijuana when he used the drug to ease pain following the amputation of his leg in 1998
T14	Sentence 1533 1633	Boyd says Lewis set out to change marijuana laws around the country, including here in Massachusetts
T15	Sentence 1635 1770	"Peter actually was the funder of 98 percent of the funding for the 2012 medical marijuana ballot measure in Massachusetts," Boyd said.
T16	Sentence 1771 1881	"And he believed very much that that sort of medical model, and a strictly regulated legal model, made sense."
T17	Sentence 1882 1952	Not all of the money in support of Question 4 comes from out of state.
T18	Sentence 1953 2052	Some of it comes from businesses that stand to benefit, should recreational marijuana be legalized.
T19	Sentence 2053 2122	For instance: 4Front Ventures, located in Boston’s financial district
T20	Sentence 2124 2218	"We are a company in the legal, medical cannabis industry," company president Kris Krane said.
T21	ArgumentFor 2219 2413	"We work with operators across the United States helping them navigate the complex regulatory environments around this industry, and helping them set up very professional, compliant operations."
T22	Sentence 2414 2567	As of early this month, 4Front Ventures had contributed $28,500 to the Yes on 4 campaign and has been providing campaign staffers with free office space.
T23	Sentence 2568 2693	Krane says the decision to contribute was a matter of moral obligation, given his company's success in the marijuana industry
T24	Sentence 2695 2855	"Now that the opportunity is here to give back, and help support the campaign financially, I don't think I could live with myself if we didn't help," Krane said
T25	Sentence 2857 2929	Supporters of legalized marijuana have had a substantial financial edge.
T26	Sentence 2930 3016	At the beginning of October, fundraising by opponents had lagged by a factor of 6 to 1
T27	ArgumentFor 3018 3189	Prior to Adelson's contribution, campaign finance records show the vast majority of funding in opposition to legalization came from the health care and alcohol industries.
T28	Sentence 3190 3321	The Wine and Spirit Wholesalers of Massachusetts contributed $50,000, and the Beer Distributors of Massachusetts kicked in $25,000.
T29	Sentence 3322 3369	Neither organization returned calls for comment
T30	Sentence 3371 3535	However, Pam Wilmot, of the campaign finance watchdog group Common Cause, says it's not unusual for industries to make contributions based on economic self-interest
T31	Sentence 3537 3696	"You can see that with the liquor industry," Wilmot said, "clearly the Question 4 approval could be a competitor, and they want to keep them out of that role."
T32	Sentence 3697 3758	There's even more money coming from the health care industry.
T33	Sentence 3759 3835	Partners HealthCare, the largest employer in the state, contributed $100,000
T34	ArgumentAgainst 3837 4047	Vice president Rich Copp says Partners is concerned about what it's heard about legalization in other states, "particularly with the uncertainty around the safety of dosing of marijuana present in edible forms.
T35	Sentence 4048 4164	We just believe that more research and understanding is needed before broadly exposing the public to this new risk."
T36	Sentence 4165 4278	The Massachusetts Medical Society also chipped in $10,000 -- the first time it's contributed to a ballot campaign
T37	Sentence 4280 4403	"We did make an exception because this ballot question we feel is very dangerous," said society president Dr. James Gessner
T38	Sentence 4405 4490	Gessner says he's concerned about the message legalization would send to young people
T39	ArgumentAgainst 4492 4740	"The acceptance of a substance that can cause harm is the wrong message to send to children and adolescents," he said, "and we know that adolescents in the risk-taking times of their lives really perceive the risks of marijuana as having declined."
T40	Sentence 4741 4796	Groups for and against Question 4 have until 11:59 p.m.
T41	Sentence 4797 4879	Thursday to file their latest financial reports covering the first half of October
T42	Sentence 4881 5061	Both sides are expected to add to their war chests as Election Day grows closer, and the issue of legalized recreational marijuana in Massachusetts may be decided once and for all.
