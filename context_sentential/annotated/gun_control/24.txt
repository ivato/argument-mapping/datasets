Background:
The role guns play in our society has long been debated. There are those who believe that guns have no role in the hands of private citizens. Whereas there are those who believe that guns are the very foundation that protects our rights and liberties expressed in the Constitution. The following is a summary of the reasoning of the two positions.
We need more restrictions on guns!
Main Reference: www.soyouwanna.com
- Guns are not safe. There are too many accidents involving children playing with guns or accidental discharges due to careless or inexperienced adults.
- Too many criminals purchase guns legally and use them to commit crimes. background checks and mandatory waiting periods help to deter criminals while negligibly inhibiting us.
- Current gun registration laws are not adequate for law enforcement. To help prevent and to help solve crimes, guns should be registered and characterized before they are use to commit a crime. The only ones that would be threatened by this policy would be criminals.
- Semi-automatic weapons are intended for military use and have no business in the hands of civilians for either self-protection or sport.
- Where guns are readily accessible, they can more easily fall into the hands of children.
- The key to protecting our democracy is not in having armed citizens, but informed ones. Privately held guns would not be a match to our military's advanced weapons.
- There are significant loopholes in our current gun regulations. These need to be plugged.
- Unfortunately there are many misguided individuals in our society. We as a society would be better off if they had less access to guns.
Return to Home Page
Restricting guns is not the answer.
Main Reference: www.soyouwanna.com
- The Second Amendment was added to our Constitution because the founders believed in private ownership of guns was necessary for protection of our liberties.
- Gun laws punish honest citizens. If guns were outlawed, criminals would still find ways to arm themselves.
- Restrictions to gun ownership put citizens at the mercy of gun-toting criminals.
- Guns protect the rights and freedoms of the individual. It was privately held guns that helped the colonists rise up against the British.
- In the past, evil governments have used gun registries as way of locating and seizing privately owned weapons.
- Citizens have the right to self protection. Local police forces are not able to protect it's citizens, they primarily investigate and try to solve the crimes after the fact.
- If people obeyed current laws, there would not be a need for more laws.
- Current laws are often poorly executed. Criminals are often able to buy guns even when the law says they should be prohibited. We need to enforce existing laws first.
- Guns don't kill people, people do. We need to concentrate on the values and morals of our citizens and at the role the media plays in glorifying violence and the lack of respect for law.
Copyright 2008; All rights reserved. ClearPictureOnline.com