T1	Sentence 0 63;64 112	A landmark dissenting opinion arguing against the death penalty Does the death penalty violate the Constitution?
T2	ArgumentAgainst 113 356	In Against the Death Penalty, Justice Stephen G. Breyer argues that it does: that it is carried out unfairly and inconsistently, and thus violates the ban on “cruel and unusual punishments” specified by the Eighth Amendment to the Constitution
T3	ArgumentAgainst 358 623	“Todayâs administration of the death penalty,” Breyer writes, “involves three fundamental constitutional defects: (1) serious unreliability, (2) arbitrariness in application, and (3) unconscionably long delays that undermine the death penaltyâs penological purpose.
T4	ArgumentAgainst 624 710	Perhaps as a result, (4) most places within the United States have abandoned its use.”
T5	Sentence 711 903	This volume contains Breyer’s dissent in the case of Glossip v. Gross, which involved an unsuccessful challenge to Oklahoma’s use of a lethal-injection drug because it might cause severe pain.
T6	Sentence 904 1164	Justice Breyer’s legal citations have been edited to make them understandable to a general audience, but the text retains the full force of his powerful argument that the time has come for the Supreme Court to revisit the constitutionality of the death penalty
T7	Sentence 1166 1245	Breyer was joined in his dissent from the bench by Justice Ruth Bader Ginsburg.
T8	Sentence 1246 1423	Their passionate argument has been cited by many legal experts â including fellow Justice Antonin Scalia â as signaling an eventual Court ruling striking down the death penalty.
T9	Sentence 1424 1606	A similar dissent in 1963 by Breyer’s mentor, Justice Arthur J. Goldberg, helped set the stage for a later ruling, imposing what turned out to be a four-year moratorium on executions
T10	Sentence 1608 1693	Stephen G. Breyer has been an Associate Justice of the U.S. Supreme Court since 1994.
T11	Sentence 1694 1946	He received his law degree from Harvard Law School, clerked for Associate Justice Arthur J. Goldberg in the 1964â65 Supreme Court term, and taught at Harvard for nearly two decades before joining the U.S. Court of Appeals for the First Circuit in 1980.
T12	Sentence 1947 2036	President Clinton nominated Breyer to succeed Harry Blackmun on the Supreme Court in 1994
T13	Sentence 2038 2186	John D. Bessler is a professor of law at the University of Baltimore School of Law and an adjunct professor at the Georgetown University Law Center.
T14	Sentence 2187 2307	He is also of counsel to the Minneapolis law firm of Berens & Miller, P.A., which handles complex commercial litigation.
