T1	Sentence 0 24;25 49;50 104	Minimum Wage Mythbusters Minimum Wage Mythbusters Myth: Raising the minimum wage will only benefit teens
T2	ArgumentAgainst 116 206	The typical minimum wage worker is not a high school student earning weekend pocket money.
T3	ArgumentFor 207 352	In fact, 89 percent of those who would benefit from a federal minimum wage increase to $12 per hour are age 20 or older, and 56 percent are women
T4	ArgumentFor 354 424	Myth: Increasing the minimum wage will cause people to lose their jobs
T5	ArgumentFor 436 941	In a letter to President Obama and congressional leaders urging a minimum wage increase, more than 600 economists, including 7 Nobel Prize winners wrote, "In recent years there have been important developments in the academic literature on the effect of increases in the minimum wage on employment, with the weight of evidence now showing that increases in the minimum wage have had little or no negative effect on the employment of minimum-wage workers, even during times of weakness in the labor market.
T6	ArgumentFor 942 1169	Research suggests that a minimum-wage increase could have a small stimulative effect on the economy as low-wage workers spend their additional earnings, raising demand and job growth, and providing some help on the jobs front."
T7	Sentence 1170 1297	Myth: Small business owners can't afford to pay their workers more, and therefore don't support an increase in the minimum wage
T8	ArgumentFor 1299 1441	Not true: A July 2015 survey found that 3 out of 5 small business owners with employees support a gradual increase in the minimum wage to $12.
T9	ArgumentFor 1442 1640	The survey reports that small business owners say an increase "would immediately put more money in the pocket of low-wage workers who will then spend the money on things like housing, food, and gas.
T10	ArgumentFor 1641 1748	This boost in demand for goods and services will help stimulate the economy and help create opportunities."
T11	ArgumentFor 1749 1845	Myth: Raising the federal tipped minimum wage ($2.13 per hour since 1991) would hurt restaurants
T12	Sentence 1857 1959	In California, employers are required to pay servers the full minimum wage of $9 per hour before tips.
T13	Sentence 1960 2133	Even with a 2014 increase in the minimum wage, the National Restaurant Association projects California restaurant sales will outpace all but only a handful of states in 2015
T14	ArgumentAgainst 2135 2244	Myth: Raising the federal tipped minimum wage ($2.13 per hour since 1991) would lead to restaurant job losses
T15	Sentence 2256 2376	As of May 2015, employers in San Francisco must pay tipped workers the full minimum wage of $12.25 per hour before tips.
T16	ArgumentFor 2377 2579	Yet, the San Francisco leisure and hospitality industry, which includes full-service restaurants, has experienced positive job growth this year, including following the most recent minimum wage increase
T17	ArgumentFor 2581 2724	Myth: Raising the federal minimum wage won't benefit workers in states where the hourly minimum rate is already higher than the federal minimum
T18	ArgumentFor 2736 2954	While 29 states and the District of Columbia currently have a minimum wage higher than the federal minimum, increasing the federal minimum wage will boost the earnings for nearly 38 million low-wage workers nationwide.
T19	Sentence 2955 3043	That includes workers in those states already earning above the current federal minimum.
T20	ArgumentFor 3044 3127	Raising the federal minimum wage is an important part of strengthening the economy.
T21	ArgumentFor 3128 3301	A raise for minimum wage earners will put more money in more families' pockets, which will be spent on goods and services, stimulating economic growth locally and nationally
T22	Sentence 3303 3363	Myth: Younger workers don't have to be paid the minimum wage
T23	Sentence 3375 3482	While there are some exceptions, employers are generally required to pay at least the federal minimum wage.
T24	Sentence 3483 3729	Exceptions allowed include a minimum wage of $4.25 per hour for young workers under the age of 20, but only during their first 90 consecutive calendar days of employment with an employer, and as long as their work does not displace other workers.
T25	ArgumentFor 3730 3940	After 90 consecutive days of employment or the employee reaches 20 years of age, whichever comes first, the employee must receive the current federal minimum wage or the state minimum wage, whichever is higher.
T26	Sentence 3941 4128	There are programs requiring federal certification that allow for payment of less than the full federal minimum wage, but those programs are not limited to the employment of young workers
T27	ArgumentFor 4130 4217	Myth: Restaurant servers don't need to be paid the minimum wage since they receive tips
T28	Sentence 4229 4487	An employer can pay a tipped employee as little as $2.13 per hour in direct wages, but only if that amount plus tips equal at least the federal minimum wage and the worker retains all tips and customarily and regularly receives more than $30 a month in tips.
T29	Sentence 4488 4627	Often, an employee's tips combined with the employer's direct wages of at least $2.13 an hour do not equal the federal minimum hourly wage.
T30	Sentence 4628 4687	When that occurs, the employer must make up the difference.
T31	Sentence 4688 4752	Some states have minimum wage laws specific to tipped employees.
T32	Sentence 4753 4909	When an employee is subject to both the federal and state wage laws, he or she is entitled to the provisions of each law which provides the greater benefits
T33	ArgumentAgainst 4911 4966	Myth: Increasing the minimum wage is bad for businesses
T34	ArgumentFor 4968 5103	Not true: Academic research has shown that higher wages sharply reduce employee turnover which can reduce employment and training costs
T35	ArgumentFor 5105 5161	Myth: Increasing the minimum wage is bad for the economy
T36	Sentence 5163 5238	Not true: Since 1938, the federal minimum wage has been increased 22 times.
T37	ArgumentFor 5239 5349	For more than 75 years, real GDP per capita has steadily increased, even when the minimum wage has been raised
T38	Sentence 5357 5422	The federal minimum wage goes up automatically as prices increase
T39	Sentence 5434 5639	While some states have enacted rules in recent years triggering automatic increases in their minimum wages to help them keep up with inflation, the federal minimum wage does not operate in the same manner.
T40	Sentence 5640 5728	An increase in the federal minimum wage requires approval by Congress and the president.
T41	ArgumentFor 5729 5879	However, in his call to gradually increase the current federal minimum, President Obama has also called for it to adjust automatically with inflation.
T42	Sentence 5880 6059	Eliminating the requirement of formal congressional action would likely reduce the amount of time between increases, and better help low-income families keep up with rising prices
T43	Sentence 6067 6153	The federal minimum wage is higher today than it was when President Reagan took office
T44	ArgumentFor 6165 6496	While the federal minimum wage was only $3.35 per hour in 1981 and is currently $7.25 per hour in real dollars, when adjusted for inflation, the current federal minimum wage would need to be more than $8 per hour to equal its buying power of the early 1980s and more nearly $11 per hour to equal its buying power of the late 1960s.
T45	Sentence 6497 6625	That's why President Obama is urging Congress to increase the federal minimum wage and give low-wage workers a much-needed boost
T46	Sentence 6627 6681	Myth: Increasing the minimum wage lacks public support
T47	ArgumentFor 6683 6765	Not true: Raising the federal minimum wage is an issue with broad popular support.
T48	ArgumentFor 6766 6963	Polls conducted since February 2013 when President Obama first called on Congress to increase the minimum wage have consistently shown that an overwhelming majority of Americans support an increase
T49	Sentence 6965 7129	Myth: Increasing the minimum wage will result in job losses for newly hired and unskilled workers in what some call a last-one-hired-equals-first-one-fired scenario
T50	ArgumentFor 7131 7279	Not true: Minimum wage increases have little to no negative effect on employment as shown in independent studies from economists across the country.
T51	ArgumentFor 7280 7410	Academic research also has shown that higher wages sharply reduce employee turnover which can reduce employment and training costs
T52	Sentence 7418 7479	The minimum wage stays the same if Congress doesn't change it
T53	Sentence 7481 7563	Not true: Congress sets the minimum wage, but it doesn't keep pace with inflation.
T54	ArgumentAgainst 7564 7682	Because the cost of living is always rising, the value of a new minimum wage begins to fall from the moment it is set.
