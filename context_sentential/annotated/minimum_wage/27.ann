T1	Sentence 0 36;37 46	National $15 Minimum Wage Is Trouble Read More
T2	Sentence 48 178	A while back I wrote in support of the $15 minimum wage initiatives in San Francisco, Seattle, Los Angeles and a few other cities.
T3	ArgumentAgainst 179 297	Now it’s time for me to write the opposite article -- the idea of a $15 minimum wage is being taken too far, too fast.
T4	Sentence 298 460	Democratic Senator Bernie Sanders just introduced a bill to mandate a federal $15 minimum wage, and he probably will make it a plank of his presidential campaign.
T5	ArgumentAgainst 461 485	This would be a bad idea
T6	Sentence 487 620	At this point, some of my more left-leaning readers will probably get mad, and say that I am shilling for employers, or for the rich.
T7	ArgumentFor 621 845	They will list a long litany of ways in which the poor and working class have suffered or been left behind in the last few decades, and demand to know why I’m opposing a measure intended to help the helpless and downtrodden.
T8	ArgumentAgainst 846 990	But it’s important to realize that the argument against minimum wages isn’t that they hurt the rich; it’s that they can end up hurting the poor.
T9	ArgumentAgainst 991 1124	Raise minimum wages too high, and you’ll eventually choke off employment, harming the very people that the policy is intended to help
T10	Sentence 1149 1250	We’re not going to get an answer to the employment question until some cities try a $15 minimum wage.
T11	Sentence 1251 1283	It might work, and it might not.
T12	ArgumentAgainst 1284 1376	Smaller minimum wage hikes in the past weren’t too damaging, but $15 is uncharted territory.
T13	Sentence 1377 1491	Experiments such as those in Seattle, San Francisco and Los Angeles will help us determine whether $15 is too high
T14	ArgumentAgainst 1493 1549	That’s exactly why a federal minimum wage is a bad idea.
T15	Sentence 1550 1659	If some cities have $15 minimum wages and other nearby cities don’t, we can compare the former to the latter.
T16	Sentence 1660 1714	We can look at their respective economic performances.
T17	Sentence 1715 1824	We can see if people and businesses move from cities with high minimum wages to those with low minimum wages.
T18	Sentence 1825 1894	If the minimum wage is $15 everywhere, we can’t make that comparison.
T19	Sentence 1895 1938	A federal minimum wage ruins the experiment
T20	Sentence 1940 2072	Now, with a federal minimum wage, there will be nothing to stop us from doing a before-and-after comparison of economic performance.
T21	Sentence 2073 2175	But that probably won’t tell us much, because a lot of other stuff will be happening at the same time.
T22	Sentence 2176 2289	There will be recessions or booms, asset prices will rise or fall, and a bunch of other policies will be enacted.
T23	Sentence 2290 2315	We won’t get a clean test
T24	Sentence 2317 2412	Suppose we enact a federal $15 minimum wage, and employment falls a lot during the next decade?
T25	Sentence 2413 2447	Pundits will be arguing for years.
T26	Sentence 2448 2544	Was it the minimum wage that caused the fall, they will ask, or too much regulation of business?
T27	Sentence 2545 2579	Was it shifting patterns of trade?
T28	Sentence 2580 2615	Was it declining business dynamism?
T29	Sentence 2616 2711	The list of possible causes will be long, and the data won’t be able to distinguish among them.
T30	Sentence 2712 2922	The same thing will happen if employment doesn’t fall -- minimum wage opponents will argue that without the minimum wage hike, it would have risen even more, while proponents will say that the hike was harmless
T31	Sentence 2924 2987	In other words, local $15 minimum wages will resolve arguments.
T32	Sentence 2988 3023	A federal $15 minimum wage will not
T33	ArgumentAgainst 3025 3124	Actually, there’s a big reason to think that a federal minimum wage isn’t a good policy in general.
T34	Sentence 3125 3300	As Slate’s Jordan Weissmann points out, the huge differences in local costs of living mean that in some areas, $15 is only a small increase, while in others it’s a huge boost.
T35	ArgumentFor 3301 3486	This means that a $15 federal minimum wage would only raise the wages of the urban working class a modest amount, but would raise the wages of the small-town working class a huge amount
T36	Sentence 3488 3592	Whether you’re a minimum wage supporter or an opponent, you should be afraid of that kind of uniformity.
T37	ArgumentAgainst 3593 3763	If minimum wages don’t hurt employment, then a federal minimum wage is unfair to workers in big cities because their raises will be less than those of small-town workers.
T38	ArgumentAgainst 3764 3887	But if minimum wages do hurt employment, then small-town workers are going to be put out of a job -- and that's much worse.
T39	ArgumentAgainst 3888 4020	A much better idea would be local “living wage” campaigns, each one pushing for a number that is appropriate to specific job markets
T40	ArgumentAgainst 4022 4225	So although I’m glad to see cities experimenting with a $15 minimum wage, I think we should avoid implementing the policy at the federal level, at least until we see the results of the local experiments.
T41	ArgumentAgainst 4226 4368	And even if the local experiments are successful, I think that targeted living wage initiatives are a better idea than a federal minimum wage.
T42	Sentence 4369 4469	If we’re going to help the working class, we need effective policies, not just well-intentioned ones
T43	Sentence 4471 4577	This column does not necessarily reflect the opinion of the editorial board or Bloomberg LP and its owners
T44	Sentence 4579 4615;4616 4653	To contact the author on this story: Noah Smith at nsmith150@bloomberg.net
T45	Sentence 4654 4690;4691 4728	To contact the editor on this story: James Greiff at jgreiff@bloomberg.net
