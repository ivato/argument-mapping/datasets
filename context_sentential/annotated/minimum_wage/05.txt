Studies abound on the minimum wage, but the conclusions drawn vary greatly.
By Jeanne Mejeur
To paraphrase an old joke, if there were only two economists left in the world, they would disagree about the minimum wage. Does it cost jobs or create jobs?
It’s a valid question, but the answer depends on who you ask.
Opponents of minimum wages contend that increased labor costs force businesses to cut staff, costing jobs. That sounds reasonable. Proponents of minimum wages argue that giving workers more disposable income puts money back into the economy, which in turn creates jobs. That makes sense, too.
So what’s the answer?
Studies abound on the minimum wage. Some make common sense arguments while others use enough fancy math to dazzle any economist. Most studies are published by interest groups that either support or oppose a minimum wage, or from authors connected to such interest groups. If you read enough of these studies, you’ll likely come to the realization that, almost without exception, they are trying to persuade you more than inform you.
The Pro Arguments
Commonly used arguments supporting increases to the minimum wage follow.
1. Increases put more money into the pockets of low-income workers. According to a 2013 Congressional Research Service report, a single parent with two children who works full time at the current minimum wage would be earning around $15,000 and living at 76 percent of the federal poverty level. If the federal minimum wage was raised from the current $7.25 to $9 an hour, the same family would be at 94 percent of the poverty line.
2. Minimum wage increases shrink the gap between low-wage and higher-paid workers, lessening income inequality, both within individual businesses and in the larger economy.
3. Minimum wage increases put more money into the economy since low-income workers are more likely to spend their higher wages than are their higher paid counterparts, who are more likely to save them. This increased demand for goods and services tends to stimulate the economy which, in turn, leads to job creation.
4. Higher minimum wages reduce turnover among low-wage workers. Lower turnover rates are a net positive for businesses, since high turnover increases training costs and results in lower productivity.
The Con Arguments
Following are the most commonly cited arguments against minimum wage increases.
1. It results in job losses. Labor costs are the largest share of the budget for many businesses. Mandatory increases in hourly wages mean that businesses will be forced to cut jobs or reduce hours to maintain their bottom line. That could mean no income or reduced income for low-wage workers.
2. There are better ways to address poverty, such as income tax credits for low-income workers or tax policies that encourage asset development and savings for low-income families.
3. Increased labor costs will be passed on to consumers through increased prices. Higher prices lead to decreased demand, which can have a depressive effect on the economy.
4. Increased labor costs result in lower profits for businesses. Lower profits mean that businesses have less money to put back into their enterprises for job creation and business expansion.
So there you have it: There is no definitive study, no final answer. Whatever your opinion on minimum wage, you can find a study that will back it up.
The last increase in the federal minimum wage was in 2009. In constant dollars, the minimum wage that was worth $7.25 in 2009 is now worth $6.67, due to the increased cost of living.
A seemingly indisputable fact is that, despite 22 increases in the 75 years since it was established in 1938, the federal minimum wage has not kept pace with inflation. In real dollars, the peak value of the federal minimum wage was in 1968, when the wage was set at $1.60. That would be $10.56 in today’s economy, well above the current $7.25.
From a consumer’s point of view, the average cost of a loaf of bread in 1968 was 22 cents, accounting for 14 percent of an hour’s pay at the $1.60 minimum wage. The average cost of a loaf of bread in 2012 was $1.88, which accounts for 26 percent of an hour’s pay at the current minimum wage. No matter how you slice it, the minimum wage hasn’t kept up with inflation.
Congress Stalls, States Act
The lack of action on the federal level has prompted many states to consider increases to their state minimum wages.
All but five have adopted state minimum wages. Alabama, Louisiana, Mississippi, South Carolina and Tennessee have no state minimum wage, relying solely on the federal minimum wage for workers who are covered by the Fair Labor Standards Act. In addition, New Hampshire repealed its state minimum wage in 2011, but left a statutory reference to the federal minimum wage.
Nineteen states have set their state minimum wage to match the federal wage of $7.25, as have Guam, Puerto Rico and the U.S. Virgin Islands.
Twenty-one states and the District of Columbia have established state minimum wages that are above the federal minimum wage. The highest state minimum wage is in Washington, at $9.32. California is set to surpass that in January 2016, when the state minimum wage will increase to $10 per hour.
Minimum wage continues to be a hot issue in state legislatures. As of Feb. 1, lawmakers had introduced legislation to increase the minimum wage in Delaware, Georgia, Iowa, Hawaii, Kentucky, Maryland, Massachusetts, Missouri, Nebraska, New Hampshire, New Jersey, New Mexico, New York, Oklahoma, Pennsylvania, Rhode Island, Tennessee, Utah, Washington and West Virginia. Delaware has already enacted an increase effective July 1, 2014, and Washington, D.C., passed an increase that is under review in Congress.
During the 2013 legislative session, lawmakers in 23 states and the District of Columbia introduced legislation to increase their state minimum wage, and bills passed in seven states. Four states—California, Connecticut, New York and Rhode Island—enacted minimum wage hikes that were signed into law.
Legislatures in Maine, New Jersey and New Mexico passed bills as well, but they were vetoed by the governors. Voters in New Jersey had the final say on the issue, however. They approved a constitutional amendment in the 2013 November election that raised the minimum wage and tied future increases to the cost of living.
The Indexing Option
States have taken a couple of different approaches to raising the minimum wage. The traditional method has been to establish a specific dollar amount with a specific effective date. Rhode Island’s 2013 bill is an example of that approach. The General Assembly passed a law establishing $8.00 as the minimum wage beginning Jan. 1, 2014.
Sometimes a legislature will enact a multiple-step increase, as California lawmakers did last year. Its state minimum wage will increase to $9.00 on July 1, 2014, and jump to $10.00 on Jan. 1, 2016.
Other states have taken a different approach called indexing, which provides automatic annual increases based on the increased cost of living as determined by the Consumer Price Index. Eleven states have adopted indexed minimum wages since 2001. Interestingly, in 10 of the states, indexing was approved by voters. Vermont is the only state where the legislature approved indexing of the minimum wage.
Which approach is best? Both have their advantages and disadvantages.
Indexing
- The minimum wage increases automatically without discussion or debate, avoiding legislative squabbles.
- Regular increases allow workers’ wages to keep pace with inflation.
- It allows businesses to plan ahead for their labor costs, since they know in advance that wages will increase each year.
- The increases tend to be slightly larger overall, with an 18 cent an hour average annual increase.
Step Increases
- Lawmakers can discuss and debate whether an increase is currently needed.
- Minimum wage bills often ignite battles as they go through the legislative process.
- Step increases take additional factors into account, such as the impact of natural disasters, high unemployment, or economic downturns, rather than looking only at inflation.
- Step increases tend to be larger when adopted, sometimes up to $1 an hour, but when averaged over the affected years, the increases are actually slightly smaller at an average of 16 cents an hour annually.
Public Opinion
Where does the public stand on the issue? In a Gallup poll conducted in November 2013, 76 percent of the public supported raising the federal minimum wage to $9 an hour. In the same poll, 69 percent of those asked supported an increase to $9 an hour and indexing future increases to the cost of living, to keep pace with inflation.
The same poll found that the level of support varies according to party affiliation but still has broad support among voters of all stripes. When asked whether they supported increasing the federal minimum wage to $9 an hour, 91 percent of Democrats, 76 percent of independents and 58 percent of Republicans said yes.
There was a bigger divide regarding indexing or “inflation-proofing” the minimum wage. Ninety-two percent of Democrats, 71 percent of independents and 43 percent of Republicans said they supported raising the federal minimum wage to $9.00 an hour and indexing future increases to inflation.
Pro or con, indexing or not, with minimum wage bills in Congress going nowhere, the issue continues to be hotly debated in state legislatures. It’s likely voters in several states will see minimum wage ballot measures when they go to the polls for the mid-term elections this fall.
Jeanne Mejeur is NCSL’s expert on state labor and employment issues.
Federal Proposals
The first minimum wage was set at 25 cents an hour in 1938 by the Fair Labor Standards Act. Congress has raised it 22 times since. In February, President Obama raised the minimum wage to $10.10 for federal contractors only. Meanwhile, in Congress, several bills on the minimum wage have been introduced. A quick look at these follows.
Fair Minimum Wage Act of 2013 (S. 460 and H.R.1010)
- Increases to $8.20, $9.15, and $10.10 over three years
- Indexes subsequent increases
- Increases tipped wage to $3
- Indexes future tipped wage increases
Fair Minimum Wage Act of 2013 (H.R. 3746)
- Increases to $8.50, $10, and $11 over three years
- Indexes subsequent increases
Minimum Wage Fairness Act (S. 1737)
- Increases to $8.20, $9.15, and $10.10 over three years
- Indexes subsequent increases
- Increases tipped wage to $3
- Indexes future tipped wage increases
- Raises limits on business expenses
Original Living American Wage Act (H.R. 229)
- Adjusts federal minimum every four years
- Keeps minimum wage at 15 percent above poverty line
WAGES Act (H.R. 650)
- Raises tipped wage to $3.75 and $5 over two years
- Indexes future tipped wage increases
Catching Up To 1968 Act of 2013 (H.R.1346)
- Increases to $10.10
- Indexes subsequent increases
- Raises tipped wage to 70 percent of the minimum wage
Minimum Wage Rates for 2014
|Minimum Wage
||Indexed to CPI
|
|Alabama | None
||
|
|Alaska | $7.75
||
|
|American Samoa | Varies
||
|
|Arizona | $7.90
||Yes
|
|Arkansas | $6.25
||
|
|California | $9 (in July 2014)
||
|
|Colorado | $8
||
|
Yes
|Connecticut | $8.70
||
|
|Delaware | $7.25
||
|
|District of Colombia | $8.25
||
|
|Florida | $7.93
||Yes
|
|Georgia | $5.15
||
|
|Guam | $7.25
||
|
|Hawaii | $7.25
||
|
|Idaho | $7.25
||
|
|Illinois | $8.25
||
|
|Indiana | $7.25
||
|
|Iowa | $7.25
||
|
|Kansas | $7.25
||
|
|Kentucky | $7.25
||
|
|Louisiana | None
||
|
|Maine | $7.50
||
|
|Maryland | $7.25
||
|
|Massachusetts | $8
||
|
|Michigan | $7.40
||
|
|Minnesota | $6.15
||
|
|Mississippi | None
||
|
|Missouri | $7.50
||Yes
|
|Montana | $7.90
||Yes
|
|Nebraska | $7.25
||
|
|Nevada | $8.25
||Yes
|
|New Hampshire | Repealed
||
|
|New Jersey | $8.25
||Yes
|
|New Mexico | $7.50
||
|
|New York | $8
||
|
|North Carolina | $7.25
||
|
|North Dakota | $7.25
||
|
|Ohio | $7.95
||Yes
|
|Oklahoma | $7.25
||
|
|Oregon | $9.10
||Yes
|
|Pennsylvania | $7.25
||
|
|Puerto Rico | $7.25
||
|
|Rhode Island | $8
||
|
|South Carolina | None
||
|
|South Dakota | $7.25
||
|
|Tennessee | None
||
|
|Texas | $7.25
||
|
|Utah | $7.25
||
|
|Vermont | $8.73
||Yes
|
|U.S. Virgin Islands | $7.25
||
|
|Virginia | $7.25
||
|
|Washington | $9.32
||Yes
|
|West Virginia | $7.25
||
|
|Wisconsin | $7.25
||
|
|Wyoming | $5.15
||
|
Source: NCSL
Burgernomics
Here’s how many minutes it takes, earning minimum wage, to make enough to buy a burger around the world.
|Country/Wages
||Minutes
|
|Australia | $16.88
||18
|
|France | $12.09
||22
|
|United Kingdom | $9.83
||23
|
|Japan | $8.17
||31
|
|United States | $7.25
||35
|
|Greece | $5.06
||53
|
|Brazil | $1.98
||172
|
|China | 80 cents
||183
|
|India | 23 cents
||347
|
Source: “The Economist” magazine developed the Big Mac index in 1986 as a way to measure whether foreign currencies are at their “correct” level. It compares exchange rates in different countries to see if they result in the same purchasing power for an identical item—the burger. The ConvergEx Group, a global brokerage firm, adapted the news magazine’s burgernomics to minimum wages in various countries to come up with this chart. Minimum-Wage.org provided the wage rates in “international dollars,” which is based on the U.S. dollar in 2009.
Who Earns Minimum Wage?
According to the Bureau of Labor Statistics, based on 2012 figures:
- About 3.6 million (or 4.8 percent) of the 75 million workers paid on an hourly basis earn $7.25 an hour or less.
- More than half of minimum wage workers are under the age of 25.
- Six percent of women and 3 percent of men earn minimum wages.
- The leisure and hospitality sector has the highest proportion of minimum wage workers.
- Louisiana, Oklahoma, Texas and Idaho have the highest percentage of minimum wage workers.
- Alaska, Oregon, California, Montana and Washington have the lowest percentage of minimum wage workers.
Source: The Bureau of Labor Statistics, 2012 data
Additional Resources
NCSL Resources
Other Resources