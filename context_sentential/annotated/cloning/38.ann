T1	Sentence 0 42	Cloning is a form of asexual reproduction.
T2	Sentence 43 124	A child produced by cloning would be the genetic duplicate of an existing person.
T3	Sentence 125 285	If you cloned yourself, the resulting child would be neither your son or daughter nor your twin brother or sister, but a new category of human being: your clone
T4	ArgumentAgainst 287 379	The great majority of people have an intuitive sense that human beings should not be cloned.
T5	Sentence 380 451	Arguments offered for and against reproductive cloning are given below.
T6	Sentence 452 505	A summary comment follows at the end of the arguments
T7	ArgumentAgainst 507 545;546 723	Arguments Against Reproductive Cloning 1. Reproductive cloning would foster an understanding of children, and of people in general, as objects that can be designed and manufactured to possess specific characteristics
T8	ArgumentAgainst 725 805	2. Reproductive cloning would diminish the sense of uniqueness of an individual.
T9	ArgumentAgainst 806 973	It would violate deeply and widely held convictions concerning human individuality and freedom, and could lead to a devaluation of clones in comparison with non-clones
T10	ArgumentAgainst 975 1156	3. Cloned children would unavoidably be raised "in the shadow" of their nuclear donor, in a way that would strongly tend to constrain individual psychological and social development
T11	ArgumentAgainst 1158 1203	4. Reproductive cloning is inherently unsafe.
T12	ArgumentAgainst 1204 1399	At least 95% of mammalian cloning experiments have resulted in failures in the form of miscarriages, stillbirths, and life-threatening anomalies; some experts believe no clones are fully healthy.
T13	ArgumentAgainst 1400 1538	The technique could not be developed in humans without putting the physical safety of the clones and the women who bear them at grave risk
T14	ArgumentAgainst 1540 1725	5. If reproductive cloning is permitted to happen and becomes accepted, it is difficult to see how any other dangerous applications of genetic engineering technology could be proscribed
T15	Sentence 1727 1778;1779 1787	Rebuttals to Arguments Against Reproductive Cloning 1 and 2.
T16	Sentence 1788 1837	This will be true only if we allow it to be true.
T17	ArgumentFor 1838 1980	There is no reason that individuals and society can't learn to embrace human clones as just one more element of human diversity and creativity
T18	Sentence 1982 2051	3. The problem of "expectations" is hardly unique to cloned children.
T19	Sentence 2052 2167	Most parents learn to communicate their expectations about their children in a moderate and ultimately positive way
T20	Sentence 2169 2230	4. Every medical technology carries with it a degree of risk.
T21	ArgumentFor 2231 2332	Cloning techniques will eventually be perfected in mammals and will then be suitable for human trials
T22	Sentence 2334 2413	5. Human society can accept or reject any proposed technology on its own merits
T23	Sentence 2415 2457;2458 2460	Arguments in Favor of Reproductive Cloning 1.
T24	ArgumentFor 2461 2625	Reproductive cloning can provide genetically related children for people who cannot be helped by other fertility treatments (i.e., who do not produce eggs or sperm)
T25	ArgumentFor 2627 2868	2. Reproductive cloning would allow lesbians to have a child without having to use donor sperm, and gay men to have a child that does not have genes derived from an egg donor (though, of course, a surrogate would have to carry the pregnancy)
T26	ArgumentFor 2870 2968	3. Reproductive cloning could allow parents of a child who has died to seek redress for their loss
T27	ArgumentFor 2970 3090	4. Cloning is a reproductive right, and should be allowed once it is judged to be no less safe than natural reproduction
T28	Sentence 3092 3147;3148 3150	Rebuttals to Arguments in Favor of Reproductive Cloning 1.
T29	Sentence 3151 3306	The number of men and women who do not produce eggs or sperm at all is very small, and has been greatly reduced by modern assisted-reproduction techniques.
T30	ArgumentAgainst 3307 3435	If cloning could be perfected and used for this limited group, it would be all but impossible to prevent its use from spreading.
T31	ArgumentAgainst 3436 3669	Further, this argument appropriates the phrase "genetically related" to embrace a condition that has never before occurred in human history, one which abolishes the genetic variations that have always existed between parent and child
T32	ArgumentAgainst 3671 3823	2. Even if cloning were safe, it would be impossible to allow reproductive cloning for lesbians or gay men without making it generally available to all.
T33	Sentence 3824 3917	Policy and social changes that protect lesbian and gay families are a much more pressing need
T34	Sentence 3919 4035	3. Throughout history, parents who have lost children have grieved and sought consolation from family and community.
T35	ArgumentAgainst 4036 4144	"Replacing" the deceased child by cloning degrades and dehumanizes the child, its replacement, and all of us
T36	ArgumentAgainst 4146 4239	4. Rights are socially negotiated, and no "right" to clone oneself has ever been established.
T37	Sentence 4240 4407	Furthermore, there is an immense difference between a woman's desire to terminate an unwanted pregnancy and the desire to create a genetic duplicate of another person.
T38	Sentence 4408 4487	There is no inconsistency between supporting the former and opposing the latter
T39	Sentence 4489 4504;4505 4597	Summary Comment Most advocates of human cloning also advocate the genetic modification of the human species.
T40	Sentence 4598 4792	Human cloning is a blunt form of eugenics-it "copies" an existing genome-while inheritable genetic modification allows the creation of "designer babies" through manipulation of individual genes.
T41	ArgumentFor 4793 4903	But cloning technologies are needed if inheritable genetic modification is to become commercially practicable.
T42	Sentence 4904 5000	This is the deeper and more far-reaching motivation behind much of the advocacy of human cloning
T43	ArgumentAgainst 5002 5194	The Center for Genetics and Society believes that when all the arguments are considered together the case for allowing human cloning is not compelling, and that the harms of doing so are great
T44	Sentence 5196 5222	Last modified May 15, 2006
