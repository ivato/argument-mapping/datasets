Arguments Against CloningQUESTION: What are some arguments against cloning?
ANSWER:
There are plenty of arguments against cloning - especially against cloning humans - ranging from scientific issues, to the practicality of cloning, to religious objections.
On the scientific side, we see that a large percentage of cloning efforts end in failure. For example, it took hundreds of attempts to clone Dolly the sheep. Often clones don't live as long as sexually reproduced animals, possibly because the genes taken from adults are more likely to have undergone mutations.
Another scientific argument is that genetic diversity helps provide the "pool" of variations available for a robust human population. It is commonly known that inbreeding animals constantly can result in reduced variations and an increased risk of genetic defects. An example is hip displasia in purebred dogs. Mutts tend to be more adaptable and healthier.
On the practical side, it is far easier to reproduce naturally than to create individuals through cloning. And it is vastly more practical to have a child within the context of marriage and a family than in a laboratory.
On the religious side, there is the fact that every human life is precious in God's eyes and, whether cloned or created naturally, deserves to live as a valuable member of God's kingdom. Just as each member of a pair of identical twins has a soul, so would a clone.
Those who favor cloning humans argue that we could produce more geniuses and thereby improve life for everyone through their contributions. Some argue that we could produce clones to provide "spare parts" for people who need transplants.
There is no proof that someone who is genetically identical to Albert Einstein, for example, would be a genius or would use his genius for socially acceptable ends. Upbringing and environment could turn an Einstein clone into another Adolf Hitler.
As for spare parts, it would be no more ethical to use a clone for that purpose than it would be to use your next door neighbor.
There are two passages in the Bible that bear directly on the issue of men playing God. The first is in Genesis, when Adam and Eve were thrown out of the Garden of Eden for taking fruit from the tree of knowledge and presuming to obtain God's wisdom.
Also, in Genesis 11 is the story of the Tower of Babel. Humans, with the help of a common language, were on the verge of building a tower all the way to Heaven. But God did not approve:
"The LORD said, 'If as one people speaking the same language they have begun to do this, then nothing they plan to do will be impossible for them. Come, let us go down and confuse their language so they will not understand each other.'"
Taken together, the arguments against cloning seem to outweigh any good that could be obtained from the process.
WHAT DO YOU THINK? - We have all sinned and deserve God's judgment. God, the Father, sent His only Son to satisfy that judgment for those who believe in Him. Jesus, the creator and eternal Son of God, who lived a sinless life, loves us so much that He died for our sins, taking the punishment that we deserve, was buried, and rose from the dead according to the Bible. If you truly believe and trust this in your heart, receiving Jesus alone as your Savior, declaring, "Jesus is Lord," you will be saved from judgment and spend eternity with God in heaven.
What is your response?
Yes, today I am deciding to follow Jesus
Yes, I am already a follower of Jesus
I still have questions
What is your response?
Yes, today I am deciding to follow Jesus
Yes, I am already a follower of Jesus
I still have questions