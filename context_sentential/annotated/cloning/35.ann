T1	ArgumentFor 0 132	When there is a lot to do and very little time to do it, you would reach a point where you wish there were two or three more of you.
T2	Sentence 133 192	Nothing like having plenty of hands to help you out, right?
T3	Sentence 193 284	If only human cloning is as accessible as your favorite drink in a local convenience store.
T4	ArgumentAgainst 285 420	Unfortunately, this is not the case and it may even be unethical, which brings us to the big question: is it ok to clone a human being?
T5	Sentence 421 513	Human cloning is a process where an exact copy of a person is created, genetically, that is.
T6	Sentence 514 684	While some would think it is similar to delivery of identical twins, cloning only reproduces human cells and tissue, and making an entire cloned human has yet to be done.
T7	Sentence 685 762	There are two types of theoretical human cloning that are commonly discussed.
T8	ArgumentFor 763 858	Therapeutic cloning is where cells from a human are cloned for transplant or medicine purposes.
T9	Sentence 859 948	Reproductive cloning, on the other hand, is what you would normally see in sci-fi movies.
T10	ArgumentAgainst 949 1041	If the films were to be believed, a person’s clone could turn out to be a person’s dark side
T11	Sentence 1043 1106	It was in January 2001, when word about human cloning surfaced.
T12	Sentence 1107 1257	The plan to clone a human was led by Panayiotis Zavos, a former professor in the University of Kentucky, and Severino Antinori, an Italian researcher.
T13	Sentence 1258 1357	Many attempts were made since then, but there are no documented and confirmed human cloning to date
T14	Sentence 1359 1423	The process of human cloning is as sci-fi as the idea behind it.
T15	Sentence 1424 1705	It starts with an egg from a human donor, where it goes through several stages — nucleus is removed, cell from a person to be cloned is removed, enucleated egg and cell is fused with electricity, embryo is implanted into surrogate mother, and surrogate mother delivers cloned baby.
T16	ArgumentFor 1706 1793	If you think about it, human cloning is similar to IVF, except for the first few stages
T17	Sentence 1795 1911	Proponents of human cloning see the value that it will bring, but opponents think some people just want to play god.
T18	Sentence 1912 2054	Clearly, there is something to be discussed about this topic, but it is almost impossible to try to determine if human cloning is good or bad.
T19	Sentence 2055 2094	The pros and cons have to be weighed in
T20	Sentence 2096 2125;2126 2128	List of Pros of Human Cloning 1.
T21	Sentence 2129 2154	Opens doors of innovation
T22	ArgumentFor 2156 2334	Since the idea of human cloning came out, cloning techniques have been continuously developed, taking basic understanding of developmental human biology to many different levels.
T23	ArgumentFor 2335 2549	Somatic cell nuclear transfer, for instance, allows production of cells that can be used in stem cell therapy or regenerative medicine, where organs for transplant can be created rather than harvested from a donor.
T24	Sentence 2550 2635	If cloning is perfected, there’s little doubt that it will bring about a huge change.
T25	ArgumentFor 2636 2732	It will not only revive animal and plant population, but also offer potential benefits to humans
T26	ArgumentFor 2734 2783	2. Has the potential to eliminate defective genes
T27	ArgumentFor 2785 2910	Cloning enables reproduction of healthy human cells that will prove beneficial when genetic illnesses come out in full force.
T28	ArgumentAgainst 2911 2991	Such health problems may not be present now, but it could be in the near future.
T29	Sentence 2992 3055	Staying one or two steps ahead, definitely gives humans an edge
T30	ArgumentFor 3057 3242	To date, cloning techniques provide great insights into the development of human embryos, showing scientist answers as to why diseases and defects happen during the developmental stage.
T31	ArgumentFor 3243 3380	Along with genetic manipulation, there is a huge potential that scientist will find ways to keep such diseases and defects from happening
T32	Sentence 3382 3430	3. Gives genetic modification (GM) a new meaning
T33	Sentence 3432 3638	Genetic engineering or GM uses biotechnology to directly manipulate the genome of an organism, enabling a scientist to change the genetic makeup of a cell, removing the bad ones and retaining the good ones.
T34	ArgumentFor 3639 3794	This technology has been used in growing genetically modified crops that are said to be bigger and way better than conventionally grown crops in many ways.
T35	Sentence 3795 3840	Imagine if GM is combined with human cloning.
T36	ArgumentAgainst 3841 3996	This would be like a scene from an old movie entitled Gattaca, where parents can choose specific traits, such as eye color and sex, for their future child.
T37	ArgumentFor 3997 4146	Any defects and diseases that are detected earlier will also be removed, ensuring that the child will only inherit the best traits and the best genes
T38	ArgumentFor 4148 4201	4. Potentially hastens recovery from traumatic injury
T39	ArgumentFor 4203 4334	A devastating automobile accident or other forms of injury can render a person unable to walk or would take a long time to recover.
T40	ArgumentFor 4335 4473	If his healthy cells will be cloned, however, there is a huge possibility that recovery times will be cut down and true healing can occur.
T41	ArgumentFor 4474 4597	If reproductive cloning is perfected, a quadriplegic can also choose to have himself cloned, so someone can take his place.
T42	ArgumentFor 4598 4703	This may seem like an unethical and weird concept, but it has merits, especially for the patient involved
T43	ArgumentFor 4705 4749	5. Allows infertile couples to have children
T44	ArgumentAgainst 4751 4872	Infertility problems are no longer as bad as it was in the past, what with all the treatments and technologies available.
T45	Sentence 4873 4970	Some treatments, however, are emotionally and physically draining, not to mention very expensive.
T46	Sentence 4971 5051	So imagine if a younger copy of the mother or father is created through cloning?
T47	ArgumentFor 5052 5206	It may not be cheap, but an infertile couple can experience the joys of having a family without going through the inconvenience of infertility procedures.
T48	ArgumentFor 5207 5289	Reproductive cloning has the potential to replace sperm donation, IVF or surrogacy
T49	ArgumentFor 5291 5329	6. Diseases and disorders can be cured
T50	ArgumentFor 5331 5494	The fact that stem cell therapy was made possible because of cloning techniques speaks volumes of the potential of the technology to cure a wide range of diseases.
T51	ArgumentFor 5495 5697	Bone marrow transplantation may be the only form of stem cell therapy used to date, but it won’t be long before this type of treatment can be used to cure diabetes, spinal cord injury, or heart disease.
T52	ArgumentFor 5698 5820	Continuous research into regenerative medicine also has the potential to extend the number of disorders that can be cured.
T53	ArgumentFor 5821 5985	A person with liver disease, for example, could have a new liver grown using the same genetic material, which will then be transplanted to replace the damaged liver
T54	Sentence 5987 6073	Remember the study on developmental defects and diseases provided by cloning research?
T55	ArgumentFor 6074 6146	This also has the potential to alter the way health problems are treated
T56	Sentence 6148 6177;6178 6180	List of Cons of Human Cloning 1.
T57	Sentence 6181 6205	Ethics and human cloning
T58	Sentence 6207 6302	Ethical positions regarding cloning in general is causing problems in the research development.
T59	ArgumentFor 6303 6428	In the United States, strategic roadblocks have been established to keep human cloning shelved for as long as it is possible.
T60	Sentence 6429 6516	The government doesn’t fund research, and the FDA regulates research on public cloning.
T61	Sentence 6517 6577	Anyone wants to clone a human must first acquire permission.
T62	Sentence 6578 6709	Countries with strict standards have legally banned research on reproductive human cloning, but give leeway to therapeutic cloning.
T63	Sentence 6710 6763	This effectively deters the pursuit for human cloning
T64	ArgumentAgainst 6765 6915	Opponents also point out that cloning promotes euthanasia, since clones of defective animals have to be put down, an act that is morally unacceptable.
T65	ArgumentAgainst 6916 7024	Because clone cells may show genetic abnormalities, using them on humans will also have a deadly implication
T66	Sentence 7026 7049	2. Has certain failures
T67	ArgumentAgainst 7051 7211	According to some scientists, the cloning technology used today is not yet ready to be tested on humans, which makes reproductive cloning a very risky endeavor.
T68	Sentence 7212 7261	In fact, nearly 98% of efforts in cloning failed.
T69	Sentence 7262 7347	Embryos are either not suitable for implantation, while some die shortly after birth.
T70	ArgumentAgainst 7348 7428	Those that survive will have genetic abnormalities, one of which is rapid aging.
T71	ArgumentAgainst 7429 7621	This is because cloning usually use older cells, increasing the possibility that the current age of the cell will be imprinted on the growing embryo which can lead to premature aging and death
T72	ArgumentAgainst 7623 7714	One of the methods used in cloning, induced pluripotent stem cells, has severe limitations.
T73	Sentence 7715 7762	The technique is used to reprogram adult cells.
T74	ArgumentAgainst 7763 7876	However, if a virus is used as a reprogramming factor, it may activate oncogenes, which are cancer-causing genes.
T75	Sentence 7877 8004	Once activated, the cancer cells will divide rapidly and will not respond to the natural cell signaling process of a human body
T76	ArgumentAgainst 8006 8136	It is also possible that some clones will be born with defective hearts, malfunctioning immune system and a host of other diseases
T77	ArgumentAgainst 8138 8173	3. Reduces a sense of individuality
T78	ArgumentAgainst 8175 8300	Having a clone may be beneficial at one point, but if there is more of you, potential loss of individuality is highly likely.
T79	Sentence 8301 8365	After all, you not only have a twin but an exact replica of you.
T80	Sentence 8366 8469	Sure, your clone will have unique preferences, but there is no denying that it came from your own cells
T81	ArgumentAgainst 8471 8515	4. Potentially turns humans into a commodity
T82	Sentence 8517 8692	What are the odds that a parent who doesn’t like the child they have will just go ahead and have another one cloned — one that has better traits, less sickly and more perfect?
T83	ArgumentAgainst 8693 8837	If disposal of defective clones will be as easy as creating human copies, having clones made will be similar to buying stuff at the supermarket.
T84	Sentence 8838 8905	Say clones with genetic problems are allowed to survive and thrive.
T85	ArgumentAgainst 8906 9003	It is possible that societal division will be created and it will be severe racism all over again
T86	Sentence 9005 9030	To clone or not to clone?
T87	Sentence 9031 9052	That is the question.
T88	Sentence 9053 9204	Even if technology still doesn’t allow reproductive cloning, it is best to understand its pros and cons, so you can decide better which side to choose.
