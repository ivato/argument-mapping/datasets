T1	Sentence 0 131	When it comes to creating quite a stir and causing controversy, a few topics are as anger-inducing and polarizing as human cloning.
T2	Sentence 132 395	At a certain point in time, it was considered as a profound achievement in biology, but this is no longer the case these days, as the number of its opponents are increasing, making such technology quickly becoming a heated topic in debates from all over the world
T3	ArgumentAgainst 397 613	The technology of cloning humans is already here, as evidenced by Dolly the sheep, but it called forth questions about the role of God in society, the soul and even the quality of life a cloned individual would have.
T4	Sentence 614 673	In return, however, its advantages are also quite apparent.
T5	Sentence 674 741	So, would it help society at large or is it ethically questionable?
T6	Sentence 742 851	Let us take a look at the pros and cons of cloning humans to come up with a well-informed decision on our end
T7	Sentence 853 883;884 886	List of Pros of Cloning Humans 1.
T8	ArgumentFor 887 921	It could eliminate defective genes
T9	Sentence 923 1060	Though genetic illnesses are not a leading killer of people as of yet, there is a huge possibility that it will become one in the future.
T10	ArgumentFor 1061 1244	As humans reproduce continually, there is also an increase in damage to their DNA lines, creating defective and mutated genes, which could be eliminated by cloning healthy human cells
T11	ArgumentFor 1246 1317	2. It is considered as the logical next step in reproductive technology
T12	ArgumentFor 1319 1451	Considering that identical twins are natural clones, we can regard reproductive cloning as the technological version of the process.
T13	ArgumentFor 1452 1563	When it comes to infertile couples, should not they be granted the opportunity to produce clones of themselves?
T14	ArgumentFor 1564 1709	For couples who have unfortunately lost their children, should not they be given the chance to replace their loved ones with clones, if possible?
T15	ArgumentFor 1710 1890	Moreover, those who have made significant contributions to science, the arts, music and literature would be ideally cloned to produce more of them to contribute more of their works
T16	ArgumentFor 1892 1958	3. It is an innovation that can change the world in a positive way
T17	ArgumentFor 1960 2084	Cloning humans is an innovative method that is an extremely powerful tool to bring about a huge change for the entire world.
T18	ArgumentFor 2085 2235	Not only that plants and animals can be revived with cloning, but also humans can be cloned to offer potential benefits to other people who are living
T19	ArgumentFor 2237 2290	4. It aids in faster recovery from traumatic injuries
T20	Sentence 2292 2506	For people who became quadriplegic due to horrific traffic accidents and professional athletes who tore their ACLs, recovery time could be long or it is even impossible for them to get back to their original state.
T21	ArgumentFor 2507 2592	However, cloning their own cells can lower recovery time and true healing could occur
T22	Sentence 2594 2643	5. It gives a new meaning to genetic modification
T23	Sentence 2645 2762	By cloning humans, it will be possible for potential parents to actually choose particular traits for their children.
T24	Sentence 2763 2862	This means that sex, eye color and other characteristics can be selected and not left up to chance.
T25	ArgumentFor 2863 2950	For many people, this is a huge advantage and a great way to use science in a new light
T26	ArgumentFor 2952 2985	6. It could eliminate infertility
T27	ArgumentFor 2987 3184	Though treatments for infertility these days are somehow successful, cloning can reached even greater heights, like being able to take cloned cells to create a younger twin of a father or a mother.
T28	ArgumentFor 3185 3346	This means infertile couples can experience happiness of having their own families without enduring painful procedures to treat infertility that are common today
T29	ArgumentFor 3348 3377	7. It can cure some disorders
T30	ArgumentFor 3379 3501	The technology can potentially help cure certain disorders, by replacing damaged tissues and organs within the human body.
T31	ArgumentFor 3502 3604	The process of transplanting human organs can become simpler, with an immensely improved success rate.
T32	ArgumentFor 3605 3800	Though the possible medical benefits of cloning humans are still not fully known, it is argued that such a technology can completely transform the ways by which many disorders are being performed
T33	Sentence 3802 3832;3833 3835	List of Cons of Cloning Humans 1.
T34	ArgumentAgainst 3836 3876	It risks the possibility of faster aging
T35	ArgumentAgainst 3878 4101	As older cells are often used to create human clones, it is possible that their imprinted age could be adopted by the growing embryos, which can then create some premature aging issues and, potentially, even premature death
T36	ArgumentAgainst 4103 4131	2. It interferes with nature
T37	ArgumentAgainst 4133 4347	Many opponents of the technology feel that its process is artificial and interferes with nature, even believing that the natural process of procreation is not something that should be corrected or altered in a way.
T38	ArgumentAgainst 4348 4534	They said that this form of interference is decidedly wrong and can lead to a domino effect, which means that other attributes of life could be changed or altered negatively as a result.
T39	ArgumentAgainst 4535 4654	If genes are modified to create beings who are smarter than others, the average person will not have a place in society
T40	ArgumentAgainst 4656 4710	3. It can bring forth a reduced sense of individuality
T41	ArgumentAgainst 4712 4934	Though human clones would be a brand new set of life with unique preferences, a loss of individuality is still potential, as clones would be simply twins of someone else, regardless of the age of that other person might be
T42	Sentence 4936 4973	4. It can cause a divide among people
T43	ArgumentAgainst 4975 5129	One big drawback to cloning humans is its ability to divide people, where clones who are not treated as human beings can lead to social unrest and divide.
T44	ArgumentAgainst 5130 5337	And since there is already a great deal of gap and difference in the world today, from race to social status, many people feel that human cloning can result to a new difference that will only cause more harm
T45	ArgumentAgainst 5339 5391	5. It might decrease the overall value of human life
T46	ArgumentAgainst 5393 5518	With human cloning on the rise, it is greatly possible that human beings would become more of a commodity than an individual.
T47	ArgumentAgainst 5519 5670	When parents do not like the children they have got, then they would just go clone another one in the hopes of getting it perfect the next time around.
T48	ArgumentAgainst 5671 5806	Also, it could create new societal division, where perfect clones would be treated in a different way than those who are naturally born
T49	ArgumentAgainst 5808 5843	6. It goes against religious ethics
T50	ArgumentAgainst 5845 6039	People who have strong religious beliefs generally oppose the process of cloning humans, as they feel that it would result in man becoming the creator, changing the authority of a supreme being.
T51	Sentence 6040 6220	Since religion often comes into play with regards to this controversial matter, there emerge very passionate opinions that are not necessarily based on facts, but rather emotional.
T52	Sentence 6221 6363	Whatever these opinions are based upon, religion is definitely a motivating factor that keeps many people from agreeing with such a technology
T53	ArgumentAgainst 6365 6401	7. It might be used for exploitation
T54	ArgumentAgainst 6403 6529	It is speculated that human cloning could eventually be used as a means to exploit people for other people’s vested interests.
T55	ArgumentAgainst 6530 6727	Though it might seem like a scenario straight from a sci-fi film that people might use clones for unlawful interests and crime, it is still possible if human cloning takes a common place in society
T56	Sentence 6729 6760	8. It has seen a lot of failure
T57	Sentence 6762 6864	Opponents of human cloning feel that is a mute debate, since the technology is primarily unsuccessful.
T58	ArgumentAgainst 6865 7011	In fact, over 90% of human cloning attempts have been labeled as “failure”, which means that the human DNA is only put at risk during the process.
T59	ArgumentAgainst 7012 7100	As you can see, human DNA can be contaminated, and the chances of success are very rare.
T60	ArgumentAgainst 7101 7279	Also, the implications of what happens when the process goes wrong are still not fully understood, which is not good and could lead to problems that we will not be able to handle
T61	Sentence 7281 7331	9. It is just a completely wrong idea for the many
T62	ArgumentAgainst 7333 7538	Many people feel strongly the cloning humans is completely wrong, with the state of the technology at the moment possibly involving a huge number of damaged pregnancies to achieve a single live baby clone.
T63	ArgumentAgainst 7539 7680	Also, evidence suggests that clones often have built-in genetic defects and are unhealthy, which can lead to premature ageing and even death.
T64	Sentence 7681 7811	They think that it would be completely wrong to bring children into the world knowing that they can be affected by these problems.
T65	ArgumentAgainst 7812 8007	With the dignity of human life and it genetic uniqueness at risk, people might be cloned unwillingly, leaving millions of cells around every day as they go about their normal lives shedding skin.
T66	Sentence 8008 8231	There is no clear information on who will have control over who gets cloned, and firms are already making money from storing tissues from dead partners and children until the time when human cloning becomes widely available
T67	Sentence 8247 8380	The science of cloning humans has continued to develop, and it is expected that it will not be long before it becomes a full reality.
T68	ArgumentAgainst 8381 8443	Do humans have a soul, which would be lost during the process?
T69	ArgumentAgainst 8444 8493	Is this a way for humanity to try to replace God?
T70	Sentence 8494 8612	Coming to an agreement on this hot subject is not easy, but understanding it deeply can lead to some level of clarity.
T71	Sentence 8613 8717	Taking a look at it from both sides and comparing its pros and cons can give us a different perspective.
T72	Sentence 8718 8793	While the debate about cloning humans is ongoing, how do you feel about it?
