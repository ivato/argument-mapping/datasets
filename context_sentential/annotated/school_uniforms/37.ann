T1	Sentence 0 190	Does requiring students to wear uniforms directly affect school environment and student achievement, or is it the equivalent of painting the walls of a crumbling building -- merely cosmetic?
T2	Sentence 191 218	What does the research say?
T3	Sentence 219 263	What do students, teachers, and parents say?
T4	Sentence 264 450	Shopping for back-to-school clothes was just a little different this year for gap-toothed third grader Adi Sirkes, who needed new clothes after his school adopted a uniform dress policy.
T5	ArgumentAgainst 451 583	Next year, he'll go to a different school, one that mandates different uniforms -- and that will mean yet another whole new wardrobe
T6	Sentence 585 692	"My son's an unusual size," his mother Irit told Education World, "so it's hard to find him clothes anyway.
T7	ArgumentAgainst 693 800	Limiting what I buy to certain colors makes shopping for him not only more expensive but that much harder."
T8	ArgumentAgainst 801 916	"My fifth-grade daughter used to like school," added Connie Terry, "but last year, her school switched to uniforms.
T9	ArgumentAgainst 917 1012	Now when I ask her how school is going, the first thing out of her mouth is she hates uniforms.
T10	ArgumentAgainst 1013 1079	Even during the summer time, she'd say, 'I hate to wear uniforms.'
T11	ArgumentAgainst 1080 1139	My daughter likes to be individual, to wear what she wants.
T12	ArgumentAgainst 1140 1203	She doesn't want to have to wear what everyone else is wearing.
T13	ArgumentAgainst 1204 1283	It doesn't make her feel good about herself; it doesn't make her feel special."
T14	Sentence 1284 1394	Despite complaints like these, public schools throughout the United States are adopting uniform dress policies
T15	Sentence 1396 1491	In 1994, the Long Beach, California, school system began requiring that students wear uniforms.
T16	ArgumentFor 1492 1625	The system recorded a drop in suspensions, assaults, thefts, vandalism, and weapon and drug violations and an increase in attendance.
T17	Sentence 1626 1746	Ten states -- plus scores of individual communities -- followed suit and adopted some type of school uniform regulation.
T18	Sentence 1747 1911	Included in those ranks were schools in Baltimore, Boston, Chicago, Cleveland, Detroit, Los Angeles, Miami, Nashville, New Orleans, Phoenix, Seattle, and St. Louis.
T19	Sentence 1912 1992	This school year, 550,000 New York City elementary students are wearing uniforms
T20	Sentence 1994 2105	Although most evidence is anecdotal, the Long Beach schools weren't the only schools to note improved behavior.
T21	ArgumentFor 2106 2192	Chicago school officials found a drop in gang violence after adopting school uniforms.
T22	ArgumentFor 2193 2315	Birmingham schools reported a drop in weapon and drug incidents, and Houston schools reported a decrease in violent crime.
T23	ArgumentAgainst 2316 2459	Miami-Dade County schools, however, found that fights nearly doubled at their middle schools after the school district adopted a uniform policy
T24	Sentence 2461 2614	"Many schools here draw from varied socioeconomic levels," Bev Heller, a teacher at Fienberg-Fisher Elementary in Miami-Dade County told Education World.
T25	ArgumentFor 2615 2777	"Wealthier students may own every uniform accessory and wear designer bracelets or shoes that light up; others -- if they do own uniforms -- have very basic ones.
T26	ArgumentAgainst 2778 2864	Adopting uniforms certainly did not blur the socioeconomic lines in our student body."
T27	Sentence 2865 2944	"Our school has had a mandatory uniform policy for three years," she continued.
T28	Sentence 2945 3038	"There is a big sign in our school, 'Uniforms Mandatory,' but not all the students wear them.
T29	ArgumentAgainst 3039 3153	Our student body is transient, and purchasing different uniforms every time a student moves can be very expensive.
T30	ArgumentAgainst 3154 3245	Requiring school uniforms could be a hardship, especially on students who frequently move."
T31	Sentence 3246 3275;3276 3395	HOW TO ADOPT A UNIFORM POLICY Because of results like those in Long Beach, Chicago, and Birmingham, many schools are adopting uniform dress policies.
T32	Sentence 3396 3461	Experts offer advice to those schools on how best to initiate it.
T33	Sentence 3462 3549;3550 3715	Among the tips included in the U.S. Department of Education's manual are the following: "Before initiating a uniform policy, administrators need to investigate options and select the ones that best meet the individual school's needs," states the manual.
T34	Sentence 3716 3864	"As the courts have yet to decide if a public school district can make students wear uniforms, some sort of opt-out policy is definitely desirable."
T35	Sentence 3879 4046;4047 4127	Proponents of school uniforms believe that in addition to reducing assaults, thefts, vandalism, and weapon and drug use in schools, requiring students to wear uniforms CAN UNIFORMS REALLY BRING ABOUT THE KINDS OF IMPROVEMENTS THAT PROPONENTS CLAIM?
T36	ArgumentAgainst 4128 4312	Sociologists David Brunsma and Kerry Rockquemore discovered that requiring students to wear uniforms had no direct effect on substance abuse, behavioral problems, or school attendance.
T37	Sentence 4313 4474	They used data on approximately 5,000 U.S. sophomores who were part of a 1988 National Educational Longitudinal Study at the University of Alabama at Huntsville.
T38	Sentence 4475 4674	The results of that study are documented in The Effects of Student Uniforms on Attendance, Behavior Problems, Substance Use, and Academic Achievement, published in The Journal of Educational Research
T39	ArgumentAgainst 4699 4888	World that the tenth-grade students who were required to wear uniforms actually scored slightly lower on standardized achievement tests than did a comparable group not required to wear them
T40	ArgumentAgainst 4890 5091	"They think uniforms will solve every problem, but don't they understand being forced to wear uniforms could make rebellious teens even more rebellious?" 13-year-old Emily Granen asked Education World.
T41	ArgumentAgainst 5092 5172	Rebellious teens forced to wear uniforms might be even less inclined to do well.
T42	ArgumentAgainst 5173 5324	Some people think that making kids wear uniforms will reform schools is the equivalent of painting the walls of a crumbling building -- merely cosmetic
T43	Sentence 5326 5591	Long Beach Unified School District public information director Dick Van Der Laan, speaking about the successes achieved after initiating a uniform policy, says in the Manual on School Uniforms, "We can't attribute the improvement exclusively to school uniforms...."
T44	Sentence 5592 5767	What has been found to decrease vandalism, school crime in general, and fights is using a combination of initiatives, one of which could be requiring students to wear uniforms
T45	ArgumentFor 5769 5968	"Uniform policies may indirectly affect school environment and student outcomes by providing a visible and public symbol of commitment to school improvement and reform," Brunsma told Education World.
T46	Sentence 5969 6081	"They are not the sole factor responsible for the numerous behavioral and academic outcomes attributed to them."
T47	Sentence 6082 6507	Schools that include, among other initiatives, see-through plastic or mesh book bags, metal detectors, aggressive truancy-reduction initiatives, drug-prevention efforts, student and/or athlete drug testing, community efforts to limit gangs, a zero-tolerance policy for weapons, character education classes, and conflict resolution proposals -- plus the uniform initiative -- frequently do improve school discipline and safety
T48	ArgumentFor 6509 6653	Although not a panacea for rectifying educational issues, students who wear uniforms may engender positive changes in themselves and in schools.
T49	Sentence 6654 6847	Teacher Kathleen Modenback of Northshore High School, a school in Louisiana that adopted a uniform policy this year, told Education World, "I've never been concerned with what my students wear.
T50	ArgumentAgainst 6848 6978	Supervising uniforms and dress codes only lengthens the long list of parental jobs that educators have taken over in recent years.
T51	Sentence 6979 7146	Uniforms, which are economical and easy for parents, are sometimes looked on as a solution to the atmosphere of impending danger that has settled on schools nationwide
T52	ArgumentFor 7148 7271	"However, after seeing our students in uniforms for the last two weeks, I see an almost magical change in the student body.
T53	ArgumentFor 7272 7390	My seniors talk of the ease with which they dress in the morning, and all the kids seem calmer and more mild-mannered.
T54	ArgumentFor 7391 7495	Almost all the students were wearing the uniforms although the deadline for wearing them was weeks away.
T55	Sentence 7496 7528	Maybe there's something to them.
T56	ArgumentAgainst 7529 7631	Perhaps they draw us all into a sense of false security and well-being that only conformity can give."
T57	Sentence 7632 7665	ON-LINE SCHOOL UNIFORMS RESOURCES
T58	Sentence 7666 7681;7682 7757	School Uniforms Education World's "School Uniforms" archive includes many links of interest
T59	Sentence 7759 7785	Custom-Made Fit for School
T60	Sentence 7786 8033	The newest school fashion trend is not ripped from the pages of the latest magazine -- it comes from the principal's handbook as a growing number of school districts are adopting more stringent dress codes and implementing school uniform policies.
T61	Sentence 8034 8039;8040 8106	(CNN) Study Says School Uniforms Might Help Attendance, Graduation Rates
T62	Sentence 8107 8257	A study of six big-city Ohio public schools showed students who were required to wear uniforms had improved graduation, behavior and attendance rates.
T63	ArgumentAgainst 8258 8292	Academic performance was unchanged
T64	Sentence 8311 8451	David L. Brunsma, a researcher at the University of Missouri-Columbia, has been studying the movement for public school uniforms since 1996.
T65	Sentence 8452 8604	That was the year that President Clinton propelled the movement into the national consciousness by endorsing the idea in his State of the Union Address.
T66	Sentence 8605 8732	In a book published in November, Brunsma seeks to set the record straight on what uniforms can and cannot do for public schools
T67	Sentence 8734 8763;8764 8807	Facts Against School Uniforms School Uniforms are a hot topic in America.
T68	Sentence 8808 8841	Following President Clintons 1996
T69	Sentence 8842 9034	State of the Union address where he said "public schools should be able to require their students to wear uniforms", people on both sides of the argument have been putting their cases strongly
T70	Sentence 9036 9061	Manual on School Uniforms
T71	Sentence 9062 9297	This manual, prepared by the U.S. Department of Education in consultation with local communities and the U.S. Department of Justice, assists parents, teachers, and school leaders in determining whether to adopt a school uniform policy.
T72	Sentence 9298 9475	If information beyond that provided in the manual is desired, interested parties can contact the U.S. Department of Education Safe and Drug Free Schools office at 1-800-624-0100
T73	Sentence 9477 9582	The Effects of Student Uniforms on Attendance, Behavior Problems, Substance Use, and Academic Achievement
T74	Sentence 9583 9818	In this study, published in The Journal of Educational Research (February 13, 1998), the researchers concluded that requiring students to wear uniforms has no direct effect on substance abuse, behavioral problems, or school attendance.
T75	ArgumentAgainst 9819 10015	They found, in fact, that the students they studied who were required to wear uniforms actually scored lower on standardized achievement tests than did a comparable group not required to wear them
T76	Sentence 10017 10051	Scientific School Uniform Research
T77	Sentence 10052 10214	This article, linked to the Polk County (Florida) School Uniforms home page, includes a section put together by parents against the district's uniform initiative.
T78	Sentence 10215 10316	That section is the abstract of the above study (for readers who prefer to read the abbreviated form)
T79	Sentence 10334 10483	This article, published in the Dallas Morning News (February 3, 1999), describes the pros and cons of initiating a school uniform policy in Arkansas.
T80	Sentence 10484 10531	The initiative was investigated but not adopted
T81	Sentence 10556 10640	This ERIC document describes the pros and cons of initiating a uniform dress policy.
T82	Sentence 10641 10694	It also includes some guidelines for implementing one
T83	Sentence 10696 10768	"School Uniforms Increasingly in Fashion," USA Today (October 15, 1998).
T84	Sentence 10769 10917	This article describes the pros and cons of uniform policies, including anecdotes from many school systems throughout the U.S. that have adopted one
T85	Sentence 10919 10999	"In State, New School Cheer: One Style Fits All," Boston Globe (March 12, 1999).
T86	Sentence 11000 11097	The article describes the pros and cons of the school uniform initiative adopted in Massachusetts
T87	Sentence 11099 11166	"Skeptics Downplay Uniform," The Wichita Eagle (November 22, 1998).
T88	Sentence 11167 11325	This article describes the results of researchers David L. Brunsma and Kerry A. Rockquemore's study on the effects of school uniforms on academic achievement.
T89	Sentence 11326 11412	The complete study can be accessed at http://www.members.tripod.com/rockqu/uniform.htm
T90	Sentence 11414 11494	"District's Dress Code Challenged," The Dallas Morning News (October, 12, 1998).
T91	Sentence 11495 11664	This article describes some problems faced by the San Antonio school district when parents opposed to requiring the wearing of school uniforms took the district to court
T92	Sentence 11666 11737	"Parents Vow to Fight Mandatory Uniforms," Miami Herald July 20, 1999).
T93	Sentence 11738 11865	Parents unhappy about Polk County's (Florida) dress code planned a boycott and established a Web site to publicize their stance
T94	Sentence 11867 11924	"Should Public Schools Require Students to Wear Uniforms?
T95	Sentence 11925 11974	Pros and Cons," Hartford Courant (July 11, 1998).
T96	Sentence 11975 12053	This article describes the pros and cons of initiating a school uniform policy
T97	Sentence 12055 12078;12079 12096;12097 12130;12131 12161;12162 12170	Article by Glori Chaika Education WorldÂ® Copyright Â© 2008 Education World Report Abusive or Spam Comment Comments
