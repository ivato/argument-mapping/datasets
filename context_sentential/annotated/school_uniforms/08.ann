T1	Sentence 0 49;50 89	Know Your Rights: School Dress Codes and Uniforms Can my school make a dress code policy?
T2	Sentence 96 231	But your school does not have the right to use the policy to disfavor a specific message or single out one particular group of students
T3	Sentence 233 304	Can my school enforce its dress code policy against only some students?
T4	Sentence 318 474	For example, a dress code that prohibits “gang-related” apparel but is only enforced against Black students would be race discrimination and against the law
T5	Sentence 476 544	Can my school have different dress code policies for boys and girls?
T6	Sentence 550 620	Many schools do have different dress code policies for boys and girls.
T7	Sentence 621 673	But some differences in dress codes may not be okay.
T8	Sentence 674 797	Such policies could include ones that prohibit girls from wearing yoga pants and leg warmers or boys from having long hair.
T9	Sentence 798 980	If you can’t be yourself at school because of a gendered school dress code policy—for example, if you are a boy and you need to have long hair for religious reasons—please contact us
T10	Sentence 982 1040	Do I have the right to be gender non-conforming at school?
T11	Sentence 1046 1158	Schools cannot discriminate against you based on your sexual orientation, gender identity, or gender expression.
T12	Sentence 1161 1295	Even if your school has a gendered dress code policy, you should still be able to wear the clothing and hairstyle allowed any student.
T13	Sentence 1296 1493	For example, if your school allows boys to wear tuxedos to prom, then it must also allow a girl to wear a tuxedo as an expression of your gender or gender identity or your political or social views
T14	Sentence 1495 1583	Do I have the right to wear clothing that communicates a political or religious message?
T15	ArgumentAgainst 1589 1772	For example, you have the right to wear a t-shirt protesting U.S. involvement in a war, endorsing or criticizing a particular politician, or in support or opposition of a social issue
T16	Sentence 1774 1840	Do I have the right to wear clothing in observance of my religion?
T17	Sentence 1846 1993	For example, you have the right to wear a headscarf or hijab if you are Muslim or a feather that holds cultural and spiritual meaning to your tribe
T18	Sentence 1995 2093	Can my school stop me from wearing something because it does not approve of the message or slogan?
T19	Sentence 2094 2116	As a general rule, NO.
T20	ArgumentAgainst 2117 2211	Your school cannot stop you simply because it does not like the message your clothing conveys.
T21	Sentence 2212 2400	But your school can prohibit you from wearing clothing with “indecent, obscene, or lewd” messages or clothing that causes a “substantial disruption” in school or school-related activities.
T22	ArgumentFor 2401 2453	It can also prohibit clothing that promotes drug use
T23	Sentence 2455 2508	What counts as “indecent, obscene and lewd” messages?
T24	Sentence 2509 2634	“Indecent, obscene, or lewd” messages include ones that are sexually explicit, have nudity, or use profane and offense words.
T25	Sentence 2635 2737	But if these kinds of messages have political value or content, you may have more freedom to wear them
T26	Sentence 2739 2812	What counts as a “substantial disruption” to school or school activities?
T27	ArgumentFor 2813 2994	A “substantial disruption” occurs when school administrators or teachers are unable to proceed with regular school activities due to the interference caused by a student’s clothing.
T28	Sentence 2995 3087	Rumors, gossip, or excitement amongst students does not count as a “substantial disruption.”
T29	Sentence 3088 3135	Can my school prohibit “gang-related” clothing?
T30	Sentence 3144 3210	But your school must define what counts as “gang-related” clothing
T31	Sentence 3212 3289	Are there rules my school must follow before it enforces a dress code policy?
T32	Sentence 3295 3387	Your school or district must get approval for its dress code policy from the school board.iv
T33	Sentence 3388 3432	Can my school adopt a school uniform policy?
T34	Sentence 3439 3525	But your school must notify you of the policy and wait six months before enforcing it.
T35	Sentence 3529 3658	Your parent or guardian has the right to opt you out for any reason and your school cannot decide whether the reason is justified
T36	Sentence 3660 3735	What if I cannot afford to buy the clothes necessary for my school uniform?
T37	Sentence 3736 3876	Your school district must offer resources to assist low-income students who may not be able to meet the requirements of a school uniform.vii
T38	Sentence 3877 3967	Your school cannot make purchasing a school uniform a requirement for getting an education
T39	Sentence 3969 3979;3980 4018;4019 4053;4054 4094;4095 4132;4133 4179;4180 4219;4220 4260;4261 4289	End notes: i California Education Code § 35183(b) ii California Education Code § 220 iii California Education Code § 35183(b) iv California Education Code 35183(b) v California Education Code §§ 35183(a)(5),(b) vi California Education Code § 35183(d) vii California Education Code § 35183(d) Source: MySchoolMyRights.com
