Talking about school uniforms
|Linguapress for mobiles - home page||English Grammar||Free advanced level resources||Free intermediate level resources|
RACHEL:
Well I think they're
a good idea, because you don't have to decide what to wear in the
morning.
You just get up and put your uniform on. You don't have to think "Oh
no,
what am I going to wear today?" So I think they're a good idea. There's
no competition either between the girls and
whatever!
ANITA: It distinguishes you from other schools! Like our uniform's totally different to the Glede* uniform, it's a lot stricter. It just makes you stand out. It makes you look like a community, rather than just normal people, if you know what I mean!
FREEWAY: Abnormal people?
ANITA: No I didn't mean it like that! Just like everyday people on the street, that aren't at school. You can tell that you're still at school, in education. I think it looks really smart as well, and then we get a lot of people sayin' how nice we look...
SARAH: Don't you think though that by wearing a certain uniform, the people who see you, they're going to identify you with the school, and have a stereotypical attitude about you. It's like people think about skinheads; they all dress the same if they want to belong to that group, and people immediately think they're all going to be hooligans, but it's not necessarily true.
ANITA: You don't necessarily lose your own individuality by wearing a uniform; there's still something special about yourself as a person. Just because you're wearing the same things, it doesn't make you all the same; it doesn't matter what anyone else thinks. Don't worry about things like that!
SARAH: There's another thing about uniform though; even if everybody wears exactly the same, they're all going to look different, because the same uniform isn't going to suit everybody. Therefore people who are a bit overweight or whatever, get ragged about it something rotten; I mean they do in this school, because our uniform is so unflattering for anybody who's a bit chubby!
EMMA: I think people should have the chance to be individual when they wear clothes... It's like we're being forced to conform to something we don't want to do. Now we're in the sixth form, we don't have to wear uniform; but I can remember in the first year, second year, even if you like just stepped out of line slightly, by wearing different coloured socks than what you were supposed to, they really came down on you. It was really bad. They seemed really piggy as well.
KATIE: I think that by putting us all in the same uniform, they're somehow trying to suppress our own identities; and they're saying like "oh we all have to look the same, we all have to think the same." That's how it seemed to me lower down the school anyway.
SARAH: I think, um... I came from primary school, and we didn't have to wear a uniform there; and coming to secondary school and wearing a uniform, I don't know, I really enjoyed it, I used to be proud of it, of wearing it. But I think, yeah, there are faults in it, but I think there are the advantages as well!
KATIE: I think that here sometimes, more emphasis was placed on the uniform, rather than our work; for instance, I got caught once by Mrs. Parker lower down the school, for wearing a too short skirt! And she's been after me ever since! So there you go! I prefer it now that we don't have to wear a uniform, but in actual fact, I didn't really mind wearing it lower down the school...
* The Glede: another school in the same town.
chubby: slightly fat - distinguishes you: shows you are different - emphasis: accent, importance - individuality: personality - overweight: too heavy - piggy: nasty, unpleasant - ragged: teased, made fun of - something rotten: in a bad way - unflattering: they do not make you look good - whatever: anything -
Find these words in the dialogue,
and explain what they mean in the context:
|a. stand out||b. normal people||c. stereotypical|
|d. hooligans||e. belong to||f. worry|
|g. to suit||h. to be individual||i. step out of line|
|j. slightly||k. they really come down on you||l. suppress|
|m. been after me||n. So there you go!|
Points of view:
Students should sum up the point of view of Rachel, Anita, Sarah, Emma and Katie.
Writing activity:
For and against: Imagine that the principal of your school has decided that from the start of next school year, uniform will be recommended for all pupils in your school: a) decide what uniform you would like to have (strict, flexible, style, etc.), and b) Write an article in English for your local newspaper, explaining why the decision has been made, and the different points of view that have been expressed on the subject of uniforms.
This
teaching
resource
is ©
copyright Linguapress 1991 - 2015.
Revised 2015 . Originally published in Freeway, the Intermediate level English newsmagazine.
Republication on other websites or in print is not authorised
|Linguapress; home||Découvrez l'Angleterre (en français)||Discover Britain|
|A
selection of other resources in graded English
|
from Linguapress
|Selected pages|
|Intermediate resources :|
|Mystery - the Titanic and the Temple of Doom|
|Are you a slave to fashion ?|
|Sport: The story of football and rugby|
|Living in the country|
|USA: Who was Buffalo Bill?|
|USA: Close encounters with a Twister|
|More: More intermediate reading texts|
|Advanced level reading :|
|Charles Babbage, the father of the computer|
|Who killed Martin Luther King?|
|USA - Discovering Route 66|
|London's Notting Hill Carnival|
|More: More advanced reading texts|
|Selected grammar pages|
|Online English grammar|
|Noun groups in English|
|Word order in English|
|Reported questions in English|
|Miscellaneous|
|Language and style|
|Word stress in English|
|The short story of English|