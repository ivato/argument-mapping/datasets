By Michelle Kouzmine
School uniforms--some love them and some hate them. There seems to be a big rift between school uniform supporters and those against school uniforms. So what's the deal? Let's look at some of the cons against school uniforms.
1. School Uniform Con 1: Limited Self Expression
The most common argument against school uniforms is that they limit personal expression. Kids and teens use they way they dress to express themselves and to identify with certain social groups. Many students who are against school uniforms argue that they lose their self identity when they lose their right to express themselves through fashion.
2. School Uniform Con 2: The Initial Cost
Though many argue that in the end, school uniforms are cheaper option to buying a school wardrobe, there is an argument that it is costly to buy several uniforms at once. Also, many schools have several uniforms such as everyday uniforms, formal uniforms for special occasions and yet another uniform for P.E. Many parents argue that buying all these uniforms for all their kids every fall is hard on the family budget.
3. School Uniform Con 3: Comfort Factor
Kids are very specific about what they are comfortable wearing. Some kids are sensitive to certain materials while others are opposed to buttons, zippers, and restrictive clothing. Some children are also uncomfortable wearing certain styles of clothing. Many girls, for example, do not like to wear skirts or dresses, which most girls' uniforms require. No uniform can suit all children.
4. School Uniform Con 4: Requires Some Organization
This one is personal; as a rather unorganized mom, I admit that sometimes it's hard to know exactly what uniform parts need to be washed, dried and ironed the night before. I have spent many a morning scrambling to find a tie or a blazer that was required for an event.
5. School Uniform Con 5: If It Isn't Broke...
"Why bother?" is possibly the most common argument against school uniforms. Many educators and parents just don't see the point in switching over to uniforms, especially in American public schools where uniforms are still the exception and not the rule.
- What's Hot in Girls' Fashion : The Best Styles Girls' Clothing Shoes and Accessories
- In Style Boys Clothes : Hot Looks in Boys Clothing Shoes and Accessories
- Kids Fashion Basics - Staples for a Child's Wardrobe
- Kid's Fashion on a Budget : Cheap and Chic Kids
- High Fashion for Kids : Designer Children's Clothing
- How to Dress Your Kid for Any Occasion
- Children's Outerwear : The Best Coats and Jackets and Hats and Gloves
- Great Shoes and Accessories for Kids
- Shop for Kids' Fashion : Where to Find Children's Clothes
- Guides, FAQ, Tips and Tricks for Parents