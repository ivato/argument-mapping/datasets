T1	Sentence 0 52;53 151	School Uniform Statistics: 23 Facts on Pros and Cons Does school uniforms really reduce bullying, improve academic performance and make schools better?
T2	Sentence 152 285	Read on for 23 thought-provoking statistics on school uniforms including how many students wear school uniforms and the cost per year
T3	Sentence 287 327;328 514	|© Jaap Joris (CC BY-SA 2.0) via Flickr| Educators are some of the staunchest advocates of uniforms, arguing that they reduce problematic behavior, cut down on the incidence of bullying and encourage more school spirit overall.
T4	Sentence 515 626	Countless research studies have attempted to pinpoint just what effect uniforms have, often with mixed results.
T5	Sentence 627 841	If you're curious about how successful uniform policies are or you're wondering how the cost adds up, the CreditDonkey team has put together a list of 23 revealing tidbits that will have your brain working overtime
T6	Sentence 843 1004	School uniforms became a hot-button issue in the mid-1990s after several high-profile incidents of violence involving teens and items of clothing made headlines.
T7	Sentence 1005 1181	Former President Bill Clinton led the charge, instructing the Department of Education to issue policy manuals to schools nationwide on how to safely institute a uniform policy.
T8	Sentence 1182 1363	While the movement has been met with criticism by some parents and students, the practice is becoming more acceptable as schools across the country continue to jump on the bandwagon
T9	Sentence 1365 1416	1. The popularity of school uniforms is on the rise
T10	Sentence 1417 1534	While uniforms have long been a staple of private schools, they're increasingly common in the public education realm.
T11	Sentence 1535 1669	The percentage of public schools implementing a uniform rule jumped from 13% to 19% between the 2003-04 and the 2011-12 academic years
T12	Sentence 1671 1723	2. A healthy number of schools have a uniform policy
T13	Sentence 1724 1812	When you run the numbers on how many schools require uniforms, it adds up to a tidy sum.
T14	Sentence 1813 1900	There are just under 27,000 public schools and nearly 14,000 private schools that do so
T15	Sentence 1902 1943;1944 2071	3. Not every state has warmed to the idea Uniforms are an issue that's decided on by individual school districts, and there are many states that have opted out entirely.
T16	Sentence 2072 2162	Currently, only 21 states and the District of Columbia have formal school uniform policies
T17	Sentence 2164 2215;2216 2314	4. Uniforms are more common at the elementary level Younger students are more likely to be affected by a uniform policy than their older counterparts.
T18	Sentence 2315 2424	For the 2011-12 school year, 20% of elementary schools required uniforms compared to just 12% of high schools
T19	Sentence 2426 2456;2457 2575	5. Income levels may influence School districts that serve economically disadvantaged students account for the highest percentage of uniform wearers.
T20	Sentence 2576 2708	Nearly 47% of schools where more than three-quarters of the student population receives free or reduced price lunch require uniforms
T21	Sentence 2710 2756;2757 2881	6. Rural schools are the least likely to do so In urban areas, nearly 40% of schools have a uniform rule in place but that's not the case with more out-of-the-way locales.
T22	Sentence 2882 2951	Just under 9% of schools found in rural areas make uniforms mandatory
T23	Sentence 2953 3002	7. The size of the school also makes a difference
T24	Sentence 3003 3162	In addition to geographic and economic factors, the size of a school's student body plays a part in determining whether enforcing a uniform policy is feasible.
T25	Sentence 3163 3297	Almost 22% of schools with less than 100 students call for uniforms, versus 9.9% of schools with a total enrollment greater than 1,500
T26	Sentence 3299 3351;3352 3447	8. Uniforms are more prevalent among private schools Altogether, 30% of public schools have instituted some type of uniform guidelines for students.
T27	Sentence 3448 3551	That seems like a lot until you consider that nearly 54% of private schools have similar rules in place
T28	Sentence 3553 3607	9. Smaller private schools require uniforms less often
T29	Sentence 3608 3734	While public schools are most likely to feature uniforms when there are fewer students, private schools are just the opposite.
T30	Sentence 3735 3838	Approximately 73% of private institutions require uniforms when there are 500 to 999 students enrolled.
T31	Sentence 3839 3882	Only 44% do so when enrollment is below 100
T32	Sentence 3884 3913	10. Uniforms don't come cheap
T33	Sentence 3914 4051	The back-to-school shopping rush is something most parents dread since it usually means shelling out hundreds of dollars for new clothes.
T34	ArgumentAgainst 4052 4175	When it comes to uniforms, it's not out of the question to spend anywhere from $140 to $160 to outfit your child for school
T35	Sentence 4177 4226;4227 4359	11. Educators argue that it's still a better deal School administrators see nothing but positives associated with the use of uniforms, starting with the financial benefit to parents.
T36	ArgumentFor 4360 4496	Eighty-six percent of elementary school principals believe that uniforms are more cost effective compared to purchasing regular clothing
T37	Sentence 4498 4543;4544 4644	12. Uniform suppliers are reaping the rewards Companies like Lands' End and French Toast are cashing in on the boom in demand for school uniforms.
T38	Sentence 4645 4707	The industry generates roughly $1 billion in revenue each year
T39	Sentence 4713 4752;4753 4868	The advantages go beyond the lower cost Aside from the reduced expense associated with uniforms, educators agree that they offer plenty of other positives.
T40	ArgumentFor 4869 5005	Around 85% argue that it reduces the need for discipline in the classroom while 79% say it promotes an increased sense of student safety
T41	Sentence 5007 5046	14. Kids aren't always sold on uniforms
T42	Sentence 5047 5166	Clothing is one way for kids to express themselves and the thought of wearing a uniform is sometimes less than welcome.
T43	Sentence 5167 5278;5279 5282	A survey of Nevada students revealed that 90% of youth said they didn't like having to wear a uniform at school 15.
T44	Sentence 5283 5325	But they do have some positive perceptions
T45	Sentence 5326 5452	Despite the fact that they aren't exactly thrilled about wearing uniforms, some students do recognize the benefits they offer.
T46	ArgumentFor 5453 5650	In the same survey, 54% of students said that having to wear a uniform didn't compromise their identity and 41% agreed that there seemed to be less gang activity at school as a result of the policy
T47	Sentence 5652 5693	16. Most schools prefer a casual approach
T48	Sentence 5694 5812	While some schools require students to adhere to more formal uniform standards, the majority take a more relaxed view.
T49	ArgumentFor 5813 5958	Approximately 90% of school leaders prefer to keep things simple by requiring students to wear polos and chinos in place of dress shirts and ties
T50	Sentence 5964 6007	But they're specific when it comes to color
T51	Sentence 6008 6152	The purpose of enforcing a uniform policy is to ensure that students are dressed the same, which means making sure everyone is on the same page.
T52	Sentence 6153 6246	Ninety percent of schools require students to wear tops and bottoms that are a specific color
T53	Sentence 6248 6291;6292 6400	18. One color sees more wear than any other Uniforms can come in a range of hues (or even plaid), but navy blue is still the standard at 38% of schools.
T54	Sentence 6401 6511	White is the second-most popular color, with 23% of the vote, while 15% of schools prefer students to wear red
T55	Sentence 6517 6556;6557 6666	Wearing uniforms may reduce absenteeism Missing school puts students at a disadvantage, especially if it causes them to fall behind in their studies.
T56	ArgumentFor 6667 6796	Research has shown that among middle and high school students, uniforms reduce absences on average by half a day each school year
T57	Sentence 6802 6837;6838 6887	It can also impact graduation rates Graduating from high school is a major milestone.
T58	ArgumentFor 6888 6989	Evidence suggests that requiring uniforms may increase the number of students who earn their diploma.
T59	ArgumentFor 6990 7083	In at least one study, the graduation rate jumped by nearly 8% after uniforms were introduced
T60	Sentence 7085 7139;7140 7308	21. Student achievement seems to be largely unaffected Students who wear uniforms may be more likely to make it to school each day and graduate on time, but it doesn't guarantee they'll go from making C's to the honor roll.
T61	ArgumentAgainst 7309 7505	Research has shown that math scores among middle and high school students increased by less than 0.01% after uniforms were adopted, while reading scores actually dropped by roughly the same amount
T62	Sentence 7507 7547;7548 7672	22. Teachers are more likely to stay put Teaching is a tough gig; roughly half a million teachers either change schools or leave the profession altogether each year.
T63	ArgumentFor 7673 7803	Studies show, however, that when students wear uniforms, the attrition rate drops to 5%, which is roughly 1/4 of the national mean
T64	Sentence 7809 7866;7867 8028	The jury is still out on whether uniforms reduce bullying Bullying is a pervasive problem in schools all across America, and many educators feel that uniforms reduce the potential for aggressive behavior among students.
T65	ArgumentFor 8029 8174	In fact, 64% of elementary school principals say that uniforms have a positive impact on bullying, despite a lack of supporting academic research
T66	Sentence 8176 8198;8199 8241;8242 8278;8279 8333;8334 8354;8355 8382;8383 8397;8398 8436;8437 8457;8458 8492;8493 8501	Sources and References - National Center for Education Statistics - Education Commission of the States - National Association of Elementary School Principals - Classroom Uniforms - University of Nevada-Reno - French Toast - National Bureau of Economic Research - Liberty University - Alliance for Excellent Education - NJ.com
T67	Sentence 8502 8535;8536 8609;8610 8713	Share this on Facebook or Twitter Follow @CreditDonkey or write to Rebecca Lake at rebecca@creditdonkey.com Rebecca Lake is a journalist at CreditDonkey, a credit card comparison and financial education website.
T68	Sentence 8714 8868	Our data-driven analysis has been recognized by major news outlets across the country and has helped families make savvy financial and lifestyle decisions
T69	Sentence 8870 8893	More from CreditDonkey:
