T1	Sentence 0 60	I am a doctor who has dedicated her career to helping woman.
T2	Sentence 61 167	If abortion is what matters to you in this election, I'd like to put rhetoric aside and focus on the facts
T3	Sentence 169 241	There are approximately 700,000 abortions per year in the United States.
T4	Sentence 242 382	The four main reasons women seek abortion are unplanned/mistimed pregnancy, rape, health of the mother, and birth defects (fetal anomalies).
T5	Sentence 383 462	Approximately 16,000 of these abortions are the result of rape, or 2.3 percent.
T6	Sentence 463 522	Exact data on health of mother and birth defects is unknown
T7	Sentence 524 590	About 45 percent of pregnancies in the United States are unplanned
T8	Sentence 592 784	These are the pregnancies most likely to end in abortion, in fact 42 percent of unplanned pregnancies end in abortion and 58 percent end in a live birth (this statistic excludes miscarriages).
T9	Sentence 785 858	If you want to reduce abortion, you have to work on unplanned pregnancies
T10	Sentence 860 1134	Most pregnancies are unplanned because of issues with access to highly reliable contraception, but some will be rape and some will be health of the mother, i.e., women who hadn't planned on getting pregnant and are happy they are pregnant, but their health is deteriorating.
T11	Sentence 1135 1296	For example, a 21-year-old woman who had post partum cardiomyopathy (heart failure) after her last pregnancy and survived (the mortality is at least 50 percent).
T12	Sentence 1297 1433	She had limited access to health care after delivery and didn't really understand the risks her doctor mentioned about a next pregnancy.
T13	Sentence 1434 1508	She thought she was tired because she was a new mom and not sleeping well.
T14	Sentence 1509 1566	She shows up in the emergency department short of breath.
T15	ArgumentFor 1567 1687	She finds out she is 7 weeks pregnant and that the strain of this second pregnancy is causing her damaged heart to fail.
T16	Sentence 1688 1730	She is admitted to the intensive care unit
T17	ArgumentFor 1732 1829	Pregnancy is a huge strain on the heart and most of that impact occurs early on in the pregnancy.
T18	ArgumentFor 1830 2012	She didn't plan on being pregnant and would not choose an abortion, except her pregnancy is killing her and unless the pregnancy ends she will die leaving her 5-month-old motherless.
T19	Sentence 2013 2149	She is so ill that she cannot be transported to the operating room for an abortion, so it is done on her bed in the intensive care unit.
T20	Sentence 2150 2203	These cases are fortunately uncommon, but they happen
T21	Sentence 2205 2268	Two-thirds of abortions (66 percent) happen before eight weeks.
T22	Sentence 2269 2415	It is unlikely any of these are for birth defects as the earliest tests cannot be done until 10 weeks and it takes a week to get the results back.
T23	ArgumentAgainst 2416 2569	A small percentage will be rape and health of the mother (like the case described above), but the majority are unplanned pregnancies and thus preventable
T24	Sentence 2571 2638;2639 2735	Birth control prevents abortion by preventing unplanned pregnancies Only 5 percent of women who have an abortion were using contraception correctly and consistently
T25	Sentence 2737 2843	Studies tell us that access to long acting reversible contraception dramatically reduces the abortion rate
T26	Sentence 2845 3082	In Colorado, a program that provided free long acting reversible contraception (the kind that is impossible to use inconsistently) resulted in a 42 percent reduction in abortions for teens and an 18 percent reduction for women ages 20-24
T27	Sentence 3084 3242	In 2014, government-funded family planning helped women avoid two million unintended pregnancies, approximately 700,000 abortions would have ended in abortion
T28	Sentence 3244 3272	Laws do not reduce abortion.
T29	Sentence 3273 3299	They reduce safe abortions
T30	ArgumentFor 3301 3393	In the 1950s and the 1960s there were an estimated 200,000 to 1.2 million illegal abortions.
T31	Sentence 3394 3464	There are currently around 700,000 abortions/year in the United States
T32	ArgumentFor 3466 3577	The abortion rate is similar in countries where it is illegal and where it is widely available, legal and safe.
T33	ArgumentFor 3578 3751	According to the most recent data, approximately 7 million women in developing countries are treated for complications from unsafe abortions annually and at least 22,000 die
T34	ArgumentFor 3753 3930	In Texas, where the abortion laws have become more restrictive and access has dropped more than 100,000 women and possibly up to 240,000 have attempted home or illegal abortions
T35	Sentence 3932 4067	I trained with men who were OB/GYNs in the 1960s and the 1970s and they saw the women who survived when they came to the emergency room
T36	Sentence 4069 4102	"Every night we admitted a woman.
T37	Sentence 4133 4159	And we were one hospital."
T38	Sentence 4160 4200	Those are the words of one of my mentors
T39	Sentence 4202 4219	Think about that.
T40	Sentence 4220 4293	Every night, one or more women with a pelvis abscess or an injured bowel.
T41	Sentence 4313 4440;4441 4513	If you really want to reduce abortion, you must vote for the candidate who will improve access to highly reliable contraception Every woman, no matter where she works, deserves birth control coverage.
T42	Sentence 4514 4543	This shouldn't be a question.
T43	Sentence 4544 4554;4555 4603	#SCOTUS -H — Hillary Clinton (@HillaryClinton) May 16, 2016
T44	Sentence 4604 4665;4666 4796	This is what Donald Trump said about birth control to Dr. Oz: I think what we have in birth control is, you know, when you have to get a prescription, that's a pretty tough something to climb.
T45	ArgumentAgainst 4797 4883	And I would say it should not be a prescription, it should not be done by prescription
T46	Sentence 4885 5044	Making birth control over the counter does not help women access the forms of birth control that are most effective at preventing pregnancy: IUDs and implants.
T47	Sentence 5045 5166	Taking away health plan contraceptive coverage makes the birth control that reduces abortion unaffordable for many women.
T48	Sentence 5167 5249	The cash price for the implant is $825.74 and you still have to pay for insertion.
T49	Sentence 5250 5343	IUDs are in the $600-1,000 range for the device and the insertion (depending on where you go)
T50	ArgumentFor 5345 5547	If your candidate focuses on restrictive laws, you will drive women underground, which will increase their financial hardship and many will risk their health and lives, but you will not reduce abortions
T51	Sentence 5549 5766	If your candidate wants to lift the birth control mandate from the Affordable Care Act or repeal the Affordable Care Act and replace it with health care that does not cover birth control you, will not reduce abortions
T52	Sentence 5768 5944	If your candidate wants to get rid of Planned Parenthood, a source of subsidized highly-reliable contraception that would otherwise be unaffordable, you will increase abortions
T53	Sentence 5946 6103	If your candidate focuses on maintaining and even expanding government and employer funding of highly reliable contraception, your vote will reduce abortions
T54	Sentence 6105 6129	It really is that simple
T55	Sentence 6131 6203	A version of this post originally appeared on DrJenGunter.wordpress.com.
