#!/bin/bash

mv argmap.mongodb /tmp
mongodump --db argmap_tests_fixture --out argmap.mongodb
mv argmap.mongodb/argmap_tests_fixture argmap.mongodb/argmap
tar cvzf argmap.mongodb.tar.gz argmap.mongodb

# macos: 
# tar -cvz -f argmap.mongodb.tar.gz --no-xattrs --exclude ._* argmap.mongodb
